<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TemplateCategory;
use App\Models\TemplateSubCategory;
use Validator;

class TemplateSubCategoryController extends Controller
{
    
    public function index(Request $request)
    {
        $template_sub_categories = TemplateSubCategory::all();
        return view('backend.template_sub_category.index',compact('template_sub_categories'));
    }

    public function create(Request $request){
        $template_categories = TemplateCategory::all();
        return view('backend.template_sub_category.add',compact('template_categories'));
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:template_sub_categories',
        ], [
            'name' => 'Please enter template sub category name.',
            'name.unique' => 'Template sub category name already exist.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $add =  new TemplateSubCategory;
        $add->user_id = 0;
        $add->template_category_id = $request->template_category_id;
        $add->name = $request->name;
        $add->slug = str_replace(' ', '_', strtolower($request->name));
        $add->save();
        return redirect()->route('admin.template_sub_category.index')->with('success','Template sub category added successfully.');
    }

    public function edit($id){
        $template_categories = TemplateCategory::all();
        $template_sub_category_details = TemplateSubCategory::where('id',$id)->first();
        return view('backend.template_sub_category.edit',compact('template_categories','template_sub_category_details'));
    }

    public function update(Request $request,$id){

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:template_sub_categories,name,'.$id,
        ], [
            'name' => 'Please enter template sub category.',
            'name.unique' => 'Template sub category name already exist.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $add = TemplateSubCategory::find($id);
        $add->template_category_id = $request->template_category_id;
        $add->name = $request->name;
        $add->slug = str_replace(' ', '_', strtolower($request->name));
        $add->save();
        return redirect()->route('admin.template_sub_category.index')->with('success','Template sub category updated successfully.');

    }

    public function delete($id){
        
        $template_sub_category = TemplateSubCategory::find($id);
        $template_sub_category->delete();
        return redirect()->route('admin.template_sub_category.index')->with('success','Template sub category deleted successfully');
    }

}
