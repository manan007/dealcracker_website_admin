<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Validator;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $categories = Category::all();
        return view('backend.category.index',compact('categories'));
    }

    public function create(Request $request){
        return view('backend.category.add');
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:categories',
        ], [
            'name' => 'Please enter category name.',
            'name.unique' => 'Category name already exist.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $add =  new Category;
        $add->name = $request->name;
        $add->save();
        return redirect()->route('admin.category.index')->with('success','Category added successfully.');
    }

    public function edit($id){
        $category_details = Category::where('id',$id)->first();
        return view('backend.category.edit',compact('category_details'));
    }

    public function update(Request $request,$id){

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:categories,name,'.$id,
        ], [
            'name' => 'Please enter category name.',
            'name.unique' => 'Category name already exist.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $add = Category::find($id);
        $add->name = $request->name;
        $add->save();
        return redirect()->route('admin.category.index')->with('success','Category updated successfully.');

    }

    public function delete($id){
        
        $category = Category::find($id);
        $category->delete();
        return redirect()->route('admin.category.index')->with('success','Category deleted successfully');
    }
}
