<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BusinessCategory;
use App\Models\BusinessSubCategory;
use Validator;

class BusinessCategoryController extends Controller
{
    
    public function index(Request $request)
    {
        $business_categories = BusinessCategory::all();
        return view('backend.business_category.index',compact('business_categories'));
    }

    public function create(Request $request){
        return view('backend.business_category.add');
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:business_categories',
        ], [
            'name' => 'Please enter business category name.',
            'name.unique' => 'Business Category name already exist.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $add =  new BusinessCategory;
        $add->user_id = 0;
        $add->name = $request->name;
        $add->save();
        return redirect()->route('admin.business_category.index')->with('success','Business category added successfully.');
    }

    public function edit($id){
        $business_category_details = BusinessCategory::where('id',$id)->first();
        return view('backend.business_category.edit',compact('business_category_details'));
    }

    public function update(Request $request,$id){

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:business_categories,name,'.$id,
        ], [
            'name' => 'Please enter business category.',
            'name.unique' => 'Business category name already exist.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $add = BusinessCategory::find($id);
        $add->name = $request->name;
        $add->save();
        return redirect()->route('admin.business_category.index')->with('success','Business category updated successfully.');

    }

    public function delete($id){
        
        $check_sub_category = BusinessSubCategory::where(['business_category_id' => $id])->get();

        if(count($check_sub_category) > 0){
            return redirect()->route('admin.business_category.index')->with('error',"You can't delete this business category beacause it's sub category exist.");
        }
        else{
            $platform = BusinessCategory::find($id);
            $platform->delete();
            return redirect()->route('admin.business_category.index')->with('success','Business category deleted successfully');   
        }
    }


}
