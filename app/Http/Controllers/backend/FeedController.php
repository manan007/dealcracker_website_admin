<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Feed;
use App\Models\FeedMedia;

class FeedController extends Controller
{
    public function index(){
        $feeds = Feed::where(['is_delete' => 0])->get();
        return view('backend.feed.index',compact('feeds'));
    }


    public function delete($id){
        
        //FeedMedia::where(['feed_id' => $id])->delete();
        Feed::where('id',$id)->update(['is_delete' => 1]);
        //Feed::where(['id' => $id])->delete();
        return redirect()->route('admin.feed.index')->with('success','feed deleted successfully');
    }

}
