<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TemplateCategory;
use App\Models\TemplateSubCategory;
use Validator;

class TemplateCategoryController extends Controller
{
    public function index(Request $request)
    {
        $template_categories = TemplateCategory::all();
        return view('backend.template_category.index',compact('template_categories'));
    }

    public function create(Request $request){
        return view('backend.template_category.add');
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:template_categories',
        ], [
            'name' => 'Please enter template category name.',
            'name.unique' => 'Template Category name already exist.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $add =  new TemplateCategory;
        $add->user_id = 0;
        $add->name = $request->name;
        $add->slug = str_replace(' ', '_', strtolower($request->name));
        $add->save();
        return redirect()->route('admin.template_category.index')->with('success','Template category added successfully.');
    }

    public function edit($id){
        $template_category_details = TemplateCategory::where('id',$id)->first();
        return view('backend.template_category.edit',compact('template_category_details'));
    }

    public function update(Request $request,$id){

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:template_categories,name,'.$id,
        ], [
            'name' => 'Please enter template category.',
            'name.unique' => 'Template category name already exist.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $add = TemplateCategory::find($id);
        $add->name = $request->name;
        $add->slug = str_replace(' ', '_', strtolower($request->name));
        $add->save();
        return redirect()->route('admin.template_category.index')->with('success','Template category updated successfully.');

    }

    public function delete($id){
        
        $check_sub_category = TemplateSubCategory::where(['template_category_id' => $id])->get();

        if(count($check_sub_category) > 0){
            return redirect()->route('admin.template_category.index')->with('error',"You can't delete this template category beacause it's sub category exist.");
        }
        else{
            $platform = TemplateCategory::find($id);
            $platform->delete();
            return redirect()->route('admin.template_category.index')->with('success','Template category deleted successfully');   
        }
    }
}
