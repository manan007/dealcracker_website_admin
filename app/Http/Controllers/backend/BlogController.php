<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\BlogMedia;

class BlogController extends Controller
{

    public function index(Request $request)
    {
        $blogs = Blog::all();
        return view('backend.blog.index',compact('blogs'));
    }

    public function create(Request $request){
        return view('backend.blog.add');
    }

    public function store(Request $request){

        $add =  new Blog;
        $add->title = $request->title;
        $add->description = $request->description;
        $add->save();

        if($request->has('media'))
        {
            $count = count($request->media);
            if ($count>0) 
            {
                for ($i=0; $i < $count;  $i++) 
                { 
                    $name = time() . mt_rand(10000, 99999);
                    $imageName = $name . '.' . $request->media[$i]->getClientOriginalExtension();
                    $request->media[$i]->move(public_path().'/upload/blog/', $imageName);  
                    $data[] = $imageName;  
                    $add_media = new BlogMedia;
                    $add_media->blog_id = $add->id;
                    $add_media->blog_media = $imageName;
                    $ext = pathinfo($imageName, PATHINFO_EXTENSION);
                    if ($ext == 'mp4'){
                        $add_media->type = 'video';
                    } 
                    else{
                        $add_media->type = 'image'; 
                    }  
                    $add_media->save();      
                }   
            }
        }


        return redirect()->route('admin.blog.index')->with('success','Blog added successfully.');
    }

    public function edit($id){
        $blog_details = Blog::where('id',$id)->first();
        $blog_medias = BlogMedia::where(['blog_id' => $id])->get();
        return view('backend.blog.edit',compact('blog_details','blog_medias'));
    }

    public function update(Request $request,$id){
        
        $add = Blog::find($id);
        $add->title = $request->title;
        $add->description = $request->description;
        $add->save();

        if($request->has('media'))
        {
            $count = count($request->media);
            if ($count>0) 
            {
                for ($i=0; $i < $count;  $i++) 
                { 
                    $name = time() . mt_rand(10000, 99999);
                    $imageName = $name . '.' . $request->media[$i]->getClientOriginalExtension();
                    $request->media[$i]->move(public_path().'/upload/blog/', $imageName);  
                    $data[] = $imageName;  
                    $add_media = new BlogMedia;
                    $add_media->blog_id = $add->id;
                    $add_media->blog_media = $imageName;
                    $ext = pathinfo($imageName, PATHINFO_EXTENSION);
                    if ($ext == 'mp4'){
                        $add_media->type = 'video';
                    } 
                    else{
                        $add_media->type = 'image'; 
                    }  
                    $add_media->save();      
                }   
            }
        }

        return redirect()->route('admin.blog.index')->with('success','Blog updated successfully.');

    }

    public function delete($id){

        BlogMedia::where(['blog_id' => $id])->delete();
        $plan = Blog::find($id);
        $plan->delete();
        return redirect()->route('admin.blog.index')->with('success','Blog deleted successfully');
    }


    public function deleteMedia(Request $request)
    {   
        BlogMedia::where(['id' => $request->blog_media_id])->delete();
        $blog_details = Blog::where('id',$request->blog_id)->first();
        $blog_medias = BlogMedia::where(['blog_id' => $request->blog_id])->get();
        return redirect()->route('admin.blog.edit',$request->blog_id)->with('success','Blog media deleted successfully.');
    }

}
