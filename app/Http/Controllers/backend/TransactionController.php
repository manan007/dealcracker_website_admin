<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\User;

class TransactionController extends Controller
{
    public function index($id){
        $user_details = User::where(['id' => $id])->first();
        $transaction_details = Payment::where(['user_id' => $id])->get();
        return view('backend.transaction.index',compact('transaction_details','user_details'));
    }
}
