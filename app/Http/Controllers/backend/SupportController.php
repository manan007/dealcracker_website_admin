<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Support;

class SupportController extends Controller
{
        
    public function index(Request $request)
    {
        $supports = Support::all();
        return view('backend.support.index',compact('supports'));
    }

    public function create(Request $request,$id){
        $question_details = Support::where(['id' => $id])->first();
        return view('backend.support.add',compact('question_details'));
    }

    public function answerUpdate(Request $request,$id){

        $add = Support::find($id);
        $add->answer = $request->answer;
        $add->save();

        return redirect()->route('admin.support.index')->with('success','Answer submited successfully.');
    }

    public function delete($id){

        $support = Support::find($id);
        $support->delete();
        return redirect()->route('admin.support.index')->with('success','Question deleted successfully');
    }

}
