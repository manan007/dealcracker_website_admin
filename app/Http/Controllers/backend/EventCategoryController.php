<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\EventCategory;
use Validator;

class EventCategoryController extends Controller
{

    public function index(Request $request)
    {
        $event_categories = EventCategory::orderBy('user_id','asc')->get();
        return view('backend.event_category.index',compact('event_categories'));
    }

    public function create(Request $request){
        return view('backend.event_category.add');
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:event_categories',
        ], [
            'name' => 'Please enter event category name.',
            'name.unique' => 'Event category name already exist.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $add =  new EventCategory;
        $add->user_id = 0;
        $add->name = $request->name;
        $add->save();
        return redirect()->route('admin.event_category.index')->with('success','Event Category added successfully.');
    }

    public function edit($id){
        $event_category_details = EventCategory::where('id',$id)->first();
        return view('backend.event_category.edit',compact('event_category_details'));
    }

    public function update(Request $request,$id){

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:event_categories,name,'.$id,
        ], [
            'name' => 'Please enter event category name.',
            'name.unique' => 'Event category name already exist.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $add = EventCategory::find($id);
        $add->user_id = 0;
        $add->name = $request->name;
        $add->save();
        return redirect()->route('admin.event_category.index')->with('success','Event Category updated successfully.');

    }

    public function delete($id){
        
        $category = EventCategory::find($id);
        $category->delete();
        return redirect()->route('admin.event_category.index')->with('success','Category deleted successfully');
    }

}
