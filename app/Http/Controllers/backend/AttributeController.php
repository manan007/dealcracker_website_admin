<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Attribute;
use Validator;

class AttributeController extends Controller
{
    
    public function index(Request $request)
    {
        $attributes = Attribute::where(['is_parent' => 0])->get();
        return view('backend.attribute.index',compact('attributes'));
    }

    public function create(Request $request){
        return view('backend.attribute.add');
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:attributes',
        ], [
            'name' => 'Please enter attribute name.',
            'name.unique' => 'Attribute name already exist.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $add =  new Attribute;
        $add->name = $request->name;
        $add->slug = strtolower($request->name);
        $add->is_parent = 0;
        $add->save();
        return redirect()->route('admin.attribute.index')->with('success','Attribute added successfully.');
    }

    public function edit($id){
        $attribute_details = Attribute::where('id',$id)->first();
        return view('backend.attribute.edit',compact('attribute_details'));
    }

    public function update(Request $request,$id){

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:attributes,name,'.$id,
        ], [
            'name' => 'Please enter attribute name.',
            'name.unique' => 'Attribute name already exist.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
            
        $add =  Attribute::find($id);
        $add->name = $request->name;
        $add->slug = strtolower($request->name);
        $add->is_parent = 0;
        $add->save();
        return redirect()->route('admin.attribute.index')->with('success','Attribute updated successfully.');

    }

    public function delete($id){
        
        $check_sub_attribute = Attribute::where(['is_parent' => $id])->get();

        if(count($check_sub_attribute) > 0){
            return redirect()->route('admin.attribute.index')->with('error',"You can't delete this attribute because sub attribute already exist.");
        }
        else{
            $attribute = Attribute::find($id);
            $attribute->delete();
            return redirect()->route('admin.attribute.index')->with('success','Attribute deleted successfully');
        }
        
    }

}
