<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Group;
use App\Models\User;

class GroupController extends Controller
{
    public function index(Request $request,$user_id)
    {   
        $groups = Group::where(['user_id' => $user_id,'is_delete' => 0])->get();
        $user_details = User::where(['id' => $user_id])->first();
        return view('backend.group.index',compact('groups','user_details'));
    }
}
