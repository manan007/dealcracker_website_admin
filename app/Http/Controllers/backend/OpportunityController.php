<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Opportunity;

class OpportunityController extends Controller
{
        
    public function index(){
        $opportunity = Opportunity::where(['is_delete' => 0])->get();
        return view('backend.opportunity.index',compact('opportunity'));
    }

    public function delete($id){

        $update = Opportunity::find($id);
        $update->is_delete = 1;
        $update->save();
        return redirect()->route('admin.opportunity.index')->with('success','Opportunity deleted successfully.');
    }


    public function changeStatus(Request $request){
        $id = $request->id;
        $getdetails = Opportunity::find($id);
        if($getdetails->status == 1){
            Opportunity::where(['id' => $id])->update(['status' => 0]);
        }
        else{
            Opportunity::where(['id' => $id])->update(['status' => 1]);
        }
        return response()->json(['success' => true,'msg' => 'Status updated successfully.']);
    }

}
