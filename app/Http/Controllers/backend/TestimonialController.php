<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Testimonial;

class TestimonialController extends Controller
{
    
    public function index(Request $request)
    {
        $testimonials = Testimonial::all();
        return view('backend.testimonial.index',compact('testimonials'));
    }

    public function create(Request $request){
        return view('backend.testimonial.add');
    }

    public function store(Request $request){

        $fileName = null;
        if($request->has('document'))
        {
            $fileName = time().'_'.rand(100,999).".".$request->file('document')->extension();
            $path="/upload/testimonial/".$fileName;
            $public_path = public_path('/upload/testimonial');
            $request->file('document')->move($public_path,$fileName);
            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            if ($ext == 'mp4'){$ext = 'video';} 
            else{
                $ext = 'image'; 
            }
        }  

        $add =  new Testimonial;
        $add->title = $request->title;
        $add->document_type = $ext;
        $add->document = $fileName;
        $add->save();

        return redirect()->route('admin.testimonial.index')->with('success','Testimonial added successfully.');
    }

    public function edit($id){
        $testimonial_details = Testimonial::where('id',$id)->first();
        return view('backend.testimonial.edit',compact('testimonial_details'));
    }

    public function update(Request $request,$id){
        
        $fileName = $request->hidden_document;
        $ext = $request->hidden_ext;
        if($request->has('document'))
        {
            $fileName = time().'_'.rand(100,999).".".$request->file('document')->extension();
            $path="/upload/testimonial/".$fileName;
            $public_path = public_path('/upload/testimonial');
            $request->file('document')->move($public_path,$fileName);
            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            if ($ext == 'mp4'){$ext = 'video';} 
            else{
                $ext = 'image'; 
            }
        }

        $add = Testimonial::find($id);
        $add->title = $request->title;
        $add->document_type = $ext;
        $add->document = $fileName;
        $add->save();
        return redirect()->route('admin.testimonial.index')->with('success','Testimonial updated successfully.');

    }

    public function delete($id){
        
        $testimonial = Testimonial::find($id);
        $testimonial->delete();
        return redirect()->route('admin.testimonial.index')->with('success','Testimonial deleted successfully.');
    }

}
