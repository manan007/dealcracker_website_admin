<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PlanFeature;
use App\Models\Plan;

class PlanFeatureController extends Controller
{
    public function index(Request $request,$plan_id)
    {
        $plan_features = PlanFeature::where(['plan_id' => $plan_id])->get();
        $plan_data = Plan::where(['id' => $plan_id])->first();
        return view('backend.plan_feature.index',compact('plan_features','plan_data'));
    }

    public function create(Request $request,$plan_id){
        $plan_data = Plan::where(['id' => $plan_id])->first();
        return view('backend.plan_feature.add',compact('plan_data'));
    }

    public function store(Request $request,$plan_id){

        $add =  new PlanFeature;
        $add->plan_id = $plan_id;
        $add->feature = $request->feature;
        $add->save();
        return redirect()->route('admin.plan_feature.index',$plan_id)->with('success','Plan feature added successfully.');
    }

    public function edit($plan_id,$id){
        $plan_feature_details = PlanFeature::where('id',$id)->first();
        $plan_data = Plan::where(['id' => $plan_id])->first();
        return view('backend.plan_feature.edit',compact('plan_feature_details','plan_data'));
    }

    public function update(Request $request,$plan_id,$id){
        
        $add = PlanFeature::find($id);
        $add->plan_id = $plan_id;
        $add->feature = $request->feature;
        $add->save();
        return redirect()->route('admin.plan_feature.index',$plan_id)->with('success','Plan feature updated successfully.');
    }

    public function delete($plan_id,$id){
        
        $plan = PlanFeature::find($id);
        $plan->delete();
        return redirect()->route('admin.plan_feature.index',$plan_id)->with('success','Plan feature deleted successfully');
    }
}
