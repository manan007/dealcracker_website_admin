<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ContactUs;

class ContactUsController extends Controller
{
    public function index(Request $request)
    {
        $contacts = ContactUs::all();
        return view('backend.contact_us.index',compact('contacts'));
    }

    public function edit($id){
        $contactus_details = ContactUs::where('id',$id)->first();
        return view('backend.contact_us.edit',compact('contactus_details'));
    }

    public function update(Request $request,$id){
        
        $add = ContactUs::find($id);
        $add->name = isset($request->name) ? $request->name:null;
        $add->email = isset($request->email) ? $request->email:null;
        $add->phone = isset($request->phone) ? $request->phone:null;
        $add->message = isset($request->message) ? $request->message:null;
        $add->save();

        return redirect()->route('admin.contact_us.index')->with('success','Contact updated successfully.');

    }

    public function delete($id){

        $contactus = ContactUs::find($id);
        $contactus->delete();
        return redirect()->route('admin.contact_us.index')->with('success','Contact deleted successfully');
    }
}
