<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Business;

class BusinessController extends Controller
{
    
    public function index(){
        $business = Business::where(['is_delete' => 0])->get();
        return view('backend.business.index',compact('business'));
    }


    public function delete($id){

        $update = Business::find($id);
        $update->is_delete = 1;
        $update->save();
        return redirect()->route('admin.business.index')->with('success','Business deleted successfully');
    }


    public function changeStatus(Request $request){
        $id = $request->id;
        $getdetails = Business::find($id);
        if($getdetails->status == 1){
            Business::where(['id' => $id])->update(['status' => 0]);
        }
        else{
            Business::where(['id' => $id])->update(['status' => 1]);
        }
        return response()->json(['success' => true,'msg' => 'Status updated successfully.']);
    }

}
