<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\User;
use App\Models\Attribute;
use App\Models\ProductAttribute;
use App\Models\Cart;
use App\Models\UserShare;


class ProductController extends Controller
{
    public function index(Request $request)
    {   
        $products = Product::where(['parent_id' => 0,'is_delete' => 0])->get();
        $attributes=[];
        return view('backend.product.index',compact('products','attributes'));
    }

    public function create(Request $request)
    {
        $product_type = 'physical';
        $users = User::where(['is_admin' => 0])->get();
        $get_attributes = Attribute::where(['is_parent' => 0])->get();
        $is_optional = 1;
        $attributes=[];
        $product_details = null;
        $variation_products = [];
        return view('backend.product.add',compact('users','product_type','get_attributes','is_optional','attributes','product_details','variation_products'));
    }


    public function getProductField(Request $request){

        $product_type = $request->product_type;
        $get_attributes = Attribute::where(['is_parent' => 0])->get();
        $is_optional = $request->is_optional;

        

        $attributes=[];
        if($request->has('attribute') && $request->attribute != null){
            $attributes = $request->attribute;   
        }
        
        $product_details = null; $variation_products = [];
        if(!empty($request->product_id)){
            $product_details = Product::where(['id' => $request->product_id])->first();

            $product_ids = [];
            $get_product_details = Product::where('id',$request->product_id)->orWhere('parent_id',$request->product_id)->get();
            if(count($get_product_details) > 0){
                foreach($get_product_details as $get_product_detail){
                    $product_ids[] = $get_product_detail->id;
                }
            }
            
            $get_product_attributes = ProductAttribute::where('is_delete',0)->whereIn('product_id',$product_ids)->get();
            $attributes=[];
            if(count($get_product_attributes) > 0){
                foreach($get_product_attributes as $get_product_attribute){
                    if(!in_array($get_product_attribute->attribute_id,$attributes)){
                        $attributes[] = $get_product_attribute->attribute_id;
                    }
                }
            }

            $variation_products = Product::where(['parent_id' => $request->product_id,'is_delete' => 0])->get();
            
        }

        if($is_optional == 1){

            if($request->is_change == 1){
              $html = view('backend.product.dynamic_product_field_section', compact('product_type','get_attributes','is_optional','attributes','product_details','variation_products'))->render();  
            }
            else{
                $html = view('backend.product.dynamic_add_variation_section', compact('product_type','get_attributes','is_optional','attributes','product_details','variation_products'))->render();
            }    
        }
        else{
            $html = view('backend.product.dynamic_add_variation_section', compact('product_type','get_attributes','is_optional','attributes','product_details','variation_products'))->render();
        }
        return response()->json(['html'=>$html]);
    }


    public function getProductFieldEditCheck(Request $request){

        $product_type = $request->product_type;
        $get_attributes = Attribute::where(['is_parent' => 0])->get();
        $is_optional = 0;//$request->is_optional;

        
        $attributes=[];
        if($request->has('attribute') && $request->attribute != null){
            $attributes = $request->attribute;   
        }
        
        
        $product_details = null; $variation_products = [];
        if(!empty($request->product_id)){
            $product_details = Product::where(['id' => $request->product_id])->first();

            $product_ids = [];
            $get_product_details = Product::where('id',$request->product_id)->orWhere('parent_id',$request->product_id)->get();
            if(count($get_product_details) > 0){
                foreach($get_product_details as $get_product_detail){
                    $product_ids[] = $get_product_detail->id;
                }
            }
            
            $get_product_attributes = ProductAttribute::where('is_delete',0)->whereIn('product_id',$product_ids)->get();
            $attributes=[];
            if(count($get_product_attributes) > 0){
                foreach($get_product_attributes as $get_product_attribute){
                    if(!in_array($get_product_attribute->attribute_id,$attributes)){
                        $attributes[] = $get_product_attribute->attribute_id;
                    }
                }
            }
            
        }


        $get_product_details = Product::where(['id' => $request->product_id,'parent_id' => 0])->first();

        //$get_main_attrs = Attribute::where(['id' => $request->attribute_id,'is_parent' => 0])->first();

        if($get_product_details){

            $getCarts = Cart::where(['product_id' => $request->product_id])->first();

            if($getCarts){
                return response()->json(['status' => false,'msg' => "This product available in cart you can not delete."]);
            }
            else{
                
                $get_variations = Product::where(['parent_id' => $get_product_details->id,'is_delete' => 0])->get();

                if(count($get_variations) > 0){

                    foreach($get_variations as $get_variation){
                        ProductAttribute::where(['product_id' => $get_variation->id])->update(['is_delete' => 1]);
                        if((int)$request->attribute_id == 0){
                            Product::where(['id' => $get_variation->id])->update(['is_delete' => 1]);                    
                        }
                        else{
                            Product::where(['id' => $get_variation->id])->update(['is_delete' => 1]);
                        }
                    }
                  
                    $variation_products = Product::where(['parent_id' => $request->product_id,'is_delete' => 0])->get();

                    if($is_optional == 1){

                        if($request->is_change == 1){
                          $html = view('backend.product.dynamic_product_field_section', compact('product_type','get_attributes','is_optional','attributes','product_details','variation_products'))->render();  
                        }
                        else{
                            $html = view('backend.product.dynamic_add_variation_section', compact('product_type','get_attributes','is_optional','attributes','product_details','variation_products'))->render();
                        }    
                    }
                    else{
                        $html = view('backend.product.dynamic_add_variation_section', compact('product_type','get_attributes','is_optional','attributes','product_details','variation_products'))->render();
                    }

                    return response()->json(['status' => true,'html'=>$html,'msg' =>  "Product variation deleted successfully."]);

                }
                // else{

                //     $checkproattr = ProductAttribute::where(['product_id' => $get_product_details->id,'attribute_id' => $request->attribute_id])->first();
                //     if($checkproattr){
                        
                //         $getCarts = Cart::where(['product_id' => $request->product_id])->first();
                //         if($getCarts){
                //             return response()->json(['status' => false,'msg' => "This product available in cart you can not delete."]);
                //         }
                //         else
                //         {

                //             $variation_products = Product::where(['parent_id' => $request->product_id,'is_delete' => 0])->get();

                //             if($is_optional == 1){

                //                 if($request->is_change == 1){
                //                   $html = view('backend.product.dynamic_product_field_section', compact('product_type','get_attributes','is_optional','attributes','product_details','variation_products'))->render();  
                //                 }
                //                 else{
                //                     $html = view('backend.product.dynamic_add_variation_section', compact('product_type','get_attributes','is_optional','attributes','product_details','variation_products'))->render();
                //                 }    
                //             }
                //             else{
                //                 $html = view('backend.product.dynamic_add_variation_section', compact('product_type','get_attributes','is_optional','attributes','product_details','variation_products'))->render();
                //             }

                //             return response()->json(['status' => true,'html'=>$html,'msg' =>  "Product variation deleted successfully."]);
                               
                            
                //         }
                //     }
                //     else{
                //         return response()->json(['status' => false,'msg' => "Product attribute not found."]);
                //     }
                    
                // }
            }
            
        }
        else{
            return response()->json(['status' => false,'msg' => "Product not found."]);
        }


    }


    public function getProductFieldEdit(Request $request){
        
        //dd($request->all());

        $product_type = $request->product_type;
        $get_attributes = Attribute::where(['is_parent' => 0])->get();
        $is_optional = $request->is_optional;

        

        $attributes=[];
        
        $product_details = null; $variation_products = [];
        if(!empty($request->product_id)){
            $product_details = Product::where(['id' => $request->product_id])->first();

            $product_ids = [];
            $get_product_details = Product::where('id',$request->product_id)->orWhere('parent_id',$request->product_id)->get();
            if(count($get_product_details) > 0){
                foreach($get_product_details as $get_product_detail){
                    $product_ids[] = $get_product_detail->id;
                }
            }
            
            $get_product_attributes = ProductAttribute::where('is_delete',0)->whereIn('product_id',$product_ids)->get();
            $attributes=[];
            if(count($get_product_attributes) > 0){
                foreach($get_product_attributes as $get_product_attribute){
                    if(!in_array($get_product_attribute->attribute_id,$attributes)){
                        $attributes[] = $get_product_attribute->attribute_id;
                    }
                }
            }

            $variation_products = Product::where(['parent_id' => $request->product_id,'is_delete' => 0])->get();
            
        }

        //dd($variation_products);
        $html1 = [];
        if($is_optional == 1){

            if($request->is_change == 1){
              $html = view('backend.product.dynamic_product_field_section', compact('product_type','get_attributes','is_optional','attributes','product_details','variation_products'))->render();  
            }
            else{
                $html = view('backend.product.dynamic_add_variation_section', compact('product_type','get_attributes','is_optional','attributes','product_details','variation_products'))->render();
            }     
        }
        else{
            $html = view('backend.product.dynamic_add_variation_section', compact('product_type','get_attributes','is_optional','attributes','product_details','variation_products'))->render();
        }

        $html1 = view('backend.product.dynamic_product_field_section', compact('product_type','get_attributes','is_optional','attributes','product_details','variation_products'))->render();  
        return response()->json(['html'=>$html,'html1' => $html1]);

    }


    public function addVariationSection(Request $request){

        $attributes = $request->attribute;

        $variationrow = '<div class="variation_row mb-3">';

         $variationrow .=  '<div class="row">
                                <div class="col-md-12 text-right">
                                    <a href="javascript:;" class="btn btn-danger remove_field"><i class="fa fa-minus"></i></a>
                                </div>
                            </div>';


        $variationrow .=    '<div class="row mb-3">';

                    if(count($attributes) > 0){
                        foreach($attributes as $attribute){
                            $attribute_details = Attribute::where(['id' => $attribute])->first();
                            $get_sub_attributes = Attribute::where(['is_parent' => $attribute])->get();

                $variationrow .= '<div class="col-md-2">
                                    <label>Select '.$attribute_details->name.'</label>
                                    <div class="input-group">
                                        <select class="form-control select2 sub_attribute" name="'.$attribute_details->id.'_sub_attribute[]">';
                                            if($get_sub_attributes){
                                                foreach($get_sub_attributes as $get_sub_attribute)
                                                {
                                                    $variationrow .= '<option value="'.$get_sub_attribute->id.'">'.$get_sub_attribute->name.'</option>';
                                                }
                                            }
                    $variationrow .= '  </select>
                                    </div>
                                </div>';
                            
                        }
                    }
        $variationrow .=     '</div>';
        $variationrow .=    '<div class="row mb-3">
                                <div class="col-md-3">
                                <label>Name</label>
                              <input class="form-control variation_name" type="text" name="variation_name[]" value="">
                                </div>
                                <div class="col-md-3">
                                <label>Price</label>
                              <div class="input-group">
                                <input class="form-control variation_price" type="text" name="variation_price[]" value="" onkeypress="return isNumberKey(event);">
                                <div class="input-group-append">
                                  <div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
                                </div>
                              </div>
                                </div>
                                <div class="col-md-3">
                                <label>Discount</label>
                              <div class="input-group">
                                <input class="form-control variation_discount" type="text" name="variation_discount[]" value="" onkeypress="return isNumberKey(event);">
                                <div class="input-group-append">
                                  <div class="input-group-text"><i class="fas fa-percentage"></i></div>
                                </div>
                              </div>
                                </div>
                                <div class="col-md-3">
                              <label>Quantity</label>
                              <input type="text" class="form-control variation_quanty" name="variation_quanty[]" placeholder="Quantity" onkeypress="return isOnlyNumber();">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                <label>Variation Image <span class="text-danger">( Accepted format: .jpg, .jpeg, .png )</span></label>
                                <input class="form-control variation_image" type="file" name="variation_image[]" accept=".jpg,.jpeg,.png">
                                </div>

                                <div class="col-md-6">
                                    <label>Description</label>
                                    <textarea name="variation_description[]" class="form-control variation_description" placeholder="Enter Description"></textarea>
                                </div>
                            </div>
                        </div>';
        
        

        return $variationrow;
    }

    
    public function store(Request $request){

        //dd($request->all());

        $get_product_attibutes = [];
        if($request->has('attribute')){
            $get_product_attibutes = $request->attribute;
        }

        $fileName = null;
        if($request->has('image'))
        {
            $fileName = time().'_'.rand(100,999).".".$request->file('image')->extension();
            $path="/upload/product/".$fileName;
            $public_path = public_path('/upload/product');
            $request->file('image')->move($public_path,$fileName);
        }

        $vfileName = null;$vtype = null;
        if($request->has('virtual_product_file'))
        {
            $vfileName = time().'_'.rand(100,999).".".$request->file('virtual_product_file')->extension();
            $path="/upload/product/virtual_product_file/".$vfileName;
            $public_path = public_path('/upload/product/virtual_product_file');
            $request->file('virtual_product_file')->move($public_path,$vfileName);
            $vtype = pathinfo($vfileName, PATHINFO_EXTENSION);
        }

        $add_product = new Product;
        $add_product->user_id = $request->user_id;
        $add_product->product_type = isset($request->product_type) ? $request->product_type:null;
        $add_product->name = isset($request->name) ? $request->name:null;
        $add_product->sku = isset($request->sku) ? $request->sku:null;
        $add_product->image = $fileName;
        $add_product->description = isset($request->description) ? $request->description:null;
        $add_product->price = isset($request->price) ? $request->price:0;
        $add_product->discount = isset($request->discount) ? $request->discount:0;
        $add_product->quantity = isset($request->quantity) ? $request->quantity:0;
        $add_product->virtual_product_file = $vfileName;
        $add_product->virtual_product_file_type = $vtype; 
        $add_product->save();

        if(count($get_product_attibutes) > 0){
            foreach($get_product_attibutes as $get_product_attibute){
                $add_product_attribute = new ProductAttribute;
                $add_product_attribute->product_id = $add_product->id;
                $add_product_attribute->attribute_id = $get_product_attibute;
                $add_product_attribute->sub_attribute_id = 0;
                $add_product_attribute->save();
            }
        }


        // VARIATION PRO.------------------------------------------------------------

        $variation_name = [];
        if($request->has('variation_name')){
            $variation_name = $request->variation_name;   
        }
        $variation_price = [];
        if($request->has('variation_price')){
            $variation_price = $request->variation_price;
        }
        $variation_discount = [];
        if($request->has('variation_discount')){
            $variation_discount = $request->variation_discount;
        }
        $variation_quanty = [];
        if($request->has('variation_quanty')){
            $variation_quanty = $request->variation_quanty;
        }
        $variation_description = [];
        if($request->has('variation_description')){
            $variation_description = $request->variation_description;
        }

        if (count($variation_name) > 0) {

            foreach($variation_name as $key => $data){

                $v_attributes=[]; $v_sub_attributes=[];
                if(count($get_product_attibutes) > 0){
                    foreach($get_product_attibutes as $sakey => $get_product_attibute){

                        if($request->has($get_product_attibute.'_sub_attribute')){

                            $subtrr = $get_product_attibute.'_sub_attribute';
                            $v_attributes[] = $get_product_attibute;
                            $v_sub_attributes[] = $request->$subtrr[$key];
                        }
                    }
                }

                $fileName = null;

                if(isset($request->variation_image[$key]))
                {
                    $fileName = time().'_'.rand(100,999).".".$request->file('variation_image')[$key]->extension();
                    $path="/upload/product/".$fileName;
                    $public_path = public_path('/upload/product');
                    $request->file('variation_image')[$key]->move($public_path,$fileName);
                }

                $add_vproduct = new Product;
                $add_vproduct->user_id = $request->user_id;
                $add_vproduct->product_type = isset($request->product_type) ? $request->product_type:null;
                $add_vproduct->parent_id = isset($add_product->id) ? $add_product->id:0;
                $add_vproduct->name = isset($variation_name[$key]) ? $variation_name[$key]:null;
                $add_vproduct->image = $fileName;
                $add_vproduct->description = isset($variation_description[$key]) ? $variation_description[$key]:null;
                $add_vproduct->price = isset($variation_price[$key]) ? $variation_price[$key]:0;
                $add_vproduct->discount = isset($variation_discount[$key]) ? $variation_discount[$key]:0;
                $add_vproduct->quantity = isset($variation_quanty[$key]) ? $variation_quanty[$key]:0;
                $add_vproduct->save();

                if(count($v_attributes) > 0){
                    foreach($v_attributes as $key_vp => $get_v_product_attibute){
                        $add_product_attribute = new ProductAttribute;
                        $add_product_attribute->product_id = $add_vproduct->id;
                        $add_product_attribute->attribute_id = $get_v_product_attibute;
                        $add_product_attribute->sub_attribute_id = $v_sub_attributes[$key_vp];
                        $add_product_attribute->save();
                    }
                }
            }
        }

        return redirect()->route('admin.product.index')->with('success','Product added successfully.');
    }


    public function edit(Request $request,$id){

        $product_details = Product::where(['id' => $id])->first();
        $product_type = $product_details->product_type;
        $users = User::where(['is_admin' => 0])->get();
        $get_attributes = Attribute::where(['is_parent' => 0])->get();
        $is_optional = 1;
        
        $product_ids = [];
        $get_product_details = Product::where('id',$id)->orWhere('parent_id',$id)->get();
        if(count($get_product_details) > 0){
            foreach($get_product_details as $get_product_detail){
                $product_ids[] = $get_product_detail->id;
            }
        }

        $get_product_attributes = ProductAttribute::where('is_delete',0)->whereIn('product_id',$product_ids)->get();
        $attributes=[];
        if(count($get_product_attributes) > 0){
            foreach($get_product_attributes as $get_product_attribute){
                if(!in_array($get_product_attribute->attribute_id,$attributes)){
                    $attributes[] = $get_product_attribute->attribute_id;
                }
            }
        }
        //dd($attributes);
        $variation_products = Product::where(['parent_id' => $id,'is_delete' => 0])->get();

        return view('backend.product.edit',compact('users','product_type','get_attributes','is_optional','attributes','product_details','variation_products'));

    }

    public function update(Request $request,$id){

        //dd($request->all());

        $poduct_data = Product::where(['id' => $id])->first();

        $get_product_attibutes = [];
        if($request->has('attribute')){
            $get_product_attibutes = $request->attribute;
        }

        $fileName = isset($request->hidden_image) ? $request->hidden_image:null;
        if($request->has('image'))
        {
            $fileName = time().'_'.rand(100,999).".".$request->file('image')->extension();
            $path="/upload/product/".$fileName;
            $public_path = public_path('/upload/product');
            $request->file('image')->move($public_path,$fileName);
        }

        $vfileName = isset($request->hidden_variation_image_edit) ? $request->hidden_variation_image_edit:null;
        $vtype = isset($request->hidden_virtual_product_file_type) ? $request->hidden_virtual_product_file_type:null;
        if($request->has('virtual_product_file'))
        {
            $vfileName = time().'_'.rand(100,999).".".$request->file('virtual_product_file')->extension();
            $path="/upload/product/virtual_product_file/".$vfileName;
            $public_path = public_path('/upload/product/virtual_product_file');
            $request->file('virtual_product_file')->move($public_path,$vfileName);
            $vtype = pathinfo($vfileName, PATHINFO_EXTENSION);
        }

        $add_product = Product::find($id);
        $add_product->name = isset($request->name) ? $request->name:null;
        $add_product->sku = isset($request->sku) ? $request->sku:null;
        $add_product->image = $fileName;
        $add_product->description = isset($request->description) ? $request->description:null;
        $add_product->price = isset($request->price) ? $request->price:0;
        $add_product->discount = isset($request->discount) ? $request->discount:0;
        $add_product->quantity = isset($request->quantity) ? $request->quantity:0;
        $add_product->virtual_product_file = $vfileName;
        $add_product->virtual_product_file_type = $vtype; 
        $add_product->save();

        ProductAttribute::where(['product_id' => $id])->delete();
        if(count($get_product_attibutes) > 0){
            foreach($get_product_attibutes as $get_product_attibute){
                $add_product_attribute = new ProductAttribute;
                $add_product_attribute->product_id = $add_product->id;
                $add_product_attribute->attribute_id = $get_product_attibute;
                $add_product_attribute->sub_attribute_id = 0;
                $add_product_attribute->save();
            }
        }


        // VARIATION PRO.------------------------------------------------------------

        $variation_name = [];
        if($request->has('variation_name')){
            $variation_name = $request->variation_name;   
        }
        $variation_price = [];
        if($request->has('variation_price')){
            $variation_price = $request->variation_price;
        }
        $variation_discount = [];
        if($request->has('variation_discount')){
            $variation_discount = $request->variation_discount;
        }
        $variation_quanty = [];
        if($request->has('variation_quanty')){
            $variation_quanty = $request->variation_quanty;
        }
        $variation_description = [];
        if($request->has('variation_description')){
            $variation_description = $request->variation_description;
        }

        if (count($variation_name) > 0) {

            foreach($variation_name as $key => $data){

                $v_attributes=[]; $v_sub_attributes=[];
                if(count($get_product_attibutes) > 0){
                    foreach($get_product_attibutes as $sakey => $get_product_attibute){

                        if($request->has($get_product_attibute.'_sub_attribute')){

                            $subtrr = $get_product_attibute.'_sub_attribute';
                            $v_attributes[] = $get_product_attibute;
                            $v_sub_attributes[] = $request->$subtrr[$key];
                        }
                    }
                }

                $fileName = null;

                if(isset($request->variation_image[$key]))
                {
                    $fileName = time().'_'.rand(100,999).".".$request->file('variation_image')[$key]->extension();
                    $path="/upload/product/".$fileName;
                    $public_path = public_path('/upload/product');
                    $request->file('variation_image')[$key]->move($public_path,$fileName);
                }

                $add_vproduct = new Product;
                $add_vproduct->user_id = $poduct_data->user_id;
                $add_vproduct->product_type = isset($poduct_data->product_type) ? $poduct_data->product_type:null;
                $add_vproduct->parent_id = isset($add_product->id) ? $add_product->id:0;
                $add_vproduct->name = isset($variation_name[$key]) ? $variation_name[$key]:null;
                $add_vproduct->image = $fileName;
                $add_vproduct->description = isset($variation_description[$key]) ? $variation_description[$key]:null;
                $add_vproduct->price = isset($variation_price[$key]) ? $variation_price[$key]:0;
                $add_vproduct->discount = isset($variation_discount[$key]) ? $variation_discount[$key]:0;
                $add_vproduct->quantity = isset($variation_quanty[$key]) ? $variation_quanty[$key]:0;
                $add_vproduct->save();

                if(count($v_attributes) > 0){
                    foreach($v_attributes as $key_vp => $get_v_product_attibute){
                        $add_product_attribute = new ProductAttribute;
                        $add_product_attribute->product_id = $add_vproduct->id;
                        $add_product_attribute->attribute_id = $get_v_product_attibute;
                        $add_product_attribute->sub_attribute_id = $v_sub_attributes[$key_vp];
                        $add_product_attribute->save();
                    }
                }
            }
        }

        return redirect()->route('admin.product.index')->with('success','Product updated successfully.');

    }

    public function deleteSingleVariation(Request $request){

        $getCarts = Cart::where(['product_id' => $request->product_id])->first();

        if($getCarts){
            return response()->json(['status' => false,'msg' => "This product available in cart you can not delete."]);
        }
        else{

            $get_product_data = Product::where(['id' => $request->product_id])->first();

            ProductAttribute::where(['product_id' => $request->product_id])->update(['is_delete' => 1]);
            Product::where(['id' => $request->product_id])->update(['is_delete' => 1]);


            $product_type = $get_product_data->product_type;
            $get_attributes = Attribute::where(['is_parent' => 0])->get();
            $is_optional = 0;

            $attributes=[];
        
            $product_details = null; $variation_products = [];
            if(!empty($get_product_data)){
                $product_details = Product::where(['id' => $get_product_data->parent_id])->first();

                $product_ids = [];
                $get_product_details = Product::where('id',$product_details->id)->orWhere('parent_id',$product_details->id)->get();
                if(count($get_product_details) > 0){
                    foreach($get_product_details as $get_product_detail){
                        $product_ids[] = $get_product_detail->id;
                    }
                }
                
                $get_product_attributes = ProductAttribute::where('is_delete',0)->whereIn('product_id',$product_ids)->get();
                $attributes=[];
                if(count($get_product_attributes) > 0){
                    foreach($get_product_attributes as $get_product_attribute){
                        if(!in_array($get_product_attribute->attribute_id,$attributes)){
                            $attributes[] = $get_product_attribute->attribute_id;
                        }
                    }
                }

                $variation_products = Product::where(['parent_id' => $product_details->id,'is_delete' => 0])->get();
                
                //dd($variation_products,$product_details->id);
            }


             $html = view('backend.product.dynamic_add_variation_section', compact('product_type','get_attributes','is_optional','attributes','product_details','variation_products'))->render();


            return response()->json(['success' => true,'html'=> $html,'message' => 'Product variation deleted successfully.']); 
        }
    }

    public function editProductVariation(Request $request){

        $get_product_data = Product::where(['id' => $request->product_id])->first();

        $attributes=[];
        
        $product_details = null; $variation_products = [];
        if(!empty($get_product_data)){
            $product_details = Product::where(['id' => $get_product_data->parent_id])->first();

            $product_ids = [];
            $get_product_details = Product::where('id',$product_details->id)->orWhere('parent_id',$product_details->id)->get();
            if(count($get_product_details) > 0){
                foreach($get_product_details as $get_product_detail){
                    $product_ids[] = $get_product_detail->id;
                }
            }
            
            $get_product_attributes = ProductAttribute::where('is_delete',0)->whereIn('product_id',$product_ids)->get();
            $attributes=[];
            if(count($get_product_attributes) > 0){
                foreach($get_product_attributes as $get_product_attribute){
                    if(!in_array($get_product_attribute->attribute_id,$attributes)){
                        $attributes[] = $get_product_attribute->attribute_id;
                    }
                }
            }

            $variation_product = $get_product_data;
            
            //dd($variation_products,$product_details->id);
        }
        
        $html = view('backend.product.dynamic_edit_variation_product_form', compact('attributes','variation_product'))->render();
        return response()->json(['html'=>$html]);

    }

    public function updateProductVariation(Request $request){

        //dd($request->all());
        $get_product_attibutes = [];
        if($request->has('attribute')){
            $get_product_attibutes = explode(',',$request->attribute);
        }

        $v_attributes=[]; $v_sub_attributes=[];
        if(count($get_product_attibutes) > 0){
            foreach($get_product_attibutes as $sakey => $get_product_attibute){

                if($request->has($get_product_attibute.'_sub_attribute')){

                    $subtrr = $get_product_attibute.'_sub_attribute';
                    $v_attributes[] = $get_product_attibute;
                    $v_sub_attributes[] = $request->$subtrr;
                }
            }
        }
        //dd($v_attributes,$v_sub_attributes);

        $fileName = isset($request->hidden_variation_image_edit) ? $request->hidden_variation_image_edit:null;

        if(isset($request->variation_image))
        {
            $fileName = time().'_'.rand(100,999).".".$request->file('variation_image')->extension();
            $path="/upload/product/".$fileName;
            $public_path = public_path('/upload/product');
            $request->file('variation_image')->move($public_path,$fileName);
        }

        $product_data = Product::where(['id' => $request->product_id])->first();

        $add_vproduct = Product::find($request->product_id);
        $add_vproduct->name = isset($request->variation_name) ? $request->variation_name:null;
        $add_vproduct->image = $fileName;
        $add_vproduct->description = isset($request->variation_description) ? $request->variation_description:null;
        $add_vproduct->price = isset($request->variation_price) ? $request->variation_price:0;
        $add_vproduct->discount = isset($request->variation_discount) ? $request->variation_discount:0;
        $add_vproduct->quantity = isset($request->variation_quanty) ? $request->variation_quanty:0;
        $add_vproduct->save();

        ProductAttribute::where(['product_id' => $request->product_id])->delete();
        if(count($v_attributes) > 0){
            foreach($v_attributes as $key_vp => $get_v_product_attibute){
                $add_product_attribute = new ProductAttribute;
                $add_product_attribute->product_id = $add_vproduct->id;
                $add_product_attribute->attribute_id = $get_v_product_attibute;
                $add_product_attribute->sub_attribute_id = $v_sub_attributes[$key_vp];
                $add_product_attribute->save();
            }
        }

        $get_product_data = Product::where(['id' => $request->product_id])->first();

        $product_type = $get_product_data->product_type;
        $get_attributes = Attribute::where(['is_parent' => 0])->get();
        $is_optional = 0;

        $attributes=[];
    
        $product_details = null; $variation_products = [];
        if(!empty($get_product_data)){
            $product_details = Product::where(['id' => $get_product_data->parent_id])->first();

            $product_ids = [];
            $get_product_details = Product::where('id',$product_details->id)->orWhere('parent_id',$product_details->id)->get();
            if(count($get_product_details) > 0){
                foreach($get_product_details as $get_product_detail){
                    $product_ids[] = $get_product_detail->id;
                }
            }
            
            $get_product_attributes = ProductAttribute::where('is_delete',0)->whereIn('product_id',$product_ids)->get();
            $attributes=[];
            if(count($get_product_attributes) > 0){
                foreach($get_product_attributes as $get_product_attribute){
                    if(!in_array($get_product_attribute->attribute_id,$attributes)){
                        $attributes[] = $get_product_attribute->attribute_id;
                    }
                }
            }

            $variation_products = Product::where(['parent_id' => $product_details->id,'is_delete' => 0])->get();
            
            //dd($variation_products,$product_details->id);
        }


        $html = view('backend.product.dynamic_add_variation_section', compact('product_type','get_attributes','is_optional','attributes','product_details','variation_products'))->render();

        return response()->json(['success' => true,'html'=> $html,'message' => 'Product variation updated successfully.']);

    }

    public function checkDeleteProduct(){

    }

    public function delete(Request $request,$id){

        //dd($id);

        $getCarts = Cart::where(['product_id' => $id])->first();
        
        $get_variations = Product::where(['parent_id' => $id,'is_delete' => 0])->get();
        
        $cnt_Variation = 0;
        if(count($get_variations)){
            foreach($get_variations as $get_variation){
                $getCartschk = Cart::where(['product_id' => $get_variation->id])->first();
                if($getCartschk){
                    $cnt_Variation = $cnt_Variation+1;
                }
            }
        }
            
        if($getCarts){
            return redirect()->route('admin.product.index')->with('error','This product available in cart you can not delete.');
        }
        elseif($cnt_Variation > 0){
            return redirect()->route('admin.product.index')->with('error','This product available in cart you can not delete.');
        }
        else{

            $get_variations = Product::where(['parent_id' => $id,'is_delete' => 0])->get();

            if(count($get_variations)){
                foreach($get_variations as $get_variation){
                    ProductAttribute::where(['product_id' => $get_variation->id])->update(['is_delete' => 1]);
                    Product::where(['id' => $get_variation->id])->update(['is_delete' => 1]);                    
                }
            }
            
            UserShare::where(['share_type' => 'product','share_id' => $id])->update(['is_delete' => 1]);
            ProductAttribute::where(['product_id' => $id])->update(['is_delete' => 1]);
            Product::where(['id' => $id])->update(['is_delete' => 1]);

            return redirect()->route('admin.product.index')->with('success','Product deleted successfully.');
        }
        

    }





}
