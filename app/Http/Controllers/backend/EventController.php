<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\User;

class EventController extends Controller
{
    public function index(Request $request,$user_id)
    {   
        $events = Event::where(['user_id' => $user_id,'is_delete' => 0])->get();
        $user_details = User::where(['id' => $user_id])->first();
        return view('backend.event.index',compact('events','user_details'));
    }
}
