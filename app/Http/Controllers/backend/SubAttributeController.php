<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Attribute;
use Validator;

class SubAttributeController extends Controller
{
    public function index(Request $request,$attribute_id)
    {
        $sub_attributes = Attribute::where(['is_parent' => $attribute_id])->get();
        $attribute_data = Attribute::where(['id' => $attribute_id])->first();
        return view('backend.sub_attribute.index',compact('sub_attributes','attribute_data'));
    }

    public function create(Request $request,$attribute_id){
        $attribute_data = Attribute::where(['id' => $attribute_id])->first();
        return view('backend.sub_attribute.add',compact('attribute_data'));
    }

    public function store(Request $request,$attribute_id){

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:attributes',
        ], [
            'name' => 'Please enter sub attribute name.',
            'name.unique' => 'Sub attribute name already exist.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $add =  new Attribute;
        $add->name = $request->name;
        $add->slug = strtolower($request->name);
        $add->is_parent = $attribute_id;
        $add->save();
        return redirect()->route('admin.sub_attribute.index',$attribute_id)->with('success','Sub attribute added successfully.');
    }

    public function edit($attribute_id,$id){
        $sub_attribute_details = Attribute::where('id',$id)->first();
        $attribute_data = Attribute::where(['id' => $attribute_id])->first();
        return view('backend.sub_attribute.edit',compact('sub_attribute_details','attribute_data'));
    }

    public function update(Request $request,$attribute_id,$id){

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:attributes,name,'.$id,
        ], [
            'name' => 'Please enter sub attribute name.',
            'name.unique' => 'Sub attribute name already exist.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
            
        $add =  Attribute::find($id);
        $add->name = $request->name;
        $add->slug = strtolower($request->name);
        $add->is_parent = $attribute_id;
        $add->save();
        return redirect()->route('admin.sub_attribute.index',$attribute_id)->with('success','Sub attribute updated successfully.');

    }

    public function delete($attribute_id,$id){
        
        $sub_attribute = Attribute::find($id);
        $sub_attribute->delete();
        return redirect()->route('admin.sub_attribute.index',$attribute_id)->with('success','Sub attribute deleted successfully');
    }
}
