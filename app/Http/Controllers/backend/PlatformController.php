<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Platform;
use Validator;

class PlatformController extends Controller
{
    public function index(Request $request)
    {
        $platforms = Platform::all();
        return view('backend.platform.index',compact('platforms'));
    }

    public function create(Request $request){
        return view('backend.platform.add');
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:platforms',
        ], [
            'name' => 'Please enter platform name.',
            'name.unique' => 'Platform name already exist.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $add =  new Platform;
        $add->name = $request->name;
        $add->save();
        return redirect()->route('admin.platform.index')->with('success','Platform added successfully.');
    }

    public function edit($id){
        $platform_details = Platform::where('id',$id)->first();
        return view('backend.platform.edit',compact('platform_details'));
    }

    public function update(Request $request,$id){

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:platforms,name,'.$id,
        ], [
            'name' => 'Please enter platform name.',
            'name.unique' => 'Platform name already exist.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $add = Platform::find($id);
        $add->name = $request->name;
        $add->save();
        return redirect()->route('admin.platform.index')->with('success','Platform updated successfully.');

    }

    public function delete($id){
        
        $platform = Platform::find($id);
        $platform->delete();
        return redirect()->route('admin.platform.index')->with('success','Platform deleted successfully');
    }
}
