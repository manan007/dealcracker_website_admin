<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Platform;
use Validator;
use Hash;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::where(['is_admin' => 0])->get();
        return view('backend.user.index',compact('users'));
    }

    public function userView($id){
        $user_details = User::where(['id' => $id])->first();
        return view('backend.user.user_detail',compact('user_details'));
    }


    public function create(Request $request){
        $platforms = Platform::all();
        return view('backend.user.add',compact('platforms'));
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email'=>'required|email|unique:users,email',
            'phone'=>'required|unique:users,phone'
        ], [
            'name' => 'Please enter category name.',
            'name.unique' => 'Category name already exist.',
            'email' => 'Please enter email.',
            'email.unique' => 'Email already exist.',
            'phone' => 'Please enter phone.',
            'phone.unique' => 'Phone Number already exist.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $platforms = null;
        if($request->has('platforms')){ if( count($request->platforms) > 0){ $platforms = implode(',', $request->platforms); }}

        $userdata = new User;
        $userdata->name = $request->name;
        $userdata->email = $request->email;
        $userdata->phone =  $request->phone;
        $userdata->password = Hash::make("123456");
        $userdata->address = isset($request->address) ? $request->address:null;
        $userdata->dob = isset($request->dob) ? $request->dob:null;
        $userdata->website =  isset($request->website) ? $request->website:null;
        $userdata->profile_description = isset($request->profile_description) ? $request->profile_description:null;
        $userdata->key_skills = isset($request->key_skills) ? $request->key_skills:null;
        $userdata->business_domain = isset($request->business_domain) ? $request->business_domain:null;
        $userdata->other_info = isset($request->other_info) ? $request->other_info:null;
        $userdata->platforms = $platforms; 
        $userdata->save_status = 1;
        $userdata->save();

        return redirect()->route('admin.user.index')->with('success','User added successfully.');
    }

    public function edit(Request $request,$id){
        $user_details = User::where('id',$id)->first();
        $platforms = Platform::all();
        return view('backend.user.edit',compact('user_details','platforms'));
    }

    public function update(Request $request,$id){

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email'=>'required|email|unique:users,email,'.$id,
            'phone'=>'required|unique:users,phone,'.$id,
        ], [
            'name' => 'Please enter category name.',
            'name.unique' => 'Category name already exist.',
            'email' => 'Please enter email.',
            'email.unique' => 'Email already exist.',
            'phone' => 'Please enter phone.',
            'phone.unique' => 'Phone Number already exist.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $platforms = null;
        if($request->has('platforms')){ if( count($request->platforms) > 0){ $platforms = implode(',', $request->platforms); }}

        $userdata = User::find($id);
        $userdata->name = $request->name;
        $userdata->email = $request->email;
        $userdata->phone =  $request->phone;
        if($request->has('password') && $request->password != null){
            $userdata->password = Hash::make($request->password);   
        }
        $userdata->address = isset($request->address) ? $request->address:null;
        $userdata->dob = isset($request->dob) ? $request->dob:null;
        $userdata->website =  isset($request->website) ? $request->website:null;
        $userdata->profile_description = isset($request->profile_description) ? $request->profile_description:null;
        $userdata->key_skills = isset($request->key_skills) ? $request->key_skills:null;
        $userdata->business_domain = isset($request->business_domain) ? $request->business_domain:null;
        $userdata->other_info = isset($request->other_info) ? $request->other_info:null;
        $userdata->platforms = $platforms; 
        $userdata->save_status = 1;
        $userdata->save();

        return redirect()->route('admin.user.index')->with('success','User updated successfully.');
    }

    public function delete(Request $request,$id){

        User::where(['id' => $id])->delete();

        return redirect()->route('admin.user.index')->with('success','User deleted successfully.');
    }

}
