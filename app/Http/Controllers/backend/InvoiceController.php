<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Models\User;

class InvoiceController extends Controller
{
    public function index(Request $request,$user_id)
    {   
        $invoices = Invoice::where(['user_id' => $user_id,'is_delete' => 0])->get();
        $user_details = User::where(['id' => $user_id])->first();
        return view('backend.invoice.index',compact('invoices','user_details'));
    }
}
