<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BusinessTemplate;

class BusinessTemplateController extends Controller
{
    
    public function index(){
        $business_template = BusinessTemplate::where(['is_delete' => 0])->get();
        return view('backend.business_template.index',compact('business_template'));
    }


    public function delete($id){

        $update = BusinessTemplate::find($id);
        $update->is_delete = 1;
        $update->save();
        return redirect()->route('admin.business_template.index')->with('success','Business template deleted successfully');
    }


    public function changeStatus(Request $request){
        $id = $request->id;
        $getdetails = BusinessTemplate::find($id);
        if($getdetails->status == 1){
            BusinessTemplate::where(['id' => $id])->update(['status' => 0]);
        }
        else{
            BusinessTemplate::where(['id' => $id])->update(['status' => 1]);
        }
        return response()->json(['success' => true,'msg' => 'Status updated successfully.']);
    }

}
