<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Plan;
use App\Models\PlanFeature;

class PlanController extends Controller
{
    public function index(Request $request)
    {
        $plans = Plan::all();
        return view('backend.plan.index',compact('plans'));
    }

    public function create(Request $request){
        return view('backend.plan.add');
    }

    public function store(Request $request){

        $add =  new Plan;
        $add->name = $request->name;
        $add->price = $request->price;
        $add->duration = $request->duration;
        $add->save();

        if(count($request->feature) > 0){
            foreach ($request->feature as $key => $value) {
                if(!empty($value)){
                    $add_pf =  new PlanFeature;
                    $add_pf->plan_id = $add->id;
                    $add_pf->feature = $value;
                    $add_pf->save();
                }
            }
        }

        return redirect()->route('admin.plan.index')->with('success','Plan added successfully.');
    }

    public function edit($id){
        $plan_details = Plan::where('id',$id)->first();
        $plan_features = PlanFeature::where('plan_id',$id)->get();
        return view('backend.plan.edit',compact('plan_details','plan_features'));
    }

    public function update(Request $request,$id){
        
        $add = Plan::find($id);
        $add->name = $request->name;
        $add->price = $request->price;
        $add->duration = $request->duration;
        $add->save();

        PlanFeature::where(['plan_id' => $id])->delete();
        if(count($request->feature) > 0){
            foreach ($request->feature as $key => $value) {
                if(!empty($value)){
                    $add_pf =  new PlanFeature;
                    $add_pf->plan_id = $add->id;
                    $add_pf->feature = $value;
                    $add_pf->save();
                }
            }
        }

        return redirect()->route('admin.plan.index')->with('success','Plan updated successfully.');

    }

    public function delete($id){

        $plan = Plan::find($id);
        $plan->delete();
        PlanFeature::where(['plan_id' => $id])->delete();
        return redirect()->route('admin.plan.index')->with('success','Plan deleted successfully');

        // $check_plan_features = PlanFeature::where(['plan_id' => $id])->get();

        // if(count($check_plan_features) > 0){
        //     return redirect()->route('admin.plan.index')->with('error',"You can't delete this plan because plan features already exist.");
        // }
        // else{
        //     $plan = Plan::find($id);
        //     $plan->delete();
        //     return redirect()->route('admin.plan.index')->with('success','Plan deleted successfully');
        // }
    }
}
