<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use Validator;

class BannerController extends Controller
{
        
    public function index(Request $request)
    {
        $banners = Banner::all();
        return view('backend.banner.index',compact('banners'));
    }

    public function create(Request $request){
        return view('backend.banner.add');
    }

    public function store(Request $request){

        $fileName = null;
        if($request->has('app_banner'))
        {
            $fileName = time().'_'.rand(100,999).".".$request->file('app_banner')->extension();
            $path="/upload/app_banner/".$fileName;
            $public_path = public_path('/upload/app_banner');
            $request->file('app_banner')->move($public_path,$fileName);
        }

        // $fileName1 = null;
        // if($request->has('web_banner'))
        // {
        //     $fileName1 = time().'_'.rand(100,999).".".$request->file('web_banner')->extension();
        //     $path1 ="/upload/web_banner/".$fileName1;
        //     $public_path1 = public_path('/upload/web_banner');
        //     $request->file('web_banner')->move($public_path1,$fileName1);
        // }

        $add =  new Banner;
        $add->app_banner = $fileName;
        $add->web_banner = null;
        $add->save();

        return redirect()->route('admin.banner.index')->with('success','Banner added successfully.');
    }

    public function edit($id){
        $banner_details = Banner::where('id',$id)->first();
        return view('backend.banner.edit',compact('banner_details'));
    }

    public function update(Request $request,$id){
        
        $fileName = $request->hidden_app_banner;
        if($request->has('app_banner'))
        {
            $fileName = time().'_'.rand(100,999).".".$request->file('app_banner')->extension();
            $path="/upload/app_banner/".$fileName;
            $public_path = public_path('/upload/app_banner');
            $request->file('app_banner')->move($public_path,$fileName);
        }

        // $fileName1 = $request->hidden_web_banner;
        // if($request->has('web_banner'))
        // {
        //     $fileName1 = time().'_'.rand(100,999).".".$request->file('web_banner')->extension();
        //     $path1 ="/upload/web_banner/".$fileName1;
        //     $public_path1 = public_path('/upload/web_banner');
        //     $request->file('web_banner')->move($public_path1,$fileName1);
        // }

        $add = Banner::find($id);
        $add->app_banner = $fileName;
        $add->web_banner = null;
        $add->save();
        return redirect()->route('admin.banner.index')->with('success','Banner updated successfully.');

    }

    public function delete($id){
        
        $banner = Banner::find($id);
        $banner->delete();
        return redirect()->route('admin.banner.index')->with('success','Banner deleted successfully');
    }

}
