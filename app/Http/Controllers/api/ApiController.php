<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use  Carbon\Carbon;
use DateTime;
use DateTimeZone;
use App\Models\User;
use Hash;
use App\Models\Platform;
use App\Models\ProfileLike;
use App\Models\Feed;
use App\Models\FeedMedia;
use App\Models\FeedLike;
use App\Models\FeedComment;
use App\Models\FeedCommentLike;

use App\Models\Business;
use App\Models\BusinessTemplate;
use App\Models\Opportunity;

use App\Models\Blog;
use App\Models\BlogMedia;

use App\Models\TemplateCategory;
use App\Models\TemplateSubCategory;
use App\Models\Testimonial;

use App\Models\Community;
use App\Models\CommunityQuestion;

use App\Models\ContactUs;
use App\Models\Support;
use Chatify\Facades\ChatifyMessenger as Chatify;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;

use App\Models\Message;

use App\Models\Plan;
use App\Models\PlanFeature;
use App\Models\Payment;
use App\Models\UserPlan;
use App\Models\BusinessCategory;

class ApiController extends Controller
{

    public function register(request $request)
    {

        $rules = array(
            'name' => 'required',
            'email'=>'required|email|unique:users,email',
            'phone'=>'required|unique:users,phone',
            'password' => 'required',
        );
        $messages = array(
            'name.required' =>'Please enter name.',
            'email.required' =>'Please enter email.',
            'phone.required' =>'Please enter phone number.',
            'password.required' =>'Please enter password.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $registerdata = new User;
        $registerdata->name = $request->name;
        $registerdata->email = $request->email;
        $registerdata->phone =  $request->phone;
        $registerdata->password = Hash::make($request->password);
        $registerdata->save();

        $save = ['id'=> $registerdata->id,'name'=> $registerdata->name,'email'=> $registerdata->email,'phone'=>$registerdata->phone,'password'=>$request->password,'created_at'=>$registerdata->created_at,'updated_at'=>$registerdata->updated_at];

        return response()->json(['status' => true,'msg' => "Register successfully.","data" => $save]);
    
    }

    public function login(Request $request)
    {
        $rules = array(
            'email'=>'required',
            'password' => 'required'
        
        );
        $messages = array(
            'email.required' =>'Please enter email.',
            'password.required' =>'Please enter password.'
         );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $loginData = User::where(['email' => $request->email,'is_admin' => 0])->first();

        if($loginData)
        {
            $passwordCheck = Hash::check($request->password, $loginData->password);

            if($passwordCheck == true)
            {
                return response()->json([
                'status' => true,
                'msg' => "Login SuccessFully.",
                'data'=> $loginData
                ]);
            }
            else{
                return response()->json(['status' => false,'msg' => "Invalid password.",]);
            }
        }
        else
        {
          return response()->json(['status' => false,'msg' => "User Not Found.",]);
        }
    }

    public function platform_list(){
        $get_platforms = Platform::all();
        if(count($get_platforms) > 0){
            return response()->json(['status' => true,'msg' => "Platform list fetch successfully.",'data' => $get_platforms]);
        }
        else{
            return response()->json(['status' => false,'msg' => "Platform not found.",]);
        }
    }

    public function complete_profile(Request $request){ //(0=default,1=complete_profile,2=subscription,3=otp_verification)

        $rules = array(
            'user_id' => 'required'
            // 'dob' => 'required',
            // 'website' => 'required',
            // 'profile_description' => 'required',
            // 'key_skills' => 'required',
            // 'business_domain' => 'required',
            // 'other_info' => 'required',
            // 'platforms' => 'required'
        );
        $messages = array(
            'user_id' => 'Please enter user id.'
            // 'dob' => 'Please enter dob.',
            // 'website' => 'Please enter website.',
            // 'profile_description' => 'Please enter profile description.',
            // 'key_skills' => 'Please enter key skills.',
            // 'business_domain' => 'Please enter business domain.',
            // 'other_info' => 'Please enter other information.',
            // 'platforms' => 'Please enter platforms'
        );

        $checkUser = User::where(['id' => $request->user_id,'is_admin' => 0])->first();

        if($checkUser)
        {
            $addprofile = User::find($checkUser->id);
            $addprofile->address = isset($request->address) ? $request->address:null;
            $addprofile->dob = isset($request->dob) ? $request->dob:null;
            $addprofile->website =  isset($request->website) ? $request->website:null;
            $addprofile->profile_description = isset($request->profile_description) ? $request->profile_description:null;
            $addprofile->key_skills = isset($request->key_skills) ? $request->key_skills:null;
            $addprofile->business_domain = isset($request->business_domain) ? $request->business_domain:null;
            $addprofile->other_info = isset($request->other_info) ? $request->other_info:null;
            $addprofile->platforms = isset($request->platforms) ? $request->platforms:null; 
            $addprofile->save_status = 1;   
            $addprofile->save();

            return response()->json(['status' => true,'msg' => "Profile details store successfully.","data" => $addprofile]);
        }
        else{
            return response()->json(['status' => false,'msg' => "User Not Found.",]);
        }

    }


    public function forgotpassword(Request $request)
    {

        $rules = array(
            'email'=>'required'
        );
        $messages = array(
            'email.required' =>'Please enter email.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $getUser = User::where(['email' => $request->email,'is_admin' => 0])->first();

        if($getUser)
        {
            
            $fromAdd=env('MAIL_FROM_ADDRESS');
            $new_pass = $this->generateToken();

            $data = ['password' => $new_pass];

            $registerdata = User::find($getUser->id);
            $registerdata->password = Hash::make($new_pass);
            $registerdata->save();

            try{
                $Mail = \Mail::send (['html' => 'email.sendpassword'], $data, function ($message) use ($getUser,$fromAdd) {
                    $message->from ("jaydeeptest3@gmail.com",env('MAIL_FROM_NAME'));
                    $message->to($getUser->email)->subject ( 'Email Verification' );
                });
                return response()->json(['status' => true,'msg'=>'Password send successfully.']);
            }
            catch(\Exception $e)
            {
                return response()->json(['status' => false,'msg'=>'Somthing went wrong.']);
            }   
        }
        else
        {
            return response()->json(['status' => false,'msg' => "User not found."]);
        }
    }



    public function edit_profile(Request $request){

        $rules = array(
            'user_id'=>'required'
        );
        $messages = array(
            'user_id.required' =>'Please enter user id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){

            $fileName = null;
            if($request->has('profile'))
            {
                $fileName = time().'_'.rand(100,999).".".$request->file('profile')->extension();
                $path="/upload/profile/".$fileName;
                $public_path = public_path('/upload/profile');
                $request->file('profile')->move($public_path,$fileName);
            }

            $editprofile = User::find($check_user->id);
            $editprofile->name = isset($request->address) ? $request->name:null;
            $editprofile->email = isset($request->address) ? $request->email:null;
            $editprofile->phone = isset($request->phone) ? $request->phone:null;
            $editprofile->address = isset($request->address) ? $request->address:null;
            $editprofile->dob = isset($request->dob) ? $request->dob:null;
            $editprofile->website =  isset($request->website) ? $request->website:null;
            $editprofile->profile_description = isset($request->profile_description) ? $request->profile_description:null;
            $editprofile->key_skills = isset($request->key_skills) ? $request->key_skills:null;
            $editprofile->business_domain = isset($request->business_domain) ? $request->business_domain:null;
            $editprofile->other_info = isset($request->other_info) ? $request->other_info:null;
            $editprofile->platforms = isset($request->platforms) ? $request->platforms:null; 
            $editprofile->profile = $fileName;   
            $editprofile->save();

            return response()->json(['status' => true,'msg' => "User profile updated successfully."]);
            
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }

    }



    public function my_profile(Request $request){   

        $rules = array(
            'user_id'=>'required'
        );
        $messages = array(
            'user_id.required' =>'Please enter user id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){

            if($check_user->profile){
               $check_user['profile'] = asset('upload/profile/'.$check_user->profile);
            }
            else{
                $check_user['profile'] = asset('upload/default_user.png');
            }

            $check_user['profile_likes'] = ProfileLike::where(['friend_id' => $check_user->id,'status' => 1])->count();

            $check_user['profile_dislikes'] = ProfileLike::where(['friend_id' => $check_user->id,'status' => 2])->count();

            return response()->json(['status' => true,'msg' => "My profile details fetch successfully.",'data' => $check_user]);
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }

    }


    public function other_user_profile(Request $request){

        $rules = array(
            'user_id'=>'required',
            'friend_id' => 'required'
        );
        $messages = array(
            'user_id.required' =>'Please enter user id.',
            'friend_id.required' => 'Please enter friend id.'
        ); 

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){

            $check_friend = User::where(['id' => $request->friend_id])->first();

            if($check_friend){

                if($check_friend->profile){
                   $check_friend['profile'] = asset('upload/profile/'.$check_friend->profile);
                }
                else{
                    $check_friend['profile'] = asset('upload/default_user.png');
                }

                $check_profile_likes = ProfileLike::where(['user_id' => $check_user->id,'friend_id' => $check_friend->id])->first();
                if($check_profile_likes){
                    $check_friend['like_dislike_status'] = $check_profile_likes->status;    
                }   
                else{
                    $check_friend['like_dislike_status'] = 0;
                } 

                return response()->json(['status' => true,'msg' => "Profile details fetch successfully.",'data' => $check_friend]);

            }
            else{   
                return response()->json(['status' => false,'msg' => "Friend not found."]);
            } 
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }



    }


    public function like_dislike_profile(Request $request){


        $rules = array(
            'user_id'=>'required',
            'friend_id' => 'required',
            'liked_status' => 'required'
        );
        $messages = array(
            'user_id.required' =>'Please enter user id.',
            'friend_id.required' => 'Please enter friend id.',
            'liked_status.required' => 'Please enter liked status.'
        ); 

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){

            $check_friend = User::where(['id' => $request->friend_id])->first();

            if($check_friend){

                if($request->liked_status == 0){

                    $check_profile = ProfileLike::where(['user_id' => $check_user->id,'friend_id' => $check_friend->id])->first();
                    if($check_profile){
                        ProfileLike::where(['id' => $check_profile->id])->delete();
                    } 

                    return response()->json(['status' => true,'msg' => "Profile status updated successfully."]);
                }
                else{

                    $check_profile = ProfileLike::where(['user_id' => $check_user->id,'friend_id' => $check_friend->id])->first();
                    if($check_profile){
                        ProfileLike::where(['id' => $check_profile->id])->update(['status' => $request->liked_status]);
                    } 
                    else{
                        $add = new ProfileLike;
                        $add->user_id = $check_user->id;
                        $add->friend_id = $check_friend->id;
                        $add->status = $request->liked_status;
                        $add->save();
                    }
                    return response()->json(['status' => true,'msg' => "Profile status updated successfully."]);
                }
            }
            else{   
                return response()->json(['status' => false,'msg' => "Friend not found."]);
            } 
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }

    }


    public function change_password(Request $request){

        $rules = array(
            'user_id' => 'required',
            'old_password' => 'required',
            'new_password' => 'required',
        );
        $messages = array(
            'user_id.required' => 'Please enter old password.',
            'old_password.required' => 'Please enter old password.',
            'new_password.required' => 'Please enter new password.',
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {

            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){

            if (Hash::check($request->old_password, $check_user->password)) { 
            
                User::where(['id' => $check_user->id])->update(['password' => Hash::make($request->new_password)]);

                return response()->json(['status' => true,'msg' => "Password reset successfully."]);

            } else {
                return response()->json(['status' => false,'msg' => "Old password not match."]);
            }

            return response()->json(['status' => true,'msg' => "My profile details fetch successfully.",'data' => $check_user]);
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }

    }


    public function create_feed(Request $request){

        $rules = array(
            'user_id' => 'required'
        );
        $messages = array(
            'user_id.required' =>'Please enter user id.'
        ); 

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){

            $add = new Feed;
            $add->user_id = $check_user->id;
            $add->title = $request->title;
            $add->description = isset($request->description) ? $request->description:null;
            $add->save();

            if($request->has('media'))
            {
                $count = count($request->media);
                if ($count>0) 
                {
                    for ($i=0; $i < $count;  $i++) 
                    { 
                        $name = time() . mt_rand(10000, 99999);
                        $imageName = $name . '.' . $request->media[$i]->getClientOriginalExtension();
                        $request->media[$i]->move(public_path().'/upload/post/', $imageName);  
                        $data[] = $imageName;  
                        $add_media = new FeedMedia;
                        $add_media->feed_id = $add->id;
                        $add_media->feed_media = $imageName;
                        $ext = pathinfo($imageName, PATHINFO_EXTENSION);
                        if ($ext == 'mp4'){
                            $add_media->type = 'video';
                        } 
                        else{
                            $add_media->type = 'image'; 
                        }  
                        $add_media->save();      
                    }   
                }
            }
            return response()->json(['status' => true,'msg'=>"Feed created successfully."]);
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }

    }


    public function edit_feed(Request $request){

        $rules = array(
            'user_id' => 'required',
            'feed_id' => 'required'
        );
        $messages = array(
            'user_id.required' => 'Please enter user id.',
            'feed_id.required' => 'Please enter feed id.'
        ); 

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){

            $check_feed = Feed::where(['id' => $request->feed_id,'is_delete' => 0])->first();

            if($check_feed){

                $edit = Feed::find($check_feed->id);
                $edit->user_id = $check_user->id;
                $edit->title = $request->title;
                $edit->description = isset($request->description) ? $request->description:null;
                $edit->save();

                // if($request->has('media'))
                // {
                //     $count = count($request->media);
                //     if ($count>0) 
                //     {
                //         for ($i=0; $i < $count;  $i++) 
                //         { 
                //             $name = time() . mt_rand(10000, 99999);
                //             $imageName = $name . '.' . $request->media[$i]->getClientOriginalExtension();
                //             $request->media[$i]->move(public_path().'/upload/post/', $imageName);  
                //             $data[] = $imageName;  
                //             $edit_media = new FeedMedia;
                //             $edit_media->feed_id = $edit->id;
                //             $edit_media->feed_media = $imageName;
                //             $ext = pathinfo($imageName, PATHINFO_EXTENSION);
                //             if ($ext == 'mp4'){
                //                 $edit_media->type = 'video';
                //             } 
                //             else{
                //                 $edit_media->type = 'image'; 
                //             }  
                //             $edit_media->save();      
                //         }   
                //     }
                // }
                return response()->json(['status' => true,'msg'=>"Feed updated successfully."]);

            }
            else{
                return response()->json(['status' => false,'msg' => "Feed not found."]);
            }
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }

    }


    public function delete_feed(Request $request){

        $rules = array(
            'feed_id' => 'required'
        );
        $messages = array(
            'feed_id.required' => 'Please enter feed id'
        ); 

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_feed = Feed::where(['id' => $request->feed_id,'is_delete' => 0])->first();

        if($check_feed){
            Feed::where(['id' => $check_feed->id])->update(['is_delete' => 1]);
            return response()->json(['status' => true,'msg' => "Feed deleted successfully."]);
        }
        else{
            return response()->json(['status' => false,'msg' => "Feed not found."]);
        }

    }


    public function delete_feed_media(Request $request){

        $rules = array(
            'feed_media_id' => 'required'
        );
        $messages = array(
            'feed_media_id.required' => 'Please enter feed media id.'
        ); 

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_feed_media = FeedMedia::where(['id' => $request->feed_media_id])->first();

        if($check_feed_media){
            FeedMedia::where(['id' => $check_feed_media->id])->delete();
            return response()->json(['status' => true,'msg' => "Feed media deleted successfully."]);
        }
        else{
            return response()->json(['status' => false,'msg' => "Feed media not found."]);
        }
    }

    public function feed_like(Request $request)
    {

        $rules = array(
            'user_id'=>'required',
            'feed_id'=>'required'
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.',
            'feed_id.required' => 'Please enter post id.'
        );
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }


        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){

            $check_feed = Feed::where(['id' => $request->feed_id,'is_delete' => 0])->first();

            if($check_feed){

                $check_like = FeedLike::where(['user_id' => $check_user->id,'feed_id' => $check_feed->id])->first();
                
                if($check_like){
                    FeedLike::where(['user_id' => $check_user->id,'feed_id' => $check_feed->id])->delete();
                    return response()->json(['status' => true,'msg' => "Feed like remove successfully."]);
                }
                else{
                    $addlike = New FeedLike;
                    $addlike->user_id = $check_user->id;
                    $addlike->feed_id = $check_feed->id;
                    $addlike->save();
                    return response()->json(['status' => true,'msg' => "Feed like add successfully."]);
                }
            }
            else{
                return response()->json(['status' => false,'msg' => "Feed not found."]);
            }
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }        
    }


    public function create_feed_comment(Request $request){

        $rules = array(
            'user_id'=>'required',
            'feed_id'=>'required',
            'comment'=>'required',
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.',
            'feed_id.required' => 'Please enter feed id.',
            'comment.required' => 'Please enter comment.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){

            $check_feed = Feed::where(['id' => $request->feed_id,'is_delete' => 0])->first();

            if($check_feed){

                $addcomments = New FeedComment;
                $addcomments->user_id = $check_user->id;
                $addcomments->feed_id = $check_feed->id;
                $addcomments->comment = $request->comment;
                $addcomments->save();

                return response()->json(['status'=> true,'msg'=>"Comment added successfully."]);
            }
            else{
                return response()->json(['status' => false,'msg' => "Feed not found."]);
            }
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]); 
        }

    }


    public function edit_feed_comment(Request $request){

        $rules = array(
            'user_id'=>'required',
            'comment_id'=>'required',
            'comment'=>'required',
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.',
            'comment_id.required' => 'Please enter comment id.',
            'comment.required' => 'Please enter comment.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_feed_comment = FeedComment::where(['id' => $request->comment_id,'user_id' => $request->user_id])->first();

        if($check_feed_comment){

            $updatecomment = FeedComment::find($check_feed_comment->id);
            $updatecomment->comment = $request->comment;
            $updatecomment->save();

            return response()->json(['status' => true,'msg'=>"Comment updated successfully."]);
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]); 
        }
    }


    public function delete_feed_comment(Request $request){

        $rules = array(
            'user_id'=>'required',
            'comment_id'=>'required',
            'feed_id'=>'required',
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.',
            'comment_id.required' => 'Please enter comment id.',
            'feed_id.required' => 'Please enter feed id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $checkcommentid = FeedComment::where('id',$request->comment_id)->first();
        $checkfeedid = Feed::where('id',$request->feed_id)->first();

        if($checkcommentid)
        {

            $checkcomment = FeedComment::where(['id' => $request->comment_id,'user_id' => $request->user_id])->first();
            
            if($checkcomment)
            {
                $deletecomment = FeedComment::where(['id' => $request->comment_id,'user_id' => $request->user_id])->delete();
                return response()->json(['status' => true,'msg' => "Comment remove successfully."]);
            }
            elseif($checkfeedid)
            {

                $checkfeed = Feed::where(['id' => $request->feed_id,'user_id' => $request->user_id])->first();

                if($checkfeed)
                {
                    $deletecomment = FeedComment::where('id',$request->comment_id)->where('post_id',$checkfeed->id)->delete();
                    return response()->json(['status' => true,'msg'=>"Comment remove successfully."]);
                }
                else{
                    return response()->json(['status'=> false,'msg'=>"Comment not found."]);
                }  
            }
            else
            {
                return response()->json(['status'=> false,'msg'=>"Feed Not Found."]);
            }
        }
        else
        {
            return response()->json(['status' => false,'msg' => "Comment Not Found."]);
        }
    }


    public function create_feed_comment_reply(Request $request){


        $rules = array(
            'user_id'=>'required',
            'feed_id'=>'required',
            'to_comment_id'=>'required',
            'comment'=>'required'
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.',
            'feed_id.required' => 'Please enter feed id.',
            'to_comment_id.required' => 'Please enter to comment id.',
            'comment.required' => 'Please enter comment.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){

            $checkfeed = Feed::where('id',$request->feed_id)->first();
            if($checkfeed)
            {
                $replycomment = New FeedComment;
                $replycomment->user_id = $request->user_id;
                $replycomment->feed_id = $request->feed_id;
                $replycomment->to_comment = $request->to_comment_id;
                $replycomment->is_reply = '1';
                $replycomment->comment = $request->comment;
                $replycomment->save();

                return response()->json(['status' => true,'msg'=>"Comment reply successfully."]);
            }
            else
            {
                return response()->json(['status' => false,'msg'=>"Feed not found."]);
            }
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }

    }


    public function feed_comment_like(Request $request){

        $rules = array(
            'user_id'=>'required',
            'feed_id'=>'required',
            'comment_id'=>'required'
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.',
            'feed_id.required' => 'Please enter feed id.',
            'comment_id.required' => 'Please enter comment id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){

            $check_feed = Feed::where(['id' => $request->feed_id,'is_delete' => 0])->first();

            if($check_feed){

                $check_comment = FeedComment::where(['id' => $request->comment_id])->first();

                if($check_comment){

                    $check_comment_like = FeedCommentLike::where(['user_id' => $check_user->id,'feed_id' => $check_feed->id,'comment_id' => $check_comment->id])->first();
                    
                    if($check_comment_like){
                        FeedCommentLike::where(['user_id' => $check_user->id,'feed_id' => $check_feed->id,'comment_id' => $check_comment->id])->delete();
                        return response()->json(['status' => true,'msg' => "Feed comment like remove successfully."]);
                    }
                    else{
                        $addlike = New FeedCommentLike;
                        $addlike->user_id = $check_user->id;
                        $addlike->feed_id = $check_feed->id;
                        $addlike->comment_id = $check_comment->id;
                        $addlike->save();
                        return response()->json(['status' => true,'msg' => "Feed comment like add successfully."]);
                    }
                }
                else{
                    return response()->json(['status' => false,'msg' => "Comment not found."]);
                }
            }
            else{
                return response()->json(['status' => false,'msg' => "Feed not found."]);
            }
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]); 
        }

    }


    public function my_feed_list(Request $request){

        $rules = array(
            'user_id'=>'required'
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){

            $get_feeds = Feed::with('feed_media')->where(['user_id' => $request->user_id,'is_delete' => 0])->get();

            $feeds=[];
            if(count($get_feeds) > 0)
            {
                foreach ($get_feeds as $key => $value) {

                    //$get_feed_details = Feed::where(['id' => $value->id])->first();
                    $get_feed_details = Feed::with(['user_details' => function($query){
                                $query->select('id','name','profile');
                            }])->where(['id' => $value->id])->first();

                    if( $get_feed_details->user_details->profile){
                        $get_feed_details->user_details['profile'] = asset('upload/profile/'.$get_feed_details->user_details->profile);
                    }
                    else{
                        $get_feed_details->user_details['profile'] = asset('upload/default_user.png');
                    }

                    $get_feed_details['time_different'] = $get_feed_details->created_at->diffForHumans();

                    /*---- FeedMedia ----*/
                    $get_feed_media = FeedMedia::where(['feed_id' => $value->id])->get();
                    $feed_media=[];
                    if(count($get_feed_media) > 0){
                        foreach($get_feed_media as $key_media => $value_media){
                            $get_feed_media_details = FeedMedia::where(['id' => $value_media->id])->first();
                            if($get_feed_media_details->feed_media){
                               $get_feed_media_details['feed_media'] = asset('upload/post/'.$get_feed_media_details->feed_media);
                            }
                            else{
                               $get_feed_media_details['feed_media'] = asset('upload/default_image.png');
                            }
                            $feed_media[] = $get_feed_media_details;
                        }
                    }
                    $get_feed_details['feed_media'] = $feed_media;


                    /*---- Is Liked or not ----*/
                    $is_like = FeedLike::where(['feed_id' => $value->id,'user_id' => $check_user->id])->first();
                    $get_feed_details['is_like'] = 0;
                    if($is_like){
                        $get_feed_details['is_like'] = 1;    
                    }

                    /*---- Total Likes Counter ----*/
                    $get_like_count = FeedLike::where(['feed_id' => $value->id])->get()->count();
                    $get_feed_details['total_like_count'] = $get_like_count;


                    /*---- Total Comments Counter ----*/
                    $get_comment_count = FeedComment::where(['feed_id' => $value->id])->get()->count();
                    $get_feed_details['total_comment_count'] = $get_comment_count;


                    /*---- Likes User list ----*/
                    $get_like_users = FeedLike::where(['feed_id' => $value->id])->get();

                    $like_user_arr=[];
                    if(count($get_like_users) > 0){
                        foreach($get_like_users as $get_like_user){
                            $like_user_details = FeedLike::with(['user_details' => function($query){
                                $query->select('id','name','profile');
                            }])->where(['id' => $get_like_user->id])->first();
                            if($like_user_details->user_details->profile){
                               $like_user_details->user_details['profile'] = asset('upload/profile/'.$like_user_details->user_details->profile);
                            }
                            else{
                                $like_user_details->user_details['profile'] = asset('upload/default_user.png');
                            }
                            $like_user_arr[] = $like_user_details;
                        }
                    }

                    $get_feed_details['like_user_list'] = $like_user_arr;


                    /*---- Comments List with reply array and user details and is Liked ----*/ 
                    $get_comments = FeedComment::where(['feed_id' => $value->id,'is_reply' => 0])->get();

                    $comments = [];
                    if(count($get_comments) > 0){
                        foreach($get_comments as $get_comment){
                            $get_comment_details = FeedComment::with(['user_details' => function($query){
                                $query->select('id','name','profile');
                            }])->where(['id' => $get_comment->id])->first();

                            if($get_comment_details->user_details->profile){
                               $get_comment_details->user_details['profile'] = asset('upload/profile/'.$get_comment_details->user_details->profile);
                            }
                            else{
                                $get_comment_details->user_details['profile'] = asset('upload/default_user.png');
                            }

                            $get_comment_replies = FeedComment::where(['feed_id' => $value->id,'to_comment' => $get_comment->id])->get();
                            $reply_comments=[];
                            if(count($get_comment_replies) > 0){
                                foreach($get_comment_replies as $get_comment_reply){
                                    $get_comment_reply_details = FeedComment::with(['user_details' => function($query){
                                                                    $query->select('id','name','profile');
                                                                }])->where(['id' => $get_comment_reply->id])->first();

                                    if($get_comment_reply_details->user_details->profile){
                                       $get_comment_reply_details->user_details['profile'] = asset('upload/profile/'.$get_comment_reply_details->user_details->profile);
                                    }
                                    else{
                                        $get_comment_reply_details->user_details['profile'] = asset('upload/default_user.png');
                                    }


                                    /*---- Reply Comment Is Liked or not ----*/
                                    $is_reply_like = FeedCommentLike::where(['feed_id' => $value->id,'user_id' => $check_user->id,'comment_id' => $get_comment_reply->id])->first();
                                    $get_comment_reply_details['is_like'] = 0;
                                    if($is_reply_like){
                                        $get_comment_reply_details['is_like'] = 1;    
                                    }

                                    /* Reply Comment Like Count */
                                    $get_reply_commnet_like_count = FeedCommentLike::where(['feed_id' => $value->id,'comment_id' => $get_comment_reply->id])->get()->count();
                                    $get_comment_reply_details['total_like_count'] = $get_reply_commnet_like_count;


                                    /* Reply Comment Like User List */
                                    $get_reply_comment_like_users = FeedCommentLike::where(['feed_id' => $value->id,'comment_id' => $get_comment_reply->id])->get();
                                    $reply_comment_like_user_arr=[];
                                    if(count($get_reply_comment_like_users) > 0){
                                        foreach($get_reply_comment_like_users as $get_reply_comment_like_user){
                                            $reply_comment_like_user_details = FeedCommentLike::with(['user_details' => function($query){
                                                $query->select('id','name','profile');
                                            }])->where(['id' => $get_reply_comment_like_user->id])->first();
                                            if($reply_comment_like_user_details->user_details->profile){
                                               $reply_comment_like_user_details->user_details['profile'] = asset('upload/profile/'.$reply_comment_like_user_details->user_details->profile);
                                            }
                                            else{
                                                $reply_comment_like_user_details->user_details['profile'] = asset('upload/default_user.png');
                                            }
                                            $reply_comment_like_user_arr[] = $reply_comment_like_user_details;
                                        }
                                    }

                                    $get_comment_reply_details['like_user_list'] = $reply_comment_like_user_arr;

                                    $reply_comments[] = $get_comment_reply_details;
                                }
                            }

                            /*---- Comment Is Liked or not ----*/
                            $is_comment_like = FeedCommentLike::where(['feed_id' => $value->id,'user_id' => $check_user->id,'comment_id' => $get_comment->id])->first();
                            $get_comment_details['is_like'] = 0;
                            if($is_comment_like){
                                $get_comment_details['is_like'] = 1;    
                            }
                            
                            /* Comment Like Count */
                            $get_commnet_like_count = FeedCommentLike::where(['feed_id' => $value->id,'comment_id' => $get_comment->id])->get()->count();
                            $get_comment_details['total_like_count'] = $get_commnet_like_count;


                            /* Comment Like User List */
                            $get_comment_like_users = FeedCommentLike::where(['feed_id' => $value->id,'comment_id' => $get_comment->id])->get();
                            $comment_like_user_arr=[];
                            if(count($get_comment_like_users) > 0){
                                foreach($get_comment_like_users as $get_comment_like_user){
                                    $comment_like_user_details = FeedCommentLike::with(['user_details' => function($query){
                                        $query->select('id','name','profile');
                                    }])->where(['id' => $get_comment_like_user->id])->first();
                                    if($comment_like_user_details->user_details->profile){
                                       $comment_like_user_details->user_details['profile'] = asset('upload/profile/'.$comment_like_user_details->user_details->profile);
                                    }
                                    else{
                                        $comment_like_user_details->user_details['profile'] = asset('upload/default_user.png');
                                    }
                                    $comment_like_user_arr[] = $comment_like_user_details;
                                }
                            }

                            $get_comment_details['like_user_list'] = $comment_like_user_arr;

                            $get_comment_details['reply'] = $reply_comments;

                            $comments[] = $get_comment_details;
                        }
                    }
                    
                    $get_feed_details['comment_list'] = $comments;


                    $feeds[]  = $get_feed_details;   
                }   
            }

            return response()->json(['status' => true,'msg' => "My feed list fetch successfully.",'data' => $feeds]); 
        }
        else{
             return response()->json(['status' => false,'msg' => "User not found."]); 
        }

    }


    public function create_business(Request $request){

        $rules = array(
            'user_id'=>'required',
            'title'=>'required' 
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.',
            'title.required'=>'Please enter title.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){    

            $add = new Business;
            $add->user_id = $request->user_id;
            $add->title = $request->title;
            $add->description = $request->description;
            $add->status = 1;
            $add->save();

            return response()->json(['status' => true,'msg' => "Business added successfully."]);
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }

    }



    public function edit_business(Request $request){

        $rules = array(
            'user_id'=>'required',
            'title'=>'required',
            'business_id'=> 'required'
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.',
            'title.required'=>'Please enter title.',
            'business_id.required'=> 'Please enter business id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){    

            $check_business = Business::where(['id' => $request->business_id,'is_delete' => 0])->first();
            
            if($check_business){

                $update = Business::find($check_business->id);
                $update->user_id = $request->user_id;
                $update->title = $request->title;
                $update->description = $request->description;
                $update->save();

                return response()->json(['status' => true,'msg' => "Business updated successfully."]);
            }
            else{
                return response()->json(['status' => false,'msg' => "Business not found."]);
            }
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }
    }



    public function delete_business(Request $request){

        $rules = array(
            'business_id'=> 'required'
        );
        $messages =array(
            'business_id.required'=> 'Please enter business id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_business = Business::where(['id' => $request->business_id,'is_delete' => 0])->first();
        
        if($check_business){

            $update = Business::find($check_business->id);
            $update->is_delete = 1;
            $update->save();

            return response()->json(['status' => true,'msg' => "Business deleted successfully."]);
        }
        else{
            return response()->json(['status' => false,'msg' => "Business not found."]);
        }

    }



    public function my_business_list(Request $request)
    {
        $rules = array(
            'user_id'=> 'required'
        );
        $messages =array(
            'user_id.required'=> 'Please enter user_id id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){   

            $get_business = Business::where(['user_id' => $request->user_id,'is_delete' => 0,'status' => 1])->get();

            if(count($get_business) > 0){
                return response()->json(['status' => true,'msg' => "Business list fetched successfully.",'data' => $get_business]);
            }
            else{
                return response()->json(['status' => false,'msg' => "Business not found."]);
            }
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }
    }



    public function create_opportunity(Request $request)
    {

        $rules = array(
            'user_id'=>'required',
            'title'=>'required' 
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.',
            'title.required'=>'Please enter title.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){    

            $add = new Opportunity;
            $add->user_id = $request->user_id;
            $add->title = $request->title;
            $add->description = $request->description;
            $add->status = 1;
            $add->save();

            return response()->json(['status' => true,'msg' => "Opportunity added successfully."]);
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }

    }


    public function edit_opportunity(Request $request){

        $rules = array(
            'user_id'=>'required',
            'title'=>'required',
            'opportunity_id'=> 'required'
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.',
            'title.required'=>'Please enter title.',
            'opportunity_id.required'=> 'Please enter opportunity id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){    

            $check_opportunity = Opportunity::where(['id' => $request->opportunity_id,'is_delete' => 0])->first();
            
            if($check_opportunity){

                $update = Opportunity::find($check_opportunity->id);
                $update->user_id = $request->user_id;
                $update->title = $request->title;
                $update->description = $request->description;
                $update->save();

                return response()->json(['status' => true,'msg' => "Opportunity updated successfully."]);
            }
            else{
                return response()->json(['status' => false,'msg' => "Opportunity not found."]);
            }
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }

    }


    public function delete_opportunity(Request $request)
    {

        $rules = array(
            'opportunity_id'=> 'required'
        );
        $messages =array(
            'opportunity_id.required'=> 'Please enter opportunity id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_opportunity = Opportunity::where(['id' => $request->opportunity_id,'is_delete' => 0])->first();
        
        if($check_opportunity){

            $update = Opportunity::find($check_opportunity->id);
            $update->is_delete = 1;
            $update->save();

            return response()->json(['status' => true,'msg' => "Opportunity deleted successfully."]);
        }
        else{
            return response()->json(['status' => false,'msg' => "Opportunity not found."]);
        }

    }   
    

    public function my_opportunity_list(Request $request){  


        $rules = array(
            'user_id'=> 'required'
        );
        $messages =array(
            'user_id.required'=> 'Please enter user_id id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){   

            $get_opportunity = Opportunity::where(['user_id' => $request->user_id,'is_delete' => 0,'status' => 1])->get();

            if(count($get_opportunity) > 0){
                return response()->json(['status' => true,'msg' => "Opportunity list fetched successfully.",'data' => $get_opportunity]);
            }
            else{
                return response()->json(['status' => false,'msg' => "Opportunity not found."]);
            }
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }

    }


    public function blog_list(Request $request){

        $get_blogs = Blog::all();

        $blogs=[];
        if(count($get_blogs) > 0)
        {
            foreach($get_blogs as $get_blog)
            {

                $get_blog_details = Blog::where(['id' => $get_blog->id])->first();

                /* Blog Media */
                $get_blog_media = BlogMedia::where(['blog_id' => $get_blog->id])->get();

                $blog_media = [];
                if(count($get_blog_media) > 0){
                    foreach($get_blog_media as $media){

                        $get_media_details = BlogMedia::where(['id' => $media->id])->first();

                        if($get_media_details->blog_media){
                           $get_media_details['blog_media'] = asset('upload/blog/'.$get_media_details->blog_media);
                        }
                        else{
                            $get_media_details['blog_media'] = asset('upload/default_image.png');
                        }

                        $blog_media[] = $get_media_details;
                    }
                }

                $get_blog_details['blog_media'] = $blog_media;

                $blogs[] = $get_blog_details;

            }
            return response()->json(['status' => true,'msg' => "Blog list fetched successfully.",'data' => $blogs]);
        }
        else{
            return response()->json(['status' => false,'msg' => "blog not found."]);
        }

    }


    public function home(Request $request){

        $rules = array(
            'user_id'=>'required'
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){

            /*======== communities Detail =========*/
            //$communities = Community::whereRaw('FIND_IN_SET('.$request->user_id.',members)')->where(['is_delete' => 0,'status' => 1])->orWhere('user_id','=',$request->user_id)->get(); //Community::where(['is_delete' => 0,'status' => 1])->get();
            $communities = Community::whereRaw('FIND_IN_SET('.$request->user_id.',members)')->orWhere('user_id','=',$request->user_id);
            $communities = $communities->where(['is_delete' => 0,'status' => 1])->get();

            $community_array=[];
            if(count($communities) > 0){
                foreach($communities as $community){
                    $community_details = Community::with(['user_details' => function($query){
                                $query->select('id','name','profile');
                            }])->where(['id' => $community->id])->first();
                    
                    if($community_details->user_details->profile){
                       $community_details->user_details['profile'] = asset('upload/profile/'.$community_details->user_details->profile);
                    }
                    else{
                        $community_details->user_details['profile'] = asset('upload/default_user.png');
                    }
                    $community_array[] = $community_details;
                }
            }
            $community_details = $community_array;

            /*======== business templates Detail =========*/
            $business_template_details = BusinessTemplate::where(['is_delete' => 0,'status' => 1])->get();
            count($business_template_details);

            /*======== Opportunities Detail =========*/
            $opportunities = Opportunity::where(['is_delete' => 0,'status' => 1])->get();

            $opportunity_array=[];
            if(count($opportunities) > 0){
                foreach($opportunities as $opportunity){
                    $opportunity_details = Opportunity::with(['user_details' => function($query){
                                $query->select('id','name','profile');
                            }])->where(['id' => $opportunity->id])->first();
                    
                    if( $opportunity_details->user_details->profile){
                        $opportunity_details->user_details['profile'] = asset('upload/profile/'.$opportunity_details->user_details->profile);
                    }
                    else{
                        $opportunity_details->user_details['profile'] = asset('upload/default_user.png');
                    }
                    $opportunity_array[] = $opportunity_details;
                }
            }
            $opportunity_details = $opportunity_array;

            /*======== business Detail =========*/
            //$business_details = Business::with('user_details')->where(['is_delete' => 0,'status' => 1])->get();


            /*======== testimonials Detail =========*/

            $get_testimonials = Testimonial::all();

            $testimonial_details = [];
            if(count($get_testimonials) > 0){
                foreach($get_testimonials as $get_testimonial){
                    $get_testimonial['document'] = asset('upload/testimonial/'.$get_testimonial->document);
                    $testimonial_details[] = $get_testimonial;
                }
            }


            /*======== blogs Detail =========*/
            $get_blogs = Blog::all();
            $blog_details=[];
            if(count($get_blogs) > 0){
                foreach($get_blogs as $get_blog){
                    $get_blog_details = Blog::where(['id' => $get_blog->id])->first();
                    /* Blog Media */
                    $get_blog_media = BlogMedia::where(['blog_id' => $get_blog->id])->get();

                    $blog_media = [];
                    if(count($get_blog_media) > 0){
                        foreach($get_blog_media as $media){
                            $get_media_details = BlogMedia::where(['id' => $media->id])->first();
                            if($get_media_details->blog_media){
                               $get_media_details['blog_media'] = asset('upload/blog/'.$get_media_details->blog_media);
                            }
                            else{
                                $get_media_details['blog_media'] = asset('upload/default_image.png');
                            }
                            $blog_media[] = $get_media_details;
                        }
                    }
                    $get_blog_details['blog_media'] = $blog_media;
                    $blog_details[] = $get_blog_details;
                }
            }


            /*======== Feed Detail =========*/

            $get_feeds = Feed::with('feed_media')->where(['is_delete' => 0])->orderBy('id','DESC')->get();
            $feeds=[];
            if(count($get_feeds) > 0)
            {
                foreach ($get_feeds as $key => $value)
                {

                    $get_feed_details = Feed::with(['user_details' => function($query){
                                $query->select('id','name','profile');
                            }])->where(['id' => $value->id])->first();

                    if( $get_feed_details->user_details->profile){
                        $get_feed_details->user_details['profile'] = asset('upload/profile/'.$get_feed_details->user_details->profile);
                    }
                    else{
                        $get_feed_details->user_details['profile'] = asset('upload/default_user.png');
                    }

                    $get_feed_details['time_different'] = $get_feed_details->created_at->diffForHumans();

                    /*---- FeedMedia ----*/
                    $get_feed_media = FeedMedia::where(['feed_id' => $value->id])->get();
                    $feed_media=[];
                    if(count($get_feed_media) > 0){
                        foreach($get_feed_media as $key_media => $value_media){
                            $get_feed_media_details = FeedMedia::where(['id' => $value_media->id])->first();
                            if($get_feed_media_details->feed_media){
                               $get_feed_media_details['feed_media'] = asset('upload/post/'.$get_feed_media_details->feed_media);
                            }
                            else{
                               $get_feed_media_details['feed_media'] = asset('upload/default_image.png');
                            }
                            $feed_media[] = $get_feed_media_details;
                        }
                    }
                    $get_feed_details['feed_media'] = $feed_media;

                    /*---- Is Liked or not ----*/
                    $is_like = FeedLike::where(['feed_id' => $value->id,'user_id' => $check_user->id])->first();
                    $get_feed_details['is_like'] = 0;
                    if($is_like){
                        $get_feed_details['is_like'] = 1;    
                    }

                    /*---- Total Likes Counter ----*/
                    $get_like_count = FeedLike::where(['feed_id' => $value->id])->get()->count();
                    $get_feed_details['total_like_count'] = $get_like_count;


                    /*---- Total Comments Counter ----*/
                    $get_comment_count = FeedComment::where(['feed_id' => $value->id])->get()->count();
                    $get_feed_details['total_comment_count'] = $get_comment_count;

                    /*---- Likes User list ----*/
                    $get_like_users = FeedLike::where(['feed_id' => $value->id])->get();

                    $like_user_arr=[];
                    if(count($get_like_users) > 0){
                        foreach($get_like_users as $get_like_user){
                            $like_user_details = FeedLike::with(['user_details' => function($query){
                                $query->select('id','name','profile');
                            }])->where(['id' => $get_like_user->id])->first();
                            if($like_user_details->user_details->profile){
                               $like_user_details->user_details['profile'] = asset('upload/profile/'.$like_user_details->user_details->profile);
                            }
                            else{
                                $like_user_details->user_details['profile'] = asset('upload/default_user.png');
                            }
                            $like_user_arr[] = $like_user_details;
                        }
                    }

                    $get_feed_details['like_user_list'] = $like_user_arr;

                    /*---- Comments List with reply array and user details and is Liked ----*/ 
                    $get_comments = FeedComment::where(['feed_id' => $value->id,'is_reply' => 0])->get();

                    $comments = [];
                    if(count($get_comments) > 0){
                        foreach($get_comments as $get_comment){
                            $get_comment_details = FeedComment::with(['user_details' => function($query){
                                $query->select('id','name','profile');
                            }])->where(['id' => $get_comment->id])->first();

                            if($get_comment_details->user_details->profile){
                               $get_comment_details->user_details['profile'] = asset('upload/profile/'.$get_comment_details->user_details->profile);
                            }
                            else{
                                $get_comment_details->user_details['profile'] = asset('upload/default_user.png');
                            }

                            $get_comment_replies = FeedComment::where(['feed_id' => $value->id,'to_comment' => $get_comment->id])->get();
                            $reply_comments=[];
                            if(count($get_comment_replies) > 0){
                                foreach($get_comment_replies as $get_comment_reply){
                                    $get_comment_reply_details = FeedComment::with(['user_details' => function($query){
                                                                    $query->select('id','name','profile');
                                                                }])->where(['id' => $get_comment_reply->id])->first();

                                    if($get_comment_reply_details->user_details->profile){
                                       $get_comment_reply_details->user_details['profile'] = asset('upload/profile/'.$get_comment_reply_details->user_details->profile);
                                    }
                                    else{
                                        $get_comment_reply_details->user_details['profile'] = asset('upload/default_user.png');
                                    }


                                    /*---- Reply Comment Is Liked or not ----*/
                                    $is_reply_like = FeedCommentLike::where(['feed_id' => $value->id,'user_id' => $check_user->id,'comment_id' => $get_comment_reply->id])->first();
                                    $get_comment_reply_details['is_like'] = 0;
                                    if($is_reply_like){
                                        $get_comment_reply_details['is_like'] = 1;    
                                    }

                                    /* Reply Comment Like Count */
                                    $get_reply_commnet_like_count = FeedCommentLike::where(['feed_id' => $value->id,'comment_id' => $get_comment_reply->id])->get()->count();
                                    $get_comment_reply_details['total_like_count'] = $get_reply_commnet_like_count;


                                    /* Reply Comment Like User List */
                                    $get_reply_comment_like_users = FeedCommentLike::where(['feed_id' => $value->id,'comment_id' => $get_comment_reply->id])->get();
                                    $reply_comment_like_user_arr=[];
                                    if(count($get_reply_comment_like_users) > 0){
                                        foreach($get_reply_comment_like_users as $get_reply_comment_like_user){
                                            $reply_comment_like_user_details = FeedCommentLike::with(['user_details' => function($query){
                                                $query->select('id','name','profile');
                                            }])->where(['id' => $get_reply_comment_like_user->id])->first();
                                            if($reply_comment_like_user_details->user_details->profile){
                                               $reply_comment_like_user_details->user_details['profile'] = asset('upload/profile/'.$reply_comment_like_user_details->user_details->profile);
                                            }
                                            else{
                                                $reply_comment_like_user_details->user_details['profile'] = asset('upload/default_user.png');
                                            }
                                            $reply_comment_like_user_arr[] = $reply_comment_like_user_details;
                                        }
                                    }

                                    $get_comment_reply_details['like_user_list'] = $reply_comment_like_user_arr;

                                    $reply_comments[] = $get_comment_reply_details;
                                }
                            }

                            /*---- Comment Is Liked or not ----*/
                            $is_comment_like = FeedCommentLike::where(['feed_id' => $value->id,'user_id' => $check_user->id,'comment_id' => $get_comment->id])->first();
                            $get_comment_details['is_like'] = 0;
                            if($is_comment_like){
                                $get_comment_details['is_like'] = 1;    
                            }
                            
                            /* Comment Like Count */
                            $get_commnet_like_count = FeedCommentLike::where(['feed_id' => $value->id,'comment_id' => $get_comment->id])->get()->count();
                            $get_comment_details['total_like_count'] = $get_commnet_like_count;


                            /* Comment Like User List */
                            $get_comment_like_users = FeedCommentLike::where(['feed_id' => $value->id,'comment_id' => $get_comment->id])->get();
                            $comment_like_user_arr=[];
                            if(count($get_comment_like_users) > 0){
                                foreach($get_comment_like_users as $get_comment_like_user){
                                    $comment_like_user_details = FeedCommentLike::with(['user_details' => function($query){
                                        $query->select('id','name','profile');
                                    }])->where(['id' => $get_comment_like_user->id])->first();
                                    if($comment_like_user_details->user_details->profile){
                                       $comment_like_user_details->user_details['profile'] = asset('upload/profile/'.$comment_like_user_details->user_details->profile);
                                    }
                                    else{
                                        $comment_like_user_details->user_details['profile'] = asset('upload/default_user.png');
                                    }
                                    $comment_like_user_arr[] = $comment_like_user_details;
                                }
                            }

                            $get_comment_details['like_user_list'] = $comment_like_user_arr;

                            $get_comment_details['reply'] = $reply_comments;

                            $comments[] = $get_comment_details;
                        }
                    }

                    $get_feed_details['comment_list'] = $comments;

                    $feeds[]  = $get_feed_details;
                }
            }

            /*========================================*/            

            $home = ['community_details' => $community_details,'opportunity_details' => $opportunity_details,
            'testimonial_details' => $testimonial_details,'blog_details' => $blog_details,'business_template_details' => $business_template_details,'feed_details' => $feeds];

            return response()->json(['status' => true,'msg' => "Home page details fetch successfully.",'data' => $home]);
        }
        else{
             return response()->json(['status' => false,'msg' => "User not found."]); 
        }
    }



    public function community_user_list(Request $request){

        $rules = array(
            'user_id'=> 'required'
        );
        $messages =array(
            'user_id.required'=> 'Please enter user id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){   
            $users = User::select('id','name')->where(['save_status' => 2,'is_admin' => 0])->where('id','!=',$check_user->id)->get();
            return response()->json(['status' => true,'msg' => "User list fetched successfully.",'data' => $users]);
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }
    }


    public function create_community(Request $request){

        $rules = array(
            'user_id'=> 'required',
            'title' => 'required',
            'description' => 'required',
            'members' => 'required'
        );
        $messages = array(
            'user_id.required'=> 'Please enter user id.',
            'title.required' => 'Please enter title.',
            'description.required' => 'Please enter description.',
            'members.required' => 'Please select members.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){   

            $add = new Community;
            $add->user_id = $request->user_id;
            $add->title = $request->title;
            $add->description = $request->description;
            $add->members = $request->members;
            $add->status = 1;
            $add->save();

            return response()->json(['status' => true,'msg' => "Community created successfully."]);
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }

    }


    public function edit_community(Request $request){

        $rules = array(
            'user_id'=> 'required',
            'title' => 'required',
            'description' => 'required',
            'members' => 'required',
            'community_id' => 'required'
        );
        $messages = array(
            'user_id.required'=> 'Please enter user_id id.',
            'title.required' => 'Please enter title.',
            'description.required' => 'Please enter description.',
            'members.required' => 'Please select members.',
            'community_id.required' => 'Please enter community id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){   

            $check_community = Community::where(['id' => $request->community_id])->first();

            if($check_community){

                $edit = Community::find($check_community->id);
                $edit->user_id = $request->user_id;
                $edit->title = $request->title;
                $edit->description = $request->description;
                $edit->members = $request->members;
                $edit->save();

                return response()->json(['status' => true,'msg' => "Community updated successfully."]);
            }
            else{
                return response()->json(['status' => false,'msg' => "Community not found."]);
            }
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }
    }


    public function delete_community(Request $request){

        $rules = array(
            'community_id' => 'required'
        );
        $messages = array(
            'community_id.required' => 'Please enter community id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_community = Community::where(['id' => $request->community_id])->first();

        if($check_community){

            $update = Community::find($check_community->id);
            $update->is_delete = 1;
            $update->save();

            return response()->json(['status' => true,'msg' => "Community deleted successfully."]);
        }
        else{
            return response()->json(['status' => false,'msg' => "Community not found."]);
        }

    }


    public function my_community_list(Request $request){

        $rules = array(
            'user_id'=> 'required'
        );
        $messages =array(
            'user_id.required'=> 'Please enter user id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){   
            $community = Community::where(['is_delete' => 0,'status' => 1,'user_id' => $check_user->id])->get();
            return response()->json(['status' => true,'msg' => "Community list fetched successfully.",'data' => $community]);
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }

    }


    public function community_detail(Request $request){

        $rules = array(
            'community_id'=> 'required'
        );
        $messages =array(
            'community_id.required'=> 'Please enter user id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_community = Community::where(['id' => $request->community_id])->first();

        if($check_community){

            $community = Community::where(['id' => $check_community->id])->first();

            $get_community_user_details = User::select('id','name','profile')->where('id',$community->user_id)->first();
            if($get_community_user_details->profile){
                $get_community_user_details['profile'] = asset('upload/profile/'.$get_community_user_details->profile);   
            }
            else{
                $get_community_user_details['profile'] = asset('frontend/images/user.jpg');
            }
            $community['user_data'] = $get_community_user_details;

            $member_array=[]; if($community->members){ $member_array = explode(',',$community->members); }

            $user_arr = [];
            if(count($member_array) > 0)
            {
                foreach($member_array as $key => $data){
                    $user_details = User::select('id','name','profile')->where('id',$data)->first();
                    if($user_details->profile){
                        $user_details['profile'] = asset('upload/profile/'.$user_details->profile);   
                    }
                    else{
                        $user_details['profile'] = asset('frontend/images/user.jpg');
                    }
                    $user_arr[] = $user_details;
                }
            }

            $community['user_details'] = $user_arr;

            /*--------------- Get Question List --------------*/
            $get_community_questions =  CommunityQuestion::with('user_details')->where(['community_id' => $check_community->id,'question_id' => 0])->get();

            $question_details_arr=[];
            if(count($get_community_questions) > 0){
                foreach($get_community_questions as $get_community_question){
                    $question_details =  CommunityQuestion::with(['user_details' => function($query){
                        $query->select('id','name','profile');
                    }])->where(['id' => $get_community_question->id])->first();

                    if($question_details->user_details->profile){
                        $question_details->user_details['profile'] = asset('upload/profile/'.$question_details->user_details->profile);   
                    }
                    else{
                        $question_details->user_details['profile'] = asset('frontend/images/user.jpg');
                    }

                    $question_details_arr[] = $question_details;
                }
            }

            $community['total_question_count'] = count($question_details_arr);
            $community['question_details'] = $question_details_arr;


            return response()->json(['status' => true,'msg' => "Community details fetch successfully.",'data' => $community]);
        }
        else{
            return response()->json(['status' => false,'msg' => "Community not found."]);
        }
    }


    public function create_community_question(Request $request){

        $rules = array(
            'user_id' => 'required',
            'community_id'=> 'required',
            'question_text'=> 'required'
        );
        $messages =array(
            'user_id.required'=> 'Please enter user id.',
            'community_id.required'=> 'Please enter community id.',
            'question_text.required'=> 'Please enter question text.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){

            $check_community = Community::where(['id' => $request->community_id])->first();

            if($check_community){

                $add_question = new CommunityQuestion;
                $add_question->user_id = $request->user_id;
                $add_question->community_id  = $request->community_id;   
                $add_question->question_text = $request->question_text;
                $add_question->question_id = 0;
                $add_question->save(); 

                return response()->json(['status' => true,'msg' => "Community question added successfully."]);

            }
            else{
                return response()->json(['status' => false,'msg' => "Community not found."]);
            }
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }
    }


    public function community_question_detail(Request $request){

        $rules = array(
            'community_question_id'=> 'required'
        );
        $messages =array(
            'community_question_id.required'=> 'Please enter community question id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_community_question = CommunityQuestion::where(['id' => $request->community_question_id])->first();

        $get_community_question_user_details = User::select('id','name','profile')->where('id',$check_community_question->user_id)->first();
        if($get_community_question_user_details->profile){
            $get_community_question_user_details['profile'] = asset('upload/profile/'.$get_community_question_user_details->profile);   
        }
        else{
            $get_community_question_user_details['profile'] = asset('frontend/images/user.jpg');
        }
        $check_community_question['user_data'] = $get_community_question_user_details;


        if($check_community_question){

            $get_community_answers = CommunityQuestion::where('question_id',$check_community_question->id)->get();

            $answer_details_arr=[];
            if(count($get_community_answers) > 0){
                foreach($get_community_answers as $get_community_answer){

                    $answer_details =  CommunityQuestion::with(['user_details' => function($query){
                        $query->select('id','name','profile');
                    }])->where(['id' => $get_community_answer->id])->first();

                    if($answer_details->user_details->profile){
                        $answer_details->user_details['profile'] = asset('upload/profile/'.$answer_details->user_details->profile);   
                    }
                    else{
                        $answer_details->user_details['profile'] = asset('frontend/images/user.jpg');
                    }
                    $answer_details_arr[] = $answer_details;
                }
            }

            $check_community_question['answer_details'] = $answer_details_arr;

            return response()->json(['status' => true,'msg' => "Community question details fetch successfully.",'data' => $check_community_question]);
        }
        else{
            return response()->json(['status' => false,'msg' => "Community question not found."]);
        }
    }
    


    public function create_community_answer(Request $request){


        $rules = array(
            'user_id' => 'required',
            'community_id'=> 'required',
            'question_text'=> 'required',
            'question_id'=>'required',
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.',
            'community_id.required' => 'Please enter community id.',
            'question_text.required' => 'Please enter question text.',
            'question_id.required' =>'Please enter question id.',
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){

            $check_community = Community::where(['id' => $request->community_id])->first();

            if($check_community){

                $check_question = CommunityQuestion::where(['id' => $request->question_id])->first();

                if($check_question){

                    $add_answer = new CommunityQuestion;
                    $add_answer->user_id = $request->user_id;
                    $add_answer->community_id  = $request->community_id;   
                    $add_answer->question_text = $request->question_text;
                    $add_answer->question_id = $request->question_id;
                    $add_answer->save(); 

                    return response()->json(['status' => true,'msg' => "Answer added successfully."]);

                }
                else{
                    return response()->json(['status' => false,'msg' => "Community question not found."]);
                }

            }
            else{
                return response()->json(['status' => false,'msg' => "Community not found."]);
            }
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }
    }


    public function delete_community_member(Request $request){

        $rules = array(
            'user_id' => 'required',
            'community_id'=> 'required'
        );
        $messages =array(
            'user_id.required'=> 'Please enter user id.',
            'community_id'=> 'Please enter community id.',
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_community = Community::where(['id' => $request->community_id])->first();

        if($check_community){

            $community_details = Community::where(['id' => $check_community->id])->first();
            $member_array=[];
            if($community_details->members){
                $member_array = explode(',',$community_details->members); 
            }
            $final_members = [];$remove_member = [];
            if(count($member_array) > 0){
                foreach ($member_array as $key => $value) {
                    if($request->user_id != $value){
                        $final_members[] = $value;
                    }
                    else{
                        $remove_member[] = $value;
                    }
                }
            }

            if(count($remove_member) == 1){
                Community::where(['id' => $check_community->id])->update(['members' => implode(',',$final_members)]);    
                return response()->json(['status' => true,'msg' => "Community member remove successfully."]);
            }
            else{
                return response()->json(['status' => false,'msg' => "Community member not found."]);
            }
        }
        else{
            return response()->json(['status' => false,'msg' => "Community not found."]);
        }

    }


    public function delete_community_answer(Request $request){

        $rules = array(
            'answer_id' => 'required'
        );
        $messages =array(
            'answer_id.required'=> 'Please enter answer id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_answer = CommunityQuestion::where(['id' => $request->answer_id])->first();

        if($check_answer){
            CommunityQuestion::where(['id' => $check_answer->id])->delete();
            return response()->json(['status' => true,'msg' => "Answer deleted successfully."]);
        }
        else{
            return response()->json(['status' => false,'msg' => "Community answer not found."]);
        }

    }


    public function all_opportunity_list(Request $request){

        /*======== Opportunities Detail =========*/
        $opportunities = Opportunity::where(['is_delete' => 0,'status' => 1])->get();

        $opportunity_array=[];
        if(count($opportunities) > 0){
            foreach($opportunities as $opportunity){
                $opportunity_details = Opportunity::with(['user_details' => function($query){
                            $query->select('id','name','profile');
                        }])->where(['id' => $opportunity->id])->first();
                
                if( $opportunity_details->user_details->profile){
                    $opportunity_details->user_details['profile'] = asset('upload/profile/'.$opportunity_details->user_details->profile);
                }
                else{
                    $opportunity_details->user_details['profile'] = asset('upload/default_user.png');
                }
                $opportunity_array[] = $opportunity_details;
            }
        }
        return response()->json(['status' => true,'msg' => "Opportunity list fetch successfully.",'data' => $opportunity_array]);
    }


    public function all_business_list(Request $request){

        /*======== business Detail =========*/
        $businesses = Business::where(['is_delete' => 0,'status' => 1])->get();

        $business_array=[];
        if(count($businesses) > 0){
            foreach($businesses as $business){
                $business_details = Business::with(['user_details' => function($query){
                            $query->select('id','name','profile');
                        }])->where(['id' => $business->id])->first();        

                if( $business_details->user_details->profile){
                    $business_details->user_details['profile'] = asset('upload/profile/'.$business_details->user_details->profile);
                }
                else{
                    $business_details->user_details['profile'] = asset('upload/default_user.png');
                }
                $business_array[] = $business_details;
            }
        }

        return response()->json(['status' => true,'msg' => "Opportunity list fetch successfully.",'data' => $business_array]);
    }


    public function all_business_template_list(Request $request){

        $get_business_templates = BusinessTemplate::where(['is_delete' => 0,'status' => 1])->get();

        if(count($get_business_templates) > 0){
            return response()->json(['status' => true,'msg' => "Business template list fetch successfully.",'data' => $get_business_templates]);
        }
        else{
            return response()->json(['status' => true,'msg' => "Business template not found."]);
        }

    }


    public function all_community_list(Request $request){

        $rules = array(
            'user_id' => 'required'
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){

            $communities = Community::whereRaw('FIND_IN_SET('.$request->user_id.',members)')->orWhere('user_id','=',$request->user_id);
            $communities = $communities->where(['is_delete' => 0,'status' => 1])->get();
            $community_array=[];
            if(count($communities) > 0){
                foreach($communities as $community){
                    $community_details = Community::with(['user_details' => function($query){
                                $query->select('id','name','profile');
                            }])->where(['id' => $community->id])->first();
                    if($community_details->user_details->profile){
                       $community_details->user_details['profile'] = asset('upload/profile/'.$community_details->user_details->profile);
                    }
                    else{
                        $community_details->user_details['profile'] = asset('upload/default_user.png');
                    }
                    $community_array[] = $community_details;
                }
            }
            $community_details = $community_array;
            return response()->json(['status' => true,'msg' => "Community list fetch successfully.",'data' => $community_details]);

        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }

    }


    public function save_chat_email_setting(Request $request){

        $rules = array(
            'user_id' => 'required'
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){

            $updateSetting = User::where('id',$check_user->id)->first();
            if($request->send_chat_message_over_email){
                $updateSetting->send_chat_message_over_email = 1;
            }
            else{
                $updateSetting->send_chat_message_over_email = 0;
            }

            $updateSetting->save();

            $user_data = User::where(['id' => $request->user_id])->first();
            return response()->json(['status' => true,'msg' => "Chat email setting updated successfully.",'data' => $user_data]);
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }

    }


    public function contact_us(Request $request){

        $rules = array(
            'user_id' => 'required'
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){
            $add = new ContactUs;
            $add->user_id = $request->user_id;
            $add->name = isset($request->name) ? $request->name:null;
            $add->email = isset($request->email) ? $request->email:null;
            $add->phone = isset($request->phone) ? $request->phone:null;
            $add->message = isset($request->message) ? $request->message:null;
            $add->save();
            return response()->json(['status' => true,'msg' => "Contact details submited successfully."]);
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }

    }



    public function add_support_question(Request $request){

        $rules = array(
            'user_id' => 'required',
            'question' => 'required'
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.',
            'question.required' => 'Please enter question.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }
        
        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){
            $add = new Support;
            $add->user_id = $request->user_id;
            $add->question = $request->question;
            $add->answer = null;
            $add->save();

            return response()->json(['status' => true,'msg' => "Support question added successfully."]);    
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);    
        }

    }



    public function support_question_list(Request $request){

        $rules = array(
            'user_id' => 'required'
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user){
            
            $support_questions = Support::where(['user_id' =>  $check_user->id])->get();

            $support_question_arr = [];
            if(count($support_questions) > 0){
                foreach($support_questions as $support_question){

                    $support_details = Support::with(['user_details' => function($query){
                                $query->select('id','name','profile');
                            }])->where(['id' => $support_question->id])->first();
                    
                    if( $support_details->user_details->profile){
                        $support_details->user_details['profile'] = asset('upload/profile/'.$support_details->user_details->profile);
                    }
                    else{
                        $support_details->user_details['profile'] = asset('upload/default_user.png');
                    }
                    $support_question_arr[] = $support_details;
                }
            }

            return response()->json(['status' => true,'msg' => "Support questions list fetch successfully.",'data' => $support_question_arr]);
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }

    }



    /*------- Chat -------*/

    public function send_message(Request $request){

        $rules = array(
            'from_id' => 'required',
            'to_id' => 'required',
            // 'message' => 'required'

        );
        $messages =array(
            'user_id.required' => 'Please enter user id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }


        // default variables
        $error_msg = $attachment = $attachment_title = null;

        // if there is attachment [file]
        if ($request->hasFile('file')) {

            // allowed extensions
            $allowed_images = Chatify::getAllowedImages();
            $allowed_files  = Chatify::getAllowedFiles();
            $allowed        = array_merge($allowed_images, $allowed_files);

            $file = $request->file('file');
            // if size less than 150MB
            if ($file->getSize() < 150000000) {
                if (in_array($file->getClientOriginalExtension(), $allowed)) {
                    // get attachment name
                    $attachment_title = $file->getClientOriginalName();
                    // upload attachment and store the new name
                    $attachment = Str::uuid() . "." . $file->getClientOriginalExtension();
                    $file->storeAs("public/" . config('chatify.attachments.folder'), $attachment);
                } else {
                    $error_msg = "File extension not allowed!";
                }
            } else {
                $error_msg = "File size is too long!";
            }
        }

        if (!$error_msg) {
            // send to database
            $messageID = mt_rand(9, 999999999) + time();
            Chatify::newMessage([
                'id' => $messageID,
                'type' => "user",
                'from_id' => $request->from_id,
                'to_id' => $request->to_id,
                'body' => trim(htmlentities($request->message)),
                'attachment' => ($attachment) ? $attachment . ',' . $attachment_title : null,
            ]);
        }

        // send the response
        // 'error' => $error_msg ? 1 : 0
        return Response::json(['status' => true,'error_msg' => $error_msg,'message' => "Message send successfully."]);

    }


    public function chat_thread_list(Request $request){
        

        $rules = array(
            'user_id' => 'required'
        );
        $messages = array(
            'user_id.required' => 'Please enter user id'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {

            $msg = $validator->messages();
            return ['status' => "false",'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user)
        {

            $userid =   $request->user_id;

            $get_message_thred = Message::where(['from_id' => $userid])->orWhere(['to_id' => $userid])->get();

            $threds = [];$friends=[];
            if(count($get_message_thred) > 0){

                foreach ($get_message_thred as $key => $value){

                    if($userid == $value->from_id){
                        if(!in_array($value->to_id,$friends)){
                            $friends[] = $value->to_id;

                            $friend_details = User::select('id','name','profile')->where(['id' => $value->to_id])->first();
                            if($friend_details->profile){
                               $friend_details['profile'] = asset('upload/profile/'.$friend_details->profile);
                            }
                            else{
                                $friend_details['profile'] = asset('upload/default_user.png');
                            }

                            $friendid = $value->to_id;
                            $unread_count = Message::where(function($query) use($userid,$friendid){
                                            $query->where('from_id','=',$friendid);    
                                            $query->where('to_id','=',$userid);
                                              })->where(['seen' => 0])->get()->count();

                            $last_message = Message::where(function($query) use($userid,$friendid){
                                                $query->where('from_id','=',$userid);
                                                $query->where('to_id','=',$friendid);
                                                  })->orWhere(function($query2) use($userid,$friendid){
                                                      $query2->where('from_id','=',$friendid);    
                                                      $query2->where('to_id','=',$userid);
                                                  })->orderBy('created_at','desc')->first(); 

                            $is_media = 0;$last=null;
                            if(!empty($last_message->attachment)){
                                $attachment = explode(',',$last_message->attachment);
                                $last = url('storage/app/public/attachments/'.$attachment[0]);
                                $is_media = 1;
                            } 

                            $threds[] = ["friend_id" => $value->to_id,'name' => $friend_details->name,'profile' => $friend_details->profile,'unread_count' => $unread_count,'is_media' => $is_media,'last_message' => $last_message->body,'attachment' => $last,'lastmessage_datetime' => $last_message->created_at->diffForHumans()];

                        }
                    }
                    else{

                        if(!in_array($value->from_id,$friends)){
                            $friends[] = $value->from_id;

                            $friend_details = User::select('id','name','profile')->where(['id' => $value->from_id])->first();
                            if($friend_details->profile){
                               $friend_details['profile'] = asset('upload/profile/'.$friend_details->profile);
                            }
                            else{
                                $friend_details['profile'] = asset('upload/profile/defaultuser.png');
                            }

                            $friendid = $value->from_id;
                            $unread_count = Message::where(function($query) use($userid,$friendid){
                                            $query->where('from_id','=',$friendid);    
                                            $query->where('to_id','=',$userid);
                                              })->where(['seen' => 0])->get()->count();

                            $last_message = Message::where(function($query) use($userid,$friendid){
                                                $query->where('from_id','=',$userid);
                                                $query->where('to_id','=',$friendid);
                                                  })->orWhere(function($query2) use($userid,$friendid){
                                                      $query2->where('from_id','=',$friendid);    
                                                      $query2->where('to_id','=',$userid);
                                                  })->orderBy('created_at','desc')->first();  

                            $is_media = 0;$last=null;
                            if(!empty($last_message->attachment)){
                                $attachment = explode(',',$last_message->attachment);
                                $last = url('storage/app/public/attachments/'.$attachment[0]);
                                $is_media = 1;
                            }


                            $threds[] = ["friend_id" => $value->from_id,'name' => $friend_details->name,'profile' => $friend_details->profile,'unread_count' => $unread_count,'is_media' => $is_media,'last_message' => $last_message->body,'attachment' => $last,'lastmessage_datetime' => $last_message->created_at->diffForHumans()];
                        }

                    }

                }

            }

            $thred = array();
            foreach ($threds as $key => $row)
            {
                $thred[$key] = $row['unread_count'];
            }
            array_multisort($thred, SORT_DESC, $threds);

            return response()->json(['status' => true,'msg' => "Threds fetch successFully.",'data' => $threds]);
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }

    }


    public function chat_message_list(Request $request){

        $rules = array(
            'user_id' => 'required',
            'friend_id' => 'required'
        );
        $messages = array(
            'user_id.required' => 'Please enter user id',
            'friend_id.required' => 'Please enter friend id'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {

            $msg = $validator->messages();
            return ['status' => "false",
            'msg' => $msg];
        }

        $userid =   $request->user_id;
        $friendid = $request->friend_id;

        $message_details = Message::where(function($query) use($userid,$friendid){
                        $query->where('from_id','=',$userid);
                        $query->where('to_id','=',$friendid);
                          })->orWhere(function($query2) use($userid,$friendid){
                              $query2->where('from_id','=',$friendid);    
                              $query2->where('to_id','=',$userid);
                          })->orderBy('created_at','desc')->get();

        Message::where(function($query) use($userid,$friendid){
            $query->where('from_id','=',$friendid);    
            $query->where('to_id','=',$userid);
        })->where(['seen' => 0])->update(['seen' => 1]);

        $msges=[];
        if(count($message_details) > 0)
        {
            foreach ($message_details as $key => $value) {

                $my_msg = 0;
                if($value->from_id == $userid){
                    $my_msg = 1;
                }

                // $user_details = User::select('id','name','profile')->where(['id' => $value->from_id])->first();
                // if($user_details->profile){
                //    $user_details['profile'] = asset('upload/profile/'.$user_details->profile);
                // }
                // else{
                //     $user_details['profile'] = asset('upload/profile/defaultuser.png');
                // }

                $friend_details = User::select('id','name','profile')->where(['id' => $friendid])->first();
                if($friend_details->profile){
                   $friend_details['profile'] = asset('upload/profile/'.$friend_details->profile);
                }
                else{
                    $friend_details['profile'] = asset('upload/profile/defaultuser.png');
                }

                $is_media = 0;$msgs=null;
                if(!empty($value->attachment)){
                    $attachment = explode(',',$value->attachment);
                    $msgs = url('storage/app/public/attachments/'.$attachment[0]);
                    $is_media = 1;
                }

                $msges[] = ["id" => $value->id,"user_id" => $value->from_id,"friend_id" => $value->to_id,'is_media' => $is_media,"message" => $value->body,"attachment" => $msgs,"is_read" => $value->seen,
                                'my_message' => $my_msg,"created_at" =>  $value->created_at->diffForHumans(),'friend_details' => $friend_details];
            }       
        }
        
   
        return response()->json(['status' => true,'msg' => "Messages fetch successFully.",'data' => $msges]);

    }


    public function plan_list(Request $request){

        $get_plans = Plan::with('plan_features')->get();

        if(count($get_plans) > 0){
            return response()->json(['status' => true,'msg' => "Plan list fetch successfully.",'data' => $get_plans]);
        }
        else{
            return response()->json(['status' => false,'msg' => "Plan not found."]);
        }

    }


    public function transaction(Request $request){

        $rules = array(
            'user_id' => 'required',
            'plan_id' => 'required',
            'payment_type' => 'required',
            // 'amount' => 'required',
            // 'paymentId' => 'required'
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.',
            'plan_id.required' => 'Please enter plan id.',
            'payment_type.required' => 'Please enter payment type.',
            // 'amount.required' => 'Please enter amount.',
            // 'paymentId.required' => 'Please enter payment id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }


        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user)
        {
            if($request->amount > 0){

                $get_plan = Plan::where('id',$request->plan_id)->first();

                $time = strtotime(date('Y-m-d H:i:s'));
                $end_date = date("Y-m-d H:i:s", strtotime("+".$get_plan->duration." days", $time));

                $payment = new Payment;
                $payment->user_id = $check_user->id;
                $payment->plan_id = $request->plan_id;
                $payment->is_paid = true;
                $payment->payment_type = 'razorpay';
                $payment->amount = $request->amount;
                $payment->paymentId = $request->paymentId;
                $payment->response = null;
                $payment->save();

                $add = new UserPlan;
                $add->user_id = $check_user->id;        
                $add->plan_id = $request->plan_id;
                $add->starts_at = date('Y-m-d H:i:s');
                $add->ends_at = $end_date;
                $add->is_expired = 0;
                $add->save();

                $payment_update = Payment::find($payment->id);
                $payment_update->user_plan_id = $add->id;
                $payment_update->save();

            }
            else{

                $get_plan = Plan::where('id',$request->plan_id)->first();
                $time = strtotime(date('Y-m-d H:i:s'));
                $end_date = date("Y-m-d H:i:s", strtotime("+".$get_plan->duration." days", $time));

                $add = new UserPlan;
                $add->user_id = $check_user->id;        
                $add->plan_id = $request->plan_id;
                $add->starts_at = date('Y-m-d H:i:s');
                $add->ends_at = $end_date;
                $add->is_expired = 0;
                $add->save();
            }

            $user = User::find($check_user->id);
            $user->save_status = 2;
            $user->save();

            return response()->json(['status' => true,'msg' => "Transaction added successfully."]);
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }

    }


    public function transaction_details(Request $request){

        $rules = array(
            'user_id' => 'required'
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }


        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user)
        {
            $get_current_plan = Payment::with('plan_details','user_plan_details')->where(['user_id' => $request->user_id])->orderBy('id','desc')->first();
            if($get_current_plan){
                $get_current_plan_details = $get_current_plan;
            }
            else{
                $get_current_plan_details = UserPlan::with('plan_details')->where(['user_id' => $request->user_id,'is_expired' => 0])->orderBy('id','desc')->first();
            }

            $get_transaction = Payment::with('plan_details','user_plan_details')->where(['user_id' => $request->user_id])->orderBy('id','desc')->get();

            $data = ['current_plan' => $get_current_plan_details,'transaction_history' => $get_transaction];

            return response()->json(['status' => true,'msg' => "Transaction details fetch successfully.",'data' => $data]);
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }

    }



    /*--------------  Business  Template  --------------*/

    public function template_category_list(Request $request)
    {   
        $template_category = TemplateCategory::all();

        if(count($template_category) > 0){
            return response()->json(['status' => true,'msg' => "Template category fetch successfully.",'data' => $template_category]);
        }
        else{
            return response()->json(['status' => false,'msg' => "Template category not found."]);
        } 
    }

    public function template_sub_category_list(Request $request)
    {
        $rules = array(
            'template_category_id' => 'required'
        );
        $messages =array(
            'template_category_id.required' => 'Please enter template category id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_template_category = TemplateCategory::where('id',$request->template_category_id)->first();        

        if($check_template_category){
            $template_sub_category = TemplateSubCategory::where(['template_category_id' =>  $check_template_category->id])->get();

            if(count($template_sub_category) > 0){
                return response()->json(['status' => true,'msg' => "Template sub category fetch successfully.",'data' => $template_sub_category]);
            }
            else{
                return response()->json(['status' => false,'msg' => "Template sub category not found."]);
            } 
        }
        else{
            return response()->json(['status' => false,'msg' => "Template category not found."]);
        }
    }


    public function business_category(Request $request)
    {
        $business_category = BusinessCategory::all();

        if(count($business_category) > 0){
            return response()->json(['status' => true,'msg' => "Business category fetch successfully.",'data' => $business_category]);
        }
        else{
            return response()->json(['status' => false,'msg' => "Business category not found." ]);
        }
    }


    public function create_business_template(Request $request)
    {
        $rules = array(
            'user_id' => 'required'
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user)
        {

            $add = new BusinessTemplate;
            $add->user_id = $check_user->id;
            $add->template_category_id = $request->template_category_id;
            $add->template_sub_category_id = $request->template_sub_category_id;
            $add->business_category_id = $request->business_category_id;
            $add->title = isset($request->title) ? $request->title:null;
            $add->description = $request->description;
            $add->established_year = isset($request->established_year) ? $request->established_year:null;
            $add->no_of_employee = isset($request->no_of_employee) ? $request->no_of_employee:null;
            $add->legal_entity = isset($request->legal_entity) ? $request->legal_entity:null;
            $add->reported_sales = $request->reported_sales;
            $add->run_rate_sales = $request->run_rate_sales;
            $add->ebitda_margin = $request->ebitda_margin;
            $add->location = $request->location;
            $add->email = $request->email;
            $add->mobile_no = $request->mobile_no;
            $add->is_available_mandate = isset($request->is_available_mandate) ? $request->is_available_mandate:0;
            $add->status = 1;
            $add->save();

            return response()->json(['status' => true,'msg' => "Business template added successfully."]);

        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }
    }


    public function edit_business_template(Request $request)
    {

        $rules = array(
            'user_id' => 'required',
            'business_template_id' => 'required'
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.',
            'business_template_id.required' => 'Please enter business template id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user)
        {

            $check_business_template = BusinessTemplate::where('id',$request->business_template_id)->first();   

            if($check_business_template){

                $update = BusinessTemplate::find($check_business_template->id);
                $update->user_id = $check_user->id;
                $update->template_category_id = $request->template_category_id;
                $update->template_sub_category_id = $request->template_sub_category_id;
                $update->business_category_id = $request->business_category_id;
                $update->title = isset($request->title) ? $request->title:null;
                $update->description = $request->description;
                $update->established_year = isset($request->established_year) ? $request->established_year:null;
                $update->no_of_employee = isset($request->no_of_employee) ? $request->no_of_employee:null;
                $update->legal_entity = isset($request->legal_entity) ? $request->legal_entity:null;
                $update->reported_sales = $request->reported_sales;
                $update->run_rate_sales = $request->run_rate_sales;
                $update->ebitda_margin = $request->ebitda_margin;
                $update->location = $request->location;
                $update->email = $request->email;
                $update->mobile_no = $request->mobile_no;
                $update->is_available_mandate = isset($request->is_available_mandate) ? $request->is_available_mandate:0;
                $update->save();

                return response()->json(['status' => true,'msg' => "Business template updated successfully."]);
            }
            else{
                return response()->json(['status' => true,'msg' => "Business template not found."]);
            }

        }
        else{

            return response()->json(['status' => false,'msg' => "User not found."]);
        }
    }


    public function delete_business_template(Request $request)
    {

        $rules = array(
            'business_template_id' => 'required'
        );
        $messages =array(
            'business_template_id.required' => 'Please enter business template id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_business_template = BusinessTemplate::where('id',$request->business_template_id)->first();

        if($check_business_template)
        {
            $update = BusinessTemplate::find($check_business_template->id);
            $update->is_delete = 1;
            $update->save();

            return response()->json(['status' => true,'msg' => "Business template deleted successfully."]);
        }
        else{
            return response()->json(['status' => true,'msg' => "Business template not found."]);
        }
    }


    public function my_business_template_list(Request $request)
    {   

        $rules = array(
            'user_id' => 'required'
        );
        $messages =array(
            'user_id.required' => 'Please enter user id.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return ['status' => false,'msg' => $msg];
        }

        $check_user = User::where(['id' => $request->user_id])->first();

        if($check_user)
        {
            $get_business_templates = BusinessTemplate::where(['is_delete' => 0,'status' => 1,'user_id' => $check_user->id])->get();

            if(count($get_business_templates) > 0){
                return response()->json(['status' => true,'msg' => "Business template list fetched successfully.",'data' => $get_business_templates]);
            }
            else{
                return response()->json(['status' => true,'msg' => "Business template not found."]);
            }
        }
        else{
            return response()->json(['status' => false,'msg' => "User not found."]);
        }
    }



    /*----------------------------------------------------------------------------------*/



    public function generateToken()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
        $randomString = ''; 
      
        for ($i = 0; $i < 6; $i++) { 
            $index = rand(0, strlen($characters) - 1); 
            $randomString .= $characters[$index]; 
        } 
      
        return $randomString; 
    }

}
