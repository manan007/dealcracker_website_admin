<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

use App\Models\Feed;
use App\Models\User;
use Validator;
use Razorpay\Api\Api;
use Illuminate\Support\Str;
use App\Models\Payment;
use App\Models\Plan;
use App\Models\UserPlan;

class HomeController extends Controller
{
    public function Index()
    {
        if(Auth::check()){
            return redirect()->route('front.index'); 
        }
        else{
            return redirect()->route('front.sign_in');
        }
    }


    public function frontendIndex(Request $request){
        $response['user_details'] = auth()->user();


        if(Auth::check()){ 

                $response['users'] = [];
                $response['getpost_media']=[];
                $response['img']=[];

                $post_load_count = 10;
    
                $post=Feed::where(['is_delete' => 0])->orderBy('id','DESC')->take($post_load_count)->get();

                $response['total_post_counter'] = count($post);
                $response['posts'] = $post;
                $response['total_views'] = 0;
                $response['post_load_count'] = $post_load_count;

                return view('frontend.home.index', $response);
        }
        else{
           return redirect()->route('front.sign_in'); 
        }
    }

    public function planView(){
        $plans = Plan::all();
        return view('auth.plan',compact('plans'));
    }

    public function planPurchase(Request $request){

        $get_plan = Plan::where('id',$request->plan_id)->first();
        $time = strtotime(date('Y-m-d H:i:s'));
        $end_date = date("Y-m-d H:i:s", strtotime("+".$get_plan->duration." days", $time));

        $add = new UserPlan;
        $add->user_id = Auth::user()->id;        
        $add->plan_id = $request->plan_id;
        $add->starts_at = date('Y-m-d H:i:s');
        $add->ends_at = $end_date;
        $add->is_expired = 0;
        $add->save();

        $user = User::find(Auth::user()->id);
        $user->save_status = 2;
        $user->save();

        return redirect()->route('front.index');
        
        // if($request->amount == 0){
        //     $getplan = Plan::where(['slug' => 'premier'])->first();

        //     $chkplan  = UserPlan::where(['user_id'=> $request->hidden_id])->first();
        //     if($chkplan){
        //         $addplan = UserPlan::find($chkplan->id);
        //         $addplan->user_id = $request->hidden_id;
        //         $addplan->plan_id = $getplan->id;
        //         $addplan->is_free = 1;
        //         $addplan->save();
        //     }
        //     else{
        //         $addplan = new UserPlan;
        //         $addplan->user_id = $request->hidden_id;
        //         $addplan->plan_id = $getplan->id;
        //         $addplan->is_free = 1;
        //         $addplan->save();
        //     }

        // }
        // else{
        //     $getplan = Plan::where(['plan_amount' => $request->amount])->first();
            
        //     $chkplan  = UserPlan::where(['user_id'=> $request->hidden_id])->first();
        //     if($chkplan){
        //         $addplan = UserPlan::find($chkplan->id);
        //         $addplan->user_id = $request->hidden_id;
        //         $addplan->plan_id = $getplan->id;
        //         $addplan->is_free = 0;
        //         $addplan->save();
        //     }
        //     else{
        //         $addplan = new UserPlan;
        //         $addplan->user_id = $request->hidden_id;
        //         $addplan->plan_id = $getplan->id;
        //         $addplan->is_free = 0;
        //         $addplan->save();
        //     }

        // }

    }


    public function checkPayment(request $request)
    {

        $checkMode = env('RAZORPAY_PLATFORM');
        if ($checkMode == 'local') {

        } else {
            $this->api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));
        }
        $api = new Api(env('RAZORPAY_KEY_TEST'), env('RAZORPAY_SECRET_TEST'));

         //dd(env('RAZORPAY_KEY_TEST'),env('RAZORPAY_SECRET_TEST'));


        $total = $request->payment;

        // Yearly Subscription
        // $api->paymentLink->create(array('amount'=>$total * 100, 'currency'=>'INR', 'customer' => array('name'=>Auth::user()->name, 'email' => Auth::user()->email, 'contact'=> Auth::user()->phone),'options'=>array('checkout'=>array('method'=>array('netbanking'=>'1', 'card'=>'1', 'upi'=>'0', 'wallet'=>'0')))));


        //         $api->paymentLink->create(array('upi_link'=>true,'amount'=>500, 'currency'=>'INR', 'accept_partial'=>true,
        // 'first_min_partial_amount'=>100, 'description' => 'For XYZ purpose', 'customer' => array('name'=>'Gaurav Kumar',
        // 'email' => 'gaurav.kumar@example.com', 'contact'=>'+919999999999'),  'notify'=>array('sms'=>true, 'email'=>true) ,
        // 'reminder_enable'=>true ,'notes'=>array('policy_name'=> 'Jeevan Bima')));



        // $order = $api->order->create([
        //     'receipt' => Str::uuid()->toString(),
        //     'amount' => $total * 100,
        //     'currency' => 'INR',
        //     'payment_capture' => '0',
        //     'options'=> array('checkout'=>array('method'=>array('netbanking'=>'1', 'card'=>'1', 'upi'=>'0', 'wallet'=>'0'))),
        // ]);

        //dd($request->all());

        $payment = new Payment([
            'user_id' => Auth::user()->id,
            'amount' => $total,
            'plan_id' => $request->plan_id,
            'is_paid' => false,
        ]);
        $payment->save();

        return response()->json([
            'payment' => $payment->toArray(),
        ]);
    }


    public function paymentSuccess(request $request)
    {
        //dd($request->data['razorpay_payment_id']);
        //dd($request->all());

        $api = new Api(env('RAZORPAY_KEY_TEST'), env('RAZORPAY_SECRET_TEST'));
        $rPayment = $api->payment->fetch($request->data['razorpay_payment_id']);

        $get_payment_details = Payment::where('id',$request->local_payment_id)->first();
        $get_plan = Plan::where('id',$get_payment_details->plan_id)->first();

        $time = strtotime(date('Y-m-d H:i:s'));
        $end_date = date("Y-m-d H:i:s", strtotime("+".$get_plan->duration." days", $time));

        $payment = Payment::find($request->local_payment_id);

        if ($rPayment && $rPayment->capture(['amount' => $request->amount, 'currency' => $request->currency])) {

            $payment->paymentId = $request->data['razorpay_payment_id'];
            $payment->response = $request->data;
            $payment->is_paid = true;
            $payment->payment_type = 'razorpay';
            $payment->save();



            $add = new UserPlan;
            $add->user_id = Auth::user()->id;        
            $add->plan_id = $get_plan->id;
            $add->starts_at = date('Y-m-d H:i:s');
            $add->ends_at = $end_date;
            $add->is_expired = 0;
            $add->save();

            $payment_update = Payment::find($request->local_payment_id);
            $payment_update->user_plan_id = $add->id;
            $payment_update->save();

            $user = User::find(Auth::user()->id);
            $user->save_status = 2;
            $user->save();

            return response()->json(['status' => true,'message' => 'Your payment has been processed successfully. Redirecting to Home...']);
        }
        $payment->payment_id = $request->data['razorpay_payment_id'];
        $payment->is_paid = false;
        $payment->save();

        return response()->json(['status' => false,'message' => 'Something went wrong! If your payment has been debited then it should be credited in 5-7 working days.']);
    }



    public function contact_us(Request $request){
        return view('frontend.contact_us');
    }

    public function about_us(Request $request){
        return view('frontend.about_us');
    }

    public function privacy_policy(Request $request){
        return view('frontend.privacy_policy');
    }

    public function terms_conditions(Request $request){
        return view('frontend.terms_conditions');
    }
     public function refund_policy(Request $request){
        return view('frontend.refund_policy');
    }

    

}
