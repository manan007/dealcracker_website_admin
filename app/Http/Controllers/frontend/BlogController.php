<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\BlogMedia;

class BlogController extends Controller
{
    public function index(){

        $blogs = Blog::all();
        return view('frontend.blog.index',compact('blogs'));

    }

    public function blogView($id)
    {
        $blog_details = Blog::where(['id' => $id])->first();
        $blog_medias = BlogMedia::where(['blog_id' => $id])->get();
        return view ('frontend.blog.view',compact('blog_details','blog_medias'));
    }
}
