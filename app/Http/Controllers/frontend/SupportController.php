<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Support;
use Auth;

class SupportController extends Controller
{
    public function index(){
        $support_questions = Support::where(['user_id' =>  Auth::user()->id])->get();
        return view('frontend.support.index',compact('support_questions'));
    }

    public function create(){
        return view('frontend.support.create');
    }

    public function store(Request $request){    

        $add = new Support;
        $add->user_id = Auth::user()->id;
        $add->question = $request->question;
        $add->answer = null;
        $add->save();

        return redirect()->route('front.support.index')->with('success','Support question added successfully.');
    }

}
