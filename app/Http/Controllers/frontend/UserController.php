<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\Feed;
use App\Models\ProfileLike;
use App\Models\FeedLike;
use App\Models\FeedComment;
use App\Models\FeedCommentLike;
use App\Models\Platform;

class UserController extends Controller
{


    public function myProfileView(Request $request,$id)
    {

        $response['users'] = [];
        $response['getpost_media']=[];
        $response['img']=[];

        $post_load_count = 10;

        $post=Feed::where(['is_delete' => 0,'user_id' => $id])->orderBy('id','DESC')->take($post_load_count)->get();

        $response['total_post_counter'] = count($post);
        $response['posts'] = $post;
        $response['total_views'] = 0;
        $response['post_load_count'] = $post_load_count;

        $response['user_detail'] = User::where(['id' => $id])->first();

        $response['platforms'] = Platform::all();

        $response['req_segment'] = \Request::segment(2);

        if(Auth::check()){
            if(Auth::user()->id == $id){
                return view('frontend.my_profile.index', $response);   
            }
            else{
                return view('frontend.user_profile', $response);    
            }
        }
        else{
            return view('frontend.user_profile', $response);
        }
    }


    public function cropProfileImage(Request $request){
        $image_file = $request->image;
        list($type, $image_file) = explode(';', $image_file);
        list(, $image_file)      = explode(',', $image_file);
        $image_file = base64_decode($image_file);
        $image_name = time().'_'.rand(100,999).'.png';
        $path = public_path('upload/profile/'.$image_name);

        $update = User::find(Auth::user()->id);
        $update->profile = $image_name;
        $update->save();

        file_put_contents($path, $image_file);
        //return response()->json(['status'=>true]);
        return response()->json(['success'=>'Crop Image Uploaded Successfully']);
    }

    public function cropCoverImage(Request $request){
        $image_file = $request->image;
        list($type, $image_file) = explode(';', $image_file);
        list(, $image_file)      = explode(',', $image_file);
        $image_file = base64_decode($image_file);
        $image_name = time().'_'.rand(100,999).'.png';
        $path = public_path('upload/cover_image/'.$image_name);

        $update = User::find(Auth::user()->id);
        $update->cover_image = $image_name;
        $update->save();

        file_put_contents($path, $image_file);
        //return response()->json(['status'=>true]);
        return response()->json(['success'=>'Crop Image Uploaded Successfully']);
    }


    public function myProfileLike(Request $request){

        if($request->id == "like"){
            $status = 1;
        }
        else{
            $status = 2;
        }

        $my_profile = ProfileLike::where(['user_id' => Auth::user()->id,'friend_id' => Auth::user()->id])->first();

        if($my_profile){
            $add = ProfileLike::find($my_profile->id);
            $add->status = $status;
            $add->save();
        }
        else{
            $add = new ProfileLike;
            $add->user_id = Auth::user()->id;
            $add->friend_id = Auth::user()->id;
            $add->status = $status;
            $add->save();    
        }

        $profile_likes = ProfileLike::where(['friend_id' => Auth::user()->id,'status' => 1])->count();
        $profile_dislikes = ProfileLike::where(['friend_id' => Auth::user()->id,'status' => 2])->count();

        return response()->json(['success'=>'Profile review change successfully.','profile_likes' => $profile_likes,'profile_dislikes' => $profile_dislikes]);

    }


    /*----------- Feed Related ---------*/

    public function editPost(request $request){

        Feed::where(['id' => $request->post_id])->update(['title' => $request->post_title,'description' => $request->post_text]);

        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0,'user_id' => $request->userid])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html,'post_id' => $request->post_id]);
    }


    public function delete(request $request){

        Feed::where('id',$request->post_id)->update(['is_delete' => 1]);

        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0,'user_id' => $request->userid])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);

    }


    public function like(Request $request){
        
        $checklike = FeedLike::where(['user_id' => $request->userid,'feed_id' => $request->post_id])->first();

        if($checklike){
            FeedLike::where(['id' => $checklike->id])->delete();
        }
        else{
            $add = new FeedLike;
            $add->user_id = $request->userid;
            $add->feed_id = $request->post_id;
            $add->save();
        }
        
        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0,'user_id' => $request->userid])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);

    }


    public function storeComment(request $request)
    {
        $add=new FeedComment;
        $add->user_id=$request->userid;
        $add->feed_id=$request->post_id;
        $add->comment=$request->post_data;
        $add->save();
        
        
        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0,'user_id' => $request->userid])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);
    }



    public function replyComment(request $request)
    {
        //dd($request->all());
        $add=new FeedComment;
        $add->user_id=$request->userid;
        $add->feed_id=$request->post_id;
        $add->comment=$request->comment;
        $add->is_reply=1;
        $add->to_comment=$request->comment_id;
        $add->save();
        
        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0,'user_id' => $request->userid])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);
    }

    public function editComment(request $request)
    {
        $add=FeedComment::find($request->comment_id);
        $add->user_id=$request->userid;
        $add->feed_id=$request->post_id;
        $add->comment=$request->comment;
        $add->save();
        
        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0,'user_id' => $request->userid])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);
    }


    public function deleteComment(request $request)
    {
        $deleteComment=FeedComment::where('id',$request->comment_id)->delete();

        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0,'user_id' => $request->userid])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);
    }


    public function likeComment(Request $request)
    {
        $checklike = FeedCommentLike::where(['user_id' => $request->userid,'feed_id' => $request->post_id,'comment_id' => $request->comment_id])->first();

        if($checklike){
            FeedCommentLike::where(['id' => $checklike->id])->delete();    
        }
        else{
            $add = new FeedCommentLike;
            $add->user_id = $request->userid;
            $add->feed_id = $request->post_id;
            $add->comment_id = $request->comment_id;
            $add->save();
        }
        
        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0,'user_id' => $request->userid])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);

    }

    public function loadMorePost(Request $request){

        $post_load_count = 10;
        if($request->has('post_load_count')){
            $post_load_count = $request->post_load_count;
        }
        $post_details=Feed::where(['is_delete' => 0,'user_id' => $request->userid])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);
    }


    


/*==========================================================================================================================================================================================================

                        ======================================================  Other Profile Feed    ======================================================

==========================================================================================================================================================================================================*/
    
    public function otherProfileLike(Request $request){

        if($request->id == "like"){
            $status = 1;
        }
        else{
            $status = 2;
        }

        $my_profile = ProfileLike::where(['user_id' => Auth::user()->id,'friend_id' => $request->friend_id])->first();

        if($my_profile){
            $add = ProfileLike::find($my_profile->id);
            $add->status = $status;
            $add->save();
        }
        else{
            $add = new ProfileLike;
            $add->user_id = Auth::user()->id;
            $add->friend_id = $request->friend_id;
            $add->status = $status;
            $add->save();    
        }

        $profile_likes = ProfileLike::where(['friend_id' => $request->friend_id,'status' => 1])->count();
        $profile_dislikes = ProfileLike::where(['friend_id' => $request->friend_id,'status' => 2])->count();

        return response()->json(['success'=>'Profile review change successfully.','profile_likes' => $profile_likes,'profile_dislikes' => $profile_dislikes]);

    }
    

    public function otherLike(Request $request){
        
        $checklike = FeedLike::where(['user_id' => $request->userid,'feed_id' => $request->post_id])->first();

        if($checklike){
            FeedLike::where(['id' => $checklike->id])->delete();
        }
        else{
            $add = new FeedLike;
            $add->user_id = $request->userid;
            $add->feed_id = $request->post_id;
            $add->save();
        }
        
        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0,'user_id' => $request->friend_id])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);
    }

    public function otherStoreComment(Request $request){

        $add=new FeedComment;
        $add->user_id=$request->userid;
        $add->feed_id=$request->post_id;
        $add->comment=$request->post_data;
        $add->save();
        
        
        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0,'user_id' => $request->friend_id])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);

    }


    public function otherReplyComment(Request $request){

        //dd($request->all());
        $add=new FeedComment;
        $add->user_id=$request->userid;
        $add->feed_id=$request->post_id;
        $add->comment=$request->comment;
        $add->is_reply=1;
        $add->to_comment=$request->comment_id;
        $add->save();
        
        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0,'user_id' => $request->friend_id])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);

    }


    public function otherEditComment(request $request){

        $add=FeedComment::find($request->comment_id);
        $add->user_id=$request->userid;
        $add->feed_id=$request->post_id;
        $add->comment=$request->comment;
        $add->save();
        
        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0,'user_id' => $request->friend_id])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);
    }


    public function otherDeleteComment(request $request){

        $deleteComment=FeedComment::where('id',$request->comment_id)->delete();

        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0,'user_id' => $request->friend_id])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);

    }


    public function otherLoadMorePost(Request $request){

        $post_load_count = 10;
        if($request->has('post_load_count')){
            $post_load_count = $request->post_load_count;
        }
        $post_details=Feed::where(['is_delete' => 0,'user_id' => $request->friend_id])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);
    }


    public function otherLikeComment(Request $request){


        $checklike = FeedCommentLike::where(['user_id' => $request->userid,'feed_id' => $request->post_id,'comment_id' => $request->comment_id])->first();

        if($checklike){
            FeedCommentLike::where(['id' => $checklike->id])->delete();    
        }
        else{
            $add = new FeedCommentLike;
            $add->user_id = $request->userid;
            $add->feed_id = $request->post_id;
            $add->comment_id = $request->comment_id;
            $add->save();
        }
        
        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0,'user_id' => $request->friend_id])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);

    }



    /*--------------------------------------------------   */

    public function editInfoView(Request $request){
        $user_detail=User::where('id',Auth::user()->id)->first();
        $platforms = Platform::all();
        $html=view('frontend.my_profile.information.dynamic_info_model',compact('user_detail','platforms'))->render(); 
        return response()->json(['html'=>$html,'user_detail' => $user_detail]);
    }


    public function updateInfo(Request $request){

        $platforms = null;
        if($request->has('platforms')){
            $platforms = implode(',',$request->platforms);
        }

        $addprofile = User::find(Auth::user()->id);
        $addprofile->address = isset($request->address) ? $request->address:null;
        $addprofile->dob = isset($request->dob) ? $request->dob:null;
        $addprofile->website =  isset($request->website) ? $request->website:null;
        $addprofile->profile_description = isset($request->profile_description) ? $request->profile_description:null;
        $addprofile->key_skills = isset($request->key_skills) ? $request->key_skills:null;
        $addprofile->business_domain = isset($request->business_domain) ? $request->business_domain:null;
        $addprofile->other_info = isset($request->other_info) ? $request->other_info:null;
        $addprofile->platforms = $platforms;   
        $addprofile->save();

        $response['user_detail'] = User::where('id',Auth::user()->id)->first();

        $html=view('frontend.my_profile.information.index',$response)->render();
        return response()->json(['html'=>$html]);
    }



    public function editNameView(request $request)
    {
        $user_detail=User::where('id',Auth::user()->id)->first();
        $html=view('frontend.my_profile.dynamic_name_model',compact('user_detail'))->render(); 
        return response()->json(['html'=>$html]);
    }

    public function updateName(request $request)
    {
        User::where('id',Auth::user()->id)->update(['name' => $request->name,'profile_description' => $request->profile_description]);
       
        $response['user_detail'] = User::where('id',Auth::user()->id)->first();

        $response['req_segment'] = $request->request_segment;
        
        $html=view('frontend.my_profile.dynamic_name_section',$response)->render();
        $html1=view('frontend.my_profile.information.index',$response)->render();
        return response()->json(['html'=>$html,'html1'=>$html1]);
    }



}
