<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Opportunity;
use Auth;

class OpportunityController extends Controller
{
    public function index(Request $request)
    {
        $opportunity = Opportunity::where(['is_delete' => 0,'status' => 1])->get(); //Opportunity::where(['user_id' => Auth::user()->id,'is_delete' => 0])->get();
        return view('frontend.opportunity.index',compact('opportunity'));
    }


    public function create()
    {
        return view('frontend.opportunity.create');
    }

    public function store(Request $request)
    {
        $add = new Opportunity;
        $add->user_id = Auth::user()->id;
        $add->title = $request->title;
        $add->description = $request->description;
        $add->status = 1;
        $add->save();

        return redirect()->route('front.opportunity.index')->with('success','Opportunity added successfully.');
    }


    public function edit($id)
    {
        $opportunity_details = Opportunity::where(['id' => $id])->first();
        return view('frontend.opportunity.edit',compact('opportunity_details'));
    }

    public function update(Request $request,$id)
    {
        $edit = Opportunity::find($id);
        $edit->user_id = Auth::user()->id;
        $edit->title = $request->title;
        $edit->description = $request->description;
        $edit->save();

        return redirect()->route('front.opportunity.index')->with('success','Opportunity updated successfully.');
    }

    public function delete($id)
    {
        $update = Opportunity::find($id);
        $update->is_delete = 1;
        $update->save();

        return redirect()->route('front.opportunity.index')->with('success','Opportunity deleted successfully.');
    }

    public function opportunityView($id)
    {
        $opportunity_details = Opportunity::where(['id' => $id])->first();
        return view ('frontend.opportunity.view',compact('opportunity_details'));
    }

}
