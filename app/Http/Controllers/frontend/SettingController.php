<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Validator;
use Hash;
use App\Models\ContactUs;
use App\Models\Payment;

class SettingController extends Controller
{
    //
    public function index(Request $request)
    {
        $get_current_plan = [];//Transaction::where(['user_id' => Auth::user()->id])->orderBy('id','desc')->first();
        $get_transaction = [];//Transaction::where(['user_id' => Auth::user()->id])->orderBy('id','desc')->get();

        $get_current_plan = Payment::with('plan_details','user_plan_details')->where(['user_id' => Auth::user()->id])->orderBy('id','desc')->first();
        $get_transaction = Payment::with('plan_details','user_plan_details')->where(['user_id' => Auth::user()->id])->orderBy('id','desc')->get();

        return view('frontend.setting.index',compact('get_current_plan','get_transaction'));
    }

    public function changePassword(Request $request){

        $this->validate($request, [
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required',
        ]);

        $user = User::where('id',Auth::user()->id)->first();
        
        if (Hash::check($request->old_password, $user->password)) { 
           $user->fill([
            'password' => Hash::make($request->new_password)
            ])->save();
            
            return redirect()->route('front.setting.index')->with('success', 'Password changed successfully.');
            
        } else {
            return redirect()->route('front.setting.index')->with('error', 'Old password does  not match.');
        }

    }

    public function storeAccountSetting(Request $request)
    {
        $updateSetting = User::where('id',Auth::User()->id)->first();
        if($updateSetting != null)
        {
            if($request->send_chat_message_over_email){
                $updateSetting->send_chat_message_over_email = 1;
            }
            else{
                $updateSetting->send_chat_message_over_email = 0;
            }

            $updateSetting->save();
            return redirect()->route('front.setting.index')->with('success', 'Account setting changed successfully.');
        }
        else
        {
            return redirect()->route('front.setting.index')->with('invalid', 'User not Found');
        }
    }

    public function storeContactUs(Request $request){

        $add = new ContactUs;
        $add->user_id = Auth::user()->id;
        $add->name = $request->name;
        $add->email = $request->email;
        $add->phone = $request->phone;
        $add->message = $request->message;
        $add->save();

        return redirect()->back()->with('success', 'Contact details submited successfully.');
    }
}
