<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Business;
use Auth;

class BusinessController extends Controller
{
    
    public function index(Request $request)
    {
        $business = Business::where(['is_delete' => 0,'status' => 1])->get();//Business::where(['user_id' => Auth::user()->id,'is_delete' => 0])->get();
        return view('frontend.business.index',compact('business'));
    }

    public function create()
    {
        return view('frontend.business.create');
    }

    public function store(Request $request)
    {
        $add = new Business;
        $add->user_id = Auth::user()->id;
        $add->title = $request->title;
        $add->description = $request->description;
        $add->status = 1;
        $add->save();

        return redirect()->route('front.business.index')->with('success','Business added successfully.');
    }


    public function edit($id)
    {
        $business_details = Business::where(['id' => $id])->first();
        return view('frontend.business.edit',compact('business_details'));
    }

    public function update(Request $request,$id)
    {
        $edit = Business::find($id);
        $edit->user_id = Auth::user()->id;
        $edit->title = $request->title;
        $edit->description = $request->description;
        $edit->save();

        return redirect()->route('front.business.index')->with('success','Business updated successfully.');
    }

    public function delete($id)
    {
        $update = Business::find($id);
        $update->is_delete = 1;
        $update->save();

        return redirect()->route('front.business.index')->with('success','Business deleted successfully.');
    }

    public function businessView($id)
    {
        $business_details = Business::where(['id' => $id])->first();
        return view ('frontend.business.view',compact('business_details'));
    }

}
