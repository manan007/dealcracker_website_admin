<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Community;
use App\Models\CommunityQuestion;
use App\Models\User;
use Auth;

class CommunityController extends Controller
{
    public function index(Request $request)
    {
        $community = Community::whereRaw('FIND_IN_SET('.Auth::user()->id.',members)')->where(['is_delete' => 0,'status' => 1])->orWhere('user_id','=',Auth::user()->id)->get();
        return view('frontend.community.index',compact('community'));
    }


    public function create()
    {
        $users = User::where(['save_status' => 2,'is_admin' => 0])->where('id','!=',Auth::user()->id)->get();
        return view('frontend.community.create',compact('users'));
    }

    public function store(Request $request)
    {
        $add = new Community;
        $add->user_id = Auth::user()->id;
        $add->title = $request->title;
        $add->description = $request->description;
        $add->members = implode(',',$request->members);
        $add->status = 1;
        $add->save();

        return redirect()->route('front.community.index')->with('success','Community added successfully.');
    }


    public function edit($id)
    {
        $community_details = Community::where(['id' => $id])->first();
        $users = User::where(['save_status' => 2,'is_admin' => 0])->where('id','!=',Auth::user()->id)->get();
        return view('frontend.community.edit',compact('community_details','users'));
    }

    public function update(Request $request,$id)
    {
        $edit = Community::find($id);
        $edit->user_id = Auth::user()->id;
        $edit->title = $request->title;
        $edit->description = $request->description;
        $edit->members = implode(',',$request->members);
        $edit->save();

        return redirect()->route('front.community.index')->with('success','Community updated successfully.');
    }

    public function delete($id)
    {
        $update = Community::find($id);
        $update->is_delete = 1;
        $update->save();

        return redirect()->route('front.community.index')->with('success','Community deleted successfully.');
    }

    public function communityView($id)
    {
        $community_details = Community::where(['id' => $id])->first();
        $community_questions = CommunityQuestion::where(['community_id' => $id,'question_id' => 0])->get();
        return view ('frontend.community.view',compact('community_details','community_questions'));
    }


    public function deleteCommunityMember($community_id,$user_id){

        $community_details = Community::where(['id' => $community_id])->first();
        $member_array=[];
        if($community_details->members){
            $member_array = explode(',',$community_details->members); 
        }
        $final_members = [];
        if(count($member_array) > 0){
            foreach ($member_array as $key => $value) {
                if($user_id != $value){
                    $final_members[] = $value;
                }
            }
        }

        Community::where(['id' => $community_id])->update(['members' => implode(',',$final_members)]);    
        return redirect()->route('front.community.view',$community_id)->with('success','Community member remove successfully.');
    }


    public function createCommunityQuestion($community_id){
        return view('frontend.community.create_question',compact('community_id'));
    }


    public function storeCommunityQuestion(Request $request,$community_id){

        $add_question = new CommunityQuestion;
        $add_question->user_id = Auth::user()->id;
        $add_question->community_id  = $community_id;   
        $add_question->question_text = $request->question_text;
        $add_question->question_id = 0;
        $add_question->save();

        return redirect()->route('front.community.view',$community_id)->with('success','Community question added successfully.');
    }


    public function communityQuestionView($community_id,$question_id){
        $community_details = Community::where(['id' => $community_id])->first();
        $question_details = CommunityQuestion::where('id',$question_id)->first();
        $answer_details = CommunityQuestion::where('question_id',$question_id)->get();
        return view('frontend.community.view_question',compact('community_id','question_details','answer_details','community_details'));
    }


    public function createCommunityAnswer(Request $request,$community_id,$question_id){
        return view('frontend.community.create_answer',compact('community_id','question_id'));
    }

    public function storeCommunityAnswer(Request $request,$community_id,$question_id){

        $add_question = new CommunityQuestion;
        $add_question->user_id = Auth::user()->id;
        $add_question->community_id  = $community_id;   
        $add_question->question_text = $request->question_text;
        $add_question->question_id = $question_id;
        $add_question->save();

        return redirect()->route('front.community_question.view',[$community_id,$question_id])->with('success','Answer added successfully.');

    }

    public function deleteCommunityAnswer(Request $request,$community_id,$question_id,$answer_id){

        CommunityQuestion::where(['id' => $answer_id])->delete();
        return redirect()->route('front.community_question.view',[$community_id,$question_id])->with('success','Answer deleted successfully.');
    }


    public function editAnswerView(request $request)
    {
        $community_id = $request->community_id;
        $community_details = Community::where(['id' => $request->community_id])->first();
        $question_details = CommunityQuestion::where('id',$request->question_id)->first();
        $answer_details = CommunityQuestion::where('question_id',$request->question_id)->get();
        $html=view('frontend.community.dynamic_answer_model',compact('community_id','question_details','answer_details','community_details'))->render(); 
        return response()->json(['html'=>$html,'question_details' => $question_details]);
    }

    public function storeAnswer(request $request)
    {
        $add_question = new CommunityQuestion;
        $add_question->user_id = Auth::user()->id;
        $add_question->community_id  = $request->community_id;   
        $add_question->question_text = $request->question_text;
        $add_question->question_id = $request->question_id;
        $add_question->save();

        return response()->json(['status' => true,'success'=>'Answer added successfully.','msg' => 'Answer added successfully.']);
    }

}
