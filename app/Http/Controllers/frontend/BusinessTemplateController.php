<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\BusinessCategory;
use App\Models\BusinessTemplate;
use App\Models\TemplateCategory;
use App\Models\TemplateSubCategory;
use Auth;

class BusinessTemplateController extends Controller
{
        
    public function index(Request $request)
    {
        $business_templates = BusinessTemplate::where(['is_delete' => 0,'status' => 1])->get(); //where(['user_id' => Auth::user()->id,'is_delete' => 0])->get();
        return view('frontend.business_template.index',compact('business_templates'));
    }

    public function create()
    {
        $business_category = BusinessCategory::all();
        $template_category = TemplateCategory::all();
        $is_main = 2;
        $category_details = [];
        $business_template_details = [];
        return view('frontend.business_template.create',compact('business_category','template_category','is_main','category_details','business_template_details'));
    }

    public function store(Request $request)
    {
        $is_available_mandate = 0;
        if($request->is_available_mandate == "Yes"){
            $is_available_mandate = 1;
        }

        $add = new BusinessTemplate;
        $add->user_id = Auth::user()->id;
        $add->template_category_id = $request->template_category_id;
        $add->template_sub_category_id = $request->template_sub_category_id;
        $add->business_category_id = $request->business_category_id;
        $add->title = isset($request->title) ? $request->title:null;
        $add->description = $request->description;
        $add->established_year = isset($request->established_year) ? $request->established_year:null;
        $add->no_of_employee = isset($request->no_of_employee) ? $request->no_of_employee:null;
        $add->legal_entity = isset($request->legal_entity) ? $request->legal_entity:null;
        $add->reported_sales = $request->reported_sales;
        $add->run_rate_sales = $request->run_rate_sales;
        $add->ebitda_margin = $request->ebitda_margin;
        $add->location = $request->location;
        $add->email = $request->email;
        $add->mobile_no = $request->mobile_no;
        $add->is_available_mandate = $is_available_mandate;
        $add->status = 1;
        $add->save();

        return redirect()->route('front.business_template.index')->with('success','Business template added successfully.');
    }

    public function edit($id)
    {
        $business_template_details  = BusinessTemplate::where('id',$id)->first();
        $business_category = BusinessCategory::all();
        $template_category = TemplateCategory::all();
        $is_main = 2;
        $category_details = [];
        return view('frontend.business_template.edit',compact('business_template_details','business_category','template_category','is_main','category_details'));
    }

    public function update(Request $request,$id)
    {
        $edit = BusinessTemplate::find($id);
        $edit->user_id = Auth::user()->id;
        $edit->title = $request->title;
        $edit->description = $request->description;
        $edit->save();

        $is_available_mandate = 0;
        if($request->is_available_mandate == "Yes"){
            $is_available_mandate = 1;
        }

        $edit = BusinessTemplate::find($id);
        $edit->user_id = Auth::user()->id;
        $edit->template_category_id = $request->template_category_id;
        $edit->template_sub_category_id = $request->template_sub_category_id;
        $edit->business_category_id = $request->business_category_id;
        $edit->title = isset($request->title) ? $request->title:null;
        $edit->description = $request->description;
        $edit->established_year = isset($request->established_year) ? $request->established_year:null;
        $edit->no_of_employee = isset($request->no_of_employee) ? $request->no_of_employee:null;
        $edit->legal_entity = isset($request->legal_entity) ? $request->legal_entity:null;
        $edit->reported_sales = $request->reported_sales;
        $edit->run_rate_sales = $request->run_rate_sales;
        $edit->ebitda_margin = $request->ebitda_margin;
        $edit->location = $request->location;
        $edit->email = $request->email;
        $edit->mobile_no = $request->mobile_no;
        $edit->is_available_mandate = $is_available_mandate;
        $edit->status = 1;
        $edit->save();

        return redirect()->route('front.business_template.index')->with('success','Business template updated successfully.');
    }

    public function delete($id)
    {
        $update = BusinessTemplate::find($id);
        $update->is_delete = 1;
        $update->save();

        return redirect()->route('front.business_template.index')->with('success','Business template deleted successfully.');
    }

    public function businessTemplateView($id)
    {
        $business_template_details = BusinessTemplate::where(['id' => $id])->first();
        return view ('frontend.business_template.view',compact('business_template_details'));
    }

    public function getBusinessTemplateSubCategory(Request $request){

        $get_template_subcategory = TemplateSubCategory::where('template_category_id',$request->template_category_id)->get();
        
        $option = '<option value="">Select Template Sub Category</option>';

        if(count($get_template_subcategory) > 0){
            foreach ($get_template_subcategory as $k => $v) {
                $option .= '<option value="'.$v->id.'" data-slug="'.$v->slug.'">'.$v->name.'</option>';
            }
        }
        return $option;
    }


    public function getBusinessTemplateSubCategoryEdit(Request $request){

        $get_template_subcategory = TemplateSubCategory::where('template_category_id',$request->template_category_id)->get();
        
        $option = '<option value="">Select Template Sub Category</option>';

        if(count($get_template_subcategory) > 0){
            foreach ($get_template_subcategory as $k => $v) {
                if($v->id == $request->hidden_template_sub_category){
                    $option .= '<option value="'.$v->id.'" data-slug="'.$v->slug.'" selected="">'.$v->name.'</option>';   
                }
                else{
                    $option .= '<option value="'.$v->id.'" data-slug="'.$v->slug.'">'.$v->name.'</option>';
                }
            }
        }
        return $option;
    }


    public function getBusinessTemplateField(Request $request){

        $is_main = $request->is_main;
        if($request->is_main == 1){
            $category_details = TemplateCategory::where('id',$request->template_category_id)->first();
        }
        else{
            $category_details = TemplateSubCategory::where('id',$request->template_category_id)->first();
        }

        $business_category = BusinessCategory::all();

        $business_template_details = [];

        if($request->is_edit == 1){
            $business_template_details  = BusinessTemplate::where('id',$request->business_template_id)->first();   
        }

        $html=view('frontend.business_template.dynamic_field_section',compact('is_main','business_category','category_details','business_template_details'))->render();
        return response()->json(['html'=>$html]);

    }


}
