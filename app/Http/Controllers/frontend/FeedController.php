<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Feed;
use App\Models\FeedMedia;
use Auth;
use App\Models\FeedLike;
use App\Models\FeedComment;
use App\Models\FeedCommentLike;

class FeedController extends Controller
{
    
    /*------------------------------------------------------------------- Feed Image -----------------------------------------------------------------------------------------------*/

    public function showUploadImage(Request $request){
        $img=[];
        $html=view('frontend.home.dynamic_upload_model',compact('img'))->render();
        return response()->json(['html'=>$html]);
    }


    public function uploadImage(request $request)
    {
        $responseImg = '';
        $validator = Validator::make($request->all(), array(

            'file.*' => 'required|mimes:gif,jpg,jpeg,png,bmp',

        ), [
            "file.*.mimes" => "Invalid image files, please upload file with file type (.gif, .jpeg, .png, .jpg,.bmp) only",
            'file.*.max' => "Maximum File Size is 2 MB"

        ]);

        if ($validator->fails()) {
            $msg = $validator->errors()->first();
            return response()->json(['html' => 0, 'status' => 0, 'msg' => $msg]);
        }

        $img=[];
        if ($request->hasFile('file')) {
            foreach ($request->file('file') as $images) {
                
                $url = "/upload/post/";

                $originalPath = public_path().$url;
                $name = time() . mt_rand(10000, 99999);
                $imageName = $name . '.' . $images->getClientOriginalExtension();
                $images->move($originalPath, $imageName);
                $path = $originalPath . $imageName;
                $path=env('APP_URL')."/public".$url.$imageName;

                $img[] = $imageName;
                
            }
        }
            
        $html=view('frontend.home.dynamic_upload_section',compact('img'))->render();
        return response()->json(['html'=>$html]);
    }


    public function loadPostView(Request $request){
        
        $post_image_media = $request->post_image_media;
        $post_image_media_name = $request->post_image_media_name;
        $getpost_media = explode(",",$request->post_image_media);
        
        $getmedia_count=0;
        if($request->post_image_media == "null"){
            $getpost_media=[];
        }
        else{
            if(count($getpost_media) > 0){
                $getmedia_count = count($getpost_media);
                $getpost_media = explode(",",$request->post_image_media);
            }
            else{
                $getpost_media=[];
            }
        }
        $hashtagarray = [];

        $html=view('frontend.home.dynamic_post_model',compact('getpost_media','post_image_media','post_image_media_name','hashtagarray'))->render();
        return response()->json(['html'=>$html,'getmedia_count' => $getmedia_count]);
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------*/





    /*----------------------------------------------------------- Feed Video ---------------------------------------------------------------------------------------*/

    public function showUploadVideo(Request $request){
        $video=[];
        $html=view('frontend.home.dynamic_upload_video_model',compact('video'))->render();
        return response()->json(['html'=>$html]);
    }

    public function uploadVideo(request $request)
    {  
        $responseImg = '';
        $validator = Validator::make($request->all(), array(
            'file_video.*' => 'required|mimes:mp4|max:55000',
        ), [
            "file_video.*.mimes" => "Invalid video files, please upload file with file type (.mp4) only",
            'file_video.*.max' => "Maximum file size to upload is 55MB (55000 KB)",
        ]);

        if ($validator->fails()) {
            $msg = $validator->errors()->first();
            return response()->json(['html' => 0, 'status' => 0, 'msg' => $msg]);
        }

        $video=null;
        if ($request->file_video) {
            $url = "/upload/post/";
            $originalPath = public_path().$url;
            $name = time() . mt_rand(10000, 99999);
            $imageName = $name . '.' . $request->file_video->getClientOriginalExtension();
            $request->file_video->move($originalPath, $imageName);
            $path = $originalPath . $imageName;
            $path=env('APP_URL')."/public".$url.$imageName;
            $video = $imageName;
        }

        $imgesarr = explode('.',$video); $name_v = $imgesarr[0];
        $video_data = ['video' => $video,'name_value' => $name_v];
        return $video_data; 
    }

    public function loadPostVideoView(Request $request){

        $post_video_media = $request->post_video_media;
        $post_video_media_name = $request->post_video_media_name;
        $getpost_media = explode(",",$request->post_video_media);
        
        $getmedia_count=0;
        if($request->post_video_media == "null"){
            $getpost_media=[];
        }
        else{
            if(count($getpost_media) > 0){
                $getmedia_count = count($getpost_media);
                $getpost_media = explode(",",$request->post_video_media);
            }
            else{
                $getpost_media=[];
            }
        }
        
        $html=view('frontend.home.dynamic_post_video_model',compact('getpost_media','post_video_media','post_video_media_name'))->render();
        return response()->json(['html'=>$html,'getmedia_count' => $getmedia_count]);
    }

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------*/

    public function storePost(Request $request)
    {           
        $add = new Feed;
        $add->user_id = Auth::user()->id;
        $add->title = isset($request->post_title) ? $request->post_title:'';
        $add->description = isset($request->post_text) ? $request->post_text:'';
        $add->save();


        if($request->has('image_name') && count($request->image_name) > 0)
        {
            foreach($request->image_name as $image_media){
                if($image_media != null){
                    $post_media = new FeedMedia;
                    $post_media->feed_id = $add->id;
                    $post_media->type="image";
                    $post_media->feed_media = $image_media;
                    $post_media->save();
                }
            }
        }   

        if($request->has('video_name') && count($request->video_name) > 0)
        {
            foreach($request->video_name as $video_media){
                if($video_media != null){
                    $post_media = new FeedMedia;
                    $post_media->feed_id = $add->id;
                    $post_media->type="video";
                    $post_media->feed_media = $video_media;
                    $post_media->save();
                }
            }
        }

        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);
    }


    public function deleteImage(request $request)
    {
        
        $getpost_media = [];
        $getmedia_count = count($getpost_media);
        $post_image_media='';
        $post_image_media_name='';
        $image = explode(",",$request->image);
        $name = explode(",",$request->name);
        $name_arr=[];
        if(count($image) > 0){
            foreach($image as $key => $value){
                $path = public_path() . '/upload/post/' . $value;
                if(file_exists($path)) {
                    unlink($path);
                }
                $name_arr[] = $name[$key];
            }

        }
        $html=view('frontend.home.dynamic_post_model',compact('getpost_media','post_image_media','post_image_media_name'))->render();
        $data=['name' => $name_arr,'html' => $html,'getmedia_count' => $getmedia_count];
        return response()->json($data);
    }


    public function editPostView(request $request)
    {
        $post_data = Feed::where(['id' => $request->post_id])->first();
        $getpost_media = FeedMedia::where(['feed_id' => $request->post_id])->get();
        $html=view('frontend.home.dynamic_edit_post_model',compact('post_data','getpost_media'))->render();
        return response()->json(['html'=>$html,'postdata' => $post_data]);
    }
    

    public function editPost(request $request){

        Feed::where(['id' => $request->post_id])->update(['title' => $request->post_title,'description' => $request->post_text]);

        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html,'post_id' => $request->post_id]);
    }


    public function delete(request $request){

        Feed::where('id',$request->post_id)->update(['is_delete' => 1]);

        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);

    }


    public function loadMorePost(Request $request){

        $post_load_count = 10;
        if($request->has('post_load_count')){
            $post_load_count = $request->post_load_count;
        }
        $post_details=Feed::where(['is_delete' => 0])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);
    }



    public function like(Request $request){
        
        $checklike = FeedLike::where(['user_id' => Auth::user()->id,'feed_id' => $request->post_id])->first();

        if($checklike){
            FeedLike::where(['id' => $checklike->id])->delete();
        }
        else{
            $add = new FeedLike;
            $add->user_id = Auth::user()->id;
            $add->feed_id = $request->post_id;
            $add->save();
        }
        
        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);

    }


    public function likeGallery(Request $request){
        
        $checklike = FeedLike::where(['user_id' => Auth::user()->id,'feed_id' => $request->post_id])->first();
        $post_id = $request->post_id;

        if($checklike){
            $is_remove = 1;
            FeedLike::where(['id' => $checklike->id])->delete();
        }
        else{
            $is_remove = 0;
            $add = new FeedLike;
            $add->user_id = Auth::user()->id;
            $add->feed_id = $request->post_id;
            $add->save();
        }

        $getlikes_data = FeedLike::where(['feed_id' => $request->post_id])->first();        
        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);

        $getpost_like = FeedLike::where(['user_id' => Auth::user()->id,'feed_id' => $request->post_id])->first();

        $html=view('frontend.home.dynamic_gallery_like',compact('posts','post_id','getlikes_data','post_load_count','total_post_counter','getpost_like'))->render();
        return response()->json(['html'=>$html,'post_id' => $post_id,'is_remove' => $is_remove]);

    }


    public function getPostLikes(Request $request){
        $getlikes_data = FeedLike::where(['feed_id' => $request->post_id])->get();

        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);
    }


    public function likeShow(request $request){
        
        $getlikes_data = FeedLike::where(['feed_id' => $request->post_id])->get();
        $post_id = $request->post_id;
        //$product_id = $request->id;        //dd($request->all());
        $html=view('frontend.home.dynamic_like_model',compact('getlikes_data','post_id'))->render(); 
        return response()->json(['html'=>$html]);

    }


    public function storeComment(request $request)
    {
        $add=new FeedComment;
        $add->user_id=Auth::user()->id;
        $add->feed_id=$request->post_id;
        $add->comment=$request->post_data;
        $add->save();
        
        
        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);
    }


    public function likeComment(Request $request)
    {
        $checklike = FeedCommentLike::where(['user_id' => Auth::user()->id,'feed_id' => $request->post_id,'comment_id' => $request->comment_id])->first();

        if($checklike){
            FeedCommentLike::where(['id' => $checklike->id])->delete();    
        }
        else{
            $add = new FeedCommentLike;
            $add->user_id = Auth::user()->id;
            $add->feed_id = $request->post_id;
            $add->comment_id = $request->comment_id;
            $add->save();
        }
        
        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);

    }

    public function replyComment(request $request)
    {
        //dd($request->all());
        $add=new FeedComment;
        $add->user_id=Auth::user()->id;
        $add->feed_id=$request->post_id;
        $add->comment=$request->comment;
        $add->is_reply=1;
        $add->to_comment=$request->comment_id;
        $add->save();
        
        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);
    }


    public function editComment(request $request)
    {
        $add=FeedComment::find($request->comment_id);
        $add->user_id=Auth::user()->id;
        $add->feed_id=$request->post_id;
        $add->comment=$request->comment;
        $add->save();
        
        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);
    }



    public function deleteComment(request $request)
    {
        $deleteComment=FeedComment::where('id',$request->comment_id)->delete();

        $post_load_count = 10;
        $post_details=Feed::where(['is_delete' => 0])->orderBy('id','DESC')->take($post_load_count)->get();
        $posts = $post_details;
        $total_post_counter = count($post_details);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);
    }



    public function deleteCommentaaaa(request $request)
    {
        $deleteComment=Comment::where('id',$request->comment_id)->delete();

        $deleteComment=Comment::where('id',$request->comment_id)->delete();

        //$posts=Post::where(['is_delete' => 0])->orderBy('id','desc')->get();
        $post_load_count = 9;

            $post_details=Post::whereHas('user', function ($query) {
                $query->where('is_delete',0);
            })->where(['is_delete' => 0])->orderBy('id','DESC')->get();


        $posts = $this->get_post_details($post_details,$post_load_count);
        $total_post_counter = $this->get_total_post($post_details,$post_load_count);
        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);

        $posts = $this->get_post_details($post_details);
        $total_post_counter = $this->get_total_post($post_details,$post_load_count);

        $total_post_counter = $this->get_total_post($post_details,$post_load_count);
        $total_post_counter = $this->get_total_post($post_details,$post_load_count1);


        $html=view('frontend.home.dynamic_post',compact('posts','post_load_count','total_post_counter'))->render();
        return response()->json(['html'=>$html]);


        $checklike = FeedCommentLike::where(['user_id' => Auth::user()->id,'feed_id' => $request->post_id,'comment_id' => $request->comment_id])->first();

        if($checklike){
            FeedCommentLike::where(['id' => $checklike->id])->delete();    
        }
        else{
            $add = new FeedCommentLike;
            $add->user_id = Auth::user()->id;
            $add->feed_id = $request->post_id;
            $add->comment_id = $request->comment_id;
            $add->save();
        }

    }











    













    public function get_total_post_profile($post_details,$post_load_count,$user_id){
        $get_friend_lists = Friend::where(['user_id' => $user_id])->where(['status' => 1])->orWhere(function ($query) use ($user_id){
                $query->where('friend_id', $user_id);
                $query->where('status',1);
            })->get();
        $friend_array=[];
        $friend_array[]=$user_id;
        if(count($get_friend_lists) > 0){
            foreach($get_friend_lists as $get_friend_list){
                if($get_friend_list->user_id == $user_id){
                    $friend_array[] = $get_friend_list->friend_id;
                }
                else{
                    $friend_array[] = $get_friend_list->user_id;
                }
            }
        }
        $post=[];
        foreach($post_details as $post_detail){
        if(in_array($post_detail->user_id,$friend_array)){
            $post[] = $post_detail;
        }
        }
        $total_post_counter = count($post);
        return $total_post_counter;
    }

























}
