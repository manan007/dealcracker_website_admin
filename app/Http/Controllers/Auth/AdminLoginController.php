<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    public function showLoginPage(){
        return view('auth.admin_login');
    }

    public function adminLogin(Request $request){
       $request->validate([
           'email'=>'required',
           'password'=> 'required|min:6'
       ]);

        if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password,'is_admin' => 1], $request->get('remember'))){
           return redirect()->route('admin.dashbord.view');
            // return redirect()->intended('admin/dashbord');
        }else{
            return back()->with('error', 'invalid credentials');
        }
        return back()->withInput($request->only('email', 'remember'));
    }

    public function adminLogout(){
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }
}
