<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Exception;
use Socialite;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        if(Auth::check()){
            return redirect()->route('front.index');    
        }
        else{
            return view('auth.login');
        }
    }

    public function frontLogin(Request $request)
    {
        //dd($request->all());
        $request->validate([
           'email'=>'required',
           'password'=> 'required'
       ]);

        if(Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password,'is_admin' => 0], $request->get('remember'))){

            $user = User::where(['id' => Auth::user()->id])->first(); 
            
            if($user->save_status == 3){
                return redirect()->route('front.otp.send');
            }
            else{
                if($user->save_status == 1){
                    return redirect()->route('front.plan.view');
                }
                elseif ($user->save_status == 2) {
                    return redirect()->route('front.index')->with('success', 'Login Successfully.');
                }
                else{
                    return redirect()->route('front.complete.profile');
                } 
            }

        }else{
            return back()->with('error', 'invalid credentials');
        }
        return back()->withInput($request->only('email', 'password'));
    }

    public function logout()
    {
        Auth::guard('web')->logout();
        //return redirect()->route('home.index');
        return redirect()->route('front.sign_in');
    }


    public  function forgotpasswordIndex()
    {
        return view('auth.forgot_password');
    }

    public function forgotpassword(Request $request){

        $request->validate([
            'email' => 'required|email',
        ]);

        $checkuser = User::where(['email' => $request->email])->first();

        if($checkuser){
                
            $dynamic_token = $this->generateToken();
            
            User::where(['id' => $checkuser->id])->update(['forgot_password_token' => $dynamic_token]);

            $fromAdd=env('MAIL_FROM_ADDRESS');

            $link = env('APP_URL').'/reset_password/'.$dynamic_token;

            $data = ['user_details' => $checkuser,'link' => $link];
            try{
                $Mail = \Mail::send (['html' => 'email.forgot_password'], $data, function ($message) use ($request) {
                    $message->to($request->email)->subject ( 'Forgot Password' );
                    $message->from (env('MAIL_FROM_ADDRESS'),env('MAIL_FROM_NAME'));
                });
                return redirect()->route('user.forgot_password.view')->with('success', 'Email Sent SuccessFully.');
            }
            catch(\Exception $e)
            {
                return redirect()->route('user.forgot_password.view')->with('invalid', 'Something went wrong.');
            }
        }
        else{
            return redirect()->back()->with('invalid', 'Email not found.');
        }

    }


    public function resetpasswordView(Request $request){
        return view('auth.reset_password');
    }

    public function resetpassword(Request $request){
        $request->validate([
            'password' => 'required',
            'confirm_password' => 'required',
        ]);

        if($request->has('token')){
            $checkuser = User::where(['forgot_password_token' => $request->token])->first();

            if($checkuser){
                User::where(['id' => $checkuser->id])->update(['forgot_password_token' => null,'password' => Hash::make($request->password)]);
                return redirect()->route('front.sign_in')->with('success', 'Password updated successfully.');
            }
            else{
                return redirect()->back()->with('invalid', 'Link has been expired.');
            }  
        }
        else{
            return redirect()->back()->with('invalid', 'User not found.');
        }
    }


    public function generateToken()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
        $randomString = ''; 
      
        for ($i = 0; $i < 12; $i++) { 
            $index = rand(0, strlen($characters) - 1); 
            $randomString .= $characters[$index]; 
        } 
      
        return $randomString; 
    }


    public function handleSocialCallback(Request $request)
    {

            $checktype = "google";
            if($request->type == "facebook")
            {
                $checktype = "facebook";
            }
        
            $finduser = User::where(['social_id' => $request->id,'social_type' => $checktype ])->first();
            
            if($finduser){

                Auth::login($finduser);

                if(Auth::user()->save_status == 3){
                    //return redirect()->route('front.otp.send');
                    $route = "send_otp";
                }
                else{
                    if(Auth::user()->save_status == 1){
                        $route = "plan";
                        //return redirect()->route('front.plan.view');
                    }
                    elseif (Auth::user()->save_status == 2) {
                        $route = "home";
                        //return redirect()->route('front.index');
                    }
                    else{
                        $route = "register/complete_profile";
                        //return redirect()->route('front.complete.profile');
                    } 
                }
            }
            else{

                $userdata = new User;
                $userdata->email = isset($request->email) ? $request->email:null;
                $userdata->social_type = isset($request->type) ? $request->type:null;
                $userdata->social_id = isset($request->id) ? $request->id:null;
                $userdata->save_status = 0;
                $userdata->save();

                Auth::loginUsingId($userdata->id);

                $route =  "register/complete_profile";
                
            }

            return $route;
    }









    public function linkedinRedirect()
    {
        return Socialite::driver('linkedin')->redirect();
    }
       
    public function linkedinCallback()
    {
        try {
     
            $user = Socialite::driver('linkedin')->user();
      
            $linkedinUser = User::where('social_id', $user->id)->first();
      
            if($linkedinUser){

                Auth::login($linkedinUser);

                if(Auth::user()->save_status == 3){
                    return redirect()->route('front.otp.send');
                    //$route = "send_otp";
                }
                else{
                    if(Auth::user()->save_status == 1){
                        //$route = "plan";
                        return redirect()->route('front.plan.view');
                    }
                    elseif (Auth::user()->save_status == 2) {
                        //$route = "home";
                        return redirect()->route('front.index');
                    }
                    else{
                        //$route = "register/complete_profile";
                        return redirect()->route('front.complete.profile');
                    } 
                }
      
            }else{

                $userdata = new User;
                $userdata->name = isset($user->name) ? $user->name:null;
                $userdata->email = isset($user->email) ? $user->email:null;
                $userdata->social_type = 'linkedin';
                $userdata->social_id = isset($user->id) ? $user->id:null;
                $userdata->save_status = 0;
                $userdata->save();

                Auth::loginUsingId($userdata->id);

                $route =  "register/complete_profile";
              
                return redirect()->route('front.complete.profile');
            }
     
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }






























}
