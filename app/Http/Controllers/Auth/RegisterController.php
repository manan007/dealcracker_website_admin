<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Platform;
use Auth;


class RegisterController extends Controller
{
    
    public function index(Request $request)
    {        
        return view('auth.register');
    }


    public function frontRegister(Request $request)//(0=default,1=complete_profile,2=subscription,3=otp_verification)
    {        
        //dd($request->all());
        $rules = array(
            'name' => 'required',
            'email'=>'required|email|unique:users,email',
            'phone'=>'required|unique:users,phone',
            'password' => 'required',
        );
        $messages = array(
            'name.required' =>'Please enter name.',
            'email.required' =>'Please enter email.',
            'phone.required' =>'Please enter phone number.',
            'password.required' =>'Please enter password.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput(); 
        }

        $unique_id = rand(1111,999999);

        $registerdata = new User;
        $registerdata->name = $request->name;
        $registerdata->email = $request->email;
        $registerdata->phone =  $request->phone;
        $registerdata->password = Hash::make($request->password);
        $registerdata->otp = "111111";
        $registerdata->save_status = 3; 
        $registerdata->save();

        Auth::guard('web')->logout();
        Auth::login($registerdata);

        return redirect()->route('front.otp.send')->with('success', 'Register successfully.');
    }


    public function otpVerification(Request $request){
        return view('auth.otp_verification');
    }

    public function sendOTP(Request $request)
    {
        //$this()->sendOtp();        //For the Send OTP
        return redirect()->route('front.otp.verification.view')->with('success', 'OTP send successfully.');
    }

    public function otpVerificationProcess(Request $request)
    {
        $final_otp = $request->first.$request->second.$request->third.$request->fourth.$request->fifth.$request->sixth;
        $check_otp = User::where(['id' => Auth::user()->id,'otp' => $final_otp])->first();
        if($check_otp)
        {
            $edit = User::find($check_otp->id);
            $edit->save_status = 0;
            $edit->save();
            return redirect()->route('front.complete.profile')->with('success', 'Verification completed successfully.');
        }
        else
        {
            return redirect()->route('front.otp.verification.view')->with('error','Invalid OTP.');
        }
    }


    // public function sendOtp(Request $request)
    // {
    //     $otp = rand(100000, 999999);
    //     $checkmobileno = SMS_Verification::where('mobile_no',$request->mobile_no)->first();
    //     if($checkmobileno)
    //     {   
    //         $update = SMS_Verification::find($checkmobileno->id);
    //         $update->otp = $otp; 
    //         $update->save();
    //     }
    //     else{
    //         $add = new SMS_Verification;
    //         $add->mobile_no = $request->mobile_no;
    //         $add->otp = $otp;
    //         $add->save();
    //     }

    //     $curl = curl_init();

    //     curl_setopt_array($curl, array(
    //       CURLOPT_URL => "https://www.fast2sms.com/dev/bulk?authorization=yDA48Nci0jIPHBeshLCUEbtZO3S5XW9GpRfVngdw2uqaomFkJY2Eo0Wvr46VQqXsktY9H8mI5BpCSxPy&sender_id=FSTSMS&message=".urlencode('OTP :'.$otp.'')."&language=english&route=t&numbers=".urlencode($request->mobile_no),
    //       CURLOPT_RETURNTRANSFER => true,
    //       CURLOPT_ENCODING => "",
    //       CURLOPT_MAXREDIRS => 10,
    //       CURLOPT_TIMEOUT => 30,
    //       CURLOPT_SSL_VERIFYHOST => 0,
    //       CURLOPT_SSL_VERIFYPEER => 0,
    //       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //       CURLOPT_CUSTOMREQUEST => "GET",
    //       CURLOPT_HTTPHEADER => array(
    //         "cache-control: no-cache"
    //       ),
    //     ));

    //     $response = curl_exec($curl);
    //     $err = curl_error($curl);

    //     curl_close($curl);

    //     if ($err) {
    //         //dd($err);
        
    //       echo "cURL Error #:" . $err;
    //     } else {
    //         //dd($response);
    //       echo $response;
    //     }

    // }


    public function skip(){
        return redirect()->route('front.index');
    }

    public function completeProfileView(){

        $platforms = Platform::all();
        return view('auth.complete_profile',compact('platforms'));
    }

    public function completeProfile(Request $request){

        $platforms = null;
        if($request->has('platforms')){
            $platforms = implode(',',$request->platforms);
        }

        $addprofile = User::find(Auth::user()->id);
        $addprofile->address = isset($request->address) ? $request->address:null;
        $addprofile->dob = isset($request->dob) ? $request->dob:null;
        $addprofile->website =  isset($request->website) ? $request->website:null;
        $addprofile->profile_description = isset($request->profile_description) ? $request->profile_description:null;
        $addprofile->key_skills = isset($request->key_skills) ? $request->key_skills:null;
        $addprofile->business_domain = isset($request->business_domain) ? $request->business_domain:null;
        $addprofile->other_info = isset($request->other_info) ? $request->other_info:null;
        $addprofile->platforms = $platforms; 
        $addprofile->save_status = 1;   
        $addprofile->save();

        return redirect()->route('front.plan.view')->with('success', 'Profile details updated successfully.');
    }
}
