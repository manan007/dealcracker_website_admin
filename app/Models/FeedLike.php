<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FeedLike extends Model
{
    use HasFactory;
    protected $table = 'feed_likes';

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function user_details()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
