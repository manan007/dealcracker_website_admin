<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = 'orders';
        
    public function order_details()
    {
        return $this->hasMany(OrderDetail::class,'order_id','id');
    }

    public function payment_details()
    {
        return $this->hasOne(Payment::class,'order_id','id');
    }

    public function user_details()
    {
        return $this->belongsTo(User::class,'user_id');
    }

}

