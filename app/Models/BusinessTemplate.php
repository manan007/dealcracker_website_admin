<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessTemplate extends Model
{
    use HasFactory;
    protected $table = 'business_templates';


    public function user_details()
    {
        return $this->belongsTo(User::class,'user_id');
    }


    public function business_category_details()
    {
        return $this->belongsTo(BusinessCategory::class,'business_category_id');
    }

    public function template_category_details()
    {
        return $this->belongsTo(TemplateCategory::class,'template_category_id');
    }

    public function template_sub_category_details()
    {
        return $this->belongsTo(TemplateSubCategory::class,'template_sub_category_id');
    }

}
