<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $table = 'payments';
    protected $fillable=['user_id','amount','is_paid','plan_id'];

    public function user_details()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function plan_details()
    {
        return $this->belongsTo(Plan::class,'plan_id');
    }

    public function user_plan_details()
    {
        return $this->belongsTo(UserPlan::class,'user_plan_id');
    }
    
}
