<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';

    public function user_details()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function protect_attributes()
    {
        return $this->hasMany(ProductAttribute::class,'product_id','id');
    }
    
}
