<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    use HasFactory;
    protected $table = 'feed';


    public function user_details()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function feed_media()
    {
        return $this->hasMany(FeedMedia::class,'feed_id');
    }

}
