<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserShare extends Model
{
    use HasFactory;
    protected $table = 'user_shares';

    public function user_details()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function friend_details()
    {
        return $this->belongsTo(User::class,'friend_id');
    }

    public function group_details()
    {
        return $this->belongsTo(Group::class,'group_id');
    }
}
