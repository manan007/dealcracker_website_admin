<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TemplateSubCategory extends Model
{
    use HasFactory;
    protected $table = 'template_sub_categories';

    public function template_category_details()
    {
        return $this->belongsTo(TemplateCategory::class,'template_category_id');
    }
}
