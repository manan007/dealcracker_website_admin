<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPlan extends Model
{
    use HasFactory;
    protected $table = 'user_plans';


    public function user_details()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function plan_details()
    {
        return $this->belongsTo(Plan::class,'plan_id');
    }

}
