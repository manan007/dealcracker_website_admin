<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    use HasFactory;
    protected $table = 'plans';

    public function plan_features()
    {
        return $this->hasMany(PlanFeature::class,'plan_id','id');
    }

}
