<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FeedCommentLike extends Model
{
    use HasFactory;
    protected $table = 'feed_comment_likes';

    public function user_details()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
