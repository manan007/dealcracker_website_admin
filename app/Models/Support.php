<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    use HasFactory;
    protected $table = 'supports';

    public function user_details()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    
}
