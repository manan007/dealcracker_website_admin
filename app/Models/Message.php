<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $table = 'messages';

    public function user_details()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function friend_details()
    {
        return $this->belongsTo(User::class,'friend_id');
    }

    // public function post_hashtag()
    // {
    //     return $this->hasMany(User::class,'post_id');
    // }
}
