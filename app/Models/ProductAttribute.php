<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    use HasFactory;

    protected $table = 'product_attributes';

    public function attribute_details()
    {
        return $this->belongsTo(Attribute::class,'attribute_id');
    }

    public function sub_attribute_details()
    {
        return $this->belongsTo(Attribute::class,'sub_attribute_id');
    }
}
