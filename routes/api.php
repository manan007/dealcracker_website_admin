<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('register',[ApiController::class,'register']);
Route::post('login',[ApiController::class,'login']);

Route::post('platform_list',[ApiController::class,'platform_list']);

Route::post('complete_profile',[ApiController::class,'complete_profile']);

Route::post('forgotpassword',[ApiController::class,'forgotpassword']);
Route::post('change_password',[ApiController::class,'change_password']);

Route::post('edit_profile',[ApiController::class,'edit_profile']);
Route::post('my_profile',[ApiController::class,'my_profile']);
Route::post('other_user_profile',[ApiController::class,'other_user_profile']);
Route::post('like_dislike_profile',[ApiController::class,'like_dislike_profile']);

/*--- Feed ---*/
Route::post('create_feed',[ApiController::class,'create_feed']);
Route::post('edit_feed',[ApiController::class,'edit_feed']);
Route::post('delete_feed',[ApiController::class,'delete_feed']);
Route::post('delete_feed_media',[ApiController::class,'delete_feed_media']);

Route::post('feed_like',[ApiController::class,'feed_like']);

Route::post('create_feed_comment',[ApiController::class,'create_feed_comment']);
Route::post('edit_feed_comment',[ApiController::class,'edit_feed_comment']);
Route::post('delete_feed_comment',[ApiController::class,'delete_feed_comment']);

Route::post('create_feed_comment_reply',[ApiController::class,'create_feed_comment_reply']);
Route::post('feed_comment_like',[ApiController::class,'feed_comment_like']);

Route::post('my_feed_list',[ApiController::class,'my_feed_list']);

/*--- Business ---*/
Route::post('create_business',[ApiController::class,'create_business']);
Route::post('edit_business',[ApiController::class,'edit_business']);
Route::post('delete_business',[ApiController::class,'delete_business']);
Route::post('my_business_list',[ApiController::class,'my_business_list']);


/*--- opportunity ---*/
Route::post('create_opportunity',[ApiController::class,'create_opportunity']);
Route::post('edit_opportunity',[ApiController::class,'edit_opportunity']);
Route::post('delete_opportunity',[ApiController::class,'delete_opportunity']);
Route::post('my_opportunity_list',[ApiController::class,'my_opportunity_list']);


/*-- blog --*/
Route::post('blog_list',[ApiController::class,'blog_list']);

/*-- Business Template --*/
Route::post('template_category_list',[ApiController::class,'template_category_list']);
Route::post('template_sub_category_list',[ApiController::class,'template_sub_category_list']);

Route::post('business_category',[ApiController::class,'business_category']);

Route::post('create_business_template',[ApiController::class,'create_business_template']);
Route::post('edit_business_template',[ApiController::class,'edit_business_template']);
Route::post('delete_business_template',[ApiController::class,'delete_business_template']);
Route::post('my_business_template_list',[ApiController::class,'my_business_template_list']);

/*-- Home Page --*/
Route::post('home',[ApiController::class,'home']);

Route::post('all_opportunity_list',[ApiController::class,'all_opportunity_list']);
Route::post('all_business_list',[ApiController::class,'all_business_list']);
Route::post('all_community_list',[ApiController::class,'all_community_list']);
Route::post('all_business_template_list',[ApiController::class,'all_business_template_list']);

/*-- Community --*/

Route::post('community_user_list',[ApiController::class,'community_user_list']);

Route::post('create_community',[ApiController::class,'create_community']);
Route::post('edit_community',[ApiController::class,'edit_community']);
Route::post('delete_community',[ApiController::class,'delete_community']);
Route::post('my_community_list',[ApiController::class,'my_community_list']);
Route::post('community_detail',[ApiController::class,'community_detail']);

Route::post('create_community_question',[ApiController::class,'create_community_question']);
Route::post('community_question_detail',[ApiController::class,'community_question_detail']);

Route::post('create_community_answer',[ApiController::class,'create_community_answer']);

Route::post('delete_community_member',[ApiController::class,'delete_community_member']);
Route::post('delete_community_answer',[ApiController::class,'delete_community_answer']);


/*-- Setting  --*/

Route::post('save_chat_email_setting',[ApiController::class,'save_chat_email_setting']);

Route::post('contact_us',[ApiController::class,'contact_us']);

Route::post('add_support_question',[ApiController::class,'add_support_question']);
Route::post('support_question_list',[ApiController::class,'support_question_list']);


/*--- chat ---*/
Route::post('send_message',[ApiController::class,'send_message']);
Route::post('chat_thread_list',[ApiController::class,'chat_thread_list']);
Route::post('chat_message_list',[ApiController::class,'chat_message_list']);


/*--- Plan ---*/
Route::post('plan_list',[ApiController::class,'plan_list']);
Route::post('transaction',[ApiController::class,'transaction']);
Route::post('transaction_details',[ApiController::class,'transaction_details']);
