<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\AdminLoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Middleware\checkRegister;


use Chatify\Http\Controllers\MessagesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/** User Sign-In Routs */
 Route::get('/route-cache', function() {
     $exitCode = Artisan::call('route:cache');
     $exitCode = Artisan::call('route:clear');
     $exitCode = Artisan::call('config:cache');
     $exitCode = Artisan::call('cache:clear');
     $exitCode = Artisan::call('view:clear');
     $exitCode = Artisan::call('config:clear');
     $exitCode = Artisan::call('optimize:clear');
     return 'Routes cache cleared';
 });



// User Auth Route Group

Route::get('/', [App\Http\Controllers\frontend\HomeController::class, 'Index'])->name('home.index');

Route::get('/login',[LoginController::class, 'index'])->name('front.sign_in');

Route::post('login/process', [LoginController::class, 'frontLogin'])->name('front.signin_process');

Route::get('/register',[RegisterController::class, 'index'])->name('front.sign_up');
Route::post('register/process', [RegisterController::class, 'frontRegister'])->name('front.signup_process');


Route::get('forgot_password', [LoginController::class, 'forgotpasswordIndex'])->name('user.forgot_password.view');
Route::post('forgot_password_send', [LoginController::class, 'forgotpassword'])->name('user.forgot_password');
Route::get('reset_password/{token}', [LoginController::class, 'resetpasswordView'])->name('user.reset_password');
Route::post('reset_password/store', [LoginController::class, 'resetpassword'])->name('reset_password.store');
Route::get('resent_email', [LoginController::class, 'forgotpassword'])->name('resend_email');

Route::get('auth/facebook/callback', [LoginController::class,'handleSocialCallback'])->name('facebook.callback');

Route::get('auth/linkedin', [LoginController::class, 'linkedinRedirect']);
Route::get('auth/linkedin/callback', [LoginController::class, 'linkedinCallback']);

Route::get('/about_us', [App\Http\Controllers\frontend\HomeController::class, 'about_us'])->name('home.about_us');
Route::get('/contact_us', [App\Http\Controllers\frontend\HomeController::class, 'contact_us'])->name('home.contact_us');
Route::get('/privacy_policy', [App\Http\Controllers\frontend\HomeController::class, 'privacy_policy'])->name('home.privacy_policy');
Route::get('/terms_conditions', [App\Http\Controllers\frontend\HomeController::class, 'terms_conditions'])->name('home.terms_conditions');
Route::get('/refund_policy', [App\Http\Controllers\frontend\HomeController::class, 'refund_policy'])->name('home.refund_policy');

Route::get('/profile/{id}', [App\Http\Controllers\frontend\UserController::class, 'myProfileView'])->name('front.my_profile.index');
Route::get('/other_profile_like', [App\Http\Controllers\frontend\UserController::class, 'otherProfileLike'])->name('other.profile.like');

Route::group(['middleware' => 'auth'], function () {


    Route::get('/otp_verification',[RegisterController::class, 'otpVerification'])->name('front.otp.verification.view');
    Route::get('/send_otp',[RegisterController::class, 'sendOTP'])->name('front.otp.send');
    Route::post('otp_verification/process', [RegisterController::class, 'otpVerificationProcess'])->name('front.otp_verification.process');

    Route::get('register/skip',[RegisterController::class, 'skip'])->name('front.register.skip');

    Route::get('register/complete_profile',[RegisterController::class, 'completeProfileView'])->name('front.complete.profile');
    Route::post('register/complete_profile/process',[RegisterController::class, 'completeProfile'])->name('front.complete.profile.process');


    Route::get('plan',[App\Http\Controllers\frontend\HomeController::class, 'planView'])->name('front.plan.view');
    Route::post('plan_purchase',[App\Http\Controllers\frontend\HomeController::class, 'planPurchase'])->name('front.plan.purchase');
        
    Route::post('check/payment',[App\Http\Controllers\frontend\HomeController::class, 'checkPayment'])->name('payment');
    Route::post('payment/success',[App\Http\Controllers\frontend\HomeController::class, 'paymentSuccess'])->name('payment.success');


    /*----------------------- Home ---------------------------*/

    Route::get('/home', [App\Http\Controllers\frontend\HomeController::class, 'frontendIndex'])->name('front.index');



    //Old-----------------------------------------------------------------------------------------------------------------------------------------------

    /*my-profile*/
    Route::post('/change_profile', [App\Http\Controllers\frontend\HomeController::class, 'changeProfile'])->name('user.changeprofile');
    Route::post('/change_cover', [App\Http\Controllers\frontend\HomeController::class, 'changeCover'])->name('user.changecover');

    //Route::get('crop-profile-image', [App\Http\Controllers\frontend\HomeController::class,'cropProfileImage' ])->name('crop.profile.image');
    Route::post('crop-image', [App\Http\Controllers\frontend\HomeController::class,'cropProfileImage'])->name('crop.profile.image');
    Route::post('/crop-cover-image', [App\Http\Controllers\frontend\HomeController::class, 'cropCoverImage'])->name('crop.cover.image');

    Route::get('/upload_photos_view', [App\Http\Controllers\frontend\HomeController::class, 'uploadPhotoView'])->name('upload.photos.view');
    Route::post('/store_photos', [App\Http\Controllers\frontend\HomeController::class, 'storePhotos'])->name('store.photos');

    /* Dynamic Section */
    Route::get('/edit_section_details', [App\Http\Controllers\frontend\HomeController::class, 'editSectionDetails'])->name('edit.section_details.view');
    Route::post('/update_section_details', [App\Http\Controllers\frontend\HomeController::class, 'updateSectionDetails'])->name('update.section_details');

    Route::post('/store_post', [App\Http\Controllers\frontend\FeedController::class, 'store'])->name('store.post');
    Route::post('/delete_post', [App\Http\Controllers\frontend\FeedController::class, 'delete'])->name('delete.post');
    Route::get('load_post_view',[App\Http\Controllers\frontend\FeedController::class, 'loadPostView'])->name('load.post.view');
    Route::get('load_post_video_view',[App\Http\Controllers\frontend\FeedController::class, 'loadPostVideoView'])->name('load.post.video.view');

    Route::get('/edit_post_view', [App\Http\Controllers\frontend\FeedController::class, 'editPostView'])->name('edit.post.view');
    Route::post('/edit_post', [App\Http\Controllers\frontend\FeedController::class, 'editPost'])->name('edit.post');

    Route::post('like_post',[App\Http\Controllers\frontend\FeedController::class, 'like'])->name('like.post');
    Route::post('post_like',[App\Http\Controllers\frontend\FeedController::class, 'post_like'])->name('post.like');

    Route::post('like_post_gallery',[App\Http\Controllers\frontend\FeedController::class, 'likeGallery'])->name('like.post.gallery');
    Route::post('get_post_likes',[App\Http\Controllers\frontend\FeedController::class, 'getPostLikes'])->name('get.post.likes');

    Route::post('like_comment',[App\Http\Controllers\frontend\FeedController::class, 'likeComment'])->name('like.comment');

    Route::post('like_show',[App\Http\Controllers\frontend\FeedController::class, 'likeShow'])->name('like.show');
    Route::post('comment_like_show',[App\Http\Controllers\frontend\FeedController::class, 'commentLikeShow'])->name('comment.like.show');

    Route::post('/store_mainpost', [App\Http\Controllers\frontend\FeedController::class, 'storePost'])->name('store.mainpost');
    Route::post('/store_mainpost_profile', [App\Http\Controllers\frontend\FeedController::class, 'storePostProfile'])->name('store.mainpost.profile'); 
    
    Route::get('store/comment',[App\Http\Controllers\frontend\FeedController::class,'storeComment'])->name('store.comment');

    Route::post('ajax/image/post',[App\Http\Controllers\frontend\FeedController::class,'uploadImage'])->name('ajax.image.post');

    Route::post('show_upload_image',[App\Http\Controllers\frontend\FeedController::class,'showUploadImage'])->name('show.upload.image');
    Route::post('show_upload_video',[App\Http\Controllers\frontend\FeedController::class,'showUploadVideo'])->name('show.upload.video');

    Route::post('ajax/video/post',[App\Http\Controllers\frontend\FeedController::class,'uploadVideo'])->name('ajax.video.post');
    Route::post('ajax/document/post',[App\Http\Controllers\frontend\FeedController::class,'uploadDoc'])->name('ajax.document.post');
    
    Route::get('ajax/image/delete_img',[App\Http\Controllers\frontend\FeedController::class,'deleteImage'])->name('delete_image');
    Route::get('ajax/loadData',[App\Http\Controllers\frontend\FeedController::class,'getLoadedeData'])->name('ajax.loadData');

    Route::get('delete/comment',[App\Http\Controllers\frontend\FeedController::class,"deleteComment"])->name('delete.comment');

    Route::get('reply/comment',[App\Http\Controllers\frontend\FeedController::class,'replyComment'])->name('reply.comment');

    Route::get('edit/comment',[App\Http\Controllers\frontend\FeedController::class,'editComment'])->name('edit.comment');

    Route::post('/load_more_post', [App\Http\Controllers\frontend\FeedController::class, 'loadMorePost'])->name('load.more.post');

    Route::get("/get_search_tag",[App\Http\Controllers\frontend\FeedController::class,'autocomplete_tagSearchDetail'])->name('fetch_tags');

    Route::post('ajax/media/photo/gallery',[App\Http\Controllers\frontend\HomeController::class,'uploadPhotoProfileGallery'])->name('ajax.photo.profile.gallery');
    Route::post('ajax/media/video/gallery',[App\Http\Controllers\frontend\HomeController::class,'uploadVideoProfileGallery'])->name('ajax.video.profile.gallery');
    Route::get('ajax/media_delete/gallery/{media_id?}/{gallery_id?}',[App\Http\Controllers\frontend\HomeController::class,'deleteMediaProfileGallery'])->name('ajax.delete.media.profile.gallery');
    Route::get('ajax/video_delete/gallery/{media_id?}/{gallery_id?}',[App\Http\Controllers\frontend\HomeController::class,'deleteVideoMediaProfileGallery'])->name('ajax.video.delete.media.profile.gallery');
    Route::post('ajax/media/photo',[App\Http\Controllers\frontend\HomeController::class,'uploadPhotoProfile'])->name('ajax.photo.profile');
    Route::post('ajax/media/video',[App\Http\Controllers\frontend\HomeController::class,'uploadVideoProfile'])->name('ajax.video.profile');
    Route::post('ajax/media_delete',[App\Http\Controllers\frontend\HomeController::class,'deleteMediaProfile'])->name('ajax.delete.media.profile');

    /* Dashboard */
    Route::get("/get_search_user",[App\Http\Controllers\frontend\HomeController::class,'autocomplete_userSearchDetail'])->name('fetch_userdetails');
    Route::get('/users_searching', [App\Http\Controllers\frontend\HomeController::class, 'userSearching'])->name('user.searching');
    Route::get('/college_searching', [App\Http\Controllers\frontend\HomeController::class, 'collegeSearching'])->name('college.searching');

    Route::get("/get_tag_user_list",[App\Http\Controllers\frontend\HomeController::class,'autocomplete_tag_user'])->name('fetch_tag_user');
    Route::get("/get_hashtag",[App\Http\Controllers\frontend\HomeController::class,'autocomplete_hashtag'])->name('fetch_hashtag');

    Route::get('/follow_plan', [App\Http\Controllers\frontend\HomeController::class, 'followPlan'])->name('follow.plan');
    Route::get('/allsearch', [App\Http\Controllers\frontend\HomeController::class, 'allsearch'])->name('allsearch');
    

    Route::post('/users_searching_following', [App\Http\Controllers\frontend\HomeController::class, 'userSearchingFollow'])->name('user.searching.follow');

    Route::get('/get_user_details', [App\Http\Controllers\frontend\HomeController::class, 'getUserdetails'])->name('get.user_details');






    /*=================== Profile Related  Feed==================*/

    Route::post('profile/edit_post', [App\Http\Controllers\frontend\UserController::class, 'editPost'])->name('profile.edit.post');

    Route::post('profile/delete_post', [App\Http\Controllers\frontend\UserController::class, 'delete'])->name('profile.delete.post');

    Route::post('profile/like_post',[App\Http\Controllers\frontend\UserController::class, 'like'])->name('profile.like.post');

    Route::get('profile/store/comment',[App\Http\Controllers\frontend\UserController::class,'storeComment'])->name('profile.store.comment');

    Route::get('profile/reply/comment',[App\Http\Controllers\frontend\UserController::class,'replyComment'])->name('profile.reply.comment');

    Route::get('profile/edit/comment',[App\Http\Controllers\frontend\UserController::class,'editComment'])->name('profile.edit.comment');

    Route::get('profile/delete/comment',[App\Http\Controllers\frontend\UserController::class,"deleteComment"])->name('profile.delete.comment');

    Route::post('profile/like_comment',[App\Http\Controllers\frontend\UserController::class, 'likeComment'])->name('profile.like.comment');

    Route::post('profile/load_more_post', [App\Http\Controllers\frontend\UserController::class, 'loadMorePost'])->name('profile.load.more.post');


    //-------------------Other profile

    Route::post('other_profile/like_post',[App\Http\Controllers\frontend\UserController::class, 'otherLike'])->name('other_profile.like.post');

    Route::get('other_profile/store/comment',[App\Http\Controllers\frontend\UserController::class,'otherStoreComment'])->name('other_profile.store.comment');

    Route::get('other_profile/reply/comment',[App\Http\Controllers\frontend\UserController::class,'otherReplyComment'])->name('other_profile.reply.comment');

    Route::get('other_profile/edit/comment',[App\Http\Controllers\frontend\UserController::class,'otherEditComment'])->name('other_profile.edit.comment');

    Route::get('other_profile/delete/comment',[App\Http\Controllers\frontend\UserController::class,"otherDeleteComment"])->name('other_profile.delete.comment');

    Route::post('other_profile/like_comment',[App\Http\Controllers\frontend\UserController::class, 'otherLikeComment'])->name('other_profile.like.comment');

    Route::post('other_profile/load_more_post', [App\Http\Controllers\frontend\UserController::class, 'otherLoadMorePost'])->name('other_profile.load.more.post');



    Route::get('/edit_name_view', [App\Http\Controllers\frontend\UserController::class, 'editNameView'])->name('edit.name.view');
    Route::post('/update_name', [App\Http\Controllers\frontend\UserController::class, 'updateName'])->name('update.name');

    Route::get('/edit_info_view', [App\Http\Controllers\frontend\UserController::class, 'editInfoView'])->name('edit.info.view');
    Route::post('/update_info', [App\Http\Controllers\frontend\UserController::class, 'updateInfo'])->name('update.info');

















    /*----------------------- opportunity ---------------------------*/

    Route::get('/opportunity', [App\Http\Controllers\frontend\OpportunityController::class, 'index'])->name('front.opportunity.index');
    Route::get('/opportunity/create', [App\Http\Controllers\frontend\OpportunityController::class, 'create'])->name('front.opportunity.create');
    Route::post('/opportunity/store', [App\Http\Controllers\frontend\OpportunityController::class, 'store'])->name('front.opportunity.store');
    Route::get('/opportunity/edit/{id}', [App\Http\Controllers\frontend\OpportunityController::class, 'edit'])->name('front.opportunity.edit');
    Route::post('/opportunity/update/{id}', [App\Http\Controllers\frontend\OpportunityController::class, 'update'])->name('front.opportunity.update');
    Route::post('/opportunity/delete/{id}', [App\Http\Controllers\frontend\OpportunityController::class, 'delete'])->name('front.opportunity.delete');
    Route::get('/opportunity/view/{id?}', [App\Http\Controllers\frontend\OpportunityController::class, 'opportunityView'])->name('front.opportunity.view');

    /*----------------------- business ---------------------------*/

    Route::get('/business', [App\Http\Controllers\frontend\BusinessController::class, 'index'])->name('front.business.index');
    Route::get('/business/create', [App\Http\Controllers\frontend\BusinessController::class, 'create'])->name('front.business.create');
    Route::post('/business/store', [App\Http\Controllers\frontend\BusinessController::class, 'store'])->name('front.business.store');
    Route::get('/business/edit/{id}', [App\Http\Controllers\frontend\BusinessController::class, 'edit'])->name('front.business.edit');
    Route::post('/business/update/{id}', [App\Http\Controllers\frontend\BusinessController::class, 'update'])->name('front.business.update');
    Route::post('/business/delete/{id}', [App\Http\Controllers\frontend\BusinessController::class, 'delete'])->name('front.business.delete');
    Route::get('/business/view/{id?}', [App\Http\Controllers\frontend\BusinessController::class, 'businessView'])->name('front.business.view');

    /*----------------------- business-template ---------------------------*/

    Route::get('/business_template', [App\Http\Controllers\frontend\BusinessTemplateController::class, 'index'])->name('front.business_template.index');
    Route::get('/business_template/create', [App\Http\Controllers\frontend\BusinessTemplateController::class, 'create'])->name('front.business_template.create');
    Route::post('/business_template/store', [App\Http\Controllers\frontend\BusinessTemplateController::class, 'store'])->name('front.business_template.store');
    Route::get('/business_template/edit/{id}', [App\Http\Controllers\frontend\BusinessTemplateController::class, 'edit'])->name('front.business_template.edit');
    Route::post('/business_template/update/{id}', [App\Http\Controllers\frontend\BusinessTemplateController::class, 'update'])->name('front.business_template.update');
    Route::post('/business_template/delete/{id}', [App\Http\Controllers\frontend\BusinessTemplateController::class, 'delete'])->name('front.business_template.delete');
    Route::get('/business_template/view/{id?}', [App\Http\Controllers\frontend\BusinessTemplateController::class, 'businessTemplateView'])->name('front.business_template.view');

    Route::post('/get_business_template_sub_category/', [App\Http\Controllers\frontend\BusinessTemplateController::class, 'getBusinessTemplateSubCategory'])->name('front.get.get_business_template_sub_category');
    Route::post('/get_business_template_sub_category_edit/', [App\Http\Controllers\frontend\BusinessTemplateController::class, 'getBusinessTemplateSubCategoryEdit'])->name('front.get.get_business_template_sub_category.edit');
    Route::post('/get_business_template_fields/', [App\Http\Controllers\frontend\BusinessTemplateController::class, 'getBusinessTemplateField'])->name('front.get.business_template.field');

    /*----------------------- Blog ---------------------------*/

    Route::get('/blog', [App\Http\Controllers\frontend\BlogController::class, 'index'])->name('front.blog.index');
    Route::get('/blog/view/{id?}', [App\Http\Controllers\frontend\BlogController::class, 'blogView'])->name('front.blog.view');


    /*----------------------- Profile ---------------------------*/

    Route::post('crop-image', [App\Http\Controllers\frontend\UserController::class,'cropProfileImage'])->name('crop.profile.image');
    Route::post('/crop-cover-image', [App\Http\Controllers\frontend\UserController::class, 'cropCoverImage'])->name('crop.cover.image');


    Route::get('/my_profile_like', [App\Http\Controllers\frontend\UserController::class, 'myProfileLike'])->name('my.profile.like');


    /*----------------------- Setting ---------------------------*/

    Route::get('/settings', [App\Http\Controllers\frontend\SettingController::class, 'index'])->name('front.setting.index');

    Route::post('change_password',[App\Http\Controllers\frontend\SettingController::class,'changePassword'])->name('change.password');

    Route::post('store_account_setting',[App\Http\Controllers\frontend\SettingController::class,'storeAccountSetting'])->name('store.account.setting');

    Route::post('setting/contact_us',[App\Http\Controllers\frontend\SettingController::class,'storeContactUs'])->name('setting.store.contactus');

    /*----------------------- Support ---------------------------*/

    Route::get('/support', [App\Http\Controllers\frontend\SupportController::class, 'index'])->name('front.support.index');
    Route::get('/support_create', [App\Http\Controllers\frontend\SupportController::class, 'create'])->name('front.support.create');
    Route::post('/support/store_question', [App\Http\Controllers\frontend\SupportController::class, 'store'])->name('front.support.store');


    /*----------------------- Notification ---------------------------*/

    Route::get('/notifications', [App\Http\Controllers\frontend\NotificationController::class, 'index'])->name('front.notification.index');


    /*----------------------- Community ---------------------------*/

    Route::get('/community', [App\Http\Controllers\frontend\CommunityController::class, 'index'])->name('front.community.index');
    Route::get('/community/create', [App\Http\Controllers\frontend\CommunityController::class, 'create'])->name('front.community.create');
    Route::post('/community/store', [App\Http\Controllers\frontend\CommunityController::class, 'store'])->name('front.community.store');

    Route::get('/community/edit/{id}', [App\Http\Controllers\frontend\CommunityController::class, 'edit'])->name('front.community.edit');
    Route::post('/community/update/{id}', [App\Http\Controllers\frontend\CommunityController::class, 'update'])->name('front.community.update');
    Route::post('/community/delete/{id}', [App\Http\Controllers\frontend\CommunityController::class, 'delete'])->name('front.community.delete');
    Route::get('/community/view/{id?}', [App\Http\Controllers\frontend\CommunityController::class, 'communityView'])->name('front.community.view');

    Route::post('/community/delete_member/{community_id?}/{user_id?}', [App\Http\Controllers\frontend\CommunityController::class, 'deleteCommunityMember'])
    ->name('front.community_member.delete');

    Route::get('/community/add/question/{community_id?}', [App\Http\Controllers\frontend\CommunityController::class, 'createCommunityQuestion'])->name('front.community_question.create');
    Route::post('/community/store/question/{community_id?}', [App\Http\Controllers\frontend\CommunityController::class, 'storeCommunityQuestion'])->name('front.community_question.store');
    Route::get('/community/question/view/{community_id}/{question_id}', [App\Http\Controllers\frontend\CommunityController::class, 'communityQuestionView'])
    ->name('front.community_question.view');

    Route::get('/community/add/answer/{community_id}/{question_id}', [App\Http\Controllers\frontend\CommunityController::class, 'createCommunityAnswer'])->name('front.community_answer.create');
    Route::post('/community/store/answer/{community_id}/{question_id}', [App\Http\Controllers\frontend\CommunityController::class, 'storeCommunityAnswer'])->name('front.community_answer.store');
    Route::post('/community/delete/answer/{community_id}/{question_id}/{answer_id?}', [App\Http\Controllers\frontend\CommunityController::class, 'deleteCommunityAnswer'])
    ->name('front.community.answer.delete');

    Route::get('/add_answer_view', [App\Http\Controllers\frontend\CommunityController::class, 'editAnswerView'])->name('edit.answer.view');
    Route::post('/store_answer', [App\Http\Controllers\frontend\CommunityController::class, 'storeAnswer'])->name('store.answer');


    /*----------------------- chat ---------------------------*/

    Route::get('/messages', [MessagesController::class,'index'])->name(config('chatify.path'));
    // Route::post('/sendMessage',[MessagesController::class,'send'])->name('send.message');
    // Route::post('/updateSettings', [MessagesController::class,'updateSettings'])->name('avatar.update');
    // Route::post('/chat/auth',[MessagesController::class,'pusherAuth'])->name('pusher.auth');
    /*logout*/
    Route::get('user/logout', [LoginController::class, 'logout'])->name('front.logout'); 

});

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', [AdminLoginController::class, 'showLoginPage'])->name('admin.login');
    Route::post('login/process', [AdminLoginController::class, 'adminLogin'] )->name('admin.login_process');
    
    Route::group(['middleware' => 'auth:admin'], function () {

        /** Dashbord Route */
        Route::get('/dashbord', [App\Http\Controllers\backend\DashboardController::class, 'index'])->name('admin.dashbord.view');


        /*-- Manage User --*/
        Route::get('/user',[App\Http\Controllers\backend\UserController::class,'index'])->name('admin.user.index');

        Route::get('/user/create',[App\Http\Controllers\backend\UserController::class,'create'])->name('admin.user.create');
        Route::post('/user/store',[App\Http\Controllers\backend\UserController::class,'store'])->name('admin.user.store');
        Route::get('/user/edit/{id}',[App\Http\Controllers\backend\UserController::class,'edit'])->name('admin.user.edit');
        Route::post('/user/update/{id}', [App\Http\Controllers\backend\UserController::class, 'update'])->name('admin.user.update');
        Route::post('/user/delete/{id}',[App\Http\Controllers\backend\UserController::class,'delete'])->name('admin.user.delete');

        Route::get('/user_detail/{id}',[App\Http\Controllers\backend\UserController::class,'userView'])->name('admin.user_detail.view');
        Route::get('user/product/{user_id}',[App\Http\Controllers\backend\UserController::class,'userProductIndex'])->name('admin.user_product.index');


        /*-- Manage Opportunities --*/

        Route::get('/opportunity/',[App\Http\Controllers\backend\OpportunityController::class,'index'])->name('admin.opportunity.index');
        Route::post('/opportunity/delete/{id}',[App\Http\Controllers\backend\OpportunityController::class,'delete'])->name('admin.opportunity.delete');

        Route::get('/opportunity/change_status/', [App\Http\Controllers\backend\OpportunityController::class, 'changeStatus'])->name('admin.opportunity.change_status');

        /*-- Manage Feed --*/

        Route::get('/feed/',[App\Http\Controllers\backend\FeedController::class,'index'])->name('admin.feed.index');
        Route::post('/feed/delete/{id}',[App\Http\Controllers\backend\FeedController::class,'delete'])->name('admin.feed.delete');


        /*-- Manage Business --*/

        Route::get('/business/',[App\Http\Controllers\backend\BusinessController::class,'index'])->name('admin.business.index');
        Route::get('/business/delete/{id}',[App\Http\Controllers\backend\BusinessController::class,'delete'])->name('admin.business.delete');

        Route::get('/business/change_status/', [App\Http\Controllers\backend\BusinessController::class, 'changeStatus'])->name('admin.business.change_status');


        /*-- Manage Business --*/

        Route::get('/business_template/',[App\Http\Controllers\backend\BusinessTemplateController::class,'index'])->name('admin.business_template.index');
        Route::post('/business_template/delete/{id}',[App\Http\Controllers\backend\BusinessTemplateController::class,'delete'])->name('admin.business_template.delete');

        Route::get('/business_template/change_status/', [App\Http\Controllers\backend\BusinessTemplateController::class, 'changeStatus'])->name('admin.business_template.change_status');



        /*-- Manage Business Category --*/

        Route::get('/business_category',[App\Http\Controllers\backend\BusinessCategoryController::class,'index'])->name('admin.business_category.index');
        Route::get('/business_category/create',[App\Http\Controllers\backend\BusinessCategoryController::class,'create'])->name('admin.business_category.create');
        Route::post('/business_category/store',[App\Http\Controllers\backend\BusinessCategoryController::class,'store'])->name('admin.business_category.store');
        Route::get('/business_category/edit/{id}',[App\Http\Controllers\backend\BusinessCategoryController::class,'edit'])->name('admin.business_category.edit');
        Route::post('/business_category/update/{id}', [App\Http\Controllers\backend\BusinessCategoryController::class, 'update'])->name('admin.business_category.update');
        Route::post('/business_category/delete/{id}',[App\Http\Controllers\backend\BusinessCategoryController::class,'delete'])->name('admin.business_category.delete');

        /*-- Template Category --*/
        Route::get('/template_category',[App\Http\Controllers\backend\TemplateCategoryController::class,'index'])->name('admin.template_category.index');
        Route::get('/template_category/create',[App\Http\Controllers\backend\TemplateCategoryController::class,'create'])->name('admin.template_category.create');
        Route::post('/template_category/store',[App\Http\Controllers\backend\TemplateCategoryController::class,'store'])->name('admin.template_category.store');
        Route::get('/template_category/edit/{id}',[App\Http\Controllers\backend\TemplateCategoryController::class,'edit'])->name('admin.template_category.edit');
        Route::post('/template_category/update/{id}', [App\Http\Controllers\backend\TemplateCategoryController::class, 'update'])->name('admin.template_category.update');
        Route::post('/template_category/delete/{id}',[App\Http\Controllers\backend\TemplateCategoryController::class,'delete'])->name('admin.template_category.delete');


        /*-- Template Sub Category --*/
        Route::get('/template_sub_category',[App\Http\Controllers\backend\TemplateSubCategoryController::class,'index'])->name('admin.template_sub_category.index');
        Route::get('/template_sub_category/create',[App\Http\Controllers\backend\TemplateSubCategoryController::class,'create'])->name('admin.template_sub_category.create');
        Route::post('/template_sub_category/store',[App\Http\Controllers\backend\TemplateSubCategoryController::class,'store'])->name('admin.template_sub_category.store');
        Route::get('/template_sub_category/edit/{id}',[App\Http\Controllers\backend\TemplateSubCategoryController::class,'edit'])->name('admin.template_sub_category.edit');
        Route::post('/template_sub_category/update/{id}', [App\Http\Controllers\backend\TemplateSubCategoryController::class, 'update'])->name('admin.template_sub_category.update');
        Route::post('/template_sub_category/delete/{id}',[App\Http\Controllers\backend\TemplateSubCategoryController::class,'delete'])->name('admin.template_sub_category.delete');

        /*-- Subscription Plans --*/

        Route::get('/plan',[App\Http\Controllers\backend\PlanController::class,'index'])->name('admin.plan.index');
        Route::get('/plan/create',[App\Http\Controllers\backend\PlanController::class,'create'])->name('admin.plan.create');
        Route::post('/plan/store',[App\Http\Controllers\backend\PlanController::class,'store'])->name('admin.plan.store');
        Route::get('/plan/edit/{id}',[App\Http\Controllers\backend\PlanController::class,'edit'])->name('admin.plan.edit');
        Route::post('/plan/update/{id}', [App\Http\Controllers\backend\PlanController::class, 'update'])->name('admin.plan.update');
        Route::post('/plan/delete/{id}',[App\Http\Controllers\backend\PlanController::class,'delete'])->name('admin.plan.delete');


        /*-- Blogs --*/

        Route::get('/blog',[App\Http\Controllers\backend\BlogController::class,'index'])->name('admin.blog.index');
        Route::get('/blog/create',[App\Http\Controllers\backend\BlogController::class,'create'])->name('admin.blog.create');
        Route::post('/blog/store',[App\Http\Controllers\backend\BlogController::class,'store'])->name('admin.blog.store');
        Route::get('/blog/edit/{id}',[App\Http\Controllers\backend\BlogController::class,'edit'])->name('admin.blog.edit');
        Route::post('/blog/update/{id}', [App\Http\Controllers\backend\BlogController::class, 'update'])->name('admin.blog.update');
        Route::post('/blog/delete/{id}',[App\Http\Controllers\backend\BlogController::class,'delete'])->name('admin.blog.delete');

        Route::post('/blog/delete_media', [App\Http\Controllers\backend\BlogController::class, 'deleteMedia'])->name('admin.blog.delete_media');

        /*-- support --*/

        Route::get('/support',[App\Http\Controllers\backend\SupportController::class,'index'])->name('admin.support.index');
        Route::get('/support/answer/{id}',[App\Http\Controllers\backend\SupportController::class,'create'])->name('admin.support.answer');
        Route::post('/support/answer_update/{id}',[App\Http\Controllers\backend\SupportController::class,'answerUpdate'])->name('admin.support.answer_store');
        Route::post('/support/delete/{id}',[App\Http\Controllers\backend\SupportController::class,'delete'])->name('admin.support.delete');


        /*-- Contact Us --*/

        Route::get('/contact_us',[App\Http\Controllers\backend\ContactUsController::class,'index'])->name('admin.contact_us.index');
        Route::get('/contact_us/edit/{id}',[App\Http\Controllers\backend\ContactUsController::class,'edit'])->name('admin.contact_us.edit');
        Route::post('/contact_us/update/{id}', [App\Http\Controllers\backend\ContactUsController::class, 'update'])->name('admin.contact_us.update');
        Route::post('/contact_us/delete/{id}',[App\Http\Controllers\backend\ContactUsController::class,'delete'])->name('admin.contact_us.delete');


        /*-- Platform  --*/
        Route::get('/platform',[App\Http\Controllers\backend\PlatformController::class,'index'])->name('admin.platform.index');
        Route::get('/platform/create',[App\Http\Controllers\backend\PlatformController::class,'create'])->name('admin.platform.create');
        Route::post('/platform/store',[App\Http\Controllers\backend\PlatformController::class,'store'])->name('admin.platform.store');
        Route::get('/platform/edit/{id}',[App\Http\Controllers\backend\PlatformController::class,'edit'])->name('admin.platform.edit');
        Route::post('/platform/update/{id}', [App\Http\Controllers\backend\PlatformController::class, 'update'])->name('admin.platform.update');
        Route::post('/platform/delete/{id}',[App\Http\Controllers\backend\PlatformController::class,'delete'])->name('admin.platform.delete');


        /*-- Banner  --*/
        Route::get('/banner',[App\Http\Controllers\backend\BannerController::class,'index'])->name('admin.banner.index');
        Route::get('/banner/create',[App\Http\Controllers\backend\BannerController::class,'create'])->name('admin.banner.create');
        Route::post('/banner/store',[App\Http\Controllers\backend\BannerController::class,'store'])->name('admin.banner.store');
        Route::get('/banner/edit/{id}',[App\Http\Controllers\backend\BannerController::class,'edit'])->name('admin.banner.edit');
        Route::post('/banner/update/{id}', [App\Http\Controllers\backend\BannerController::class, 'update'])->name('admin.banner.update');
        Route::post('/banner/delete/{id}',[App\Http\Controllers\backend\BannerController::class,'delete'])->name('admin.banner.delete');


        /*-- testimonial  --*/
        Route::get('/testimonial',[App\Http\Controllers\backend\TestimonialController::class,'index'])->name('admin.testimonial.index');
        Route::get('/testimonial/create',[App\Http\Controllers\backend\TestimonialController::class,'create'])->name('admin.testimonial.create');
        Route::post('/testimonial/store',[App\Http\Controllers\backend\TestimonialController::class,'store'])->name('admin.testimonial.store');
        Route::get('/testimonial/edit/{id}',[App\Http\Controllers\backend\TestimonialController::class,'edit'])->name('admin.testimonial.edit');
        Route::post('/testimonial/update/{id}', [App\Http\Controllers\backend\TestimonialController::class, 'update'])->name('admin.testimonial.update');
        Route::post('/testimonial/delete/{id}',[App\Http\Controllers\backend\TestimonialController::class,'delete'])->name('admin.testimonial.delete');



        /** Logout Route */
        Route::get('admin/logout', [AdminLoginController::class, 'adminLogout'])->name('admin.logout');



        // Route::post('/category/change_status/{id}',[App\Http\Controllers\backend\CategoryController::class,'changeStatus'])->name('admin.category.change_status');

    });
});


