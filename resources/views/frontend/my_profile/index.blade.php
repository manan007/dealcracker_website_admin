@extends('frontend.layouts.main')
@section('title','My Profile')
@section('css')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css"/>
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/croppie.min.css') }}">

<link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{ asset('assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

<style>
.radioButton label,
.radioButton input {
display: block;
position: absolute;
top: 0;
left: 0;
right: 0;
bottom: 0;
}

.radioButton {
	/*float: left;*/
	/*margin: 0 5px 0 0;*/
	width: 100%;
	height: 40px;
	position: relative;
}

.btn:hover
{
    color: #464646;
    text-decoration: none;
}

.radioButton input[type="radio"] {
opacity: 0.011;
z-index: 100;
}

.radioButton input[type="checkbox"] {
opacity: 0.011;
z-index: 100;
}

.radioButton input[type="radio"]:checked + label {
background: #1E2C75;
color: #ffffff;
border-radius: 4px;
}

.radioButton input[type="checkbox"]:checked + label {
background: #1E2C75;
color: #ffffff;
border-radius: 4px;
}

.radioButton label {
color:#000;
background: #d3d3d3;
border-radius: 4px;
}
.cover_image{
	background: #fff;
	border: 2px solid #1E2C75;
	border-radius: 3px;
	color: #1E2C75;
	cursor: pointer;
	display: inline-block;
	font-size: 15px;
	font-weight: 600;
	outline: none;
	padding: 12px 20px;
	position: relative;
	transition: all 0.3s;
	vertical-align: middle;
	margin: 0;
	float: right;
	text-transform: uppercase;
}
</style>
@endsection

@section('content')


<!-- <section class="cover-sec">
	<img src="{{ asset('frontend/images/resources/cover-img.jpg')}}" alt="">
	<div class="add-pic-box">
		<div class="container">
			<div class="row no-gutters">
				<div class="col-lg-12 col-sm-12">					
					<input type="file" id="file">
					<label for="file">Change Image</label>				
				</div>
			</div>
		</div>
	</div>
</section> -->

<section class="cover-sec">
	@if(Auth::user()->cover_image != null)
    	<img src="{{ asset('upload/cover_image/'.Auth::user()->cover_image)}}" alt="" height="400px">
	@else	
		<img src="{{ asset('frontend/images/resources/cover-img.jpg') }}" alt="">
	@endif
	<div class="add-pic-box">
		<div class="container">
			<div class="row no-gutters">
				<div class="col-lg-12 col-sm-12">					
					<button id="cover_image" class="cover_image">Change Image</button>
					<!-- <input type="file" name="cover_image"  class="cover_image"> -->
							<!-- <label for="cover_image" id="cover_image" class="cover_image">Change Image</label>				 -->
				</div>
			</div>
		</div>
	</div>
</section>

@include('frontend.home.dynamic_like_form')
@include('frontend.home.dynamic_post_form')
@include('frontend.home.dynamic_post_video_form')
@include('frontend.home.dynamic_edit_post_form')

@include('frontend.home.dynamic_upload_form')
@include('frontend.home.dynamic_upload_video_form')
@include('frontend.home.dynamic_follow_plan_form')
@include('frontend.home.dynamic_comment_like_form')
@include('frontend.home.dynamic_share_news_feed_form')

@include('frontend.my_profile.information.dynamic_info_form')
@include('frontend.my_profile.dynamic_name_form')

<main>
	<div class="main-section">
		<div class="container">
			<div class="main-section-data">
				<div class="row">

					<input type="hidden" name="userid" id="userid" value="{{ Auth::user()->id }}">
					<!-- ======================================================= main-left-sidebar start  ===============================================================-->
					<div class="col-lg-3">
						<div class="main-left-sidebar">
							<div class="user_profile">
								<div class="user-pro-img">
						            @if(Auth::user()->profile != null)
						            <img src="{{ asset('upload/profile/'.Auth::user()->profile)}}" alt="">
						            @else
						            <img src="{{ asset('frontend/images/user.jpg')}}" alt="">
						            @endif
						            <!-- <img src="{{ asset('assets/frontend/images/resources/user-pro-img.png') }}" alt=""> -->
						            <div class="add-dp" id="OpenImgUpload">
						                <!-- <input type="file" id="file"> -->
						                <!-- <input type="file" name="image" id="image_profile" class="image_profile"> -->
						                <label for="image_profile" class="image_profile"><i class="fas fa-camera"></i></label>												
						            </div>
						        </div><!--user-pro-img end-->
						        @php 
						        	$profile_likes = App\Models\ProfileLike::where(['friend_id' => Auth::user()->id,'status' => 1])->count();
            						$profile_dislikes = App\Models\ProfileLike::where(['friend_id' => Auth::user()->id,'status' => 2])->count();

            						$my_profile = App\Models\ProfileLike::where(['user_id' => Auth::user()->id,'friend_id' => Auth::user()->id])->first();	
						       	@endphp
						    {{--  
								<div class="user_pro_status">
									<ul class="flw-status">
										<li>
									        <div class="radioButton w-50">
									            <input type="radio" id="like" name="profile_review" class="specializations profile_review" value="Like" @if($my_profile) 
									            @if($my_profile->status == 1) checked="" @endif @endif />
									            <label class="btn btn-default" for="like"><i class="fa fa-thumbs-up"></i></label>
									        </div>
									    </li>

										<li>
											<div class="radioButton w-50">
									            <input type="radio" id="dislike" name="profile_review" class="specializations profile_review" value="Like" @if($my_profile) 
									            @if($my_profile->status == 2) checked="" @endif @endif />
									            <label class="btn btn-default" for="dislike"><i class="fa fa-thumbs-down"></i></label>
									        </div>
									    </li>
									</ul>
								</div>
								--}}
								<div class="user_pro_status" style="padding-top: 27px;">
									<ul class="flw-status">
										<li>
											<span>Likes</span>
											<span class="font-weight-bold" id="profile_likes">{{ $profile_likes ?? 0 }}</span>
										</li>
										<li>
											<span>Dislikes</span>
											<span class="font-weight-bold" id="profile_dislikes">{{ $profile_dislikes ?? 0 }}</span>
										</li>
									</ul>
								</div><!--user_pro_status end-->
								
							</div><!--user_profile end-->
							
						</div>
					</div>
					<!-- ======================================================= main-left-sidebar end  ===============================================================-->


					<!-- ======================================================= main-center-section start  ===============================================================-->
					<div class="col-lg-6 user_profile_main_center_section">
						<div class="main-ws-sec">
							<div class="user-tab-sec rewivew">
								
								<input type="hidden" name="request_segment" id="request_segment" value="{{ \Request::segment(2) }}">
								<div class="dynamic_name_section">
									@include('frontend.my_profile.dynamic_name_section')
								</div>

                                <div class="tab-feed st2 settingjb main_heading_icons">
									<ul>
										<li data-tab="feed-dd" class="active">
											<a href="#" title="">
												<i class="fa fa-flag" aria-hidden="true"></i>
												<span>Feed</span>
											</a>
										</li>
										<li data-tab="info-dd">
											<a href="#" title="">
												<i class="fa fa-info-circle" aria-hidden="true"></i>
												<span>Info</span>
											</a>
										</li>
									</ul>
								</div>
							</div>

							<div class="product-feed-tab current" id="feed-dd">
								<div class="posts-section">
									<div class="dynamic_post">
										@include('frontend.home.dynamic_post')
									</div>
								</div>
							</div>

							<div class="product-feed-tab" id="info-dd">
								<div class="user-profile-ov">
									<h3><a href="#" title="" class="overview-open">Information</a>
											@if(\Request::segment(2) == Auth::user()->id)<a href="javascript:;" title="Edit Information" class="edit-info"><i class="fa fa-pencil"></i></a>@endif
									</h3>
									<div class="dynamic_info">
										@include('frontend.my_profile.information.index')
									</div>
								</div>
							</div>

						</div>
					</div>
					<!-- ======================================================= main-center-section end  ===============================================================-->


					<!-- ======================================================= main-right-sidebar start  ===============================================================-->
					<div class="col-lg-3">
						<div class="right-sidebar">
							<!-- <div class="message-btn">
								<a href="profile-account-setting.html" title=""><i class="fas fa-cog"></i> Setting</a>
							</div> -->
							
						</div>
					</div>
					<!-- ======================================================= main-right-sidebar end  ===============================================================-->
				</div>
			</div>
		</div> 
	</div>
</main>


<div class="modal fade" id="crop_profile_image_model" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-light" id="modalLabel">Crop Profile Image</h5>
				<button type="button" class="close p-0 m-0 text-light" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="la la-close"></i></span></button>
			</div>
			<div class="modal-body">	
				<div class="row">
					<div class="col-md-4" style="padding:1%;visibility:hidden;">
						<strong>Choose image to crop:</strong>
						<input type="file" id="image_file">
						<div class="alert alert-success" id="upload-success" style="display: none;margin-top:10px;"></div>
					</div>
					<div class="col-md-12 text-center">
						<div id="upload-demo"></div>
					</div>
					

					<!--<div class="col-md-4">
						<div id="preview-crop-image" style="background:#9d9d9d;width:300px;padding:50px 50px;height:300px;"></div>
					</div>-->
				</div>				
			</div>
			<div class="modal-footer">
			<button class="btn1-primary upload-image">Upload Image</button>
				<button type="button" class="btn1-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="modal_cover_image" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-light" id="modalLabel">Crop Profile Image</h5>
				<button type="button" class="close p-0 m-0 text-light" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="la la-close"></i></span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4" style="padding:1%;visibility:hidden;">
						<strong>Choose image to crop:</strong>
						<input type="file" id="image_file_cover">
						<div class="alert alert-success" id="upload-success" style="display: none;margin-top:10px;"></div>
					</div>
					<div class="col-md-12 text-center">
						<div id="upload-demo-cover"></div>
					</div>
				</div>				
			</div>
			<div class="modal-footer">
				<button class="btn1-primary upload-image-cover">Upload Image</button>
				<button type="button" class="btn1-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

@endsection




@section('scripts')
<script type="text/javascript" src="{{ asset('frontend/js/croppie.js') }}"></script>

<script src="{{ asset('assets/plugins/select2/js/select2.full.min.js')}}"></script>

<script type="text/javascript">
  $('.select2').select2();
</script>

<script type="text/javascript">

	$(document).on('click','.post_collapse',function(){
		var post_id = $(this).attr('data-post-id');
		if($( "#ed-options-"+post_id ).hasClass( "active" )){
			$('#ed-options-'+post_id).removeClass('active');
		}
		else{
			$('#ed-options-'+post_id).addClass('active');
		}
	});

	function clearData()
	{
		$('#post_text').val("");
		$('#create_post_form_video').trigger('reset');
		$('#create_post_form_document').trigger('reset');
		$('#create_post_form_media').trigger('reset');
		$('#create_post_form').trigger('reset');
		$('.post_media').html("");
	}


	$(document).on("change",".profile_review",function(){
		var getid = $(this).attr('id');
		$.ajax({
			url: "{{route('my.profile.like')}}",
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			type: "GET",
			data: {"id":getid},
			success: function (data) {
				$('.profile_review').prop('checked',false);
				$('#'+getid).prop('checked',true);
				$('#profile_likes').text(data.profile_likes);
				$('#profile_dislikes').text(data.profile_dislikes);
			}
		});
	});


	$(document).on("click", ".image_profile", function(){		
		$('#crop_profile_image_model').modal('show');
		$('#image_file').trigger('click');
	});
	
	
	$(document).on('hidden.bs.modal','#crop_profile_image_model', function () {
		$('#image_profile').val('');	
	});

	//crop profile image..

	$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
	});
	var resize = $('#upload-demo').croppie({
		enableExif: true,
		enableOrientation: true,    
		viewport: { // Default { width: 100, height: 100, type: 'square' } 
			width: 200,
			height: 200,
			type: 'circle' //square
		},
		boundary: {
			width: 300,
			height: 300
		}
	});

	$('#image_file').on('change', function () { 
		var reader = new FileReader();
		reader.onload = function (e) {
		resize.croppie('bind',{
			url: e.target.result
		}).then(function(){
			console.log('jQuery bind complete');
		});
		}
		reader.readAsDataURL(this.files[0]);
	});


	$('.upload-image').on('click', function (ev) {
		resize.croppie('result', {
			type: 'canvas',
			size: 'viewport'
		}).then(function (img) {
			html = '<img src="' + img + '" />';
			$("#preview-crop-image").html(html);
			$.ajax({
				url: "{{route('crop.profile.image')}}",
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				type: "POST",
				data: {
					"image":img
				},
				success: function (data) {
					$('#crop_profile_image_model').modal('hide');
					location.reload();
				}
			});
		});
	});


	$(document).on("click", ".cover_image", function(){
		$('#modal_cover_image').modal('show');
		$('#image_file_cover').trigger('click');
	});


	//crop profile image..
	$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
	});
	var resize1 = $('#upload-demo-cover').croppie({
		enableExif: true,
		enableOrientation: true,    
		viewport: { // Default { width: 100, height: 100, type: 'square' } 
			width: 900,
			height: 200,
			type: 'square' //square
		},
		boundary: {
			width: 1000,
			height: 300
		}
	});

	$('#image_file_cover').on('change', function () { 
		var reader = new FileReader();
		reader.onload = function (e) {
		resize1.croppie('bind',{
			url: e.target.result
		}).then(function(){
			console.log('jQuery bind complete');
		});
		}
		reader.readAsDataURL(this.files[0]);
	});


	$('.upload-image-cover').on('click', function (ev) {
	
		resize1.croppie('result', {
			type: 'canvas',
			size: 'viewport'
		}).then(function (img) {
			html = '<img src="' + img + '" />';
			// $("#preview-crop-image").html(html);
			//$("#upload-success").html("Images cropped and uploaded successfully.");
			//$("#upload-success").show();
			$.ajax({
			url: "{{route('crop.cover.image')}}",
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			type: "POST",
			data: {"image":img},
			success: function (data) {
				$('#modal_cover_image').modal('hide');
				location.reload();
			}
			});
		});
	});	



	/*---- Edit Post */

	$(document).on('click','.edit_post',function(){

		var post_id = $(this).attr('data-post-id');
		var url="{{ route('edit.post.view') }}";

		$.ajax({
			url: url,
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			},
			data: {
			"post_id":post_id,
			},
			type: "GET",
			success: function(result) {
				$('#update_post_id_'+result.postdata.id).val(result.postdata.id);
				$('.update_post').attr('id', 'update_post_'+result.postdata.id);
				$('#update_post_'+result.postdata.id).val(result.postdata.id);
				$('.edit_post_data').attr('id', 'edit_post_data_'+result.postdata.id);
				$('#edit_post_data_'+result.postdata.id).html(result.html);
				$('#editpost_view').modal({show:true});
			} 
		});
	});

	$(document).on('click','.update_post',function(){

		var id = $(this).attr('id');
		var post_id = $('#'+id).val();
		var url="{{ route('profile.edit.post') }}";
		var userid = $('#userid').val();
		//var post_text = $('#post_text_'+post_id).val();
		
		var post_text = $('#post_text_'+post_id).html();
		var post_title = $('#post_title_'+post_id).val();

		if(post_id != 0){
			$.ajax({
				url: url,
				headers: {
					'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
				},
				data: {
				"post_id":post_id,
				"post_text":post_text,
				"post_title":post_title,
				"userid":userid,
				},
				type: "POST",
				success: function(result) {
					//$('#edit_post_data').html(result.html);
					$('#editpost_view').modal('hide');
					$('.dynamic_post').html(result.html);
					clearData();
				} 
			});
		}

		
	});


	$(document).on('click','.delete_post',function(){
	
		var post_id = $(this).attr('data-post-id');
		var userid = $('#userid').val();
		var url="{{ route('profile.delete.post') }}";

		bootbox.dialog({
			message: "Are you sure you want to delete this feed ?",
			title: "Delete Feed",
			onEscape: function() {},
			show: true,
			backdrop: true,
			closeButton: true,
			animate: true,
			className: "sartpost",
			buttons: {
				"Danger!": {
					label:"No",
					className: "btn-cancel",
					callback: function() {}
				},
				success: {   
					label: "Yes",
					className: "btn-default1 text-right",
					callback: function() {
						$.ajax({
							url: url,
							headers: {
								'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
							},
							data: {
								"post_id":post_id,
								"userid":userid,		
							},
							type: 'POST',
							success: function(data) {
								$('.dynamic_post').html(data.html);
								$('.modal').modal('hide');
								clearData();
							}
						});
					},	
				}
			}
		});

	});



	$(document).on('click','.create_like',function(){
	
		var post_id = $(this).attr('data-post-id');
		var userid = $('#userid').val();
		var url="{{ route('profile.like.post') }}";
		$.ajax({
			url: url,
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			},
			data: {
				"post_id":post_id,
				"userid":userid,
			},
			type: 'POST',
			success: function(data) {
				$('.dynamic_post').html(data.html);
				$('.modal').modal('hide');
				clearData();
			}
		});
		
	});


	$(document).on('click','.create_comment_like',function(){
	
		var post_id = $(this).attr('data-post-id');
		var comment_id = $(this).attr('data-comment_id');
		var userid = $('#userid').val();
		var url="{{ route('profile.like.comment') }}";

		$.ajax({
			url: url,
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			},
			data: {
				"post_id":post_id,
				"comment_id":comment_id,
				"userid":userid,
			},
			type: 'POST',
			success: function(data) {
				$('.dynamic_post').html(data.html);
				$('.modal').modal('hide');
				clearData();
			}
		});
		
	});



	/*---------------------------------------------------------------------------------------------*/


	//------------------------------------  Comment section start ----------------------------------------------------------

	$(document).on('click','.commentdiv',function(){
		var post_id=$(this).data('id');
		var post_data=$('.commentdiv_'+post_id).val();
		var userid = $('#userid').val();
		if(post_data == "")
		{
			alert("Please Enter comment...");
			return false;
		}
		$.ajax({
			url: "{{route('profile.store.comment')}}",
			data: {
				"post_id":post_id,
				"post_data":post_data,
				"userid":userid
			},
			type: 'GET',
			success: function(data) {
				$('.dynamic_post').html(data.html);
			}
		});
	});

	$(document).on('keyup','.commenttext',function(e){
		if(e.keyCode == 13){
			var post_id=$(this).data('id');
			var post_data=$('.commentdiv_'+post_id).val();
			var userid = $('#userid').val();
			if(post_data == "")
			{
				alert("Please Enter comment...");
				return false;
			}
			$.ajax({
				url: "{{route('profile.store.comment')}}",
				data: {
					"post_id":post_id,
					"post_data":post_data,
					"userid":userid
				},
				type: 'GET',
				success: function(data) {
					$('.dynamic_post').html(data.html);
				}
			});
		}
	});

	$(document).on('click','.replyComment',function(){
		var post_id=$(this).data('post_id');
		var valueCheck=$('.reply_comment_'+post_id).val();
		var userid = $('#userid').val();
		if(valueCheck == "")
		{
			alert("Please enter proper comment..");
			return false;
		}
		var formData=$('#reply_'+post_id).serialize();

		$.ajax({
			url: "{{route('profile.reply.comment')}}",
			data:formData+"&userid="+userid,
			type: 'GET',
			success: function(data) {
				$('.dynamic_post').html(data.html);
			}
		});	
	});


	$(document).on('keyup','.replaytext',function(e){

		if(e.keyCode == 13){
			var post_id=$(this).data('post_id');
			var valueCheck=$('.reply_comment_'+post_id).val();
			var userid = $('#userid').val();
			if(valueCheck == "")
			{
				alert("Please enter proper comment..");
				return false;
			}
			var formData=$('#reply_'+post_id).serialize();

			$.ajax({
				url: "{{route('profile.reply.comment')}}",
				data:formData+"&userid="+userid,
				type: 'GET',
				success: function(data) {
					$('.dynamic_post').html(data.html);
				}
			});
		}
	});




	$(document).on('click','.editComment',function(){
		var post_id=$(this).data('post_id');
		var valueCheck=$('.edit_comment_'+post_id).val();
		var userid = $('#userid').val();
		if(valueCheck == "")
		{
			alert("Please enter proper comment..");
			return false;
		}
		var formData=$('#edit_comment_'+post_id).serialize();

		$.ajax({
			url: "{{route('profile.edit.comment')}}",
			data:formData+"&userid="+userid,
			type: 'GET',
			success: function(data) {
				$('.dynamic_post').html('');
				$('.dynamic_post').html(data.html);
			}
		});	
	});


	$(document).on('keyup','.edit_commenttext',function(e){
		if(e.keyCode == 13){
			var post_id=$(this).data('post_id');
			var valueCheck=$('.edit_comment_'+post_id).val();
			var userid = $('#userid').val();
			if(valueCheck == "")
			{
				alert("Please enter proper comment..");
				return false;
			}
			var formData=$('#edit_comment_'+post_id).serialize();

			$.ajax({
				url: "{{route('profile.edit.comment')}}",
				data:formData+"&userid="+userid,
				type: 'GET',
				success: function(data) {
					$('.dynamic_post').html(data.html);
				}
			});				
		}

	});

	$(document).on('click','.editCommentEdit',function(){
		var comment_id=$(this).data('id');
		var post_id=$(this).data('post_id');
		var comment=$(this).data('comment');

		$('.edit_comment_'+post_id).val(comment);
		$('.hideEditDiv_'+post_id).css('display','block');
		$('.hideReplyDiv_'+post_id).css('display','none');
		$('.edit_comment_id_'+post_id).val(comment_id);
	});

	$(document).on('click','.getComment',function(){

		var comment_id=$(this).data('comment_id');
		var post_id=$(this).data('post_id');
		$('.hideEditDiv_'+post_id).css('display','none');
		$('.hideReplyDiv_'+post_id).css('display','block');
		$('.comment_id_'+post_id).val(comment_id);
	});

	$(document).on('click','.deleteComment',function(){
		var comment_id=$(this).data('id');
		var userid = $('#userid').val();

		bootbox.dialog({
			message: "Are you sure you want to delete this comment!",
			title: "Delete Comment",
			onEscape: function() {},
			show: true,
			backdrop: true,
			closeButton: true,
			animate: true,
			className: "sartpost",
			buttons: {
			"Danger!": {
				label:"No",
				className: "btn-cancel",
				callback: function() {}
			},
			success: {   
				label: "Yes",
				className: "btn-default1 text-right",
				callback: function() {
					$.ajax({
						url: "{{route('profile.delete.comment')}}",
						data: {
							"comment_id":comment_id,
							"userid":userid,
						},
						type: 'GET',
						success: function(data) {
							$('.dynamic_post').html(data.html);
						}
					});	
				},
				
			}
			}
		});
	});


	$(document).on('click','.com_show',function(){
		var post_id=$(this).data('post_id');
		$('.hideCommentDiv_'+post_id).css('display','block');
		$('.showCommentDiv_'+post_id).css('display','none');
		$('.hideMoreComment_'+post_id).css('display','block');
	});

	$(document).on('click','.showCommentDiv',function(){
		var post_id=$(this).data('post_id');
		$('.hideCommentDiv_'+post_id).css('display','block');
		$('.showCommentDiv_'+post_id).css('display','none');
		$('.hideMoreComment_'+post_id).css('display','block');
	});

	$(document).on('click','.hideMoreComment',function(){
		var post_id=$(this).data('post_id');
		$('.hideCommentDiv_'+post_id).css('display','none');
		$('.showCommentDiv_'+post_id).css('display','block');
		$('.hideMoreComment_'+post_id).css('display','none');
	});

	
	//------------------------------------  Comment section  Over ---------------------------------------------------------- 





















	/*----------------------------------    Load More Feed  --------------------------------------------*/

	$(document).on('click','.load_more_btn',function(){

		var get_post_load_count = $(this).attr('id');
		var post_load_count = parseInt(get_post_load_count);
		var userid = $('#userid').val();
		post_load_count = post_load_count += 10;
		$('.loader').show();
		$.ajax({
			url: "{{ route('profile.load.more.post') }}",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			},
			data: {
			"post_load_count":post_load_count,
			"userid":userid,
			},
			type: "POST",
			success: function(result) {
				$('.loader').hide();
				$(this).attr('id',post_load_count);
				$('.dynamic_post').html(result.html);
				clearData();
			} 
		});
	});

	/*-------------------------------------------------------------------------------------------------*/




	/*======================================================================== START  Information Section  ======================================================================================*/

	/*-----------------  Edit intro details  -----------------*/

	$(document).on('click','.edit-info',function(){
		$.ajax({
			url: "{{ route('edit.info.view') }}",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			},
			type: "GET",
			success: function(result) {
				$('#dynamic_edit_info_data').html(result.html);
				$('#edit-info').modal({show:true});
			} 
		});
	});

	$(document).on('click','.update_info',function(){

		var address = $('#address').val();
		var dob = $('#dob').val();
		var website = $('#website').val();
		var profile_description = $('#profile_description').val();
		var key_skills = $('#key_skills').val();
		var business_domain = $('#business_domain').val();
		var other_info = $('#other_info').val();
		var platforms = $(".platforms").map(function()
           	{
               return $(this).val();
           	}).get();

		$.ajax({
			url: "{{ route('update.info') }}",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			},
			type: "POST",
			data:{
				'address':address,
				'dob':dob,
				'website':website,
				'profile_description':profile_description,
				'key_skills':key_skills,
				'business_domain':business_domain,
				'other_info':other_info,
				'platforms':platforms
			},
			success: function(result) {
				$('.dynamic_info').html(result.html);
				$('#edit-info').modal('hide');
			} 
		});
	});

	/*--------------------------------------------- */

	/*======================================================================== END  Information Section  ======================================================================================*/


	/*-----------------  Edit Name details  -----------------*/

	$(document).on('click','.edit_name',function(){
		$.ajax({
			url: "{{ route('edit.name.view') }}",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			},
			type: "GET",
			success: function(result) {
				$('#dynamic_edit_name_data').html(result.html);
				$('#edit-name').modal({show:true});
			} 
		});
	});

	$(document).on('click','.update_name',function(){
		var name = $('#nameedit_name').val();
		var profile_description = $('#nameedit_profile_description').val();
		var request_segment = $('#request_segment').val();

		$.ajax({
			url: "{{ route('update.name') }}",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			},
			type: "POST",
			data:{
				'name':name,
				'profile_description':profile_description,
				'request_segment':request_segment
			},
			success: function(result) {
				$('.dynamic_name_section').html(result.html);
				$('.dynamic_info').html(result.html1);
				$('#edit-name').modal('hide');
			} 
		});
	});
	/*--------------------------------------------- */


</script>



@endsection


