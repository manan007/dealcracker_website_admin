
<div class="row mb-3">
	<div class="col-md-12 d-flex">
		<div class="mr-2">
			<i class="fas fa-map-marker"></i>
		</div>
		<div>
			<label class="font-weight-bold">Address</label>
			<p>{{ $user_detail->address ?? '' }}</p>
		</div>	
	</div>
</div>


<div class="row mb-3">
	<div class="col-md-12 d-flex">
		<div class="mr-2">
			<i class="fa fa-birthday-cake" aria-hidden="true"></i>
		</div>
		<div>
			<label class="font-weight-bold">Date Of Birth</label>
			<p>@if($user_detail->dob) {{  date('d M Y',strtotime($user_detail->dob)) }} @endif</p>
		</div>	
	</div>
</div>


<div class="row mb-3">
	<div class="col-md-12 d-flex">
		<div class="mr-2">
			<i class="fas fa-link" aria-hidden="true"></i>
		</div>
		<div>
			<label class="font-weight-bold">Website URL</label>
			<p>{{ $user_detail->website ?? '' }}</p>
		</div>	
	</div>
</div>


<div class="row mb-3">
	<div class="col-md-12 d-flex">
		<div class="mr-2">
			<i class="fa fa-info-circle" aria-hidden="true"></i>
		</div>
		<div>
			<label class="font-weight-bold">Job/Business Profile Description</label>
			<p>{{ $user_detail->profile_description	 ?? '' }}</p>
		</div>	
	</div>
</div>

<div class="row mb-3">
	<div class="col-md-12 d-flex">
		<div class="mr-2">
			<i class="fa fa-thumb-tack" aria-hidden="true"></i>
		</div>
		<div>
			<label class="font-weight-bold">Key Skills</label>
			<p>{{ $user_detail->key_skills ?? '' }}</p>
		</div>	
	</div>
</div>


<div class="row mb-3">
	<div class="col-md-12 d-flex">
		<div class="mr-2">
			<i class="fas fa-link" aria-hidden="true"></i>
		</div>
		<div>
			<label class="font-weight-bold">Business Domain</label>
			<p>{{ $user_detail->business_domain ?? '' }}</p>
		</div>	
	</div>
</div>

<div class="row mb-3">
	<div class="col-md-12 d-flex">
		<div class="mr-2">
			<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
		</div>
		<div>
			<label class="font-weight-bold">Platforms</label>
			@php 
                $platform_array=[];$platform_arr=[];
                if($user_detail->platforms){
                    $platform_array = explode(',',$user_detail->platforms); 
                    if(count($platform_array) > 0){
                    	foreach($platform_array as $platform){
                    		$getplatform = App\Models\Platform::where(['id' => $platform])->first();
                    		$platform_arr[] = $getplatform->name;
                    	}
                    }
                }
            @endphp
            <p>{{ implode(',',$platform_arr) }}</p>
		</div>
	</div>
</div>

<div class="row mb-3">
	<div class="col-md-12 d-flex">
		<div class="mr-2">
			<i class="fa fa-info-circle" aria-hidden="true"></i>
		</div>
		<div>
			<label class="font-weight-bold">Other Info</label>
			<p>{{ $user_detail->other_info ?? '' }}</p>
		</div>	
	</div>
</div>
	




	

	

	

	

