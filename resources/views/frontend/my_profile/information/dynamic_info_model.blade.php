<style type="text/css">
    .select2{
        width: 100%!important;
    }
    .select2-container--default .select2-selection--multiple{
        height:38px!important;
            display: block;
            width: 100%;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
            transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
    .select2-selection__choice__remove{
        outline: none!important;
    }
</style>

<div class="row mb-3">
    <div class="col-md-12">
        <label class="mb-2">Address</label>
        <textarea class="form-control" id="address" name="address" placeholder="Enter Address" value="{{ $user_detail->address ?? '' }}">{{ Auth::user()->address ?? '' }}</textarea>
    </div>
</div>


<div class="row mb-3">
    <div class="col-md-6">
        <label class="mb-2">Date Of Birth</label>
        <input type="date" class="form-control" id="dob" name="dob" value="{{ $user_detail->dob ?? '' }}">
    </div>
    <div class="col-md-6">
        <label class="mb-2">Website URL</label>
        <input type="url" class="form-control" id="website" placeholder="Website URL" name="website" value="{{ $user_detail->website ?? '' }}">
    </div>
</div>


<div class="row mb-3">
    <div class="col-md-12">
        <label class="mb-2">Job/Business Profile Description</label>
        <textarea class="form-control" id="profile_description" name="profile_description" placeholder="Profile Description" value="{{ $user_detail->profile_description ?? '' }}">{{ $user_detail->profile_description ?? '' }}</textarea>
    </div>  
</div>

<div class="row mb-3">
    <div class="col-md-6">
        <label class="mb-2">Key Skills</label>
        <input type="text" class="form-control" id="key_skills" placeholder="Key Skills" name="key_skills" value="{{ $user_detail->key_skills ?? '' }}">
    </div>
    <div class="col-md-6">
        <label class="mb-2">Business Domain</label>
        <input type="url" class="form-control" id="business_domain" placeholder="Business Domain" name="business_domain" value="{{ $user_detail->business_domain ?? '' }}">
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <label class="mb-2">Select Platform</label><br/>
        <select class="form-control select2 platforms" name="platforms[]" id="platforms" multiple="true">
            @php 
                $platform_array=[];
                if($user_detail->platforms){
                    $platform_array = explode(',',$user_detail->platforms); 
                }
            @endphp
            @if($platforms)
                @foreach($platforms as $platform)
                  <option value="{{ $platform->id }}" @if(in_array($platform->id,$platform_array)) selected="" @endif>{{ $platform->name ?? '' }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>


<div class="row mb-3">
    <div class="col-md-12">
        <label class="mb-2">Other Info</label>
        <input class="form-control" id="other_info" name="other_info" placeholder="Other Information" value="{{ Auth::user()->other_info ?? '' }}" />
    </div>
</div>
    




<script type="text/javascript">
$('.select2').select2();
</script>