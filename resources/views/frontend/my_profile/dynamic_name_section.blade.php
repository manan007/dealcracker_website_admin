
<div class="d-flex">
	<a href="{{ route('front.my_profile.index',Auth::user()->id) }}"><h3>{{ $user_detail->name }}</h3></a>
	@if($req_segment == $user_detail->id)<a href="javascript:;" title="Edit Name" class="edit_name ml-2 mt-1 text-dark"><i class="fa fa-pencil"></i></a>@endif
</div>
<div class="star-descp">
	<span>{{ $user_detail->profile_description }}</span>
	
</div>
