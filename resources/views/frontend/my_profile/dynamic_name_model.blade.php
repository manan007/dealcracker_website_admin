<div class="row mb-3">
    <div class="col-lg-12">
        <label class="form-label">Name</label>
        <input type="text" class="form-control mt-1" name="nameedit_name" id="nameedit_name" placeholder="Name" value="{{ $user_detail->name ?? '' }}">
    </div>
</div>
<div class="row mb-3">
    <div class="col-lg-12">
        <label class="form-label">Profile Description</label>
        <textarea class="form-control mt-1" id="nameedit_profile_description" name="nameedit_profile_description" placeholder="Profile Description" 
        value="{{ $user_detail->profile_description ?? '' }}">{{ $user_detail->profile_description ?? '' }}</textarea>
    </div>
</div>

