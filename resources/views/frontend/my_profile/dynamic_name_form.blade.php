
<div class="modal fade sartpost mainpost-box" id="edit-name" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Edit Profile Name Details</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>

			<div class="modal-body" style="height:auto;max-height:640px;overflow-y:auto;">
			<div id="dynamic_edit_name_data">
			
			</div>
			</div>

			<div class="modal-footer">
				
				<div class="right-part">
					<a href="javascript:;" type="button" data-dismiss="modal">Cancel</a>
					<a href="javascript:;" type="button" class="update_name">Update</a>
				</div>
			</div>

		</div>
	</div>
</div>


