
@if($business_template_details)

	@if($is_main == 1)
		<div class="row mb-3">
			<div class="col-md-6">
				<label class="mb-2">Select Industry</label>
				<select class="form-control" id="business_category_id" name="business_category_id">
					<option value="">Select Industry</option>
					@if(count($business_category) > 0)
						@foreach($business_category as $business_category_data)
							<option value="{{ $business_category_data->id }}" @if($business_template_details->business_category_id == $business_category_data->id) selected="" @endif>{{ $business_category_data->name ?? '' }}</option>
						@endforeach
					@endif
				</select>
			</div>

			<div class="col-md-6">
				<label class="mb-2">Business Title</label>
				<input type="text" class="form-control" id="title" name="title" value="{{ $business_template_details->title ?? '' }}" placeholder="Enter Business Title">
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-12">
				<label class="mb-2">Business Description</label>
				<textarea name="description" class="form-control" id="description" value="{{ $business_template_details->description ?? '' }}" placeholder="Enter Business Description">{{ $business_template_details->description ?? '' }}</textarea>
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-4">
				<label class="mb-2">Established Year</label>
				<input type="text" class="form-control" name="established_year" value="{{ $business_template_details->established_year }}" id="established_year" placeholder="Enter Established Year" onkeypress="return isOnlyNumber();" maxlength="4">
			</div>
			<div class="col-md-4">
				<label class="mb-2">Number of Employees</label>
				<input type="text" class="form-control" name="no_of_employee" value="{{ $business_template_details->no_of_employee }}" id="no_of_employee" placeholder="Enter Number of Employees" onkeypress="return isOnlyNumber();">
			</div>
			<div class="col-md-4">
				<label class="mb-2">Legal Entity</label>
				<input type="text" class="form-control" name="legal_entity" value="{{ $business_template_details->legal_entity }}" id="legal_entity" placeholder="Enter Legal Entity">
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-4">
				<label class="mb-2">Annual Sales <span class="text-danger">(Yearly)</span></label>
				<input type="text" class="form-control" name="reported_sales" value="{{ $business_template_details->reported_sales ?? '' }}" id="reported_sales" placeholder="Enter Reported Sales" onkeypress="return isOnlyNumber();">
			</div>
			<!--<div class="col-md-4">-->
			<!--	<label class="mb-2">Enter Run Rate Sales <span class="text-danger">(Yearly)</span></label>-->
			<!--	<input type="text" class="form-control" name="run_rate_sales" id="run_rate_sales" value="{{ $business_template_details->run_rate_sales ?? '' }}" placeholder="Enter Number of Employees" onkeypress="return isOnlyNumber();">-->
			<!--</div>-->
			<div class="col-md-4">
				<label class="mb-2">EBITDA Margin</label>
				<input type="text" class="form-control" name="ebitda_margin" id="ebitda_margin" value="{{ $business_template_details->ebitda_margin ?? '' }}" placeholder="Enter EBITDA Margin">
			</div>
		</div>

		<div class="row mb-3">
		
			<div class="col-md-4">
				<label class="mb-2">Location</label>
				<input type="text" class="form-control" name="location" id="location" placeholder="Enter Location" value="{{ $business_template_details->location ?? '' }}">
			</div>
			<div class="col-md-4">
				<label class="mb-2">Email</label>
				<input type="email" class="form-control" name="email" id="email" placeholder="Enter Email" value="{{ $business_template_details->email ?? '' }}">
			</div>

			<div class="col-md-4">
				<label class="mb-2">Enter Mobile Number</label>
				<input type="text" class="form-control" name="mobile_no" id="mobile_no" value="{{ $business_template_details->mobile_no ?? '' }}"  placeholder="Enter Mobile Number" onkeypress="return isOnlyNumber();" maxlength="10">
			</div>
		</div>
		
		<div class="row mb-3">	
			<div class="col-md-4">
				<label>Do you have the Mandate?</label>
	            <div class="form-group form-radio">
	                <div class="pt-2">
	                    <input type="radio" class="form-radio-input" id="yes" name="is_available_mandate" value="Yes" @if($business_template_details->is_available_mandate == 1) checked="" @endif>
	                    <label class="form-radio-label" for="yes">Yes</label>&nbsp;&nbsp;
	                    <input type="radio" class="form-radio-input" id="no" name="is_available_mandate" value="No" @if($business_template_details->is_available_mandate != 1) checked="" @endif>
	                    <label class="form-radio-label" for="no">No</label>
	                </div>
	            </div>
			</div>
			
		</div>

	@else

		@php
			$getcategory_first = App\Models\TemplateSubCategory::whereIn('slug',['offering','buyer','debt','seeking_services'])->get(); 
			$getcategory_second = App\Models\TemplateSubCategory::whereIn('slug',['seeking','seller','equity','offering_services'])->get(); 
			$first_array=[]; foreach($getcategory_first as $firstdata){ $first_array[] = $firstdata->id; }
			$second_array=[]; foreach($getcategory_second as $seconddata){ $second_array[] = $seconddata->id; }
		@endphp
		
		@if($category_details)

		@if(in_array($category_details->id,$first_array))

		<div class="row mb-3">
			<div class="col-md-6">
				<label class="mb-2">Select Industry</label>
				<select class="form-control" id="business_category_id" name="business_category_id">
					<option value="">Select Industry</option>
					@if(count($business_category) > 0)
						@foreach($business_category as $business_category_data)
							<option value="{{ $business_category_data->id }}" @if($business_template_details->business_category_id == $business_category_data->id) selected="" @endif>{{ $business_category_data->name ?? '' }}</option>
						@endforeach
					@endif
				</select>
			</div>

			<div class="col-md-6">
				<label class="mb-2">Business Title</label>
				<input type="text" class="form-control" id="title" name="title"  value="{{ $business_template_details->title ?? '' }}" placeholder="Enter Business Title">
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-12">
				<label class="mb-2">Business Description</label>
				<textarea name="description" class="form-control" id="description" value="{{ $business_template_details->description ?? '' }}" placeholder="Enter Business Description">{{ $business_template_details->description ?? '' }}</textarea>
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-4">
				<label class="mb-2">Established Year</label>
				<input type="text" class="form-control" name="established_year" id="established_year" value="{{ $business_template_details->established_year }}" placeholder="Enter Established Year" onkeypress="return isOnlyNumber();" maxlength="4">
			</div>
			<div class="col-md-4">
				<label class="mb-2">Number of Employees</label>
				<input type="text" class="form-control" name="no_of_employee" id="no_of_employee" value="{{ $business_template_details->no_of_employee }}" placeholder="Enter Number of Employees" onkeypress="return isOnlyNumber();">
			</div>
			<div class="col-md-4">
				<label class="mb-2">Legal Entity</label>
				<input type="text" class="form-control" name="legal_entity" id="legal_entity" value="{{ $business_template_details->legal_entity }}" placeholder="Enter Legal Entity">
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-4">
				<label class="mb-2">Annual Sales <span class="text-danger">(Yearly)</span></label>
				<input type="text" class="form-control" name="reported_sales" id="reported_sales" value="{{ $business_template_details->reported_sales ?? '' }}" placeholder="Enter Reported Sales" onkeypress="return isOnlyNumber();">
			</div>
			<!--<div class="col-md-4">-->
			<!--	<label class="mb-2">Enter Run Rate Sales <span class="text-danger">(Yearly)</span></label>-->
			<!--	<input type="text" class="form-control" name="run_rate_sales" id="run_rate_sales" value="{{ $business_template_details->run_rate_sales ?? '' }}" placeholder="Enter Number of Employees" onkeypress="return isOnlyNumber();">-->
			<!--</div>-->
			<div class="col-md-4">
				<label class="mb-2">EBITDA Margin</label>
				<input type="text" class="form-control" name="ebitda_margin" id="ebitda_margin" value="{{ $business_template_details->ebitda_margin ?? '' }}" placeholder="Enter EBITDA Margin">
			</div>
		</div>

		<div class="row mb-3">
		
			<div class="col-md-4">
				<label class="mb-2">Location</label>
				<input type="text" class="form-control" name="location" id="location" value="{{ $business_template_details->location ?? '' }}" placeholder="Enter Location">
			</div>
			<div class="col-md-4">
				<label class="mb-2">Email</label>
				<input type="email" class="form-control" name="email" id="email" value="{{ $business_template_details->email ?? '' }}" placeholder="Enter Email">
			</div>

			<div class="col-md-4">
				<label class="mb-2">Enter Mobile Number</label>
				<input type="text" class="form-control" name="mobile_no" id="mobile_no" value="{{ $business_template_details->mobile_no ?? '' }}" placeholder="Enter Mobile Number" onkeypress="return isOnlyNumber();" maxlength="10">
			</div>
		</div>
		
		<div class="row mb-3">	
			<div class="col-md-4">
				<label>Do you have the Mandate?</label>
	            <div class="form-group form-radio">
	                <div class="pt-2">
	                    <input type="radio" class="form-radio-input" id="yes" name="is_available_mandate" value="Yes" @if($business_template_details->is_available_mandate == 1) checked="" @endif>
	                    <label class="form-radio-label" for="yes">Yes</label>&nbsp;&nbsp;
	                    <input type="radio" class="form-radio-input" id="no" name="is_available_mandate" value="No" @if($business_template_details->is_available_mandate != 1) checked="" @endif>
	                    <label class="form-radio-label" for="no">No</label>
	                </div>
	            </div>
			</div>
			
		</div>

		@endif


		@if(in_array($category_details->id,$second_array))

		<div class="row mb-3">
			<div class="col-md-12">
				<label class="mb-2">Select Industry</label>
				<select class="form-control" id="business_category_id" name="business_category_id">
					<option value="">Select Industry</option>
					@if(count($business_category) > 0)
						@foreach($business_category as $business_category_data)
							<option value="{{ $business_category_data->id }}" @if($business_template_details->business_category_id == $business_category_data->id) selected="" @endif>{{ $business_category_data->name ?? '' }}</option>
						@endforeach
					@endif
				</select>
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-12">
				<label class="mb-2">Business Description</label>
				<textarea name="description" class="form-control" id="description" placeholder="Enter Business Description">{{ $business_template_details->description ?? '' }}</textarea>
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-4">
				<label class="mb-2">Location</label>
				<input type="text" class="form-control" name="location" id="location" value="{{ $business_template_details->location ?? '' }}"  placeholder="Enter Location">
			</div>
			<div class="col-md-4">
				<label class="mb-2">Email</label>
				<input type="email" class="form-control" name="email" id="email" value="{{ $business_template_details->email ?? '' }}" placeholder="Enter Email">
			</div>

			<div class="col-md-4">
				<label class="mb-2">Enter Mobile Number</label>
				<input type="text" class="form-control" name="mobile_no" value="{{ $business_template_details->mobile_no ?? '' }}" id="mobile_no" placeholder="Enter Mobile Number" onkeypress="return isOnlyNumber();" maxlength="10">
			</div>
		</div>
			
		@endif

		@endif

	@endif
@else
	@if($is_main == 1)
		<div class="row mb-3">
			<div class="col-md-6">
				<label class="mb-2">Select Industry</label>
				<select class="form-control" id="business_category_id" name="business_category_id">
					<option value="">Select Industry</option>
					@if(count($business_category) > 0)
						@foreach($business_category as $business_category_data)
							<option value="{{ $business_category_data->id }}">{{ $business_category_data->name ?? '' }}</option>
						@endforeach
					@endif
				</select>
			</div>

			<div class="col-md-6">
				<label class="mb-2">Business Title</label>
				<input type="text" class="form-control" id="title" name="title" placeholder="Enter Business Title">
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-12">
				<label class="mb-2">Business Description</label>
				<textarea name="description" class="form-control" id="description" placeholder="Enter Business Description"></textarea>
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-4">
				<label class="mb-2">Established Year</label>
				<input type="text" class="form-control" name="established_year" id="established_year" placeholder="Enter Established Year" onkeypress="return isOnlyNumber();" maxlength="4">
			</div>
			<div class="col-md-4">
				<label class="mb-2">Number of Employees</label>
				<input type="text" class="form-control" name="no_of_employee" id="no_of_employee" placeholder="Enter Number of Employees" onkeypress="return isOnlyNumber();">
			</div>
			<div class="col-md-4">
				<label class="mb-2">Legal Entity</label>
				<input type="text" class="form-control" name="legal_entity" id="legal_entity" placeholder="Enter Legal Entity">
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-4">
				<label class="mb-2">Annual Sales <span class="text-danger">(Yearly)</span></label>
				<input type="text" class="form-control" name="reported_sales" id="reported_sales" placeholder="Enter Reported Sales" onkeypress="return isOnlyNumber();">
			</div>
			<!--<div class="col-md-4">-->
			<!--	<label class="mb-2">Enter Run Rate Sales <span class="text-danger">(Yearly)</span></label>-->
			<!--	<input type="text" class="form-control" name="run_rate_sales" id="run_rate_sales" placeholder="Enter Number of Employees" onkeypress="return isOnlyNumber();">-->
			<!--</div>-->
			<div class="col-md-4">
				<label class="mb-2">EBITDA Margin</label>
				<input type="text" class="form-control" name="ebitda_margin" id="ebitda_margin" placeholder="Enter EBITDA Margin">
			</div>
		</div>

		<div class="row mb-3">
		
			<div class="col-md-4">
				<label class="mb-2">Location</label>
				<input type="text" class="form-control" name="location" id="location" placeholder="Enter Location">
			</div>
			<div class="col-md-4">
				<label class="mb-2">Email</label>
				<input type="email" class="form-control" name="email" id="email" placeholder="Enter Email">
			</div>

			<div class="col-md-4">
				<label class="mb-2">Enter Mobile Number</label>
				<input type="text" class="form-control" name="mobile_no" id="mobile_no"  placeholder="Enter Mobile Number" onkeypress="return isOnlyNumber();" maxlength="10">
			</div>
		</div>
		
		<div class="row mb-3">	
			<div class="col-md-4">
				<label>Do you have the Mandate?</label>
	            <div class="form-group form-radio">
	                <div class="pt-2">
	                    <input type="radio" class="form-radio-input" id="yes" name="is_available_mandate" value="Yes" checked="">
	                    <label class="form-radio-label" for="yes">Yes</label>&nbsp;&nbsp;
	                    <input type="radio" class="form-radio-input" id="no" name="is_available_mandate" value="No">
	                    <label class="form-radio-label" for="no">No</label>
	                </div>
	            </div>
			</div>
			
		</div>

	@else

		
		@php
			$getcategory_first = App\Models\TemplateSubCategory::whereIn('slug',['offering','buyer','debt','seeking_services'])->get(); 
			$getcategory_second = App\Models\TemplateSubCategory::whereIn('slug',['seeking','seller','equity','offering_services'])->get();
			$first_array=[]; foreach($getcategory_first as $firstdata){ $first_array[] = $firstdata->id; }
			$second_array=[]; foreach($getcategory_second as $seconddata){ $second_array[] = $seconddata->id; }
		@endphp
		
		@if(in_array($category_details->id,$first_array))

		<div class="row mb-3">
			<div class="col-md-6">
				<label class="mb-2">Select Industry</label>
				<select class="form-control" id="business_category_id" name="business_category_id">
					<option value="">Select Industry</option>
					@if(count($business_category) > 0)
						@foreach($business_category as $business_category_data)
							<option value="{{ $business_category_data->id }}">{{ $business_category_data->name ?? '' }}</option>
						@endforeach
					@endif
				</select>
			</div>

			<div class="col-md-6">
				<label class="mb-2">Business Title</label>
				<input type="text" class="form-control" id="title" name="title" placeholder="Enter Business Title">
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-12">
				<label class="mb-2">Business Description</label>
				<textarea name="description" class="form-control" id="description" placeholder="Enter Business Description"></textarea>
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-4">
				<label class="mb-2">Established Year</label>
				<input type="text" class="form-control" name="established_year" id="established_year" placeholder="Enter Established Year" onkeypress="return isOnlyNumber();" maxlength="4">
			</div>
			<div class="col-md-4">
				<label class="mb-2">Number of Employees</label>
				<input type="text" class="form-control" name="no_of_employee" id="no_of_employee" placeholder="Enter Number of Employees" onkeypress="return isOnlyNumber();">
			</div>
			<div class="col-md-4">
				<label class="mb-2">Legal Entity</label>
				<input type="text" class="form-control" name="legal_entity" id="legal_entity" placeholder="Enter Legal Entity">
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-4">
				<label class="mb-2">Annual Sales <span class="text-danger">(Yearly)</span></label>
				<input type="text" class="form-control" name="reported_sales" id="reported_sales" placeholder="Enter Reported Sales" onkeypress="return isOnlyNumber();">
			</div>
			<!--<div class="col-md-4">-->
			<!--	<label class="mb-2">Enter Run Rate Sales <span class="text-danger">(Yearly)</span></label>-->
			<!--	<input type="text" class="form-control" name="run_rate_sales" id="run_rate_sales" placeholder="Enter Number of Employees" onkeypress="return isOnlyNumber();">-->
			<!--</div>-->
			<div class="col-md-4">
				<label class="mb-2">EBITDA Margin</label>
				<input type="text" class="form-control" name="ebitda_margin" id="ebitda_margin" placeholder="Enter EBITDA Margin">
			</div>
		</div>

		<div class="row mb-3">
		
			<div class="col-md-4">
				<label class="mb-2">Location</label>
				<input type="text" class="form-control" name="location" id="location" placeholder="Enter Location">
			</div>
			<div class="col-md-4">
				<label class="mb-2">Email</label>
				<input type="email" class="form-control" name="email" id="email" placeholder="Enter Email">
			</div>

			<div class="col-md-4">
				<label class="mb-2">Enter Mobile Number</label>
				<input type="text" class="form-control" name="mobile_no" id="mobile_no" placeholder="Enter Mobile Number" onkeypress="return isOnlyNumber();" maxlength="10">
			</div>
		</div>
		
		<div class="row mb-3">	
			<div class="col-md-4">
				<label>Do you have the Mandate?</label>
	            <div class="form-group form-radio">
	                <div class="pt-2">
	                    <input type="radio" class="form-radio-input" id="yes" name="is_available_mandate" value="Yes" checked="">
	                    <label class="form-radio-label" for="yes">Yes</label>&nbsp;&nbsp;
	                    <input type="radio" class="form-radio-input" id="no" name="is_available_mandate" value="No">
	                    <label class="form-radio-label" for="no">No</label>
	                </div>
	            </div>
			</div>
			
		</div>

		@endif


		@if(in_array($category_details->id,$second_array))

		<div class="row mb-3">
			<div class="col-md-12">
				<label class="mb-2">Select Industry</label>
				<select class="form-control" id="business_category_id" name="business_category_id">
					<option value="">Select Industry</option>
					@if(count($business_category) > 0)
						@foreach($business_category as $business_category_data)
							<option value="{{ $business_category_data->id }}">{{ $business_category_data->name ?? '' }}</option>
						@endforeach
					@endif
				</select>
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-12">
				<label class="mb-2">Business Description</label>
				<textarea name="description" class="form-control" id="description" placeholder="Enter Business Description"></textarea>
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-4">
				<label class="mb-2">Location</label>
				<input type="text" class="form-control" name="location" id="location"  placeholder="Enter Location">
			</div>
			<div class="col-md-4">
				<label class="mb-2">Email</label>
				<input type="email" class="form-control" name="email" id="email" placeholder="Enter Email">
			</div>

			<div class="col-md-4">
				<label class="mb-2">Enter Mobile Number</label>
				<input type="text" class="form-control" name="mobile_no" id="mobile_no" placeholder="Enter Mobile Number" onkeypress="return isOnlyNumber();" maxlength="10">
			</div>
		</div>
			
		@endif
		@endif
@endif