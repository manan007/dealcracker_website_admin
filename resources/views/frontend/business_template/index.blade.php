@extends('frontend.layouts.main')
@section('title','Business Template')

@section('css')
<style>

</style>
@endsection

@section('content')

<br/><br/>
<div class="container mt-5">

	<div class="card">
		<div class="row">
			<div class="col-md-9 p-3">
				<h2 class="h2">Opportunity Details</h2>
			</div>
			<div class="col-md-3 p-3 text-right">
				<a class="btn btn-primary" href="{{ route('front.business_template.create') }}">Add New Opportunity</a>
			</div>
		</div>
		<hr class="m-0" />


		<div class="card-body">

			<table id="datatable" class="display" style="width:100%">
				<thead>
		            <tr>
		                <th>Business Template</th>
		            </tr>
		        </thead>
		        <tbody>
		        	@if(count($business_templates) > 0)
						@foreach($business_templates as $key => $data)
							@php $template_category_details  = App\Models\TemplateCategory::where(['id' => $data->template_category_id])->first(); @endphp
							<tr>
								<td>
									<div class="row">
										<div class="col-xl-9 col-lg-9 col-md-9">
											<h3 class="h6 font-weight-bold">
												@if($template_category_details->slug == "m&a")
													{{ $data->business_category_details->name ?? '' }}
												@else
													{{ $data->title ?? '' }}
												@endif
											</h3>	
											<span>{{ $data->description ?? '' }}</span>
										</div>
										<div class="col-xl-3 col-lg-3 col-md-3 text-right actionbtns">

											@if(Auth::user()->id == $data->user_id)
											<a href="{{ route('front.business_template.view',$data->id) }}" class="btn-sm btn-info"><i class="fas fa-eye"></i></a>
											<a href="{{ route('front.business_template.edit',$data->id) }}" class="btn-sm btn-success"><i class="fas fa-pen"></i></a>

											<form action="{{ route('front.business_template.delete',$data->id) }}" method="POST" id="change_status_{{$data->id}}">
					                        @csrf
					                        <button type="submit" name="submit" data-id="{{$data->id}}"  class="hideBtn submitData_changestatus_{{$data->id}} d-none"><i class="ft-trash-2"></i> Submit</button>
					                        <a class="btn-sm btn-danger action-btn delete text-light" title="Delete" data-id="{{ $data->id }}"><i class="fas fa-trash"></i></a>
					                        </form>

					                        @else
					                        <a href="{{ route('front.business_template.view',$data->id) }}" class="btn-sm btn-info"><i class="fas fa-eye"></i></a>
					                        <a href="{{ route('user',$data->user_id) }}" class="btn-sm btn-info">Send Message</a>
					                        @endif

										</div>
									</div>
								</td>
							</tr>
						@endforeach
					@endif
		        </tbody>
			</table>

		</div>
	</div>

	

</div>

@endsection




@section('scripts')

<script type="text/javascript">
 $(document).on('click','.delete',function(){
    var id=$(this).data('id');
    var getval = $(this).data('value');
    bootbox.confirm("Are you sure you want to delete this opportunity ?", function(result){ 
        if(result == true)
        {   
          $('.submitData_changestatus_'+id).trigger('click');
        }
    });
});
</script>

@endsection
































































<!-- <div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12">
		<h3 class="h6 font-weight-bold">Business Template Name</h3>
		<span>Business Template Description</span>
	</div>
</div>
<hr/>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12">
		<h3 class="h6 font-weight-bold">If corn oil is made from corn and vegetable oil is made from vegetables. What is baby oil made from?</h3>
		<span>You can get a job based on these dummy intewrview questions that were created based on the research and survey of the company. The first question in our mind is How to crack dummy interview processed.</span>
	</div>
</div> -->
















