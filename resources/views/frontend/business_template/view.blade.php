@extends('frontend.layouts.main')
@section('title','Business Template Details')

@section('css')
<style>
.card-body .row{
	border-bottom: 1px solid lightgray;
}
</style>
@endsection

@section('content')

<br/><br/>
<div class="container mt-5">

	<div class="card">
		<div class="row">
			<div class="col-md-11 p-3">
				<h2 class="h2">Opportunity Details</h2>
			</div>
			<div class="col-md-1 p-3">
				<a class="btn btn-light" href="{{ route('front.business_template.index') }}">Back</a>
			</div>
		</div>
		<hr class="m-0" />


		<div class="card-body">
			

			@php $template_category_details  = App\Models\TemplateCategory::where(['id' => $business_template_details->template_category_id])->first(); @endphp


			@if($template_category_details->slug == "m&a")

				<div class="row mb-3">
					<div class="col-md-12">
						<label class="mb-2">Category</label>
						<p>{{ $business_template_details->template_category_details->name ?? '' }}</p>
					</div>
				</div>

				<div class="row mb-3">
					<div class="col-md-6">
						<label class="mb-2">Select Industry</label>
						<p>{{ $business_template_details->business_category_details->name ?? '' }}</p>
					</div>

					<div class="col-md-6">
						<label class="mb-2">Business Title</label>
						<p>{{ $business_template_details->title ?? '' }}</p>
					</div>
				</div>

				<div class="row mb-3">
					<div class="col-md-12">
						<label class="mb-2">Business Description</label>
						<p>{{ $business_template_details->description ?? '' }}</p>
					</div>
				</div>

				<div class="row mb-3">
					<div class="col-md-4">
						<label class="mb-2">Established Year</label>
						<p>{{ $business_template_details->established_year }}</p>
					</div>
					<div class="col-md-4">
						<label class="mb-2">Number of Employees</label>
						<p>{{ $business_template_details->no_of_employee }}</p>
					</div>
					<div class="col-md-4">
						<label class="mb-2">Legal Entity</label>
						<p>{{ $business_template_details->legal_entity }}</p>
					</div>
				</div>

				<div class="row mb-3">
					<div class="col-md-4">
						<label class="mb-2">Annual Sales <span class="text-danger">(Yearly)</span></label>
						<p>{{ $business_template_details->reported_sales ?? '' }}</p>
					</div>
					<!--<div class="col-md-4">-->
					<!--	<label class="mb-2">Enter Run Rate Sales <span class="text-danger">(Yearly)</span></label>-->
					<!--	<p>{{ $business_template_details->run_rate_sales ?? '' }}</p>-->
					<!--</div>-->
					<div class="col-md-4">
						<label class="mb-2">EBITDA Margin</label>
						<p>{{ $business_template_details->ebitda_margin ?? '' }}</p>
					</div>
				</div>

				<div class="row mb-3">
				
					<div class="col-md-4">
						<label class="mb-2">Location</label>
						<p>{{ $business_template_details->location ?? '' }}</p>
					</div>
					<div class="col-md-4">
						<label>Do you have the Mandate?</label>
						<p>@if($business_template_details->is_available_mandate == 1) Yes @else No @endif</p>
					</div>
					<!--<div class="col-md-4">-->
					<!--	<label class="mb-2">Email</label>-->
					<!--	<p>{{ $business_template_details->email ?? '' }}</p>-->
					<!--</div>-->

					<!--<div class="col-md-4">-->
					<!--	<label class="mb-2">Enter Mobile Number</label>-->
					<!--	<p>{{ $business_template_details->mobile_no ?? '' }}</p>-->
					<!--</div>-->
				</div>
				
				<!--<div class="row mb-3">	-->
					
				<!--</div>-->

			@else

				@php

					$getcategory_first = App\Models\TemplateSubCategory::whereIn('slug',['offering','buyer','debt','seeking_services'])->get(); 
					$getcategory_second = App\Models\TemplateSubCategory::whereIn('slug',['seeking','seller','equity','offering_services'])->get();
					$first_array=[]; foreach($getcategory_first as $firstdata){ $first_array[] = $firstdata->id; }
					$second_array=[]; foreach($getcategory_second as $seconddata){ $second_array[] = $seconddata->id; }

					$category_details = App\Models\TemplateSubCategory::where('id',$business_template_details->template_sub_category_id)->first();
					
				@endphp
				
				@if($category_details)

					@if(in_array($category_details->id,$first_array))

						<div class="row mb-3">
							<div class="col-md-6">
								<label class="mb-2">Category</label>
								<p>{{ $business_template_details->template_category_details->name ?? ' - ' }}</p>
							</div>
							<div class="col-md-6">
								<label class="mb-2">Details</label>
								<p>{{ $business_template_details->template_sub_category_details->name ?? ' - ' }}</p>
							</div>
						</div>

						<div class="row mb-3">
							<div class="col-md-6">
								<label class="mb-2">Select Industry</label>
								<p>{{ $business_template_details->business_category_details->name ?? ' - ' }}</p>
							</div>

							<div class="col-md-6">
								<label class="mb-2">Business Title</label>
								<p>{{ $business_template_details->title ?? ' - ' }}</p>
							</div>
						</div>

						<div class="row mb-3">
							<div class="col-md-12">
								<label class="mb-2">Business Description</label>
								<p>{{ $business_template_details->description ?? ' - ' }}</p>
							</div>
						</div>

						<div class="row mb-3">
							<div class="col-md-4">
								<label class="mb-2">Established Year</label>
								<p>{{ $business_template_details->established_year ?? ' - ' }}</p>
							</div>
							<div class="col-md-4">
								<label class="mb-2">Number of Employees</label>
								<p>{{ $business_template_details->no_of_employee ?? ' - ' }}</p>
							</div>
							<div class="col-md-4">
								<label class="mb-2">Legal Entity</label>
								<p>{{ $business_template_details->legal_entity ?? ' - ' }}</p>
							</div>
						</div>

						<div class="row mb-3">
							<div class="col-md-4">
								<label class="mb-2">Annual Sales <span class="text-danger">(Yearly)</span></label>
								<p>{{ $business_template_details->reported_sales ?? ' - ' }}</p>
							</div>
							<!--<div class="col-md-4">-->
							<!--	<label class="mb-2">Enter Run Rate Sales <span class="text-danger">(Yearly)</span></label>-->
							<!--	<p>{{ $business_template_details->run_rate_sales ?? ' - ' }}</p>-->
							<!--</div>-->
							<div class="col-md-4">
								<label class="mb-2">EBITDA Margin</label>
								<p>{{ $business_template_details->ebitda_margin ?? ' - ' }}</p>
							</div>
						</div>

						<div class="row mb-3">
						
							<div class="col-md-4">
								<label class="mb-2">Location</label>
								<p>{{ $business_template_details->location ?? ' - ' }}</p>
							</div>
								<div class="col-md-4">
								<label>Do you have the Mandate?</label>
								<p>@if($business_template_details->is_available_mandate == 1) Yes @else No @endif</p>
							</div>
							<!--<div class="col-md-4">-->
							<!--	<label class="mb-2">Email</label>-->
							<!--	<p>{{ $business_template_details->email ?? ' - ' }}</p>-->
							<!--</div>-->

							<!--<div class="col-md-4">-->
							<!--	<label class="mb-2">Enter Mobile Number</label>-->
							<!--	<p>{{ $business_template_details->mobile_no ?? ' - ' }}</p>-->
							<!--</div>-->
						</div>
						
						<!--<div class="row mb-3">	-->
						
						<!--</div>-->

					@endif


					@if(in_array($category_details->id,$second_array))


						<div class="row mb-3">
							<div class="col-md-6">
								<label class="mb-2">Category</label>
								<p>{{ $business_template_details->template_category_details->name ?? '' }}</p>
							</div>
							<div class="col-md-6">
								<label class="mb-2">Details</label>
								<p>{{ $business_template_details->template_sub_category_details->name ?? '' }}</p>
							</div>
						</div>

						<div class="row mb-3">
							<div class="col-md-12">
								<label class="mb-2">Select Industry</label>
								<p>{{ $business_template_details->business_category_details->name ?? '' }}</p>
							</div>
						</div>

						<div class="row mb-3">
							<div class="col-md-12">
								<label class="mb-2">Business Description</label>
								<p>{{ $business_template_details->description ?? '' }}</p>
							</div>
						</div>

						<div class="row mb-3">
						
							<div class="col-md-4">
								<label class="mb-2">Location</label>
								<p>{{ $business_template_details->location ?? '' }}</p>
							</div>
							<!--<div class="col-md-4">-->
							<!--	<label class="mb-2">Email</label>-->
							<!--	<p>{{ $business_template_details->email ?? '' }}</p>-->
							<!--</div>-->

							<!--<div class="col-md-4">-->
							<!--	<label class="mb-2">Enter Mobile Number</label>-->
							<!--	<p>{{ $business_template_details->mobile_no ?? '' }}</p>-->
							<!--</div>-->
						</div>
						
					@endif

				@endif

			@endif
			
		</div>
	</div>

	

</div>

@endsection




@section('scripts')

@endsection


