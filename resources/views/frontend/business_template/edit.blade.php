@extends('frontend.layouts.main')
@section('title','Business Template Edit')

@section('css')
<style>

</style>
@endsection

@section('content')


<br/><br/>
<div class="container mt-5 mb-4">

	<div class="card">
		<div class="row">
			<div class="col-md-12 p-3">
				<h2 class="h2">Edit Business Template</h2>
			</div>
		</div>
		<hr class="m-0" />


		<div class="card-body">
			<input type="hidden" name="business_template_id" id="business_template_id" value="{{ $business_template_details->id }}"> 
			<form action="{{ route('front.business_template.update',$business_template_details->id) }}" method="POST" name="business_template_form">
			@csrf

				<div class="row mb-3">
					<div class="col-md-6">
						<label class="mb-2">Select Template Category</label>
						<select class="form-control template_category_id" id="template_category_id" name="template_category_id">
							<option value="">Select Template Category</option>
							@if(count($template_category) > 0)
								@foreach($template_category as $template_category_data)
									<option value="{{ $template_category_data->id }}" @if($template_category_data->id == $business_template_details->template_category_id)  selected="" @endif data-slug="{{ $template_category_data->slug }}">
										{{ $template_category_data->name ?? '' }}
									</option>
								@endforeach
							@endif
						</select>
					</div>

					<div class="col-md-6 template_sub_category_box">
						<label class="mb-2">Select Template Sub Category</label>
						<input type="hidden" name="hidden_template_sub_category" id="hidden_template_sub_category" value="{{ $business_template_details->template_sub_category_id }}">
						<select class="form-control template_sub_category_id" id="template_sub_category_id" name="template_sub_category_id">
							<option value="">Select Template Sub Category</option>
						</select>
					</div>
				</div>

					
				<div class="dynamic_field_section">
					@include('frontend.business_template.dynamic_field_section')
				</div>


				<div class="row">
					<div class="col-md-12">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="{{ route('front.business_template.index') }}" class="btn btn-danger">Cancel</a>
					</div>
				</div>

			</form>

		</div>
	</div>

	

</div>

@endsection


@section('scripts')


<script type="text/javascript">


	$(document).ready(function() {
	   
	   	var template_category_id = $('#template_category_id').val();
		var get_slug = $('select#template_category_id').find(':selected').data('slug');
		var business_template_id = $('#business_template_id').val();
	

		if(get_slug == "m&a"){

			$('.template_sub_category_box').addClass('d-none');
			$("#template_sub_category_id").rules("remove", "required");

			$('.loader').show();
			$.ajax({
				url: "{{ route('front.get.business_template.field') }}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
				},
				type: "POST",
				data:{
					'template_category_id':template_category_id,
					'is_main':1,
					'is_edit':1,
					'business_template_id':business_template_id,
				},
				success: function(result) {
					//$('.loader').hide();
					$('.dynamic_field_section').html(result.html);


					/*--------------  get fields  ------------*/
					var template_sub_category_id = $('#template_sub_category_id').val();
					var get_sub_slug = $('select#template_sub_category_id').find(':selected').data('slug');

					$.ajax({
						url: "{{ route('front.get.business_template.field') }}",
						headers: {
							'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
						},
						type: "POST",
						data:{
							'template_category_id':template_sub_category_id,
							'is_main':1,
							'is_edit':1,
							'business_template_id':business_template_id,
						},
						success: function(result) {
							$('.loader').hide();
							$('.dynamic_field_section').html(result.html);
							if(get_sub_slug == "seeking_funds" || get_sub_slug == "offering" || get_sub_slug == "seeking_services" || get_sub_slug == "buyer"){

								$("#business_category_id").rules("add", "required");
								$("#title").rules("add", "required");
								$("#description").rules("add", "required");
								$("#established_year").rules("add", "required");
								$("#no_of_employee").rules("add", "required");
								$("#legal_entity").rules("add", "required");
								$("#reported_sales").rules("add", "required");
								$("#run_rate_sales").rules("add", "required");
								$("#ebitda_margin").rules("add", "required");
								$("#location").rules("add", "required");
								$("#email").rules("add", "required");
								$("#mobile_no").rules("add", "required");
							}
							else if(get_sub_slug == "offering_funds" || get_sub_slug == "offering_services"){

								$("#business_category_id").rules("add", "required");
								$("#description").rules("add", "required");
								$("#location").rules("add", "required");
								$("#email").rules("add", "required");
								$("#mobile_no").rules("add", "required");

								$("#title").rules("remove", "required");
								$("#established_year").rules("remove", "required");
								$("#no_of_employee").rules("remove", "required");
								$("#legal_entity").rules("remove", "required");
								$("#reported_sales").rules("remove", "required");
								$("#run_rate_sales").rules("remove", "required");
								$("#ebitda_margin").rules("remove", "required");
							}
						} 
					});

					
				} 
			});

		}
		else{
			$('.template_sub_category_box').removeClass('d-none');
        	$("#template_sub_category_id").rules("add", "required");

        	var hidden_template_sub_category = $('#hidden_template_sub_category').val();

        	$('.loader').show();
        	$.ajax({
				url: "{{ route('front.get.get_business_template_sub_category.edit') }}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
				},
				type: "POST",
				data:{
					'template_category_id':template_category_id,
					'hidden_template_sub_category':hidden_template_sub_category
				},
				success: function(result) {
					//$('.loader').hide();
					$('.template_sub_category_id').html(result);
					$('.dynamic_field_section').html('');


					/*--------------  get fields  ------------*/
					var template_sub_category_id = $('#template_sub_category_id').val();
					var get_sub_slug = $('select#template_sub_category_id').find(':selected').data('slug');

					$.ajax({
						url: "{{ route('front.get.business_template.field') }}",
						headers: {
							'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
						},
						type: "POST",
						data:{
							'template_category_id':template_sub_category_id,
							'is_main':0,
							'is_edit':1,
							'business_template_id':business_template_id,
						},
						success: function(result) {
							$('.loader').hide();
							$('.dynamic_field_section').html(result.html);
							if(get_sub_slug == "seeking_funds" || get_sub_slug == "offering" || get_sub_slug == "seeking_services" || get_sub_slug == "buyer"){

								$("#business_category_id").rules("add", "required");
								$("#title").rules("add", "required");
								$("#description").rules("add", "required");
								$("#established_year").rules("add", "required");
								$("#no_of_employee").rules("add", "required");
								$("#legal_entity").rules("add", "required");
								$("#reported_sales").rules("add", "required");
								$("#run_rate_sales").rules("add", "required");
								$("#ebitda_margin").rules("add", "required");
								$("#location").rules("add", "required");
								$("#email").rules("add", "required");
								$("#mobile_no").rules("add", "required");
							}
							else if(get_sub_slug == "offering_funds" || get_sub_slug == "offering_services"){

								$("#business_category_id").rules("add", "required");
								$("#description").rules("add", "required");
								$("#location").rules("add", "required");
								$("#email").rules("add", "required");
								$("#mobile_no").rules("add", "required");

								$("#title").rules("remove", "required");
								$("#established_year").rules("remove", "required");
								$("#no_of_employee").rules("remove", "required");
								$("#legal_entity").rules("remove", "required");
								$("#reported_sales").rules("remove", "required");
								$("#run_rate_sales").rules("remove", "required");
								$("#ebitda_margin").rules("remove", "required");
							}
						} 
					});
				
				} 
			});

		}
		


	});


	$("form[name='business_template_form']").validate({
        rules: {
            template_category_id: 'required',
            template_sub_category_id: 'required'
        },
        messages: {
            template_category_id: 'Please select template category.',
            template_sub_category_id: 'Please select template sub category.',
            business_category_id:'Please select business category.',
            title:'Please enter business title.',
            description:'Please enter business description.',
            established_year:'Please enter established year.',
            no_of_employee:'Please enter number of employee.',
            legal_entity:'Please enter legal entity.',
            reported_sales:'Please enter reported sales.',
            run_rate_sales:'Please enter run rate sales.',
            ebitda_margin:'Please enter ebitda margin.',
            location:'Please enter location.',
            email:'Please enter email.',
            mobile_no:'Please enter mobile number.'
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "platforms[]")
                error.insertAfter("#platforms_msg");                       
            else
                error.insertAfter(element);
        }
    });





	$(document).on('change','.template_category_id',function(){
		var template_category_id = $(this).val();
		var get_slug = $('select#template_category_id').find(':selected').data('slug');

		if(get_slug == "m&a"){

			$('.template_sub_category_box').addClass('d-none');
			$("#template_sub_category_id").rules("remove", "required");

			$('.loader').show();
			$.ajax({
				url: "{{ route('front.get.business_template.field') }}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
				},
				type: "POST",
				data:{
					'template_category_id':template_category_id,
					'is_main':1,
					'is_edit':0,
				},
				success: function(result) {
					$('.loader').hide();
					$('.dynamic_field_section').html(result.html);

					$("#business_category_id").rules("add", "required");
					$("#title").rules("add", "required");
					$("#description").rules("add", "required");
					$("#established_year").rules("add", "required");
					$("#no_of_employee").rules("add", "required");
					$("#legal_entity").rules("add", "required");
					$("#reported_sales").rules("add", "required");
					$("#run_rate_sales").rules("add", "required");
					$("#ebitda_margin").rules("add", "required");
					$("#location").rules("add", "required");
					$("#email").rules("add", "required");
					$("#mobile_no").rules("add", "required");
				} 
			});

		}
		else{
			$('.template_sub_category_box').removeClass('d-none');
        	$("#template_sub_category_id").rules("add", "required");

        	$('.loader').show();
        	$.ajax({
				url: "{{ route('front.get.get_business_template_sub_category') }}",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
				},
				type: "POST",
				data:{
					'template_category_id':template_category_id
				},
				success: function(result) {
					$('.loader').hide();
					$('.template_sub_category_id').html(result);
					$('.dynamic_field_section').html('');

					$("#business_category_id").rules("remove", "required");
					$("#title").rules("remove", "required");
					$("#description").rules("remove", "required");
					$("#established_year").rules("remove", "required");
					$("#no_of_employee").rules("remove", "required");
					$("#legal_entity").rules("remove", "required");
					$("#reported_sales").rules("remove", "required");
					$("#run_rate_sales").rules("remove", "required");
					$("#ebitda_margin").rules("remove", "required");
					$("#location").rules("remove", "required");
					$("#email").rules("remove", "required");
					$("#mobile_no").rules("remove", "required");
				} 
			});

		}
		
	});


	$(document).on('change','.template_sub_category_id',function(){
		var template_sub_category_id = $(this).val();
		var get_slug = $('select#template_sub_category_id').find(':selected').data('slug');

		$('.loader').show();
		$.ajax({
			url: "{{ route('front.get.business_template.field') }}",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			},
			type: "POST",
			data:{
				'template_category_id':template_sub_category_id,
				'is_main':0,
				'is_edit':0,
			},
			success: function(result) {
				$('.loader').hide();
				$('.dynamic_field_section').html(result.html);
				if(get_slug == "seeking_funds" || get_slug == "offering" || get_slug == "seeking_services" || get_slug == "buyer"){

					$("#business_category_id").rules("add", "required");
					$("#title").rules("add", "required");
					$("#description").rules("add", "required");
					$("#established_year").rules("add", "required");
					$("#no_of_employee").rules("add", "required");
					$("#legal_entity").rules("add", "required");
					$("#reported_sales").rules("add", "required");
					$("#run_rate_sales").rules("add", "required");
					$("#ebitda_margin").rules("add", "required");
					$("#location").rules("add", "required");
					$("#email").rules("add", "required");
					$("#mobile_no").rules("add", "required");
				}
				else if(get_slug == "offering_funds" || get_slug == "offering_services"){

					$("#business_category_id").rules("add", "required");
					$("#description").rules("add", "required");
					$("#location").rules("add", "required");
					$("#email").rules("add", "required");
					$("#mobile_no").rules("add", "required");

					$("#title").rules("remove", "required");
					$("#established_year").rules("remove", "required");
					$("#no_of_employee").rules("remove", "required");
					$("#legal_entity").rules("remove", "required");
					$("#reported_sales").rules("remove", "required");
					$("#run_rate_sales").rules("remove", "required");
					$("#ebitda_margin").rules("remove", "required");
				}
			} 
		});
	});

</script>


@endsection


