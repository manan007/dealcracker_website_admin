
@extends('auth.include.main')
@section('title','About Us')
@section('css')
@endsection

@section('content')
<!-- <section class="breatcome_area" style="background-image: url('{{ asset('assets/frontend/images/slider/slide1.jpg')}}');">
		<div class="container">
			<h4>About Us</h4>
		</div>
	</section> -->
	<section class="commform regstep contactus">
		<div class="container">
			<div class="titlebox">
				<!-- <h2>Coming Soon</h2> -->
			</div>
			
			
<p style='margin-top:0in;margin-right:0in;margin-bottom:8.0pt;margin-left:0in;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'>Dealcracker&trade; used to be a business group comprising of CAs, CSs, Investment Bankers and Corporate Lawyers since September 2015. The group has been active in Fund raising (debt &amp; equity), JVs, Buy/Sell, M&amp;A and Strategy consulting.</p>
<p style='margin-top:0in;margin-right:0in;margin-bottom:8.0pt;margin-left:0in;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'>Going forward, our presence on the web will help us in our own private market network while connecting with Business owners, Entrepreneurs, CEOs, Investors, Acquirers, Business Buyers, Lenders and Financial Advisors from different parts of the world discover each other and pursue business opportunities.</p>
<p style='margin-top:0in;margin-right:0in;margin-bottom:8.0pt;margin-left:0in;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'>Our platform lists opportunities in deal origination, valuation, matching and introducing businesses to investors/advisors across India.</p>
<p style='margin-top:0in;margin-right:0in;margin-bottom:8.0pt;margin-left:0in;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'>(Only subscribers can access this website).</p>
				
		</div>
	</section>
@endsection
@section('scripts')


@endsection	
    
