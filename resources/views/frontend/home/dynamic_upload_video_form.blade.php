<div class="modal fade sartpost uploadvideo" id="upload-video-text" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Edit Video</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      
      <div class="modal-body" style="height:auto;max-height:640px;overflow-y:auto;">

      <form id="uploadForm" action="{{ route('ajax.video.post') }}" method="post">
      @csrf
      <div class="data_upload_video"></div>
      
      
        
      <div class="modal-footer">
        <div class="right-part right-submit"><div><input type="submit" id="btnSubmit" value="Upload" class="btn btn-default1 btnSubmit" /></div></div>
        <div class="right-part right-done">
          <a href="#" class="closebtn" data-dismiss="modal" aria-label="Close">Cancel</a>
          <a href="javascript:;" type="button" id="done_post_media_video_text">Done</a>
        </div>
      </div>    
      </form>
      </div>
      
      
    </div>
  </div>
</div>
