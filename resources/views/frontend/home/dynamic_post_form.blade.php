

<div class="modal fade sartpost mainpost-box" id="start-post" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Create a feed</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			
			<form action="{{ route('store.mainpost') }}" method="post" id="create_post_form">
			@csrf

				<div class="modal-body" style="height:auto;max-height:640px;overflow-y:auto;">
				<div class="row">
					<div class="col-md-12 alert alert-danger error-msgs d-none"></div>
				</div>
				<div id="dynamic_post_data_model">
				
				</div>
				</div>
			
			</form>

			<div class="modal-footer">
				<div class="left-part" id="model_custom_footer">
					<ul>
					@if(count($getpost_media) == 0)
						<!-- <li><a href="javascript:;" class="subbox upload-photo-btn" id="upload-photo-btn"><i class="far fa-image"></i></a></li>
						<li><a href="javascript:;" class="subbox upload-video-btn" id="upload-video-btn"><i class="fab fa-youtube"></i></a></li> -->
						<!-- <li><a href="javascript:;" class="subbox upload-document-btn" id="upload-document-btn"><i class="far fa-file-alt"></i></a></li> -->    
					@else
						<!-- <li><a href="javascript:;" class="subbox" style="cursor: not-allowed;"><i class="far fa-image"></i></a></li>
						<li><a href="javascript:;" class="subbox" style="cursor: not-allowed;"><i class="fab fa-youtube"></i></a></li> -->
						<!-- <li><a href="javascript:;" class="subbox" style="cursor: not-allowed;"><i class="far fa-file-alt"></i></a></li> -->
					@endif
					</ul>
				</div>
				<div class="right-part">
					<a href="javascript:;" type="button" id="create_post">Post</a>
				</div>
			</div>
			<div class="visibilitysection">
				<div class="modal-header">
					<div class="backbtt"><i class="fas fa-chevron-left"></i></div>
					<h4 class="modal-title" id="myModalLabel">Who can see your post?</h4>
				</div>
				<div class="visibility-body">
					<p>Your post will be visible on feed, on your profile and in search results</p>
					<ul >
						<li class="anyone">
							<div class="type">
								<h4>Anyone</h4>
								<span>Anyone on or off esportsrecruiter</span>
							</div>
							<div class="input-part">
								<input type="radio" name="radio" class="inputradio">
							</div>
						</li>
						<li class="connect-only">
							<div class="type">
								<h4>Connections only</h4>
								<span>Connections on esportsrecruiter</span>
							</div>
							<div class="input-part">
								<input type="radio" name="radio" class="inputradio">
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>


