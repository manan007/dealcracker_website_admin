<div class="modal fade sartpost uploadphoto" id="upload-photo-text" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Edit Photo</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      
      <div class="modal-body" style="height:auto;max-height:640px;overflow-y:auto;">
      <div class="data_upload"></div>
        
          
          
      </div>
      <div class="modal-footer">
        <div class="right-part">
          <a href="#" class="closebtn" data-dismiss="modal" aria-label="Close">Cancel</a>
          <a href="javascript:;" type="button" id="done_post_media_text">Done</a>
        </div>
      </div>
      
    </div>
  </div>
</div>
