
<style type="text/css">
    .like-com li a .gallery_i {top: 0px; }
</style>
@if(isset($post_id))
    @if($getlikes_data)
        @if($post_id == $getlikes_data->feed_id)
            <a href="javascript:;" class="create_like_gallery btn-sm btn-default like" id="gallery_like_{{ $post_id }}" data-post-id="{{ $post_id }}"><i class="fas fa-thumbs-up gallery_i"></i> Like</a>
        @else
            <a href="javascript:;" class="create_like_gallery btn-sm btn-default" id="gallery_like_{{ $post_id }}" data-post-id="{{ $post_id }}"><i class="fas fa-thumbs-up gallery_i"></i> Like</a>
        @endif
    @else
        <a href="javascript:;" class="create_like_gallery btn-sm btn-default" id="gallery_like_{{ $post_id }}" data-post-id="{{ $post_id }}"><i class="fas fa-thumbs-up gallery_i"></i> Like</a>
    @endif
@else
<a href="javascript:;" class="create_like_gallery btn-sm btn-default @if($getpost_like) like @endif" id="gallery_like_{{ $post->id }}" data-post-id="{{ $post->id }}"><i class="fas fa-thumbs-up gallery_i"></i> Like</a>
@endif