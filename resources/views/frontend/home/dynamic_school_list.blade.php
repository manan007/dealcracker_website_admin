
@php
$freeplan = App\Models\Plan::where(['slug' => 'free'])->first();    
$get_userplan =  App\Models\UserPlan::where(['user_id' => Auth::user()->id])->first();
@endphp

<div class="suggestions-list">
@if(count($schools) > 0)
    @php $counts = 1;  @endphp
    @foreach($schools as $key =>  $school)
        @if(!in_array($school->id,$user_arr))
        @if($counts <= 5)
        <div class="suggestion-usd">
            <!--<img src="{{ asset('assets/frontend/images/resources/s1.png') }}" alt="">-->
            <div class="row">
                <div class="col-md-8 sgt-text">
                <h4><a href="{{ route('school.profile',$school->slug) }}" class='link_label'>{{ $school->school_name }}</a></h4>
                </div>
                <div class="col-md-4">
                @if($get_userplan)
                @if($freeplan->id != $get_userplan->plan_id)
                <span><a href="javascript:;" class="btn btn-default1 follow_btn" data-school-id="{{ $school->id }}">Follow</a></span>
                @else
                <span><a href="javascript:;" class="btn btn-default1 follow_btn_plan">Follow</a></span>
                @endif
                @endif
                </div>
            </div>
        </div>
        @php $counts = $counts + 1;  @endphp
        @endif
        @endif
    @endforeach
    <div class="view-more">
        <a href="{{ route('college.view') }}" title="">View More</a>
    </div>
@endif
</div>


{{--
<div class="suggestions-list">
    <div class="suggestion-usd">
        <img src="{{ asset('assets/frontend/images/resources/s1.png') }}" alt="">
        <div class="sgt-text">
            <h4>Jessica William</h4>
            <span>Graphic Designer</span>
        </div>
        <span><i class="la la-plus"></i></span>
    </div>
    <div class="suggestion-usd">
        <img src="{{ asset('assets/frontend/images/resources/s2.png') }}" alt="">
        <div class="sgt-text">
            <h4>John Doe</h4>
            <span>PHP Developer</span>
        </div>
        <span><i class="la la-plus"></i></span>
    </div>
    <div class="suggestion-usd">
        <img src="{{ asset('assets/frontend/images/resources/s3.png') }}" alt="">
        <div class="sgt-text">
            <h4>Poonam</h4>
            <span>Wordpress Developer</span>
        </div>
        <span><i class="la la-plus"></i></span>
    </div>
    <div class="suggestion-usd">
        <img src="{{ asset('assets/frontend/images/resources/s4.png') }}" alt="">
        <div class="sgt-text">
            <h4>Bill Gates</h4>
            <span>C &amp; C++ Developer</span>
        </div>
        <span><i class="la la-plus"></i></span>
    </div>
    <div class="suggestion-usd">
        <img src="{{ asset('assets/frontend/images/resources/s5.png') }}" alt="">
        <div class="sgt-text">
            <h4>Jessica William</h4>
            <span>Graphic Designer</span>
        </div>
        <span><i class="la la-plus"></i></span>
    </div>
    <div class="suggestion-usd">
        <img src="{{ asset('assets/frontend/images/resources/s6.png') }}" alt="">
        <div class="sgt-text">
            <h4>John Doe</h4>
            <span>PHP Developer</span>
        </div>
        <span><i class="la la-plus"></i></span>
    </div>
    <div class="view-more">
        <a href="#" title="">View More</a>
    </div>
</div><!--suggestions-list end-->
--}}