<style type="text/css">
    ul.tabs{
        margin: 0px;
        padding: 0px;
        list-style: none;
        display: inline-flex;
    }
    ul.tabs li{
        background: none;
        color: #222;
        display: inline-block;
        padding: 10px 15px;
        cursor: pointer;
    }

    ul.tabs li.current{
        /*background: #ededed;*/
        border-bottom: 5px solid #e44d3a;
        color: #222;
    }

    .tab-content{
        display: none;
        background: #fff;
        padding: 15px;
        max-height: 450px;
        overflow-y: auto;
    }

    .tab-content.current{
        display: inherit;
    }
    .pdall{
        padding-top: 3px;
    }
    hr{
        margin-top: 0px;
    }
    .likecounts{
        line-height: 1.5;
        padding-left: 10px;
        font-size: 20px;
    }
    .likecountsall{
        line-height: 0.8;
        padding-left: 10px;
        font-size: 20px!important;
        font-weight: 500;
    }
    .alltext{
        font-size: 22px!important;
    }
</style>

@php
    $getemojies = App\Models\LikeEmoji::all();
@endphp

<div class="container comment_likes_box p-0">

        <ul class="tabs">
            <li class="tab-link current" data-tab="comment_tab-All">
                @php $getlikes_details_count_all = App\Models\CommentLike::where(['post_id' => $post_id,'comment_id' => $comment_id])->count('id');  @endphp
                <h3 height="30" width="30" class="font-weight-bold pdall"><span class="alltext">All</span> <span class="likecountsall">{{ $getlikes_details_count_all }}</span></h3>
            </li>
            @if(count($getemojies) > 0)
                @foreach($getemojies as $key => $getemojie)
                @php $getlikes_details_count = App\Models\CommentLike::where(['post_id' => $post_id,'comment_id' => $comment_id,'like_emoji_id' => $getemojie->id])->count('id');  @endphp
                    @if($getlikes_details_count > 0)
                    <li class="tab-link" data-tab="comment_tab-{{ $key }}">
                        <img src="{{asset('public/uploads/emoji/'.$getemojie->image)}}"  height="30" width="30" class="emojibtn_class"><span class="likecounts">{{ $getlikes_details_count }}</span>
                    </li>
                    @endif
                @endforeach
            @endif
        </ul>
        <hr/>

        @php $getlikes_details_all = App\Models\CommentLike::where(['post_id' => $post_id,'comment_id' => $comment_id])->get();  @endphp
        <div id="comment_tab-All" class="tab-content current">
            <table class="tablewidth">
                <tbody>
                    @if(count($getlikes_details_all) > 0)
                        @foreach($getlikes_details_all as $getlike_data)
                            <tr class="borderbottom">
                                <td class="pb-2 pt-2">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="usy-dt">
                                                @if($getlike_data->user->profile_image)
                                                    <a href="{{ route('user.profile',strtolower($getlike_data->user->first_name.'_'.$getlike_data->user->last_name)) }}"><img src="{{ url('public/uploads/profile_image/'.$getlike_data->user->profile_image)}}" alt="" width="50" height="50"></a>
                                                @else
                                                    <a href="{{ route('user.profile',strtolower($getlike_data->user->first_name.'_'.$getlike_data->user->last_name)) }}"><img src="{{ asset('assets/frontend/images/user.jpg')}}" alt="" width="50" height="50"></a>
                                                @endif
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                {{ $getlike_data->user->first_name ?? '' }}  {{ $getlike_data->user->last_name ?? '' }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="pb-2 pt-2">No Record Found.</td>
                        </tr>
                    @endif    
                </tbody>
            </table>
        </div>

        <div class="dynamic_tab_likes d-none"> 
        @if(count($getemojies) > 0)
            @foreach($getemojies as $key => $getemojie)

                @php $getlikes_details = App\Models\CommentLike::where(['post_id' => $post_id,'comment_id' => $comment_id,'like_emoji_id' => $getemojie->id])->get();  @endphp
                <div id="comment_tab-{{ $key }}" class="tab-content @if($key == 0) current @endif">
                    <table class="tablewidth">
                        <tbody>
                            @if(count($getlikes_details) > 0)
                                @foreach($getlikes_details as $getlike_data)
                                    <tr class="borderbottom">
                                        <td class="pb-2 pt-2">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="usy-dt">
                                                        @if($getlike_data->user->profile_image)
                                                            <a href="{{ route('user.profile',strtolower($getlike_data->user->first_name.'_'.$getlike_data->user->last_name)) }}"><img src="{{ url('public/uploads/profile_image/'.$getlike_data->user->profile_image)}}" alt="" width="50" height="50"></a>
                                                        @else
                                                            <a href="{{ route('user.profile',strtolower($getlike_data->user->first_name.'_'.$getlike_data->user->last_name)) }}"><img src="{{ asset('assets/frontend/images/user.jpg')}}" alt="" width="50" height="50"></a>
                                                        @endif
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                        {{ $getlike_data->user->first_name ?? '' }}  {{ $getlike_data->user->last_name ?? '' }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            {{--@else
                                <tr>
                                    <td class="pb-2 pt-2">No Record Found.</td>
                                </tr>--}}
                            @endif    
                        </tbody>
                    </table>
                </div>
            @endforeach
        @endif
        </div>
        

</div><!-- container -->


