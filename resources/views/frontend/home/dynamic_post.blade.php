<div class="posts-section">
    <div class="mainRow">
    @if(count($posts) > 0)
        @php $postcnt = 0; @endphp
        @foreach($posts as $post)

            @php $postcnt = $postcnt + 1; @endphp
            @php $getpost_media = App\Models\FeedMedia::where('feed_id',$post->id)->get();
                 $getpost_like = App\Models\FeedLike::where(['user_id' => Auth::user()->id,'feed_id' => $post->id])->first();
                 $getpostlike_count = App\Models\FeedLike::where(['feed_id' => $post->id])->count('id');

                 $comments = App\Models\FeedComment::with('user')->where(['feed_id' => $post->id])->orderBy('id','desc')->get();
                 $getemojies = [];
            @endphp

           
                <input type="hidden" class="post_ids" value="{{$post->id}}">

                <div class="post-bar">
                    @php $getshared_post = 0;  @endphp

                    <div class="post_topbar">
                        <div class="usy-dt">
                            @if(isset($post->user->profile))
                                @if(Auth::user()->id == $post->user->id)
                                <a href="{{ route('front.my_profile.index',$post->user->id) }}" class='link_label'><img src="{{ asset('upload/profile/'.$post->user->profile)}}" alt="" width="40" height="40"></a>
                                @else
                                <a href="{{ route('front.my_profile.index',$post->user->id) }}" class='link_label'><img src="{{ asset('upload/profile/'.$post->user->profile)}}" alt="" width="40" height="40"></a>    
                                @endif
                            @else
                                @if(Auth::user()->id == $post->user->id)
                                <a href="{{ route('front.my_profile.index',$post->user->id) }}" class='link_label'><img src="{{ asset('frontend/images/user.jpg')}}" alt="" width="40" height="40"></a>
                                @else
                                <a href="{{ route('front.my_profile.index',$post->user->id) }}" class='link_label'><img src="{{ asset('frontend/images/user.jpg')}}" alt="" width="40" height="40"></a>
                                @endif
                            @endif
                            <div class="usy-name">
                                @if(Auth::user()->id == $post->user->id)
                                <a href="{{ route('front.my_profile.index',$post->user->id) }}" class='link_label'><h3>{{ isset($post->user->name) ? $post->user->name:'' }}</h3></a>
                                @else
                                <a href="{{ route('front.my_profile.index',$post->user->id) }}" class='link_label'><h3>{{ isset($post->user->name) ? $post->user->name:'' }}</h3></a>
                                @endif
                                <span><img src="{{ asset('frontend/images/clock.png') }}" alt="">{{ $post->created_at->diffForHumans() }}</span>
                            </div>
                        </div>
                        @if($post->user_id == Auth::user()->id)
                        <div class="ed-opts">
                            <a href="javascript:;" title="" class="ed-opts-open post_collapse" data-post-id="{{ $post->id }}"><i class="la la-ellipsis-v"></i></a>
                            <ul class="ed-options" id="ed-options-{{ $post->id }}">
                                <li><a href="javascript:;" title="Edit Post" class="edit_post" data-post-id="{{ $post->id }}">Edit Post</a></li>
                                <li><a href="javascript:;" title="Delete Post" class="delete_post" data-post-id="{{ $post->id }}">Delete Post</a></li>
                            </ul>
                        </div>
                        @endif
                    </div>

                    <div class="epi-sec">
                        <ul class="descp" style="margin-bottom:0px">
                            <!-- <li><img src="{{ asset('assets/frontend/images/icon8.png') }}" alt=""><span>Epic Coder</span></li> -->
                            <!-- <li><img src="{{ asset('assets/frontend/images/icon9.png') }}" alt=""><span>India</span></li> -->
                        </ul>
                        <!--<ul class="bk-links">
                            <li><a href="#" title=""><i class="la la-bookmark"></i></a></li>
                            <li><a href="#" title=""><i class="la la-envelope"></i></a></li>
                        </ul>-->
                    </div>
                    <div class="job_descp">
                        <!-- <h3>Senior Wordpress Developer</h3> -->
                        <!-- <ul class="job-dt">
                            <li><a href="#" title="">Full Time</a></li>
                            <li><span>$30 / hr</span></li>
                        </ul> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam luctus hendrerit metus, ut ullamcorper quam finibus at. Etiam id magna sit amet... <a href="#" title="">view more</a></p>-->
                        @if($post->title)
                        <h5 class="h5 m-0">{{ $post->title }}</h5>
                        @endif
                        @if(strlen($post->description) > 142)
                      
                        <!-- view more -->
                        <p class="viewtextpost"  id="view_more_{{ $post->id }}">
                            {{ $post->description }}
                            <br/><a href="javascript:;" class="view_more" data-post-id="{{ $post->id }}" title="View Less">view more</a>
                        </p>

                        <!-- view less -->
                        <p class="viewtextpost hide" id="view_less_{{ $post->id }}">{{ $post->description }}<br/><a href="javascript:;" class="view_less" data-post-id="{{ $post->id }}" 
                            title="View More">view less</a></p>
                        @else
                        <p>
                           {{ $post->description }}
                        </p>
                        @endif

                        @if(count($getpost_media) > 0)
                        
                            @if(count($getpost_media) == 1)
                                @foreach($getpost_media as $media)
                                    <div class="gallerybox">						
                                        <div class="col12">
                                            <div class="imgbox">
                                                @if($media->type ==  "image")
                                                    <img class="w-100" src="{{asset('upload/post/'.$media->feed_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}" data data-slide-to="0">
                                                @elseif($media->type ==  "video")
                                                    <video width="100%" height="100%" controls>
                                                        <source src="{{asset('upload/post/'.$media->feed_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
                                                    </video>
                                                @endif    
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @elseif(count($getpost_media) == 2)
                                @foreach($getpost_media as $media)
                                    <div class="gallerybox">
                                        <div class="col2">
                                            <div class="imgbox">
                                                @if($media->type ==  "image")
                                                    <img class="w-100" src="{{asset('upload/post/'.$media->feed_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}" data-post-id="{{ $post->id }}" data-slide-to="0">
                                                @elseif($media->type ==  "video")
                                                    <video width="100%" height="100%" controls>
                                                        <source src="{{asset('upload/post/'.$media->feed_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
                                                    </video>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @elseif(count($getpost_media) == 3)
                                @foreach($getpost_media as $key => $media)
                                    <div class="gallerybox">
                                        @if($key == 0)						
                                        <div class="col12">
                                            <div class="imgbox">
                                                @if($media->type ==  "image")
                                                    <img class="w-100" src="{{asset('upload/post/'.$media->feed_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}" data-post-media-id="{{ $media->id }}" data-post-id="{{ $post->id }}" data-slide-to="0">
                                                @elseif($media->type ==  "video")
                                                    <video width="100%" height="100%" controls>
                                                        <source src="{{asset('upload/post/'.$media->feed_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
                                                    </video>
                                                @endif
                                            </div>
                                        </div>
                                        @else
                                        <div class="col2">
                                            <div class="imgbox">
                                                @if($media->type ==  "image")
                                                    <img class="w-100" src="{{asset('upload/post/'.$media->feed_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}" data-post-id="{{ $post->id }}" data-slide-to="0">
                                                @elseif($media->type ==  "video")
                                                    <video width="100%" height="100%" controls>
                                                        <source src="{{asset('upload/post/'.$media->feed_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
                                                    </video>
                                                @endif
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                @endforeach
                            @elseif(count($getpost_media) == 4)
                                @foreach($getpost_media as $media)
                                    <div class="gallerybox">
                                        <div class="col2">
                                            <div class="imgbox">
                                                @if($media->type ==  "image")
                                                    <img class="w-100" src="{{asset('upload/post/'.$media->feed_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}" data-post-id="{{ $post->id }}" data-slide-to="0">
                                                @elseif($media->type ==  "video")
                                                    <video width="100%" height="100%" controls>
                                                        <source src="{{asset('upload/post/'.$media->feed_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
                                                    </video>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @elseif(count($getpost_media) == 5)
                                @foreach($getpost_media as $key => $media)
                                    <div class="gallerybox">
                                        @if($key == 0 || $key == 1)						
                                        <div class="col2">
                                            <div class="imgbox">
                                                @if($media->type ==  "image")
                                                    <img class="w-100" src="{{asset('upload/post/'.$media->feed_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}" data-post-id="{{ $post->id }}" data-slide-to="0">
                                                @elseif($media->type ==  "video")
                                                    <video width="100%" height="100%" controls>
                                                        <source src="{{asset('upload/post/'.$media->feed_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
                                                    </video>
                                                @endif
                                            </div>
                                        </div>
                                        @else
                                        <div class="col1">
                                            <div class="imgbox">
                                                @if($media->type ==  "image")
                                                    <img class="w-100" src="{{asset('upload/post/'.$media->feed_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}" data-post-id="{{ $post->id }}" data-slide-to="0">
                                                @elseif($media->type ==  "video")
                                                    <video width="100%" height="100%" controls>
                                                        <source src="{{asset('upload/post/'.$media->feed_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
                                                    </video>
                                                @endif
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                @endforeach
                            @endif

                            
                        @endif
                        

                        {{--
                        @if(count($getpost_media) > 0)
                        
                            <div class="row">
                                @foreach($getpost_media as $media)
                                    <div class="col-md-6" style="margin-bottom:10px">
                                        @if($media->type == "image")
                                            <img src="{{asset('upload/post/'.$media->post_media)}}" height="150" width="150"> 
                                        @endif
                                        @if($media->type == "video")
                                        <video width="100%" height="150px" controls>
                                            <source src="{{asset('upload/post/'.$media->post_media)}}" type='video/mp4'>
                                        </video>
                                        @endif
                                        @if($media->type == "document")
                                        <a href="{{asset('upload/post/'.$media->post_media)}}" target='_blank'>{{$media->post_media}}</a>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        @endif--}}
                    </div>



                    <div class="checkimg"></div>
                    <div class="job-status-bar border-none">
                        {{--  && $post->parent_post_id == 0 --}}
                        @if($post->shared_post_id != 0)
                        @php  $getshared_post = App\Models\Post::where(['id' => $post->shared_post_id])->first(); @endphp
                        <div class="share-details-row mt-3">
                            <div class="usy-name-share"> 
                                @if(Auth::user()->id == $getshared_post->user->id)
                                <a href="#" class='link_label'><h3>{{ isset($getshared_post->user->first_name) ? $getshared_post->user->first_name:'' }} {{ isset($getshared_post->user->last_name) ? $getshared_post->user->last_name:'' }}</h3></a>
                                @else
                                <a href="#" class='link_label'><h3>{{ isset($getshared_post->user->first_name) ? $getshared_post->user->first_name:'' }} {{ isset($getshared_post->user->last_name) ? $getshared_post->user->last_name:'' }}</h3></a>
                                @endif
                                <span>{{ date('d M Y',strtotime($getshared_post->created_at)) }} at {{ date('h:i:s A',strtotime($getshared_post->created_at)) }}</span>
                            </div>
                        </div>
                        @endif

                        <ul class="like-com mt-0">
                            <li class="mr-0">
                                <button class="emjbtn_like">
                                    
                                    {{--
                                    @if($getpost_like)
                                        @php $getemojies_details = App\Models\LikeEmoji::where('id',$getpost_like->like_emoji_id)->first();   @endphp

                                        @if($getemojies_details)
                                        <a href="javascript:;" class="create_like" data-dislike-status="1" data-emoji-id="{{ $getemojies_details->id }}" 
                                            data-post-id="{{ $post->id }}">
                                            <img src="{{asset('public/uploads/emoji/'.$getemojies_details->image)}}"  height="30" width="30" class="emojibtn_class">
                                        </a>
                                        @else
                                        <a href="javascript:;">
                                            <img src="{{asset('assets/frontend/images/default_like.png')}}" height="30" width="30" class="emojibtn_class">
                                        </a>
                                        @endif
                                    @else
                                        <a href="javascript:;">
                                            <img src="{{asset('assets/frontend/images/default_like.png')}}" height="30" width="30" class="emojibtn_class">
                                        </a>
                                    @endif
                                    --}}

                                    <div class="emoji-container">
                                        @if(count($getemojies) > 0)
                                        @foreach($getemojies as $keyemoji =>  $getemojie)
                                            <div class="emoji">
                                                <div class="icon create_like" data-emoji-id="{{ $getemojie->id }}" data-dislike-status="0" data-post-id="{{ $post->id }}">
                                                    <img src="{{asset('public/uploads/emoji/'.$getemojie->image)}}" height="50">
                                                </div>
                                            </div>
                                        @endforeach
                                        @endif
                                    </div>
                                </button>
                            </li>
                            
                               {{-- @php $getpost_likes = App\Models\FeedLike::where(['feed_id' => $post->id])->where('user_id','!=',Auth::user()->id)->get();  @endphp

                                @if(count($getpost_likes) > 0)
                                    <li>
                                        @foreach($getpost_likes as $likes_key => $getpostlike)
                                            @php $getemojies_likes_details = App\Models\LikeEmoji::where('id',$getpostlike->like_emoji_id)->first();   @endphp
                                            <img src="{{asset('public/uploads/emoji/'.$getemojies_likes_details->image)}}"  height="30" width="30" class="@if($likes_key != 0) emojibtn_class1 @endif">
                                        @endforeach
                                    </li>
                                @endif
                                
                                --}}
                            
                            <!--== For Like ==-->
                            
                            <li class="mt-2">
                                <!-- <div class="dynamic_like_box">                        
                                    <a href="javascript:;" class="create_like"><i class="fas fa-thumbs-up"></i> Like</a>
                                </div> -->
                                <span class="ml-0 likebt">
                                    <a href="javascript:;" class="create_like @if($getpost_like) like @endif" data-post-id="{{ $post->id }}"><i class="fas fa-thumbs-up"></i></a>
                                </span>
                                @if($getpostlike_count != 0)
                                <span class="ml-0 like_show mrcountbox_postlike" data-post-id="{{ $post->id }}">{{ $getpostlike_count }}</span>
                                @else
                                <span class="ml-0 mrcountbox_postlike" data-post-id="{{ $post->id }}">{{ $getpostlike_count }}</span>
                                @endif
                            </li>
                            

                            <!-- <li>
                                <a href="#"><i class="fas fa-heart"></i> Like</a>
                                <img src="{{ asset('assets/frontend/images/liked-img.png') }}" alt="">
                                <span>25</span>
                            </li>   -->
                            {{--
                            <li><a href="javascript:;" class="com @if(count($comments) > 2) com_show com_{{$post->id}} @endif" data-post_id="{{$post->id}}"><i class="fas fa-comment-alt"></i>  @if(count($comments) > 0) Comments {{count($comments)}} @else Comment @endif</a></li>
                            
                            <li><a href="javascript:;" class="share-opts-open com ml-1" id="{{$post->id}}"><i class="fa fa-share"></i>Share</a></li>

                            
                            <ul class="share-options" data-active="inactive" id="share-options-{{$post->id}}">
                                <form action="{{ route('share.post') }}" method="post" id="share-options-form-{{$post->id}}">
                                @csrf
                                    <input type="hidden" name="post_id" value="{{ $post->id }}"> 
                                    <input type="hidden" name="post_user_id" value="{{ $post->user_id }}">
                                    <li class="sharenowli"><button type="submit" title="Share now" class="share_now">Share now (Only me)</button></li>
                                </form>
                                <!-- <li><a href="javascript:;" title="Share to News Feed" class="share_to_news_feed" data-post-user-id="{{ $post->user_id }}" data-post-id="{{$post->id}}">Share to News Feed</button></li> -->
                            </ul>
                            --}}
                        </ul>
                        {{-- <a href="#"><i class="fas fa-eye"></i>Views {{$post->views}}</a>  --}}
                    </div>
                    
                    <div class="post-comment mt-2">
                        <div class="cm_img">
                            @if(isset(Auth::user()->profile))
                                @if(Auth::user()->profile != null)
                                    <a href="{{ route('front.my_profile.index',Auth::user()->id) }}"><img src="{{ asset('upload/profile/'.Auth::user()->profile)}}" class="borderradius" alt="" width="40" height="40"></a>
                                @else
                                    <a href="{{ route('front.my_profile.index',Auth::user()->id) }}"><img src="{{ asset('frontend/images/user.jpg')}}" alt="" width="40" class="borderradius" height="40"></a>
                                @endif
                            @else
                                <a href="{{ route('front.my_profile.index',Auth::user()->id) }}"><img src="{{ asset('frontend/images/user.jpg')}}" alt="" width="40" class="borderradius" height="40"></a>
                            @endif
                            <!--<img src="{{ asset('assets/frontend/images/resources/bg-img4.png') }}" alt="">-->
                        </div>
                        <div class="comment_box">
                            <form>
                                <input type="text" class="commentdiv_{{$post->id}} commenttext" data-id="{{$post->id}}" placeholder="Post a comment">
                                <button type="button" class="commentdiv" data-id="{{$post->id}}">Send</button>
                            </form>
                        </div>
                    </div>

                    <div class="comment-section">                      
                        <div class="comment-sec">
                            <ul>
                                @if(count($comments->where('is_reply',0)) > 0)
                                    @foreach($comments->where('is_reply',0) as $key => $comment)

                                        @if(count($comments->where('is_reply',0)) > 2 && $key > 1)
                                            <li class="hideCommentDiv hideCommentDiv_{{$comment->feed_id}}">
                                        @else
                                            <li>
                                        @endif
                                                <div class="comment-list pt-3">
                                                    <div class="bg-img">
                                                        @if($comment->user && $comment->user->profile != null) 
                                                            <img src="{{ asset('upload/profile/'.$comment->user->profile)}}" alt="" class="borderradius" alt="" width="40" height="40">
                                                        @else
                                                        <img src="{{asset('frontend/images/user.jpg')}}" alt="" class="borderradius" alt="" width="40" height="40">
                                                        @endif
                                                    </div>
                                                    <div class="comment">

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="comment-profile">
                                                                    @if($comment->user && $comment->user->profile != null)
                                                                    <a href="{{ route('front.my_profile.index',$comment->user->id) }}"><img src="{{ asset('upload/profile/'.$comment->user->profile)}}" alt="" class="borderradius" alt="" width="40" height="40"></a>
                                                                    @else
                                                                    <a href="{{ route('front.my_profile.index',$comment->user->id) }}"><img src="{{asset('frontend/images/user.jpg')}}" alt="" class="borderradius" alt="" width="40" height="40"></a>
                                                                    @endif
                                                                    <!--<img src="images/resources/us-pc2.png" alt="">-->
                                                                </div>
                                                                <div class="comment-part commentboxs">
                                                                    <div class="box">
                                                                        @if($comment->user && $comment->user->name != null)
                                                                            <a href="{{ route('front.my_profile.index',$comment->user->id) }}"><h3>{{$comment->user->name}} </h3></a>
                                                                        @else
                                                                            <a href="{{ route('front.my_profile.index',$comment->user->id) }}"><h3>User</h3></a>
                                                                        @endif
                                                                        <span style="float: right!important;"><i class="fas fa-clock"></i> {{ $comment->created_at->diffForHumans() }}</span>
                                                                        <p>{{$comment->comment}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row mt-1 ml40cmt">
                                                            <!-- <div class="col-md-1 comment_first_sec"></div> -->
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-2 pl-0 pr-0 widthcolmd2">
                                                                <button class="emjbtn_comment">
                                                                    @php $getpost_comment_like = App\Models\FeedCommentLike::where(['user_id' => Auth::user()->id,'feed_id' => $post->id,'comment_id' => $comment->id])->first();  @endphp
                                                                    <span class="ml-0 likebt likebtcmt">
                                                                        <a href="javascript:;" class="create_comment_like @if($getpost_comment_like) like @endif" 
                                                                        data-comment_id="{{$comment->id}}" data-post-id="{{ $post->id }}"><i class="fas fa-thumbs-up"></i></a>
                                                                    </span>
                                                                </button>

                                                            </div>

                                                            <div class="col-md-7 pl-0 replaybtnwidth replaybtn-margin1">

                                                                @php $getpost_comments_likes = App\Models\FeedCommentLike::where(['feed_id' => $post->id,'comment_id' => $comment->id])->where('user_id','!=',Auth::user()->id)->get();  @endphp

                                                                {{--@if(count($getpost_comments_likes) > 0)
                                                                    @foreach($getpost_comments_likes as $likes_comment_key => $getpost_comments_likes)
                                                                        @php $getemojies_likes_details = App\Models\LikeEmoji::where('id',$getpost_comments_likes->like_emoji_id)->first();   @endphp
                                                                        <img src="{{asset('public/uploads/emoji/'.$getemojies_likes_details->image)}}"  height="30" width="30" class="@if($likes_comment_key != 0) emojibtn_class1 @endif">
                                                                    @endforeach
                                                                @endif--}}

                                                                <a class="">
                                                                @php $getpostlike_comment_count = App\Models\FeedCommentLike::where(['feed_id' => $post->id,'comment_id' => $comment->id])->count('id'); @endphp
                                                                @if($getpostlike_comment_count != 0)
                                                                <span class="text-light count_span comment_like_show cursor_pointer"  data-post-id="{{ $post->id }}" data-comment-id="{{ $comment->id }}">{{ $getpostlike_comment_count }}</span>
                                                                @else
                                                                <span class="text-light count_span" data-post-id="{{ $post->id }}">{{ $getpostlike_comment_count }}</span>
                                                                @endif
                                                                </a>
                                                            <!-- </div>

                                                            <div class="col-md-7 mt-1 pt-1 replybtn_section"> -->
                                                                <a href="javascript:;" title="" data-comment_id="{{$comment->id}}" data-post_id="{{$post->id}}" class="active getComment default-color replaybtntxt_coment ml-2"><i class="fa fa-reply-all"></i>Reply</a> 
                                                            </div>

                                                            <div class="col-md-2 comment_actionbtn ptcmt3 text-right">

                                                                @if($post->user_id == Auth::user()->id)
                                                                    @if($comment->user && $comment->user->id == Auth::user()->id)
                                                                        <span><i class="fa fa-edit text-info editCommentEdit" data-comment="{{$comment->comment}}" data-id="{{$comment->id}}"  data-post_id="{{$post->id}}"></i>&nbsp;&nbsp;<i class="fa fa-trash text-danger deleteComment" data-id="{{$comment->id}}"></i>
                                                                        &nbsp;&nbsp;</span>
                                                                    @else
                                                                        <span><i class="fa fa-trash text-danger deleteComment" data-id="{{$comment->id}}"></i></span>   
                                                                    @endif
                                                                @else

                                                                    @if($comment->user && $comment->user->id == Auth::user()->id)
                                                                    <span><i class="fa fa-edit text-info editCommentEdit" data-comment="{{$comment->comment}}" data-id="{{$comment->id}}"  data-post_id="{{$post->id}}"></i>&nbsp;&nbsp;<i class="fa fa-trash text-danger deleteComment" data-id="{{$comment->id}}"></i>&nbsp;&nbsp;</span>
                                                                    @endif
                                                                @endif

                                                            </div>

                                                        </div>

                                                    </div>
                                                </div><!--comment-list end-->
                                                <ul class="replayDiv mt-0">
                                                    @if(count($comments->where('to_comment',$comment->id)) > 0)
                                                        @foreach($comments->where('to_comment',$comment->id) as $reply_comment)
                                                            <li>
                                                                <div class="comment-list">
                                                                    <div class="comment">

                                                                        <div class="row">
                                                                            <div class="col-md-12 pr-0">
                                                                                <div class="comment-profile">
                                                                                    @if($reply_comment->user && $reply_comment->user->profile)
                                                                                        <a href="{{ route('front.my_profile.index',$reply_comment->user->id) }}"><img src="{{ asset('upload/profile/'.$reply_comment->user->profile)}}" alt="" class="borderradius" alt="" width="40" height="40"></a>
                                                                                    @else
                                                                                        <a href="{{ route('front.my_profile.index',$reply_comment->user->id) }}"><img src="{{asset('frontend/images/user.jpg')}}" class="borderradius" alt=""></a>
                                                                                    @endif
                                                                                </div>
                                                                                <div class="comment-part replayboxs" style="width:100%;margin-left:14%;">
                                                                                    <div class="box">
                                                                                        @if($reply_comment->user && $reply_comment->user->name)
                                                                                            <a href="{{ route('front.my_profile.index',$reply_comment->user->id) }}"><h3>{{ $reply_comment->user->name}}</h3></a>
                                                                                        @else
                                                                                            <a href="{{ route('front.my_profile.index',$reply_comment->user->id) }}"><h3>User</h3></a>
                                                                                        @endif
                                                                                        <span style="float: right!important;"><i class="fas fa-clock"></i>{{ $reply_comment->created_at->diffForHumans() }}</span>
                                                                                        <p>{{$reply_comment->comment}} </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row mt-2">
                                                                            <div class="col-md-2 replayfirst_section pr-0"></div><!-- style="max-width:13%" -->
                                                                            <div class="col-md-2 pl-1 pr-0 widthcolmd2_replay">

                                                                                <button class="emjbtn_replay">
                                                                                    @php $getpost_comment_like = App\Models\FeedCommentLike::where(['user_id' => Auth::user()->id,'feed_id' => $post->id,'comment_id' => $reply_comment->id])->first();  @endphp
                                                                                    <span class="ml-0 likebt likebtcmt">
                                                                                        <a href="javascript:;" class="create_comment_like @if($getpost_comment_like) like @endif" 
                                                                                        data-comment_id="{{$reply_comment->id}}" data-post-id="{{ $post->id }}"><i class="fas fa-thumbs-up"></i></a>
                                                                                    </span>
                                                                                </button> 
                                                                            </div>
                                                                            <div class="col-md-5 reply_cntbtn pl-0">


                                                                                @php $getpost_comments_replay_likes = App\Models\FeedCommentLike::where(['feed_id' => $post->id,'comment_id' => $reply_comment->id])->where('user_id','!=',Auth::user()->id)->get();  @endphp

                                                                                {{--
                                                                                @if(count($getpost_comments_replay_likes) > 0)
                                                                                    @foreach($getpost_comments_replay_likes as $likes_comment_replay_key => $getpost_comments_replay_likes)
                                                                                        @php $getemojies_likes_details = App\Models\LikeEmoji::where('id',$getpost_comments_replay_likes->like_emoji_id)->first();   @endphp
                                                                                        <img src="{{asset('public/uploads/emoji/'.$getemojies_likes_details->image)}}"  height="30" width="30" class="@if($likes_comment_replay_key != 0) emojibtn_class1 @endif">
                                                                                    @endforeach
                                                                                @endif
                                                                                --}}

                                                                                <a class="text-light">
                                                                                @php $getpostlike_comment_replay_count = App\Models\FeedCommentLike::where(['feed_id' => $post->id,'comment_id' => $reply_comment->id])->count('id'); @endphp
                                                                                @if($getpostlike_comment_replay_count != 0)
                                                                                <span class="count_span text-light comment_like_show ml_counterspan cursor_pointer"  data-post-id="{{ $post->id }}" data-comment-id="{{ $reply_comment->id }}">{{ $getpostlike_comment_replay_count }}</span>
                                                                                @else
                                                                                <span class="count_span text-light ml_counterspan" data-post-id="{{ $post->id }}">{{ $getpostlike_comment_replay_count }}</span>
                                                                                @endif
                                                                                </a>
                                                                            </div>
                                                                            <div class="col-md-3 ptcmt3 commentaction_replybtn text-right">
                                                                        
                                                                                @if(Auth::user()->id == $post->user_id)
                                                                                    @if($reply_comment->user && $reply_comment->user->id == Auth::user()->id)
                                                                                    <span><i class="fa fa-edit text-info editCommentEdit" data-comment="{{$reply_comment->comment}}" data-id="{{$reply_comment->id}}"  data-post_id="{{$post->id}}"></i>&nbsp;&nbsp;<i class="fa fa-trash text-danger deleteComment" data-id="{{$reply_comment->id}}"></i></span>
                                                                                    @else
                                                                                    <span><i class="fa fa-trash text-danger deleteComment" data-id="{{$reply_comment->id}}"></i></span>   
                                                                                    @endif
                                                                                @else
                                                                                    @if($reply_comment->user && $reply_comment->user->id == Auth::user()->id)
                                                                                    <span><i class="fa fa-edit text-info editCommentEdit" data-comment="{{$reply_comment->comment}}" data-id="{{$reply_comment->id}}"  data-post_id="{{$post->id}}"></i>&nbsp;&nbsp;<i class="fa fa-trash text-danger deleteComment" data-id="{{$reply_comment->id}}"></i></span>
                                                                                    @else
                                                                                    
                                                                                    @endif
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </li>
                                                        @endforeach
                                                    @endif
                                                    
                                                </ul>
                                            </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div><!--comment-sec end-->
                    </div><!--comment-section end-->
                    
                    @if(count($comments->where('is_reply',0)) > 2)
                    <a href="javascript:;" class="plus-ic default-color showCommentDiv showCommentDiv_{{$post->id}}" data-post_id="{{$post->id}}">
                        Load more comments <!--<i class="la la-plus"></i>-->
                    </a>
                    @endif
                    <a href="javascript:;" class="plus-ic hideMoreComment hideMoreComment_{{$post->id}}" data-post_id="{{$post->id}}">
                        <i class="la la-minus"></i>
                    </a>
                    <div class="post-comment hideReplyDiv hideReplyDiv_{{$post->id}}">
                        <div class="cm_img">
                        @if(Auth::user()->profile != null)
                            <a href="{{ route('front.my_profile.index',Auth::user()->id) }}"><img src="{{ asset('upload/profile/'.Auth::user()->profile)}}" class="borderradius" alt="" width="40" height="40"></a>
                        @else
                            <a href="{{ route('front.my_profile.index',Auth::user()->id) }}"><img src="{{ asset('frontend/images/user.jpg')}}" alt="" width="40" class="borderradius" height="40"></a>
                        @endif
                            {{-- <img src="{{ asset('frontend/images/resources/bg-img4.png') }}" alt=""> --}}
                        </div>
                        <div class="comment_box">
                            <form id="reply_{{$post->id}}">
                                <input type="hidden" name="post_id" value="{{$post->id}}">
                                <input type="hidden" name="comment_id" class="comment_id_{{$post->id}}">
                                <input type="text" name="comment" class="reply_comment_{{$post->id}} replaytext" data-post_id="{{$post->id}}" placeholder="Post a comment">
                                <button type="button" class="replyComment" data-post_id="{{$post->id}}">Send</button>
                            </form>
                        </div>
                    </div>

                    <div class="post-comment hideEditDiv hideEditDiv_{{$post->id}}">
                        <div class="cm_img">
                            {{--<img src="{{ asset('assets/frontend/images/resources/bg-img4.png') }}" alt="">--}}
                            @if(Auth::user()->profile != null)
                                <a href="{{ route('front.my_profile.index',Auth::user()->id) }}"><img src="{{ asset('upload/profile/'.Auth::user()->profile)}}" class="borderradius" alt="" width="40" height="40"></a>
                            @else
                                <a href="{{ route('front.my_profile.index',Auth::user()->id) }}"><img src="{{ asset('frontend/images/user.jpg')}}" alt="" width="40" class="borderradius" height="40"></a>
                            @endif
                        </div>
                        <div class="comment_box">
                            <form id="edit_comment_{{$post->id}}">
                                <input type="hidden" name="post_id" value="{{$post->id}}">
                                <input type="hidden" name="comment_id" class="edit_comment_id_{{$post->id}}">
                                <input type="text" name="comment" class="edit_comment_{{$post->id}} edit_commenttext" data-post_id="{{$post->id}}" placeholder="Edit a comment">
                                <button type="button" class="editComment" data-post_id="{{$post->id}}">Send</button>
                            </form>
                        </div>
                    </div>                                                    
                </div><!--post-bar end-->

                <div class="modal fade img-modal gallery_model" id="exampleModalCenter_{{ $post->id }}" data-post-id="{{ $post->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-8 modal-image">
                                        @foreach($getpost_media as $mediakey => $media)
                                            
                                            <img class="img-responsive slider_gallery" id="slide_{{ $media->id }}" src="{{asset('upload/post/'.$media->feed_media)}}">

                                        @endforeach                            
                                            <!--<img class="img-responsive hidden" src="http://upload.wikimedia.org/wikipedia/commons/1/1a/Bachalpseeflowers.jpg" />
                                        <img class="img-responsive hidden" src="http://www.netflights.com/media/216535/hong%20kong_03_681x298.jpg" />-->

                                        <a href="" class="img-modal-btn left"><i class="fas fa-chevron-left"></i></a>
                                        <a href="" class="img-modal-btn right"><i class="fas fa-chevron-right"></i> </a>
                                    </div>
                                    <div class="col-lg-4 col-md-12 modal-meta">
                                        <div class="modal-meta-top">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="far fa-times-circle"></i></span></button>
                                            <div class="img-poster clearfix">
                                                <div class="imgbox">
                                                    @if(isset($post->user->profile))
                                                        <img src="{{ asset('upload/profile/'.$post->user->profile)}}" class="img-circle borderradius" alt="">
                                                    @else
                                                        <img src="{{ asset('frontend/images/user.jpg')}}" alt="" class="img-circle borderradius">
                                                    @endif
                                                    <!--<img class="img-circle" src="https://s-media-cache-ak0.pinimg.com/736x/3b/7d/6f/3b7d6f60e2d450b899c322266fc6edfd.jpg"/>-->
                                                </div>
                                                <div class="usy-name">
                                                    <h3>{{ $post->user->name ?? '' }}</h3>
                                                    <span>{{ $post->created_at->diffForHumans() }}</span>
                                                </div>
                                            </div>

                                            <div class="modelmeta">
                                                <h4 class="mt-3 h4">{{ $post->title ?? '' }}</h4>
                                                <p class="mt-0">{{ $post->description ?? '' }}</p>
                                            </div>
                                            <div class="job-status-bar">
                                                <ul class="like-com">
                                                    <li>
                                                        <div class="dynamic_like_box">                        
                                                            @include('frontend.home.dynamic_gallery_like')
                                                        </div>
                                                    </li> 
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
          
        @endforeach
        @if($postcnt == 0)
        <div class="post-bar">
            <div class="post_topbar">
                No post found..
            </div>
        </div>
        @endif
    </div>

    @if($total_post_counter >= $post_load_count)                    
        <div class="process-comm">
            <button class="btn loadmore load_more_btn" id="{{ $post_load_count }}">Load More Feeds</button>
        </div>
    @endif

        {{--<div class="hideLoader">
        <input type="hidden" class="last_data_number" value="0">
        <div class="process-comm">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div><!--process-comm end-->
        </div>--}}
    @else
    </div>
    <div class="post-bar">
        <div class="post_topbar">
            No post found..
        </div>
    </div>
    @endif

    
</div><!--posts-section end-->
    

