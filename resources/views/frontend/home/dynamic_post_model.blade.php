<div class="toppart">
    <div class="item col1">
        <div class="profileimg">
        @if(Auth::user()->profile != null)
            <img src="{{ asset('upload/profile/'.Auth::user()->profile)}}" class="borderradius" alt="" width="50" height="50">
        @else
            <img src="{{ asset('frontend/images/user.jpg')}}" alt="" class="borderradius" width="50" height="50">
        @endif
        </div>
    </div>
    <div class="item col2">
        <div class="lockup_title ">
            <h3>{{ Auth::user()->name ?? '' }}</h3>
            <!-- <a href="#" data-toggle="modal" data-target="#visibility"><i class="fas fa-globe-americas"></i> Anyone</a> -->
        </div>
    </div>
</div>
<div class="description">
    <!-- <input type="text" class="aboutpost post_text" placeholder="What do you want to talk about?" name="post_text" id="post_text"> -->
    <!-- <textarea class="aboutpost post_text" placeholder="What do you want to talk about?" name="post_text" id="post_text"></textarea> -->
    <!-- <div contenteditable="true" class="aboutpost post_text" placeholder="What do you want to talk about?" id="post_text"></div> -->

    <input type="text" id="post_title" name="title" class="form-control title" placeholder="Title">
    <br/>
    <div id="post_text" class="aboutpost post_text" contenteditable="true" data-placeholder="Enter Description"></div>
    <!-- <div id="menu1" class="menu" role="listbox"></div> -->
    <br/>
    <div id="menu" class="menu" role="listbox"></div>
</div>


<div class="media_part">

@if(count($getpost_media) > 0)

        <div class="row">
            <div class="col-md-12 text-right pr-0">
                <a href='javascript:;' class='delete_img' data-name="{{ $post_image_media }}" data-value="{{ $post_image_media_name }}"><i class='fa fa-close text-danger' aria-hidden='true'></i></a>
            </div> 
        </div>             

        @if(count($getpost_media) == 1)
            @foreach($getpost_media as $media)
                <div class="gallerybox">						
                    <div class="col12">
                        <div class="imgbox">
                            <img class="w-100" src="{{asset('upload/post/'.$media)}}" alt="First slide" data-slide-to="0">
                        </div>
                    </div>
                </div>
            @endforeach
        @elseif(count($getpost_media) == 2)
            @foreach($getpost_media as $media)
                <div class="gallerybox">
                    <div class="col2">
                        <div class="imgbox">
                            <img class="w-100" src="{{asset('upload/post/'.$media)}}" alt="First slide" data-slide-to="0">
                        </div>
                    </div>
                </div>
            @endforeach
        @elseif(count($getpost_media) == 3)
            @foreach($getpost_media as $key => $media)
                <div class="gallerybox">
                    @if($key == 0)						
                    <div class="col12">
                        <div class="imgbox">
                            <img class="w-100" src="{{asset('upload/post/'.$media)}}" alt="First slide" data-slide-to="0">
                        </div>
                    </div>
                    @else
                    <div class="col2">
                        <div class="imgbox">
                            <img class="w-100" src="{{asset('upload/post/'.$media)}}" alt="First slide" data-slide-to="0">
                        </div>
                    </div>
                    @endif
                </div>
            @endforeach
        @elseif(count($getpost_media) == 4)
            @foreach($getpost_media as $media)
                <div class="gallerybox">
                    <div class="col2">
                        <div class="imgbox">
                            <img class="w-100" src="{{asset('upload/post/'.$media)}}" alt="First slide" data-slide-to="0">
                        </div>
                    </div>
                </div>
            @endforeach
        @elseif(count($getpost_media) == 5)
            @foreach($getpost_media as $key => $media)
                <div class="gallerybox">
                    @if($key == 0 || $key == 1)						
                    <div class="col2">
                        <div class="imgbox">
                            <img class="w-100" src="{{asset('upload/post/'.$media)}}" alt="First slide" data-slide-to="0">
                        </div>
                    </div>
                    @else
                    <div class="col1">
                        <div class="imgbox">
                            <img class="w-100" src="{{asset('upload/post/'.$media)}}" alt="First slide" data-slide-to="0">
                        </div>
                    </div>
                    @endif
                </div>
            @endforeach
        @endif
    @endif
</div>


<script type="text/javascript">

</script>