<style type="text/css">
    ul.tabs{
        margin: 0px;
        padding: 0px;
        list-style: none;
        display: inline-flex;
    }
    ul.tabs li{
        background: none;
        color: #222;
        display: inline-block;
        padding: 10px 15px;
        cursor: pointer;
    }

    ul.tabs li.current{
        /*background: #ededed;*/
        border-bottom: 5px solid #e44d3a;
        color: #222;
    }

    .tab-content{
        display: none;
        background: #fff;
        padding: 15px;
        max-height: 450px;
        overflow-y: auto;
    }

    .tab-content.current{
        display: inherit;
    }
    .pdall{
        padding-top: 3px;
    }
    hr{
        margin-top: 0px;
    }
    .likecounts{
        line-height: 1.5;
        padding-left: 10px;
        font-size: 20px;
    }
    .likecountsall{
        line-height: 0.8;
        padding-left: 10px;
        font-size: 20px!important;
        font-weight: 500;
    }
    .alltext{
        font-size: 22px!important;
    }
</style>


<div class="container post_likes_box p-0">

    @php $getlikes_details_all = App\Models\FeedLike::where(['feed_id' => $post_id])->get();  @endphp
    <!-- <div id="post_tab-All" class="tab-content current"> -->
    <table class="tablewidth">
        <tbody>
            @if(count($getlikes_details_all) > 0)
                @foreach($getlikes_details_all as $getlike_data)
                    <tr class="borderbottom">
                        <td class="pb-2 pt-2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="usy-dt">
                                        @if(isset($getlike_data->user->profile))
                                            <a href="#"><img src="{{ asset('upload/profile/'.$getlike_data->user->profile)}}" alt="" width="50" height="50"></a>
                                        @else
                                            <a href="#"><img src="{{ asset('frontend/images/user.jpg')}}" alt="" width="50" height="50"></a>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 pt-2">
                                        {{ $getlike_data->user->name ?? '' }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td class="pb-2 pt-2">No Record Found.</td>
                </tr>
            @endif    
        </tbody>
    </table>

























        {{--    
        <ul class="tabs">
            
            <li class="tab-link current" data-tab="post_tab-All">
                @php $getlikes_details_count_all = App\Models\FeedLike::where(['feed_id' => $post_id])->count('id');  @endphp
                <h3 height="30" width="30" class="font-weight-bold pdall"><span class="alltext">All</span> <span class="likecountsall">{{ $getlikes_details_count_all }}</span></h3>
            </li>
            
            @if(count($getemojies) > 0)
                                        @foreach($getemojies as $key => $getemojie)
                                        @php $getlikes_details_count = App\Models\Like::where(['post_id' => $post_id,'like_emoji_id' => $getemojie->id])->count('id');  @endphp
                                            @if($getlikes_details_count > 0)
                                            <li class="tab-link" data-tab="post_tab-{{ $key }}">
                                                <img src="{{asset('public/uploads/emoji/'.$getemojie->image)}}"  height="30" width="30" class="emojibtn_class"><span class="likecounts">{{ $getlikes_details_count }}</span>
                                            </li>
                                            @endif
                                        @endforeach
                                    @endif 
        </ul>
        <hr/>
        --}}
        
        <!-- </div> -->

        {{--
        <div class="dynamic_tab_likes d-none"> 
        

                @php $getlikes_details = App\Models\FeedLike::where(['feed_id' => $post_id])->get();  @endphp
                <div id="post_tab-All" class="tab-content current">
                    <table class="tablewidth">
                        <tbody>
                            @if(count($getlikes_details) > 0)
                                @foreach($getlikes_details as $getlike_data)
                                    <tr class="borderbottom">
                                        <td class="pb-2 pt-2">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="usy-dt">
                                                        @if(isset($getlike_data->user->profile))
                                                            <a href="#"><img src="{{ asset('uploads/profile/'.$getlike_data->user->profile)}}" alt="" width="50" height="50"></a>
                                                        @else
                                                            <a href="#"><img src="{{ asset('frontend/images/user.jpg')}}" alt="" width="50" height="50"></a>
                                                        @endif
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                        {{ $getlike_data->user->name ?? '' }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif    
                        </tbody>
                    </table>
                </div>
            
        </div>
        --}}

</div><!-- container -->


