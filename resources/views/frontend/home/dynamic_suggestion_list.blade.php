@if(count($top_athletes) > 0)
<div class="suggestions-list">
    @foreach($top_athletes as $top_athlete)
    <div class="suggestion-usd d-flex">
        @if(isset($top_athlete->profile_image))
        <a href="{{ route('get.user.profile',[$top_athlete->roles->slug,$top_athlete->slug]) }}" class='link_label'><img src="{{ url('public/uploads/profile_image/'.$top_athlete->profile_image)}}" alt="" width="35" height="35"></a>
        @else
        <a href="{{ route('get.user.profile',[$top_athlete->roles->slug,$top_athlete->slug]) }}" class='link_label'><img src="{{ asset('assets/frontend/images/user.jpg')}}" alt="" width="35" height="35"></a>
        @endif
        <!-- <img src="{{ asset('assets/frontend/images/resources/s1.png') }}" alt=""> -->
        <div class="sgt-text">
            
            <div class='col-md-12 pl-0'>
            <a href="{{ route('get.user.profile',[$top_athlete->roles->slug,$top_athlete->slug]) }}" class='link_label'><h4>{{ isset($top_athlete->first_name) ? $top_athlete->first_name:'' }} {{ isset($top_athlete->last_name) ? $top_athlete->last_name:'' }}</h4></a>
            </div>
            @if(isset($top_athlete->high_school))
            <span><a href="{{ route('school.profile',$top_athlete->high_school) }}" class='link_label'>{{ $top_athlete->school->school_name ?? '' }}</a></span>
            @else
            <span>{{ $top_athlete->school->school_name ?? '' }}</span>
            @endif
        </div>
        <!-- <span><i class="la la-plus"></i></span> -->
    </div>
    @endforeach
    <div class="view-more">
        <a href="{{ route('my.network.view','athlete') }}" title="">View More</a>
    </div>
</div>
@else
<div class="suggestions-list pb-3">
    <div class="suggestion-usd">
        <div class="sgt-text">
            <span>No record found.</span>
        </div>
    </div>
</div>
@endif