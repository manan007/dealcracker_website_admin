



@if(count($img) == 0)
<div class="modal-flex">
    <div class="inputfile">
        <input type="file" name="file-5[]" id="file-5" class="custom-input-text file-5" multiple="true" accept=".jpg,.jpeg,.png">
        <label for="file-5"><span>Select images to share</span></label>
    </div>
</div>
@endif


@if(count($img) > 0)
      @foreach($img as $imgdata)
        @php $imgesarr = explode('.',$imgdata); $name=$imgesarr[0];  @endphp
        <input type='hidden' name='image_name[]' value="{{ $imgdata }}" data-name-value="{{ $name }}" class='imagevalue'>
     @endforeach
@endif

<div id="sync1" class="owl-carousel owl-theme">
    @if(count($img) > 0)
        @foreach($img as $imgdata)
            <div class="item">
                <img src="{{ url('public/upload/post/'.$imgdata)}}" />
            </div>
        @endforeach
    @endif
</div>