<style type="text/css">
    .comment-section{
        box-shadow: none;
    }
    /*.widthcolmd2{
        pointer-events: none;
        display: none;
    }
    .widthcolmd2_replay{
        pointer-events: none;
        display: none;
    }*/
    .widthcolmd2{
        pointer-events: none;
        display: none;
    }
    .widthcolmd2_replay{
        pointer-events: none;
        display: none;
    }
    .mainpost_likebtn{
        pointer-events: none;
        display: none!important;
    }

</style>

<div class="posts-section">
    <div class="mainRow">
    @if(count($posts) > 0)
        @php $postcnt = 0; @endphp
        @foreach($posts as $post)

            @php $postcnt = $postcnt + 1; @endphp
            @php $getpost_media = App\Models\PostMedia::where('post_id',$post->id)->get();
                 $getpost_like = App\Models\Like::where(['user_id' => $user_detail->id,'post_id' => $post->id])->first();
                 $getpostlike_count = App\Models\Like::where(['post_id' => $post->id])->count('id');

                 $comments = App\Models\Comment::with('user')->where(['post_id' => $post->id])->orderBy('id','desc')->get();
                 $getemojies = App\Models\LikeEmoji::all();
            @endphp
            <input type="hidden" class="post_ids" value="{{$post->id}}">
            <div class="post-bar">
                <div class="post_topbar">
                    <div class="usy-dt">
                        @if(isset($post->user->profile_image))
                            @if($user_detail->id == $post->user->id)
                            <a href="{{ route('get.user.profile',[$user_detail->roles->slug,$user_detail->slug]) }}" class='link_label'><img src="{{ url('public/uploads/profile_image/'.$post->user->profile_image)}}" alt="" width="40" height="40"></a>
                            @else
                            <a href="{{ route('get.user.profile',[$post->user->roles->slug,$post->user->slug]) }}" class='link_label'><img src="{{ url('public/uploads/profile_image/'.$post->user->profile_image)}}" alt="" width="40" height="40"></a>    
                            @endif
                        @else
                            @if($user_detail->id == $post->user->id)
                            <a href="{{ route('get.user.profile',[$user_detail->roles->slug,$user_detail->slug]) }}" class='link_label'><img src="{{ asset('assets/frontend/images/user.jpg')}}" alt="" width="40" height="40"></a>
                            @else
                            <a href="{{ route('get.user.profile',[$post->user->roles->slug,$post->user->slug]) }}" class='link_label'><img src="{{ asset('assets/frontend/images/user.jpg')}}" alt="" width="40" height="40"></a>
                            @endif
                        @endif
                        <!--<img src="{{ asset('assets/frontend/images/resources/us-pic.png') }}" alt="">-->
                        <div class="usy-name">
                            @if($user_detail->id == $post->user->id)
                            <a href="{{ route('get.user.profile',[$user_detail->roles->slug,$user_detail->slug]) }}" class='link_label'><h3>{{ isset($post->user->first_name) ? $post->user->first_name:'' }} {{ isset($post->user->last_name) ? $post->user->last_name:'' }}</h3></a>
                            @else
                            <a href="{{ route('get.user.profile',[$post->user->roles->slug,$post->user->slug]) }}" class='link_label'><h3>{{ isset($post->user->first_name) ? $post->user->first_name:'' }} {{ isset($post->user->last_name) ? $post->user->last_name:'' }}</h3></a>
                            @endif
                            <span><img src="{{ asset('assets/frontend/images/clock.png') }}" alt="">{{ $post->created_at->diffForHumans() }}</span>
                        </div>
                    </div>
                    @if($post->user_id == $user_detail->id)
                    <div class="ed-opts">
                        <a href="javascript:;" title="" class="ed-opts-open post_collapse" data-post-id="{{ $post->id }}"><i class="la la-ellipsis-v"></i></a>
                        <ul class="ed-options" id="ed-options-{{ $post->id }}">
                            <li><a href="javascript:;" title="Edit Post" class="edit_post" data-post-id="{{ $post->id }}">Edit Post</a></li>
                            
                            <li><a href="javascript:;" title="Delete Post" class="delete_post" data-post-id="{{ $post->id }}">Delete Post</a></li>
                            
                            <!--<li><a href="#" title="">Unsaved</a></li>
                            <li><a href="#" title="">Unbid</a></li>
                            <li><a href="#" title="">Close</a></li>
                            <li><a href="#" title="">Hide</a></li>-->
                        </ul>
                    </div>
                    @endif
                </div>
                <div class="epi-sec">
                    <ul class="descp">
                        <!-- <li><img src="{{ asset('assets/frontend/images/icon8.png') }}" alt=""><span>Epic Coder</span></li> -->
                        <!-- <li><img src="{{ asset('assets/frontend/images/icon9.png') }}" alt=""><span>India</span></li> -->
                    </ul>
                    <!--<ul class="bk-links">
                        <li><a href="#" title=""><i class="la la-bookmark"></i></a></li>
                        <li><a href="#" title=""><i class="la la-envelope"></i></a></li>
                    </ul>-->
                </div>
                <div class="job_descp">
                    <!-- <h3>Senior Wordpress Developer</h3> -->
                    <!-- <ul class="job-dt">
                        <li><a href="#" title="">Full Time</a></li>
                        <li><span>$30 / hr</span></li>
                    </ul> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam luctus hendrerit metus, ut ullamcorper quam finibus at. Etiam id magna sit amet... <a href="#" title="">view more</a></p>-->
                    {{--@php 

                    if(strlen($post->post_text) > 142){

                        $str = substr($post->post_text, 0, 142) . '...';

                        $get_word = explode(' ',$str);

                        $posttext1="";
                        if(count($get_word) > 0){
                            foreach ($get_word as $key => $value){
                                if (substr($value, 0, 1) === '#'){
                                  $final_term = ltrim($value, $value[0]);
                                  $get_hashtag = App\Models\HashTag::where(['name' => $final_term])->first();

                                  $posttext1.="&nbsp;<a href='javascript:;' data-hashtag-id='".$get_hashtag->id."' class='hashtaglink'>".$value."</a>&nbsp;";
                                }
                                else{

                                  $posttext1.="&nbsp;".$value."&nbsp;";  
                                }
                            }
                        }
                    }

                    $get_word = explode(' ',$post->post_text);

                    $posttext="";
                    if(count($get_word) > 0){
                        foreach ($get_word as $key => $value){
                            if (substr($value, 0, 1) === '#'){
                                $final_term = ltrim($value, $value[0]);
                                $get_hashtag = App\Models\HashTag::where(['name' => $final_term])->first();
                                $posttext.="&nbsp;<a href='javascript:;' data-hashtag-id='".$get_hashtag->id."' class='hashtaglink'>".$value."</a>&nbsp;";
                            }
                            else{

                              $posttext.="&nbsp;".$value."&nbsp;";  
                            }
                        }
                    }
                    
                         
                    @endphp--}}

                    <?php 

                        $posttext='';$getstring='';$posttext1='';
                        $dom = new \DOMDocument();
                        $dom->loadHTML($post->post_text);
                        $domx = new \DOMXPath($dom);
                        $entries = $domx->evaluate("//span");
                        $arr = array();

                        if(count($entries) > 0){
                            foreach ($entries as $key_entries => $entry) {


                                if($entry){

                                    if($entry->attributes->length == 3)
                                    {
                                       
                                        $tag_type="user";
                                        $is_school =  $entry->getAttribute("is_school");

                                        $tag_id = $entry->getAttribute('data-id');

                                        if($tag_id){
                                            if($is_school == 1){
                                                $gettaginfo = App\Models\School::where(['id' => $tag_id])->first();
                                                $url = route("school.profile",$gettaginfo->slug);
                                                $posttext .="<a href='".$url."'>".$entry->nodeValue."</a>";
                                            }
                                            elseif ($is_school == 0) {
                                                $gettaginfo = App\Models\User::where(['id' => $tag_id])->first();
                                                $url = route("get.user.profile",[$gettaginfo->roles->slug,$gettaginfo->slug]);
                                                $posttext .="<a href='".$url."'>".$entry->nodeValue."</a>";
                                            }
                                        }
                                    }
                                    elseif ($entry->attributes->length == 1) {

                                        $checklast_string = explode(' ',$entry->nodeValue);
                                        //dd($checklast_string);

                                        if(count($checklast_string) > 1){

                                            foreach ($checklast_string as $key_laststring => $value_laststring) {
                                                //dd($value_laststring);
                                                $mystring_check = preg_replace('/\s+/u', '', $value_laststring);//$value_laststring;
                                                $findme_check   = '#';
                                                $pos1 = strpos($mystring_check, $findme_check);

                                                if($pos1 !== false){
                                                    //dd('2');
                                                    $newhashtag = str_replace( array( '#' ),'',$mystring_check);
                                                    //dd($newhashtag);
                                                    //dd(str_replace(' ', '', $mystring),trim($mystring),'df');
                                                    $check_hashtag = App\Models\HashTag::where(['name' => $newhashtag,'status' => 'Active'])->first();
                                                    if($check_hashtag){
                                                        if(Request::segment(3)){
                                                            if($user_detail->id == $post->user->id){
                                                                $url = route("get.user.profile",[Request::segment(2),Request::segment(3),$check_hashtag->id]);
                                                            }
                                                            else{
                                                                $url = route("get.user.profile",[Request::segment(2),Request::segment(3),$check_hashtag->id]);
                                                            }    
                                                        }
                                                        else{
                                                            $url = route("user.home",$check_hashtag->id);
                                                        }
                                                            
                                                    }
                                                    else{
                                                        $url = "#";
                                                    }
                                                    
                                                    $posttext .="<a href='".$url."'>&nbsp;#".$newhashtag."&nbsp;</a>";
                                                }
                                                else{
                                                    $newhashtag = str_replace( array( '#' ),'',$mystring_check);
                                                    $posttext .="<span>&nbsp;".$newhashtag."&nbsp;</span>";
                                                }
                                            }
                                        }
                                        else{

                                            $mystring = preg_replace('/\s+/u', '', $entry->nodeValue);//$entry->nodeValue;
                                            $findme   = '#';
                                            $pos = strpos($mystring, $findme);

                                            if($pos !== false){
                                                $newhashtag = str_replace( array( '#' ),'',$mystring);
                                                //dd(str_replace(' ', '', $mystring),trim($mystring),'df');
                                                $check_hashtag = App\Models\HashTag::where(['name' => $newhashtag,'status' => 'Active'])->first();
                                                if($check_hashtag){
                                                    if(Request::segment(3)){
                                                        if($user_detail->id == $post->user->id){
                                                            $url = route("get.user.profile",[Request::segment(2),Request::segment(3),$check_hashtag->id]);
                                                        }
                                                        else{
                                                            $url = route("get.user.profile",[Request::segment(2),Request::segment(3),$check_hashtag->id]);
                                                        }
                                                    }
                                                    else{
                                                        $url = route("user.home",$check_hashtag->id);
                                                    }    
                                                }
                                                else{
                                                    $url = "#";
                                                }
                                                
                                                $posttext .="<a href='".$url."'>&nbsp;#".$entry->nodeValue."</a>";
                                            }
                                            else{
                                                $posttext .="<span>&nbsp;".$entry->nodeValue."&nbsp;</span>";
                                            }

                                            //$posttext .="<span>".$entry->nodeValue."</span>";
                                        }

                                        //$posttext .="<span>".$entry->nodeValue."</span>";
                                    }
                                    else{
                                        
                                        if($entry->attributes->length == 2){
                                            $is_school =  $entry->getAttribute("is_school");
                                            $tag_id = $entry->getAttribute('data-id');
                                            if ($is_school == 2) {
                                                if(Request::segment(3)){
                                                    if($user_detail->id == $post->user->id){
                                                        $url = route("get.user.profile",[Request::segment(2),Request::segment(3),$tag_id]);
                                                    }
                                                    else{
                                                       $url = route("get.user.profile",[Request::segment(2),Request::segment(3),$tag_id]);     
                                                    }
                                                }
                                                else{
                                                    $url = route("user.home",$tag_id);
                                                }
                                                $posttext .="<a href='".$url."'>".$entry->nodeValue."</a>";
                                            }
                                        }
                                        else{
                                            if($entry->attributes->length == 0){
                                                $mystring = $entry->nodeValue;
                                                $findme   = '#';
                                                $pos = strpos($mystring, $findme);
                                               
                                                if($pos !== false){
                                                    
                                                    $newhashtag = str_replace( array( '#' ),'',$entry->nodeValue);
                                                    //dd(str_replace(' ', '', $mystring),trim($mystring),'df');
                                                    $check_hashtag = App\Models\HashTag::where(['name' => $newhashtag])->first();
                                                    if($check_hashtag){
                                                        if(Request::segment(3)){
                                                            $url = route("get.user.profile",[Request::segment(2),Request::segment(3),$check_hashtag->id]);
                                                        }
                                                        else{
                                                           $url = route("get.user.profile",[Request::segment(2),Request::segment(3),$check_hashtag->id]); 
                                                        }    
                                                    }
                                                    else{
                                                        $url = "#";
                                                    }
                                                    
                                                    $posttext .="<a href='".$url."'>".$entry->nodeValue."</a>";
                                                }
                                                else{
                                                    $posttext .="$entry->nodeValue";    
                                                }

                                            }
                                            else{
                                                $posttext .="$entry->nodeValue";    
                                            }
                                            
                                        }
                                        
                                    }

                                    $getstring .="$entry->nodeValue";
                                    
                                }  
                            }  
                        }
                        // else{
                        //     $posttext .="$post->post_text";
                        // }
                        

                        if(strlen($getstring) > 142){

                            $posttext1 = substr($posttext, 0, 400) . '...';
                        }

                    ?>
                    @if(strlen($getstring) > 142)
                  
                    <!-- view more -->
                    <p class="viewtextpost"  id="view_more_{{ $post->id }}">
                        {!!  $posttext1 !!}
                        <br/><a href="javascript:;" class="view_more" data-post-id="{{ $post->id }}" title="View Less">view more</a>
                    </p>

                    <!-- view less -->
                    <p class="viewtextpost hide" id="view_less_{{ $post->id }}">{!! $posttext!!}<br/><a href="javascript:;" class="view_less" data-post-id="{{ $post->id }}" 
                        title="View More">view less</a></p>
                    @else
                    <p>
                 
                       {!! $posttext !!}
                    </p>
                    @endif

                    @if(count($getpost_media) > 0)
                    
                        @if(count($getpost_media) == 1)
                            @foreach($getpost_media as $media)
                                <div class="gallerybox">						
                                    <div class="col12">
                                        <div class="imgbox">
                                            @if($media->type ==  "image")
                                                <img class="w-100" src="{{asset('public/uploads/posts/'.$media->post_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}" data data-slide-to="0">
                                            @elseif($media->type ==  "video")
                                                <video width="100%" height="100%" controls>
                                                    <source src="{{asset('public/uploads/posts/'.$media->post_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
                                                </video>
                                            @endif    
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @elseif(count($getpost_media) == 2)
                            @foreach($getpost_media as $media)
                                <div class="gallerybox">
                                    <div class="col2">
                                        <div class="imgbox">
                                            @if($media->type ==  "image")
                                                <img class="w-100" src="{{asset('public/uploads/posts/'.$media->post_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}" data-post-id="{{ $post->id }}" data-slide-to="0">
                                            @elseif($media->type ==  "video")
                                                <video width="100%" height="100%" controls>
                                                    <source src="{{asset('public/uploads/posts/'.$media->post_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
                                                </video>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @elseif(count($getpost_media) == 3)
                            @foreach($getpost_media as $key => $media)
                                <div class="gallerybox">
                                    @if($key == 0)						
                                    <div class="col12">
                                        <div class="imgbox">
                                            @if($media->type ==  "image")
                                                <img class="w-100" src="{{asset('public/uploads/posts/'.$media->post_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}" data-post-media-id="{{ $media->id }}" data-post-id="{{ $post->id }}" data-slide-to="0">
                                            @elseif($media->type ==  "video")
                                                <video width="100%" height="100%" controls>
                                                    <source src="{{asset('public/uploads/posts/'.$media->post_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
                                                </video>
                                            @endif
                                        </div>
                                    </div>
                                    @else
                                    <div class="col2">
                                        <div class="imgbox">
                                            @if($media->type ==  "image")
                                                <img class="w-100" src="{{asset('public/uploads/posts/'.$media->post_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}" data-post-id="{{ $post->id }}" data-slide-to="0">
                                            @elseif($media->type ==  "video")
                                                <video width="100%" height="100%" controls>
                                                    <source src="{{asset('public/uploads/posts/'.$media->post_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
                                                </video>
                                            @endif
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            @endforeach
                        @elseif(count($getpost_media) == 4)
                            @foreach($getpost_media as $media)
                                <div class="gallerybox">
                                    <div class="col2">
                                        <div class="imgbox">
                                            @if($media->type ==  "image")
                                                <img class="w-100" src="{{asset('public/uploads/posts/'.$media->post_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}" data-post-id="{{ $post->id }}" data-slide-to="0">
                                            @elseif($media->type ==  "video")
                                                <video width="100%" height="100%" controls>
                                                    <source src="{{asset('public/uploads/posts/'.$media->post_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
                                                </video>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @elseif(count($getpost_media) == 5)
                            @foreach($getpost_media as $key => $media)
                                <div class="gallerybox">
                                    @if($key == 0 || $key == 1)						
                                    <div class="col2">
                                        <div class="imgbox">
                                            @if($media->type ==  "image")
                                                <img class="w-100" src="{{asset('public/uploads/posts/'.$media->post_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}" data-post-id="{{ $post->id }}" data-slide-to="0">
                                            @elseif($media->type ==  "video")
                                                <video width="100%" height="100%" controls>
                                                    <source src="{{asset('public/uploads/posts/'.$media->post_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
                                                </video>
                                            @endif
                                        </div>
                                    </div>
                                    @else
                                    <div class="col1">
                                        <div class="imgbox">
                                            @if($media->type ==  "image")
                                                <img class="w-100" src="{{asset('public/uploads/posts/'.$media->post_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}" data-post-id="{{ $post->id }}" data-slide-to="0">
                                            @elseif($media->type ==  "video")
                                                <video width="100%" height="100%" controls>
                                                    <source src="{{asset('public/uploads/posts/'.$media->post_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
                                                </video>
                                            @endif
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            @endforeach
                        @endif

                        
                    @endif
                    

                    {{--
                    @if(count($getpost_media) > 0)
                    
                        <div class="row">
                            @foreach($getpost_media as $media)
                                <div class="col-md-6" style="margin-bottom:10px">
                                    @if($media->type == "image")
                                        <img src="{{asset('public/uploads/posts/'.$media->post_media)}}" height="150" width="150"> 
                                    @endif
                                    @if($media->type == "video")
                                    <video width="100%" height="150px" controls>
                                        <source src="{{asset('public/uploads/posts/'.$media->post_media)}}" type='video/mp4'>
                                    </video>
                                    @endif
                                    @if($media->type == "document")
                                    <a href="{{asset('public/uploads/posts/'.$media->post_media)}}" target='_blank'>{{$media->post_media}}</a>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    @endif--}}
                    
                    
                    
                    
                    <!-- <ul class="skill-tags">
                        <li><a href="#" title="">HTML</a></li>
                        <li><a href="#" title="">PHP</a></li>
                        <li><a href="#" title="">CSS</a></li>
                        <li><a href="#" title="">Javascript</a></li>
                        <li><a href="#" title="">Wordpress</a></li> 	
                    </ul> -->
                </div>
                <div class="checkimg"></div>
                <div class="job-status-bar">
                    <ul class="like-com mt-2">
                        <li class="mr-0 mainpost_likebtn">
                            <button class="emjbtn_like">
                                
                                @if($getpost_like)
                                    @php $getemojies_details = App\Models\LikeEmoji::where('id',$getpost_like->like_emoji_id)->first();   @endphp

                                    @if($getemojies_details)
                                    <a href="javascript:;" class="create_like" data-dislike-status="1" data-emoji-id="{{ $getemojies_details->id }}" 
                                        data-post-id="{{ $post->id }}">
                                        <img src="{{asset('public/uploads/emoji/'.$getemojies_details->image)}}"  height="30" width="30" class="emojibtn_class">
                                    </a>
                                    @else
                                    <a href="javascript:;">
                                        <img src="{{asset('assets/frontend/images/default_like.png')}}" height="30" width="30" class="emojibtn_class">
                                    </a>
                                    @endif
                                @else
                                    <a href="javascript:;">
                                        <img src="{{asset('assets/frontend/images/default_like.png')}}" height="30" width="30" class="emojibtn_class">
                                    </a>
                                @endif
                                
                                <div class="emoji-container">
                                    @if(count($getemojies) > 0)
                                    @foreach($getemojies as $keyemoji =>  $getemojie)
                                        <div class="emoji">
                                            <div class="icon create_like" data-emoji-id="{{ $getemojie->id }}" data-dislike-status="0" data-post-id="{{ $post->id }}">
                                                <img src="{{asset('public/uploads/emoji/'.$getemojie->image)}}" height="50">
                                            </div>
                                        </div>
                                    @endforeach
                                    @endif
                                </div>
                            </button>
                        </li>
                        
                            @php $getpost_likes = App\Models\Like::where(['post_id' => $post->id])->where('user_id','!=',$user_detail->id)->groupby('like_emoji_id')->get();  @endphp

                            @if(count($getpost_likes) > 0)
                                <li>
                                    @foreach($getpost_likes as $likes_key => $getpostlike)
                                        @php $getemojies_likes_details = App\Models\LikeEmoji::where('id',$getpostlike->like_emoji_id)->first();   @endphp
                                        <img src="{{asset('public/uploads/emoji/'.$getemojies_likes_details->image)}}"  height="30" width="30" class="@if($likes_key != 0) emojibtn_class1 @endif">
                                    @endforeach
                                </li>
                            @endif
                            
                        
                        <li>
                            <!-- <a href="javascript:;" class="create_like @if($getpost_like) like @endif" data-post-id="{{ $post->id }}"><i class="fas fa-thumbs-up"></i> Like</a> -->
                            @if($getpostlike_count != 0)
                            <span class="ml-0 like_show mrcountbox_postlike" data-post-id="{{ $post->id }}">{{ $getpostlike_count }}</span>
                            @else
                            <span class="ml-0 mrcountbox_postlike" data-post-id="{{ $post->id }}">{{ $getpostlike_count }}</span>
                            @endif
                        </li>

                        <!-- <li>
                            <a href="#"><i class="fas fa-heart"></i> Like</a>
                            <img src="{{ asset('assets/frontend/images/liked-img.png') }}" alt="">
                            <span>25</span>
                        </li>   -->
                        <li><a href="javascript:;" class="com @if(count($comments) > 2) com_show com_{{$post->id}} @endif" data-post_id="{{$post->id}}"><i class="fas fa-comment-alt"></i>  @if(count($comments) > 0) Comments {{count($comments)}} @else Comment @endif</a></li>
                    </ul>
                    {{-- <a href="#"><i class="fas fa-eye"></i>Views {{$post->views}}</a>  --}}
                </div>
                
                {{--<div class="post-comment mt-2">
                    <div class="cm_img">
                        @if($user_detail->profile_image != null)
                            <a href="{{ route('get.user.profile',[$user_detail->roles->slug,$user_detail->slug]) }}"><img src="{{ url('public/uploads/profile_image/'.$user_detail->profile_image)}}" class="borderradius" alt="" width="40" height="40"></a>
                        @else
                            <a href="{{ route('get.user.profile',[$user_detail->roles->slug,$user_detail->slug]) }}"><img src="{{ asset('assets/frontend/images/user.jpg')}}" alt="" width="40" class="borderradius" height="40"></a>
                        @endif
                        <!--<img src="{{ asset('assets/frontend/images/resources/bg-img4.png') }}" alt="">-->
                    </div>
                    <div class="comment_box">
                        <form>
                            <input type="text" class="commentdiv_{{$post->id}} commenttext" data-id="{{$post->id}}" placeholder="Post a comment">
                            <button type="button" class="commentdiv" data-id="{{$post->id}}">Send</button>
                        </form>
                    </div>
                </div>--}}

                <div class="comment-section">                      
                    <div class="comment-sec">
                        <ul>
                            @if(count($comments->where('is_reply',0)) > 0)
                                @foreach($comments->where('is_reply',0) as $key => $comment)

                                    @if(count($comments->where('is_reply',0)) > 2 && $key > 1)
                                        <li class="hideCommentDiv hideCommentDiv_{{$comment->post_id}}">
                                    @else
                                        <li>
                                    @endif
                                            <div class="comment-list pt-3">
                                                <div class="bg-img">
                                                    @if($comment->user && $comment->user->profile_image != null) 
                                                        <img src="{{ url('public/uploads/profile_image/'.$comment->user->profile_image)}}" alt="" class="borderradius" alt="" width="40" height="40">
                                                    @else
                                                    <img src="{{asset('assets/frontend/images/user.jpg')}}" alt="" class="borderradius" alt="" width="40" height="40">
                                                    @endif
                                                </div>
                                                <div class="comment">

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="comment-profile">
                                                                @if($comment->user && $comment->user->profile_image != null)
                                                                    
                                                                    <img src="{{ url('public/uploads/profile_image/'.$comment->user->profile_image)}}" alt="" class="borderradius" alt="" width="40" height="40">
                                                                @else
                                                                <img src="{{asset('assets/frontend/images/user.jpg')}}" alt="" class="borderradius" alt="" width="40" height="40">
                                                                @endif
                                                                <!--<img src="images/resources/us-pc2.png" alt="">-->
                                                            </div>
                                                            <div class="comment-part">
                                                                <div class="box">
                                                                    @if($comment->user && $comment->user->first_name && $comment->user->last_name != null)
                                                                        <h3>{{$comment->user->first_name}}  {{$comment->user->last_name}}</h3>
                                                                    @else
                                                                    <h3>User</h3>
                                                                    @endif
                                                                    <span style="float: right!important;"><i class="fas fa-clock"></i> {{ $comment->created_at->diffForHumans() }}</span>
                                                                    <p>{{$comment->comment}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row mt-1">
                                                        <div class="col-md-1"></div>
                                                        <div class="col-md-2 pl-0 pr-0 widthcolmd2">
                                                            <button class="emjbtn_comment" style="margin-top: 4px;">
                                                                @php $getpost_comment_like = App\Models\CommentLike::where(['user_id' => $user_detail->id,'post_id' => $post->id,'comment_id' => $comment->id])->first();  @endphp
                                                                @if($getpost_comment_like)
                                                                    @php $getemojies_details = App\Models\LikeEmoji::where('id',$getpost_comment_like->like_emoji_id)->first();   @endphp

                                                                    @if($getemojies_details)
                                                                    <a href="javascript:;" class="create_comment_like" data-dislike-status=
                                                                        "1" data-emoji-id="{{ $getemojies_details->id }}" data-post-id="{{ $post->id }}" data-comment_id="{{$comment->id}}">
                                                                        <img src="{{asset('public/uploads/emoji/'.$getemojies_details->image)}}"  height="30" width="30" class="emojibtn_class">
                                                                    </a>
                                                                    @else
                                                                    <a href="javascript:;">
                                                                        <img src="{{asset('assets/frontend/images/default_like.png')}}" height="30" width="30" class="emojibtn_class">
                                                                    </a>
                                                                    @endif
                                                                @else
                                                                    <a href="javascript:;">
                                                                        <img src="{{asset('assets/frontend/images/default_like.png')}}" height="30" width="30" class="emojibtn_class">
                                                                    </a>
                                                                @endif
                                                                
                                                                <div class="emoji-container">
                                                                    @if(count($getemojies) > 0)
                                                                    @foreach($getemojies as $keyemoji =>  $getemojie)
                                                                        <div class="emoji">
                                                                            <div class="icon create_comment_like" data-dislike-status=
                                                                        "0" data-emoji-id="{{ $getemojie->id }}" data-post-id="{{ $post->id }}" data-comment_id="{{$comment->id}}">
                                                                                <img src="{{asset('public/uploads/emoji/'.$getemojie->image)}}" height="50">
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                    @endif
                                                                </div>
                                                            </button>

                                                        </div>

                                                        <div class="col-md-3 pl-0 replaybtn-margin1">

                                                            @php $getpost_comments_likes = App\Models\CommentLike::where(['post_id' => $post->id,'comment_id' => $comment->id])->where('user_id','!=',$user_detail->id)->groupby('like_emoji_id')->get();  @endphp

                                                            @if(count($getpost_comments_likes) > 0)
                                                                @foreach($getpost_comments_likes as $likes_comment_key => $getpost_comments_likes)
                                                                    @php $getemojies_likes_details = App\Models\LikeEmoji::where('id',$getpost_comments_likes->like_emoji_id)->first();   @endphp
                                                                    <img src="{{asset('public/uploads/emoji/'.$getemojies_likes_details->image)}}"  height="30" width="30" class="@if($likes_comment_key != 0) emojibtn_class1 @endif">
                                                                @endforeach
                                                            @endif

                                                            <a class="">
                                                            @php $getpostlike_comment_count = App\Models\CommentLike::where(['post_id' => $post->id,'comment_id' => $comment->id])->count('id'); @endphp
                                                            @if($getpostlike_comment_count != 0)
                                                            <span class="text-light count_span comment_like_show cursor_pointer"  data-post-id="{{ $post->id }}" data-comment-id="{{ $comment->id }}">{{ $getpostlike_comment_count }}</span>
                                                            @else
                                                            <span class="text-light count_span" data-post-id="{{ $post->id }}">{{ $getpostlike_comment_count }}</span>
                                                            @endif
                                                            </a>
                                                        </div>

                                                        <div class="col-md-3 mt-1">
                                                            {{-- <a href="javascript:;" title="" data-comment_id="{{$comment->id}}" data-post_id="{{$post->id}}" class="active getComment default-color"><i class="fa fa-reply-all"></i>Reply</a> --}} 
                                                        </div>

                                                        <div class="col-md-3">

                                                           {{-- @if($post->user_id == $user_detail->id)
                                                                @if($comment->user && $comment->user->id == $user_detail->id)
                                                                    <span><i class="fa fa-trash text-danger deleteComment" data-id="{{$comment->id}}"></i>
                                                                    &nbsp;&nbsp;<i class="fa fa-edit text-info editCommentEdit" data-comment="{{$comment->comment}}" data-id="{{$comment->id}}"  data-post_id="{{$post->id}}"></i>&nbsp;&nbsp;</span>
                                                                @else
                                                                    <span><i class="fa fa-trash text-danger deleteComment" data-id="{{$comment->id}}"></i></span>   
                                                                @endif
                                                            @else

                                                                @if($comment->user && $comment->user->id == $user_detail->id)
                                                                <span><i class="fa fa-trash text-danger deleteComment" data-id="{{$comment->id}}"></i>
                                                                &nbsp;&nbsp;<i class="fa fa-edit text-info editCommentEdit" data-comment="{{$comment->comment}}" data-id="{{$comment->id}}"  data-post_id="{{$post->id}}"></i>&nbsp;&nbsp;</span>
                                                                @endif
                                                            @endif --}}

                                                        </div>

                                                    </div>

                                                    <!--  -->

                                                    






                                                    
                                                    <!-- -->


                                                    

                                                </div>
                                            </div><!--comment-list end-->
                                            <ul class="replayDiv">
                                                @if(count($comments->where('to_comment',$comment->id)) > 0)
                                                    @foreach($comments->where('to_comment',$comment->id) as $reply_comment)
                                                        <li>
                                                            <div class="comment-list">
                                                                <div class="comment">

                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                        <div class="comment-profile">
                                                                            @if($reply_comment->user && $reply_comment->user->profile_image)
                                                                            <img src="{{ url('public/uploads/profile_image/'.$reply_comment->user->profile_image)}}" alt="" class="borderradius" alt="" width="40" height="40">
                                                                            @else
                                                                            <img src="{{asset('assets/frontend/images/user.jpg')}}" class="borderradius" alt="">
                                                                            @endif
                                                                        </div>
                                                                        <div class="comment-part">
                                                                            <div class="box">
                                                                                @if($reply_comment->user && $reply_comment->user->first_name && $reply_comment->user->last_name != null)
                                                                                    <h3>{{ $reply_comment->user->first_name}}  {{$reply_comment->user->last_name}} </h3>
                                                                                @else
                                                                                <h3>User</h3>
                                                                                @endif
                                                                                <span style="float: right!important;"><i class="fas fa-clock"></i>{{ $reply_comment->created_at->diffForHumans() }}</span>
                                                                                <p>{{$reply_comment->comment}} </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    </div>

                                                                    <div class="row mt-1">
                                                                        <div class="col-md-2"></div>
                                                                        <div class="col-md-2 pl-1 pr-0 widthcolmd2_replay">

                                                                            <button class="emjbtn_replay">
                                                                                @php $getpost_comment_like = App\Models\CommentLike::where(['user_id' => $user_detail->id,'post_id' => $post->id,'comment_id' => $reply_comment->id])->first();  @endphp
                                                                                @if($getpost_comment_like)
                                                                                    @php $getemojies_details = App\Models\LikeEmoji::where('id',$getpost_comment_like->like_emoji_id)->first();   @endphp

                                                                                    @if($getemojies_details)
                                                                                    <a href="javascript:;" class="create_comment_like" data-dislike-status=
                                                                                        "1" data-emoji-id="{{ $getemojies_details->id }}" data-post-id="{{ $post->id }}" data-comment_id="{{$reply_comment->id}}">
                                                                                        <img src="{{asset('public/uploads/emoji/'.$getemojies_details->image)}}"  height="30" width="30" class="emojibtn_class">
                                                                                    </a>
                                                                                    @else
                                                                                    <a href="javascript:;">
                                                                                        <img src="{{asset('assets/frontend/images/default_like.png')}}" height="30" width="30" class="emojibtn_class">
                                                                                    </a>
                                                                                    @endif
                                                                                @else
                                                                                    <a href="javascript:;">
                                                                                        <img src="{{asset('assets/frontend/images/default_like.png')}}" height="30" width="30" class="emojibtn_class">
                                                                                    </a>
                                                                                @endif
                                                                                
                                                                                <div class="emoji-container">
                                                                                    @if(count($getemojies) > 0)
                                                                                    @foreach($getemojies as $keyemoji =>  $getemojie)
                                                                                        <div class="emoji">
                                                                                            <div class="icon create_comment_like" data-dislike-status=
                                                                                        "0" data-emoji-id="{{ $getemojie->id }}" data-post-id="{{ $post->id }}" data-comment_id="{{$reply_comment->id}}">
                                                                                                <img src="{{asset('public/uploads/emoji/'.$getemojie->image)}}" height="50">
                                                                                            </div>
                                                                                        </div>
                                                                                    @endforeach
                                                                                    @endif
                                                                                </div>

                                                                                
                                                                            </button>


                                                                            
                                                                        </div>
                                                                        <div class="col-md-5 mt1reply pl-3">


                                                                            @php $getpost_comments_replay_likes = App\Models\CommentLike::where(['post_id' => $post->id,'comment_id' => $reply_comment->id])->where('user_id','!=',$user_detail->id)->groupby('like_emoji_id')->get();  @endphp

                                                                            @if(count($getpost_comments_replay_likes) > 0)
                                                                                @foreach($getpost_comments_replay_likes as $likes_comment_replay_key => $getpost_comments_replay_likes)
                                                                                    @php $getemojies_likes_details = App\Models\LikeEmoji::where('id',$getpost_comments_replay_likes->like_emoji_id)->first();   @endphp
                                                                                    <img src="{{asset('public/uploads/emoji/'.$getemojies_likes_details->image)}}"  height="30" width="30" class="@if($likes_comment_replay_key != 0) emojibtn_class1 @endif">
                                                                                @endforeach
                                                                            @endif


                                                                            <a class="text-light">
                                                                            @php $getpostlike_comment_replay_count = App\Models\CommentLike::where(['post_id' => $post->id,'comment_id' => $reply_comment->id])->count('id'); @endphp
                                                                            @if($getpostlike_comment_replay_count != 0)
                                                                            <span class="count_span text-light comment_like_show ml_counterspan cursor_pointer"  data-post-id="{{ $post->id }}" data-comment-id="{{ $reply_comment->id }}">{{ $getpostlike_comment_replay_count }}</span>
                                                                            @else
                                                                            <span class="count_span text-light ml_counterspan" data-post-id="{{ $post->id }}">{{ $getpostlike_comment_replay_count }}</span>
                                                                            @endif
                                                                            </a>
                                                                        </div>
                                                                        {{-- <div class="col-md-3">
                                                                    
                                                                            @if($user_detail->id == $post->user_id)
                                                                                @if($reply_comment->user && $reply_comment->user->id == $user_detail->id)
                                                                                <span><i class="fa fa-trash text-danger deleteComment" data-id="{{$reply_comment->id}}"></i>&nbsp;&nbsp;<i class="fa fa-edit text-info editCommentEdit" data-comment="{{$reply_comment->comment}}" data-id="{{$reply_comment->id}}"  data-post_id="{{$post->id}}"></i></span>
                                                                                @else
                                                                                <span><i class="fa fa-trash text-danger deleteComment" data-id="{{$reply_comment->id}}"></i></span>   
                                                                                @endif
                                                                            @else
                                                                                @if($reply_comment->user && $reply_comment->user->id == $user_detail->id)
                                                                                <span><i class="fa fa-trash text-danger deleteComment" data-id="{{$reply_comment->id}}"></i>&nbsp;&nbsp;<i class="fa fa-edit text-info editCommentEdit" data-comment="{{$reply_comment->comment}}" data-id="{{$reply_comment->id}}"  data-post_id="{{$post->id}}"></i></span>
                                                                                @else
                                                                                
                                                                                @endif
                                                                            @endif
                                                                        </div> --}}
                                                                    </div>
                                                                </div>

                                                            </div>



                                                            




                                                        </li>
                                                    @endforeach
                                                @endif
                                                
                                            </ul>
                                        </li>
                                @endforeach
                            @endif
                        </ul>
                    </div><!--comment-sec end-->
                </div><!--comment-section end-->
                @if(count($comments->where('is_reply',0)) > 2)
                <a href="javascript:;" class="plus-ic default-color showCommentDiv showCommentDiv_{{$post->id}}" data-post_id="{{$post->id}}">
                    Load more comments <!--<i class="la la-plus"></i>-->
                </a>
                @endif
                <a href="javascript:;" class="plus-ic hideMoreComment hideMoreComment_{{$post->id}}" data-post_id="{{$post->id}}">
                    <i class="la la-minus"></i>
                </a>
                {{--<div class="post-comment hideReplyDiv hideReplyDiv_{{$post->id}}">
                    <div class="cm_img">
                    @if($user_detail->profile_image != null)
                        <a href="{{ route('get.user.profile',[$user_detail->roles->slug,$user_detail->slug]) }}"><img src="{{ url('public/uploads/profile_image/'.$user_detail->profile_image)}}" class="borderradius" alt="" width="40" height="40"></a>
                    @else
                        <a href="{{ route('get.user.profile',[$user_detail->roles->slug,$user_detail->slug]) }}"><img src="{{ asset('assets/frontend/images/user.jpg')}}" alt="" width="40" class="borderradius" height="40"></a>
                    @endif
                    </div>
                    <div class="comment_box">
                        <form id="reply_{{$post->id}}">
                            <input type="hidden" name="post_id" value="{{$post->id}}">
                            <input type="hidden" name="comment_id" class="comment_id_{{$post->id}}">
                            <input type="text" name="comment" class="reply_comment_{{$post->id}} replaytext" data-post_id="{{$post->id}}" placeholder="Post a comment">
                            <button type="button" class="replyComment" data-post_id="{{$post->id}}">Send</button>
                        </form>
                    </div>
                </div>--}}

                <div class="post-comment hideEditDiv hideEditDiv_{{$post->id}}">
                    <div class="cm_img">
                        {{--<img src="{{ asset('assets/frontend/images/resources/bg-img4.png') }}" alt="">--}}
                        @if($user_detail->profile_image != null)
                            <a href="{{ route('get.user.profile',[$user_detail->roles->slug,$user_detail->slug]) }}"><img src="{{ url('public/uploads/profile_image/'.$user_detail->profile_image)}}" class="borderradius" alt="" width="40" height="40"></a>
                        @else
                            <a href="{{ route('get.user.profile',[$user_detail->roles->slug,$user_detail->slug]) }}"><img src="{{ asset('assets/frontend/images/user.jpg')}}" alt="" width="40" class="borderradius" height="40"></a>
                        @endif
                    </div>
                    {{--<div class="comment_box">
                        <form id="edit_comment_{{$post->id}}">
                            <input type="hidden" name="post_id" value="{{$post->id}}">
                            <input type="hidden" name="comment_id" class="edit_comment_id_{{$post->id}}">
                            <input type="text" name="comment" class="edit_comment_{{$post->id}} edit_commenttext" data-post_id="{{$post->id}}" placeholder="Edit a comment">
                            <button type="button" class="editComment" data-post_id="{{$post->id}}">Send</button>
                        </form>
                    </div>--}}
                </div>                                                    
            </div><!--post-bar end-->

            <div class="modal fade img-modal gallery_model" id="exampleModalCenter_{{ $post->id }}" data-post-id="{{ $post->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-8 modal-image">
                                @foreach($getpost_media as $mediakey => $media)
                                    
                                    <img class="img-responsive slider_gallery" id="slide_{{ $media->id }}" src="{{asset('public/uploads/posts/'.$media->post_media)}}">

                                @endforeach                            
                                    <!--<img class="img-responsive hidden" src="http://upload.wikimedia.org/wikipedia/commons/1/1a/Bachalpseeflowers.jpg" />
                                <img class="img-responsive hidden" src="http://www.netflights.com/media/216535/hong%20kong_03_681x298.jpg" />-->

                                <a href="" class="img-modal-btn left"><i class="fas fa-chevron-left"></i></a>
                                <a href="" class="img-modal-btn right"><i class="fas fa-chevron-right"></i> </a>
                            </div>
                            <div class="col-lg-4 col-md-12 modal-meta">
                                <div class="modal-meta-top">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="far fa-times-circle"></i></span></button>
                                    <div class="img-poster clearfix">
                                    <div class="imgbox">
                                        @if(isset($post->user->profile_image))
                                            <img src="{{ url('public/uploads/profile_image/'.$post->user->profile_image)}}" class="img-circle borderradius" alt="">
                                        @else
                                            <img src="{{ asset('assets/frontend/images/user.jpg')}}" alt="" class="img-circle borderradius">
                                        @endif
                                        <!--<img class="img-circle" src="https://s-media-cache-ak0.pinimg.com/736x/3b/7d/6f/3b7d6f60e2d450b899c322266fc6edfd.jpg"/>-->
                                    </div>
                                    <div class="usy-name">
                                        <h3>{{ $post->user->first_name ?? '' }}  {{ $post->user->last_name ?? '' }}</h3>
                                        <span>{{ $post->created_at->diffForHumans() }}</span>
                                    </div>
                                    </div>

                                    <div class="modelmeta">
                                    <p class="mt-3">{{ isset($post->post_text) ? $post->post_text:'' }}</p>
                                    <!--<p>hello<br>
                                        Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, <br><br>graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book.</p>-->
                                    </div>
                                <div class="job-status-bar">
                                    <ul class="like-com">
                                        <li>
                                            <div class="dynamic_like_box">                        
                                                @include('frontend.home.dynamic_gallery_like')
                                            </div>                    
                                            <!-- <a href="javascript:;" class="create_like_gallery @if($getpost_like) like @endif" data-post-id="{{ $post->id }}"><i class="fas fa-heart"></i> Like</a>-->                    
                                            <!-- <a href="#"><i class="fas fa-heart"></i> Like</a> -->
                                        </li> 
                                    </ul>
                                </div>
                                </div>
                                </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            
        @endforeach
        @if($postcnt == 0)
        <div class="post-bar">
            <div class="post_topbar">
                No post found..
            </div>
        </div>
        @endif
    </div>

    @if($total_post_counter >= $post_load_count)                    
    <div class="process-comm">
        <button class="btn loadmore load_more_btn" id="{{ $post_load_count }}">Load More Posts</button>
    </div>
    @endif

        {{--<div class="hideLoader">
        <input type="hidden" class="last_data_number" value="0">
        <div class="process-comm">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div><!--process-comm end-->
        </div>--}}
    @else
    </div>
    <div class="post-bar">
        <div class="post_topbar">
            No post found..
        </div>
    </div>
    @endif

    
</div><!--posts-section end-->
    

