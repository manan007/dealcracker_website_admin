<!--main-left-sidebar start-->
<div class="main-left-sidebar no-margin">
    <div class="user-data full-width">
        <div class="user-profile">
            <div class="username-dt">
                <div class="usr-pic">
                    @if(Auth::user()->profile_image != null)
                        <a href="{{ route('user.profile',[Auth::user()->roles->slug,Auth::user()->slug]) }}"><img src="{{ url('public/uploads/profile_image/'.Auth::user()->profile_image)}}" alt="" width="110" height="110"></a>
                    @else
                        <a href="{{ route('user.profile',[Auth::user()->roles->slug,Auth::user()->slug]) }}"><img src="{{ asset('assets/frontend/images/user.jpg')}}" alt="" width="110" height="110"></a>
                    @endif
                </div>
            </div><!--username-dt end-->
            <div class="user-specs">
                <a href="{{ route('user.profile',[Auth::user()->roles->slug,Auth::user()->slug]) }}"><h3>{{ Auth::user()->first_name ?? '' }}  {{ Auth::user()->last_name ?? '' }}</h3></a>
                <div class="mb-2">{{ Auth::user()->sport->name ?? '' }}</div>
                @if(isset(Auth::user()->high_school))
                <div><a href="{{ route('school.profile',Auth::user()->school->slug) }}" class='link_label'>{{ Auth::user()->school->school_name ?? '' }}</a></div>
                @else
                <div>{{ Auth::user()->school->school_name ?? '' }}</div>
                @endif
                <!--<span>Graphic Designer at Self Employed</span>-->
               <div class="left_sidebar_overviewsection">
                @include('frontend.home.dynamic_overview_section')
                </div>
            </div>

        </div><!--user-profile end-->
        <ul class="user-fw-status">
            <li>
                <a href="{{ route('user.profile',[Auth::user()->roles->slug,Auth::user()->slug]) }}" title="View Profile">View Profile</a>
            </li>
            <!-- <li>
                <a href="javascript:;" class="edit_overview" title="Add Bio">Add Bio</a>
            </li> -->
            {{--<li>
                <h4>Following</h4>
                <span>{{ $total_following ?? '' }}</span>
            </li>--}}
            <li>
                <a href="{{ route('my.network.view') }}"><h4>Friends</h4></a>
                <span>{{ $total_followers ?? '' }}</span>
            </li>
            {{--<li>
                <h4>Views</h4>
                <span>{{ $total_views ?? '' }}</span>
            </li>--}}
            <!--<li>
                <h4>Groups </h4>
                <span>10</span>
            </li>
            <li>
                <h4>Forums </h4>
                <span>15</span>
            </li>-->
            
        </ul>
    </div><!--user-data end-->
    <div class="suggestions full-width">
        <div class="sd-title">
            <h3>Suggested Connections</h3>
            <!-- <i class="la la-ellipsis-v"></i> -->
        </div><!--sd-title end-->
        <div class="dynamic_suggestion_list">
            @include('frontend.home.dynamic_suggestion_list')
        </div>
        
    {{--    <div class="suggestions-list">
            <div class="suggestion-usd">
                <img src="{{ asset('assets/frontend/images/resources/s1.png') }}" alt="">
                <div class="sgt-text">
                    <h4>Jessica William</h4>
                    <span>Graphic Designer</span>
                </div>
                <span><i class="la la-plus"></i></span>
            </div>
            <div class="suggestion-usd">
                <img src="{{ asset('assets/frontend/images/resources/s2.png') }}" alt="">
                <div class="sgt-text">
                    <h4>John Doe</h4>
                    <span>PHP Developer</span>
                </div>
                <span><i class="la la-plus"></i></span>
            </div>
            <div class="suggestion-usd">
                <img src="{{ asset('assets/frontend/images/resources/s3.png') }}" alt="">
                <div class="sgt-text">
                    <h4>Poonam</h4>
                    <span>Wordpress Developer</span>
                </div>
                <span><i class="la la-plus"></i></span>
            </div>
            <div class="suggestion-usd">
                <img src="{{ asset('assets/frontend/images/resources/s4.png') }}" alt="">
                <div class="sgt-text">
                    <h4>Bill Gates</h4>
                    <span>C & C++ Developer</span>
                </div>
                <span><i class="la la-plus"></i></span>
            </div>
            <div class="suggestion-usd">
                <img src="{{ asset('assets/frontend/images/resources/s5.png') }}" alt="">
                <div class="sgt-text">
                    <h4>Jessica William</h4>
                    <span>Graphic Designer</span>
                </div>
                <span><i class="la la-plus"></i></span>
            </div>
            <div class="suggestion-usd">
                <img src="{{ asset('assets/frontend/images/resources/s6.png') }}" alt="">
                <div class="sgt-text">
                    <h4>John Doe</h4>
                    <span>PHP Developer</span>
                </div>
                <span><i class="la la-plus"></i></span>
            </div>
            <div class="view-more">
                <a href="#" title="">View More</a>
            </div>
        </div><!--suggestions-list end--> --}}
    </div><!--suggestions end-->
    <div class="tags-sec full-width home_footer">
        <ul>
            <!-- <li><a href="">Privacy Policy</a></li> -->
			<li><a href="{{ route('home.legal') }}">Legal Disclaimers</a></li>
            <!-- <li><a href="{{ route('home.about_us') }}" title="">About Us</a></li> -->
            <li><a href="{{ route('home.how_esportsrecruiter_work') }}" title="">How It Works</a></li>
            <li><a href="{{ route('home.terms_conditions') }}" title="">Terms & Conditions</a></li>
            <li><a href="{{route('home.contact_us')}}" title="">Contact Us</a></li>
            <li style="border-right: 0px solid #b2b2b2;"><a href="{{route('home.sitemap')}}" title="">Site Map</a></li>
            <!-- <li><a href="#" title="">Help Center</a></li>
            <li><a href="#" title="">About</a></li>
            <li><a href="#" title="">Privacy Policy</a></li>
            <li><a href="#" title="">Community Guidelines</a></li>
            <li><a href="#" title="">Cookies Policy</a></li>
            <li><a href="#" title="">Career</a></li>
            <li><a href="#" title="">Language</a></li>
            <li><a href="#" title="">Copyright Policy</a></li> -->
        </ul>
        <div class="cp-sec ccp-sec">
            <p><img src="{{ asset('assets/frontend/images/cp.png') }}" alt="">Copyright {{ date('Y') }}</p>
        </div>
    </div><!--tags-sec end-->
</div><!--main-left-sidebar end-->