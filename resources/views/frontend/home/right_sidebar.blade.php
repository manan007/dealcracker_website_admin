<!--right-sidebar start-->
<div class="right-sidebar">
    <!--<div class="widget widget-about">
        <img src="{{ asset('assets/frontend/images/Faith-Logo.png') }}" alt="" width="100px">
        <h3>Lorem Ipsum is simply</h3>
        <span>Pay only for the Hours worked</span>
        <div class="sign_link">
            <h3><a href="sign-in.html" title="">Sign up</a></h3>
            <a href="#" title="">Learn More</a>
        </div>
    </div>--><!--widget-about end-->
    @php $getrole = App\Models\Role::whereIn('slug',['recruiter','agent','scout','coach'])->get(); $roleid_array=[]; foreach($getrole as $roledata){ $roleid_array[] = $roledata->id; }  
    @endphp

    @if(!in_array(Auth::user()->role_id,$roleid_array))
    <div class="widget suggestions full-width">
        <div class="sd-title">
            <h3>Connect with Schools</h3>
            <i class="la la-ellipsis-v"></i>
        </div><!--sd-title end-->
        <div class="dynamic_school_list">
        @include('frontend.home.dynamic_school_list')
        </div>
    </div>
    @endif

    <div class="widget suggestions full-width">
        <div class="sd-title">
            <h3>Top Athletes in your sport</h3>
            <i class="la la-ellipsis-v"></i>
        </div><!--sd-title end-->
        <div class="dynamic_athlete_list">
            @include('frontend.home.dynamic_top_athlete_list')
        </div>
        
        {{--<div class="suggestions-list">
            <div class="suggestion-usd">
                <img src="{{ asset('assets/frontend/images/resources/s1.png') }}" alt="">
                <div class="sgt-text">
                    <h4>Jessica William</h4>
                    <span>Graphic Designer</span>
                </div>
                <span><i class="la la-plus"></i></span>
            </div>
            <div class="suggestion-usd">
                <img src="{{ asset('assets/frontend/images/resources/s2.png') }}" alt="">
                <div class="sgt-text">
                    <h4>John Doe</h4>
                    <span>PHP Developer</span>
                </div>
                <span><i class="la la-plus"></i></span>
            </div>
            <div class="suggestion-usd">
                <img src="{{ asset('assets/frontend/images/resources/s3.png') }}" alt="">
                <div class="sgt-text">
                    <h4>Poonam</h4>
                    <span>Wordpress Developer</span>
                </div>
                <span><i class="la la-plus"></i></span>
            </div>
            <div class="suggestion-usd">
                <img src="{{ asset('assets/frontend/images/resources/s4.png') }}" alt="">
                <div class="sgt-text">
                    <h4>Bill Gates</h4>
                    <span>C &amp; C++ Developer</span>
                </div>
                <span><i class="la la-plus"></i></span>
            </div>
            <div class="suggestion-usd">
                <img src="{{ asset('assets/frontend/images/resources/s5.png') }}" alt="">
                <div class="sgt-text">
                    <h4>Jessica William</h4>
                    <span>Graphic Designer</span>
                </div>
                <span><i class="la la-plus"></i></span>
            </div>
            <div class="view-more">
                <a href="#" title="">View More</a>
            </div>
        </div>--}}<!--suggestions-list end-->
    </div>

    @if(!in_array(Auth::user()->role_id,$roleid_array))
    <div class="widget suggestions full-width">
        <div class="sd-title">
            <h3>Most Viewed Schools</h3>
            <i class="la la-ellipsis-v"></i>
        </div><!--sd-title end-->
        <div class="dynamic_most_viewed_school">
            @include('frontend.home.most_viewed_school')
        </div>    
        
        {{--<div class="suggestions-list">
            <div class="suggestion-usd">
                <img src="{{ asset('assets/frontend/images/resources/s1.png') }}" alt="">
                <div class="sgt-text">
                    <h4>Jessica William</h4>
                    <span>Graphic Designer</span>
                </div>
                <span><i class="la la-plus"></i></span>
            </div>
            <div class="suggestion-usd">
                <img src="{{ asset('assets/frontend/images/resources/s2.png') }}" alt="">
                <div class="sgt-text">
                    <h4>John Doe</h4>
                    <span>PHP Developer</span>
                </div>
                <span><i class="la la-plus"></i></span>
            </div>
            <div class="suggestion-usd">
                <img src="{{ asset('assets/frontend/images/resources/s3.png') }}" alt="">
                <div class="sgt-text">
                    <h4>Poonam</h4>
                    <span>Wordpress Developer</span>
                </div>
                <span><i class="la la-plus"></i></span>
            </div>
            <div class="suggestion-usd">
                <img src="{{ asset('assets/frontend/images/resources/s4.png') }}" alt="">
                <div class="sgt-text">
                    <h4>Bill Gates</h4>
                    <span>C &amp; C++ Developer</span>
                </div>
                <span><i class="la la-plus"></i></span>
            </div>
            <div class="suggestion-usd">
                <img src="{{ asset('assets/frontend/images/resources/s5.png') }}" alt="">
                <div class="sgt-text">
                    <h4>Jessica William</h4>
                    <span>Graphic Designer</span>
                </div>
                <span><i class="la la-plus"></i></span>
            </div>
            <div class="view-more">
                <a href="#" title="">View More</a>
            </div>
        </div>--}}<!--suggestions-list end-->
    </div>
    @endif
    
</div><!--right-sidebar end-->