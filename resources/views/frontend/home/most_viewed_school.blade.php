
<div class="suggestions-list">
@if(count($top_schools) > 0)
    @foreach($top_schools as $school)
        
        <div class="suggestion-usd d-flex">
        <div class='col-md-2 pl-0 pt-1 pr-0'>
            <a href="{{ route('school.profile',$school->slug) }}" class='link_label'><img src="{{ asset('assets/frontend/images/default_school.png') }}" alt=""  width="35" height="35"></a>
        </div>
            <div class="sgt-text">
                <div class='col-md-12 pl-0 pt-1'>
                <h4><a href="{{ route('school.profile',$school->slug) }}" class='link_label'>{{ $school->school_name }}</h4></a>
                </div>
            </div>
        </div>
        
    @endforeach
@endif
</div>