<div class="row">
    <div class="col-md-12 text-center">
        <h1 class="h3">You don't have access to this feature, please upgrade your plan.</h1>
    </div>
</div>
<div class="row mt-3">
    <div class="col-md-12 text-center">
        <span><a href="{{ route('user.athlete_plan.sign_up') }}" class="btn btn-default1">Update Plan</a></span>
    </div>
</div>

