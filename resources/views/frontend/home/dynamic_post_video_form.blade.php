

<div class="modal fade sartpost mainpost-box" id="start-video-post" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Create a post</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			
			<form action="#" method="post" id="create_post_form">
			@csrf

				<div class="modal-body" style="height:auto;max-height:640px;overflow-y:auto;">
				<div class="row">
					<div class="col-md-12 alert alert-danger error-msgs d-none"></div>
				</div>	
				<div id="dynamic_post_video_data_model">
				
				</div>
				</div>
			
			</form>

			<div class="modal-footer">
				<div class="left-part" id="model_custom_video_footer">
					<ul>
					@if(count($getpost_media) == 0)
						<li><a href="javascript:;" class="subbox upload-photo-btn_video" id="upload-photo-btn_video"><i class="far fa-image"></i></a></li>
						<li><a href="javascript:;" class="subbox upload-video-btn_video" id="upload-video-btn_video"><i class="fab fa-youtube"></i></a></li>
						<!-- <li><a href="javascript:;" class="subbox upload-document-btn_video" id="upload-document-btn_video"><i class="far fa-file-alt"></i></a></li> -->    
					@else
						<li><a href="javascript:;" class="subbox" style="cursor: not-allowed;"><i class="far fa-image"></i></a></li>
						<li><a href="javascript:;" class="subbox" style="cursor: not-allowed;"><i class="fab fa-youtube"></i></a></li>
						<!-- <li><a href="javascript:;" class="subbox" style="cursor: not-allowed;"><i class="far fa-file-alt"></i></a></li> -->
					@endif
					</ul>
				</div>
				<div class="right-part">
					<a href="javascript:;" type="button" id="create_video_post">Post</a>
				</div>
			</div>
			<div class="visibilitysection">
				<div class="modal-header">
					<div class="backbtt"><i class="fas fa-chevron-left"></i></div>
					<h4 class="modal-title" id="myModalLabel">Who can see your post?</h4>
				</div>
				<div class="visibility-body">
					<p>Your post will be visible on feed, on your profile and in search results</p>
					<ul>
						<li class="anyone">
							<div class="type">
								<h4>Anyone</h4>
								<span>Anyone on or off esportsrecruiter</span>
							</div>
							<div class="input-part">
								<input type="radio" name="radio" class="inputradio">
							</div>
						</li>
						<li class="connect-only">
							<div class="type">
								<h4>Connections only</h4>
								<span>Connections on esportsrecruiter</span>
							</div>
							<div class="input-part">
								<input type="radio" name="radio" class="inputradio">
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>


