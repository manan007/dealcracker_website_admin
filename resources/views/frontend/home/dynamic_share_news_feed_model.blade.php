<div class="toppart">
    <div class="item col1">
        <div class="profileimg">
        @if(Auth::user()->profile_image != null)
            <img src="{{ url('public/uploads/profile_image/'.Auth::user()->profile_image)}}" class="borderradius" alt="" width="50" height="50">
        @else
            <img src="{{ asset('assets/frontend/images/user.jpg')}}" alt="" class="borderradius" width="50" height="50">
        @endif
        </div>
    </div>
    <div class="item col2">
        <div class="lockup_title ">
            <h3>{{ Auth::user()->first_name ?? '' }}  {{ Auth::user()->last_name ?? '' }}</h3>
            <a href="#" data-toggle="modal" data-target="#visibility"><i class="fas fa-globe-americas"></i> Anyone</a>
        </div>
    </div>
</div>

<div class="description">
    {{-- <div class="aboutpost post_text" contenteditable="true" id="post_text_{{ $post_data->id }}">{!! $post_data->post_text !!}</div> --}}
    <textarea class="aboutpost post_text" placeholder="What do you want to talk about?" name="post_text" id="post_text_{{ $post_data->id }}" value="{{ $post_data->post_text }}"></textarea>
</div>

<div class="media_part">

        <input type="hidden" class="update_post_id" id="update_post_id_{{ $post_data->id }}" value="0">

@if(count($getpost_media) > 0)

        <!-- <div class="row">
            <div class="col-md-12 text-right pr-0">
                <a href='javascript:;' class='delete_img' data-name="" data-value=""><i class='fa fa-close text-danger' aria-hidden='true'></i></a>
            </div> 
        </div>              -->

        @if(count($getpost_media) == 1)
            @foreach($getpost_media as $media)
                <div class="gallerybox">                        
                    <div class="col12">
                        <div class="imgbox">
                            @if($media->type == "image")
                                <img class="w-100" src="{{asset('public/uploads/posts/'.$media->post_media)}}" alt="First slide" data-slide-to="0"> 
                            @endif
                            @if($media->type == "video")
                            <video width="100%" controls>
                                <source src="{{asset('public/uploads/posts/'.$media->post_media)}}" type='video/mp4'>
                            </video>
                            @endif
                            @if($media->type == "document")
                            <a href="{{asset('public/uploads/posts/'.$media->post_media)}}" target='_blank'>{{$media->post_media}}</a>
                            @endif

                        </div>
                    </div>
                </div>
            @endforeach
        @elseif(count($getpost_media) == 2)
            @foreach($getpost_media as $media)
                <div class="gallerybox">
                    <div class="col2">
                        <div class="imgbox">
                            <img class="w-100" src="{{asset('public/uploads/posts/'.$media->post_media)}}" alt="First slide" data-slide-to="0">
                        </div>
                    </div>
                </div>
            @endforeach
        @elseif(count($getpost_media) == 3)
            @foreach($getpost_media as $key => $media)
                <div class="gallerybox">
                    @if($key == 0)                      
                    <div class="col12">
                        <div class="imgbox">
                            <img class="w-100" src="{{asset('public/uploads/posts/'.$media->post_media)}}" alt="First slide" data-slide-to="0">
                        </div>
                    </div>
                    @else
                    <div class="col2">
                        <div class="imgbox">
                            <img class="w-100" src="{{asset('public/uploads/posts/'.$media->post_media)}}" alt="First slide" data-slide-to="0">
                        </div>
                    </div>
                    @endif
                </div>
            @endforeach
        @elseif(count($getpost_media) == 4)
            @foreach($getpost_media as $media)
                <div class="gallerybox">
                    <div class="col2">
                        <div class="imgbox">
                            <img class="w-100" src="{{asset('public/uploads/posts/'.$media->post_media)}}" alt="First slide" data-slide-to="0">
                        </div>
                    </div>
                </div>
            @endforeach
        @elseif(count($getpost_media) == 5)
            @foreach($getpost_media as $key => $media)
                <div class="gallerybox">
                    @if($key == 0 || $key == 1)                     
                    <div class="col2">
                        <div class="imgbox">
                            <img class="w-100" src="{{asset('public/uploads/posts/'.$media->post_media)}}" alt="First slide" data-slide-to="0">
                        </div>
                    </div>
                    @else
                    <div class="col1">
                        <div class="imgbox">
                            <img class="w-100" src="{{asset('public/uploads/posts/'.$media->post_media)}}" alt="First slide" data-slide-to="0">
                        </div>
                    </div>
                    @endif
                </div>
            @endforeach
        @endif
    @endif
</div>

<div class="mt-5">
    @php  $getshared_post = App\Models\Post::where(['id' => $post_data->shared_post_id])->first(); @endphp
    <div class="share-details-row mt-5" style="border-bottom: none;">
        <div class="usy-name-share"> 
            @if(Auth::user()->id == $getshared_post->user->id)
            <a href="{{ route('user.profile',[Auth::user()->roles->slug,Auth::user()->slug]) }}" class='link_label'><h3>{{ isset($getshared_post->user->first_name) ? $getshared_post->user->first_name:'' }} {{ isset($getshared_post->user->last_name) ? $getshared_post->user->last_name:'' }}</h3></a>
            @else
            <a href="{{ route('get.user.profile',[$post->user->roles->slug,$getshared_post->user->slug]) }}" class='link_label'><h3>{{ isset($getshared_post->user->first_name) ? $getshared_post->user->first_name:'' }} {{ isset($getshared_post->user->last_name) ? $getshared_post->user->last_name:'' }}</h3></a>
            @endif
            <span>{{ date('d M Y',strtotime($getshared_post->created_at)) }}</span>
        </div>
    </div>
    
    <textarea class="aboutpost post_text" placeholder="What do you want to talk about?" name="post_text" id="post_text_{{ $post_data->id }}" readonly="true">{!! $post_data->post_text !!}</textarea>
</div>