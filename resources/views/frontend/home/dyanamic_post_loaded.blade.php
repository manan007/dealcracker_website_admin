
    @if(count($posts) > 0)
        @foreach($posts as $post)

            @php $getpost_media = App\Models\PostMedia::where('post_id',$post->id)->get(); @endphp
            <input type="hidden" class="post_ids" value="{{$post->id}}">
            <div class="post-bar">
                <div class="post_topbar">
                    <div class="usy-dt">
                        @if(isset($post->user->profile_image))
                            <img src="{{ url('public/uploads/profile_image/'.$post->user->profile_image)}}" alt="" width="40" height="40">
                        @else
                            <img src="{{ asset('assets/frontend/images/user.jpg')}}" alt="" width="40" height="40">
                        @endif
                        <!--<img src="{{ asset('assets/frontend/images/resources/us-pic.png') }}" alt="">-->
                        <div class="usy-name">
                            <h3>{{ isset($post->user->first_name) ? $post->user->first_name:'' }} {{ isset($post->user->last_name) ? $post->user->last_name:'' }}</h3>
                            <span><img src="{{ asset('assets/frontend/images/clock.png') }}" alt="">{{ $post->created_at->diffForHumans() }}</span>
                        </div>
                    </div>
                    <div class="ed-opts">
                        <a href="#" title="" class="ed-opts-open"><i class="la la-ellipsis-v"></i></a>
                        <ul class="ed-options">
                            <li><a href="#" title="">Edit Post</a></li>
                            <li><a href="#" title="">Unsaved</a></li>
                            <li><a href="#" title="">Unbid</a></li>
                            <li><a href="#" title="">Close</a></li>
                            <li><a href="#" title="">Hide</a></li>
                        </ul>
                    </div>
                </div>
                <div class="epi-sec">
                    <ul class="descp">
                        <!-- <li><img src="{{ asset('assets/frontend/images/icon8.png') }}" alt=""><span>Epic Coder</span></li> -->
                        <li><img src="{{ asset('assets/frontend/images/icon9.png') }}" alt=""><span>India</span></li>
                    </ul>
                    <ul class="bk-links">
                        <li><a href="#" title=""><i class="la la-bookmark"></i></a></li>
                        <li><a href="#" title=""><i class="la la-envelope"></i></a></li>
                    </ul>
                </div>
                <div class="job_descp">
                    <!-- <h3>Senior Wordpress Developer</h3> -->
                    <!-- <ul class="job-dt">
                        <li><a href="#" title="">Full Time</a></li>
                        <li><span>$30 / hr</span></li>
                    </ul> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam luctus hendrerit metus, ut ullamcorper quam finibus at. Etiam id magna sit amet... <a href="#" title="">view more</a></p>-->
                    <div class="row">
                        <div class="col-md-6">
                            <p>{{ isset($post->post_text) ? $post->post_text:'' }}</p>
                        </div>
                    </div>
                    
                    @if(count($getpost_media) > 0)
                    <hr/>
                        <div class="row">
                            @foreach($getpost_media as $media)
                                <div class="col-md-6" style="margin-bottom:10px">
                                    @if($media->type == "image")
                                        <img src="{{asset('public/uploads/posts/'.$media->post_media)}}" height="150" width="150"> 
                                    @endif
                                    @if($media->type == "video")
                                    <video width="100%" height="150px" controls>
                                        <source src="{{asset('public/uploads/posts/'.$media->post_media)}}" type='video/mp4'>
                                    </video>
                                    @endif
                                    @if($media->type == "document")
                                    <a href="{{asset('public/uploads/posts/'.$media->post_media)}}" target='_blank'>{{$media->post_media}}</a>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    @endif
                    
                    
                    
                    <!-- <ul class="skill-tags">
                        <li><a href="#" title="">HTML</a></li>
                        <li><a href="#" title="">PHP</a></li>
                        <li><a href="#" title="">CSS</a></li>
                        <li><a href="#" title="">Javascript</a></li>
                        <li><a href="#" title="">Wordpress</a></li> 	
                    </ul> -->
                </div>
                <div class="job-status-bar">
                    <ul class="like-com">
                        <li>
                            <a href="#"><i class="fas fa-heart"></i> Like</a>
                            <img src="{{ asset('assets/frontend/images/liked-img.png') }}" alt="">
                            <span>25</span>
                        </li> 
                        <li><a href="#" class="com"><i class="fas fa-comment-alt"></i> Comment 15</a></li>
                    </ul>
                    <a href="#"><i class="fas fa-eye"></i>Views 50</a>
                </div>
            </div><!--post-bar end-->
        @endforeach
    @endif
    