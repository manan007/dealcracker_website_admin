


<div class="toppart">
    <div class="item col1">
        <div class="profileimg">
        @if(Auth::user()->profile != null)
            <img src="{{ url('public/upload/profile/'.Auth::user()->profile) }}" class="borderradius" alt="" width="50" height="50">
        @else
            <img src="{{ asset('frontend/images/user.jpg')}}" alt="" class="borderradius" width="50" height="50">
        @endif
        </div>
    </div>
    <div class="item col2">
        <div class="lockup_title ">
            <h3>{{ Auth::user()->name ?? '' }}</h3>
            <!-- <a href="#" data-toggle="modal" data-target="#visibility"><i class="fas fa-globe-americas"></i> Anyone</a> -->
        </div>
    </div>
</div>
<div class="description">
    <!-- <textarea class="aboutpost" placeholder="What do you want to talk about?" name="post_text_video" id="post_text_video"></textarea> -->
    <input type="text" id="post_title_video" name="post_title_video" class="form-control post_title_video" placeholder="Title">
    <br/>
    <div id="post_text_video" class="aboutpost post_text_video" contenteditable="true" data-placeholder="Enter Description"></div>
    <br/>

</div>
<div class="media_part">

@if(count($getpost_media) > 0)

        <div class="row">
            <div class="col-md-12 text-right pr-0">
                <a href='javascript:;' class='delete_video' data-name="{{ $post_video_media }}" data-value="{{ $post_video_media_name }}"><i class='fa fa-close text-danger' aria-hidden='true'></i></a>
            </div> 
        </div>             

        @if(count($getpost_media) == 1)
            @foreach($getpost_media as $media)
                <div class="gallerybox">						
                    <div class="col12">
                        <div class="imgbox">
                            <video width="100%" height="250px" controls>
                                <source src="{{asset('upload/post/'.$media)}}" type='video/mp4' alt="First slide" data-slide-to="0">
                            </video>
                            <!--<img class="w-100" src="{{asset('public/uploads/posts/'.$media)}}" alt="First slide" data-slide-to="0">-->
                        </div>
                    </div>
                </div>
            @endforeach
        @elseif(count($getpost_media) == 2)
            @foreach($getpost_media as $media)
                <div class="gallerybox">
                    <div class="col2">
                        <div class="imgbox">
                            <video width="100%" height="250px" controls>
                                <source src="{{asset('upload/post/'.$media)}}" type='video/mp4' alt="First slide" data-slide-to="0">
                            </video>
                        </div>
                    </div>
                </div>
            @endforeach
        @elseif(count($getpost_media) == 3)
            @foreach($getpost_media as $key => $media)
                <div class="gallerybox">
                    @if($key == 0)						
                    <div class="col12">
                        <div class="imgbox">
                            <video width="100%" height="250px" controls>
                                <source src="{{asset('upload/post/'.$media)}}" type='video/mp4' alt="First slide" data-slide-to="0">
                            </video>
                        </div>
                    </div>
                    @else
                    <div class="col2">
                        <div class="imgbox">
                            <video width="100%" height="250px" controls>
                                <source src="{{asset('upload/post/'.$media)}}" type='video/mp4' alt="First slide" data-slide-to="0">
                            </video>
                        </div>
                    </div>
                    @endif
                </div>
            @endforeach
        @elseif(count($getpost_media) == 4)
            @foreach($getpost_media as $media)
                <div class="gallerybox">
                    <div class="col2">
                        <div class="imgbox">
                            <video width="100%" height="250px" controls>
                                <source src="{{asset('upload/post/'.$media)}}" type='video/mp4' alt="First slide" data-slide-to="0">
                            </video>
                        </div>
                    </div>
                </div>
            @endforeach
        @elseif(count($getpost_media) == 5)
            @foreach($getpost_media as $key => $media)
                <div class="gallerybox">
                    @if($key == 0 || $key == 1)						
                    <div class="col2">
                        <div class="imgbox">
                            <video width="100%" height="250px" controls>
                                <source src="{{asset('upload/post/'.$media)}}" type='video/mp4' alt="First slide" data-slide-to="0">
                            </video>
                        </div>
                    </div>
                    @else
                    <div class="col1">
                        <div class="imgbox">
                            <video width="100%" height="250px" controls>
                                <source src="{{asset('upload/post/'.$media)}}" type='video/mp4' alt="First slide" data-slide-to="0">
                            </video>
                        </div>
                    </div>
                    @endif
                </div>
            @endforeach
        @endif
    @endif
</div>

<script type="text/javascript">

</script>