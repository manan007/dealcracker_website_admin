<div class="modal fade sartpost" id="share_news_feed" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Share News Feed</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			
			<div class="modal-body">
				<div id="share_news_feed_data"></div>
			</div>
			<div class="modal-footer">
			    <div class="left-part" id="model_custom_footer">
			        <ul>
			        
			            <li><a href="javascript:;" class="subbox" style="cursor: not-allowed;"><i class="far fa-image"></i></a></li>
			            <li><a href="javascript:;" class="subbox" style="cursor: not-allowed;"><i class="fab fa-youtube"></i></a></li>
			            <!-- <li><a href="javascript:;" class="subbox" style="cursor: not-allowed;"><i class="far fa-file-alt"></i></a></li> -->
			        
			        </ul>
			    </div>
			    <div class="right-part">
			        <a href="javascript:;" type="button" class="update_post">Post</a>
			    </div>
			</div>
			
		</div>
	</div>
</div>


