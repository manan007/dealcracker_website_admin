
@if(isset($user_detail))
	@php $user_details = $user_detail->overview; @endphp
@else
	@php $user_details = Auth::user()->overview; @endphp
@endif
@if($user_details)
	<p style="word-wrap: break-word;">{{Str::limit($user_details,65) }}  @if(Request::Route()->getName() != 'user.home')<a href="javascript:;" title="Edit Bio"><i class="fa fa-pencil edit_overview cursorPointer text-dark pl-1" data-slug="Edit"></i></a>@endif</p>
@else
	@if(Request::Route()->getName() != 'user.home')<a href="javascript:;" title="Add Bio" data-slug="Add" class="edit_overview cursorPointer">Add Bio</a>@endif
@endif

{{--@if(isset($user_detail))
	@php $user_details = $user_detail->overview; @endphp
@else
	@php $user_details = Auth::user()->overview; @endphp
@endif
<p style="word-wrap: break-word;" class="pt-1">{{Str::limit($user_details,65) }}</p>--}}