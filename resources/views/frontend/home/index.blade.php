@extends('frontend.layouts.main')
@section('title','Dashboard')
@section('css')

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">

<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.carousel.min.css'>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.theme.default.min.css'>
<style>

.borderradius{
	border-radius:50%;
}
.custom-input-text{
display: none;
}

</style>

<style>
#sync1 .item {
  background: #0c83e7;
  padding: 0px 0px;
  margin: 5px;
  color: #FFF;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  text-align: center;
}

#sync2 .item {
  background: #C9C9C9;
  padding: 10px 0px;
  margin: 5px;
  color: #FFF;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  text-align: center;
  cursor: pointer;
}
#sync2 .item h1 {
  font-size: 18px;
}
#sync2 .current .item {
  background: #0c83e7;
}

.owl-theme .owl-nav {
  /*default owl-theme theme reset .disabled:hover links */
}
.owl-theme .owl-nav [class*='owl-'] {
  transition: all .3s ease;
}
.owl-theme .owl-nav [class*='owl-'].disabled:hover {
  background-color: #D6D6D6;
}

#sync1.owl-theme {
  position: relative;
}
#sync1.owl-theme .owl-next, #sync1.owl-theme .owl-prev {
  width: 22px;
  height: 40px;
  margin-top: -20px;
  position: absolute;
  top: 50%;
}
#sync1.owl-theme .owl-prev {
  left: 10px;
}
#sync1.owl-theme .owl-next {
  right: 10px;
}
.ui-front {
    z-index: 2000 !important;
}

.aboutpost:empty:before {
    content: attr(data-placeholder);
    color:#999;
}
.form-control:-ms-input-placeholder{
  color:#999;
}
.form-control::-webkit-input-placeholder{
  color:#999;
}

/*.aboutpost:-ms-input-placeholder{
  color:#999
}*/

[contenteditable="true"]:active,
[contenteditable="true"]:focus{
	border: 1px solid #ced4da;
    border-radius: 0.25rem;
    color: #495057!important;
    padding: 0.375rem 0.75rem!important;
  /*border:1px solid #666666!important;*/
  outline:none!important;
  padding: 5px;
}
[contenteditable="true"]{
	border: 1px solid #ced4da;
    border-radius: 0.25rem;
    color: #495057!important;
    padding: 0.375rem 0.75rem!important;
  /*border:1px solid #666666!important;*/
  padding: 5px;
}

</style>
<style>
	.borderradius{
		border-radius:50%;
	}
	.custom-input-text{
	display: none;
	}

	.usr-pic > a > img {
		float: none;
		border: 5px solid #fff;
		-webkit-border-radius: 100px;
		-moz-border-radius: 100px;
		-ms-border-radius: 100px;
		-o-border-radius: 100px;
		border-radius: 100px;
		width: 100%;
	}

	.messenger-listView{
		width:100%!important;
	}



	#app {
	display: flex;
	flex-wrap: wrap;
	justify-content: space-around;
	margin: 30px 0;
	width: 100%;
	}

	#chart {
		
	/* box-shadow: 0 2px 8px rgba(0, 0, 0, 0.26); */
	padding: 1rem;
	}

	#data {
	display: flex;
	align-items: center;
	margin-top: 20px;
	}

	#data ul {
	list-style: none;
	margin: 0;
	padding: 0;
	}

	#data li {
	margin-bottom: 1rem;
	padding: 1rem;
	box-shadow: 0 2px 8px rgba(0, 0, 0, 0.26);
	width: 10rem;
	display: flex;
	justify-content: space-between;
	align-items: center;
	font-weight: bold;
	}

	.bar {
	fill: #30649c;
	}

	.label {
	fill: #30649c;
	}

	/* .domain{
		display:none;
	}
	.tick{
		display:none;
	} */

	.chat1_select{
		width:190px;
		margin-top:20px;
		margin-left:20px;
		margin-bottom:20px;
	}
	.chat1_select_btn{
		width:190px;
		/* margin-top:20px; */
		margin-left:20px;
		margin-bottom:20px;
	}
	rect.bar:first-child{
		fill:#333!important;
	}

	#chart1_first{
		fill:#1E2C75;
	}
	.card-chart{
		background-color:#fff;
	}

	#uploadForm {border-top:#F0F0F0 2px solid;background:transparent;padding:10px;}
	#uploadForm label {margin:2px; font-size:1em; font-weight:bold;}
	.demoInputBox{padding:5px; border:#F0F0F0 1px solid; border-radius:4px; background-color:#FFF;}
	#progress-bar {background-color: #1E2C75;height:20px;color: #FFFFFF;width:0%;-webkit-transition: width .3s;-moz-transition: width .3s;transition: width .3s;}
	.btnSubmit{background-color:#09f;border:0;padding:10px 40px;color:#FFF;border:#F0F0F0 1px solid; border-radius:4px;}
	#progress-div {border:#1E2C75 1px solid;padding: 5px 0px;margin:30px 0px;border-radius:4px;text-align:center;}
	#targetLayer{width:100%;text-align:center;}

	.viewtextpost{word-break: break-all;}
	.hashtag_box{
		font-weight: bold;
	    border-radius: 15px;
	    font-size: 46px!important;
	    background-color: #1E2C75!important;
	    color: #fff;
	    padding:2px 24px;
	    border-radius: 10px; 
	}
	.fa-redo{
		color: #b2b2b2!important;
	}

	.menu {
      background-color: #f2f2f2;
      position: fixed!important;
      max-height: 203px;
      overflow: auto;
      z-index: 2000 !important;
      width: 140px;
    }

    .menu-item {
      cursor: default;
      padding: 1rem;
    }

    .menu-item.selected {
      background-color: #1E2C75;
      color: #fff;
    }

    .menu-item:hover:not(.selected) {
      background-color: #1E2C75!important;
      color: #fff;
    }

    .testimonialdoc{
    	height: 400px;
    	width: 100%;
    	margin-bottom: 16px;
    }

</style>

@endsection

@section('content')


@include('frontend.home.dynamic_like_form')
@include('frontend.home.dynamic_post_form')
@include('frontend.home.dynamic_post_video_form')
@include('frontend.home.dynamic_edit_post_form')

@include('frontend.home.dynamic_upload_form')
@include('frontend.home.dynamic_upload_video_form')
@include('frontend.home.dynamic_follow_plan_form')
@include('frontend.home.dynamic_comment_like_form')
@include('frontend.home.dynamic_share_news_feed_form')

@php

	$get_testimonials = App\Models\Testimonial::all();
	$get_banners = App\Models\Banner::all();

@endphp

		<main>
			<div class="main-section">
				<div class="container">
					<div class="main-section-data">

						@if(count($get_banners) > 0)
						<div class="row mb-4">
							<div class="col-lg-12 col-md-12 pd-right-none pd-left-none no-pd pb-10">
								@foreach($get_banners as $key => $get_banner)
								@if($key == 0)
								<img class="d-block w-100" style="max-height:400px;height: auto!important;" src="{{ asset('upload/app_banner/'.$get_banner->app_banner) }}">
								@endif
								@endforeach

							</div>
						</div>
						@endif


						{{-- 
						<div class="widget suggestions full-width">
							<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
							    <ol class="carousel-indicators">
							    		@foreach($get_banners as $key_slider => $get_banner)
							        		<li data-target="#carouselExampleIndicators" data-slide-to="{{ $key_slider }}" @if($key_slider == 0) active @endif></li>
							        	@endforeach
							    </ol>
							    <div class="carousel-inner" style="max-height:400px;height: auto!important;">
							        @foreach($get_banners as $key => $get_banner)
							        <div class="carousel-item @if($key == 0) active @endif">
							            <img class="d-block w-100" src="{{ asset('upload/app_banner/'.$get_banner->app_banner) }}">
							        </div>
							        @endforeach
							    </div>
							    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
							        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
							        <span class="sr-only">Previous</span>
							    </a>
							    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
							        <span class="carousel-control-next-icon" aria-hidden="true"></span>
							        <span class="sr-only">Next</span>
							    </a>
							</div>
						</div> --}}

						<div class="row">
							<div class="col-lg-3 col-md-4 pd-left-none no-pd">
								<div class="main-left-sidebar no-margin">
									<div class="user-data full-width">
										<div class="user-profile">
											<div class="username-dt">
												<div class="usr-pic">
													@if(Auth::user()->profile != null)
						                            	<a href="{{ route('front.my_profile.index',Auth::user()->id) }}"><img src="{{ asset('upload/profile/'.Auth::user()->profile)}}" alt="" width="100" height="100"></a>
						                          	@else
						                            	<a href="{{ route('front.my_profile.index',Auth::user()->id) }}"><img src="{{ asset('frontend/images/user.jpg')}}" alt="" width="100" height="100"></a>
						                          	@endif
												</div>
											</div><!--username-dt end-->
											<div class="user-specs">
												<h3>{{ Auth::user()->name ?? '' }}</h3>
												<span>{{ Auth::user()->profile_description ?? ''  }}</span>
											</div>
										</div><!--user-profile end-->
										@php 
						        	$profile_likes = App\Models\ProfileLike::where(['friend_id' => Auth::user()->id,'status' => 1])->count();
            						$profile_dislikes = App\Models\ProfileLike::where(['friend_id' => Auth::user()->id,'status' => 2])->count();
						       	@endphp
										<ul class="user-fw-status">
											<li>
												<h4>Likes</h4>
												<span>{{ $profile_likes }}</span>
											</li>
											<li>
												<h4>Dislikes</h4>
												<span>{{ $profile_dislikes }}</span>
											</li>
											<li>
												<a href="{{ route('front.my_profile.index',Auth::user()->id) }}" title="">View Profile</a>
											</li>
										</ul>
									</div><!--user-data end-->

									{{--
									<div class="suggestions full-width">
										<div class="sd-title">
											<h3>Suggestions</h3>
											<i class="la la-ellipsis-v"></i>
										</div>
										<div class="suggestions-list">
											<div class="suggestion-usd">
												<img src="{{ asset('frontend/images/resources/s1.png')}}" alt="">
												<div class="sgt-text">
													<h4>Jessica William</h4>
													<span>Graphic Designer</span>
												</div>
												<span><i class="la la-plus"></i></span>
											</div>
											<div class="suggestion-usd">
												<img src="{{ asset('frontend/images/resources/s2.png')}}" alt="">
												<div class="sgt-text">
													<h4>John Doe</h4>
													<span>PHP Developer</span>
												</div>
												<span><i class="la la-plus"></i></span>
											</div>
											<div class="suggestion-usd">
												<img src="{{ asset('frontend/images/resources/s3.png')}}" alt="">
												<div class="sgt-text">
													<h4>Poonam</h4>
													<span>Wordpress Developer</span>
												</div>
												<span><i class="la la-plus"></i></span>
											</div>
											<div class="suggestion-usd">
												<img src="{{ asset('frontend/images/resources/s4.png')}}" alt="">
												<div class="sgt-text">
													<h4>Bill Gates</h4>
													<span>C & C++ Developer</span>
												</div>
												<span><i class="la la-plus"></i></span>
											</div>
											<div class="suggestion-usd">
												<img src="{{ asset('frontend/images/resources/s5.png')}}" alt="">
												<div class="sgt-text">
													<h4>Jessica William</h4>
													<span>Graphic Designer</span>
												</div>
												<span><i class="la la-plus"></i></span>
											</div>
											<div class="suggestion-usd">
												<img src="{{ asset('frontend/images/resources/s6.png')}}" alt="">
												<div class="sgt-text">
													<h4>John Doe</h4>
													<span>PHP Developer</span>
												</div>
												<span><i class="la la-plus"></i></span>
											</div>
											<div class="view-more">
												<a href="#" title="">View More</a>
											</div>
										</div><!--suggestions-list end-->
									</div><!--suggestions end-->
									--}}

									<div class="tags-sec full-width">
										<ul>
											<li><a href="{{ route('home.about_us') }}" title="About Us">About Us</a></li>
											<li><a href="{{ route('home.contact_us') }}" title="Contact Us">Contact Us</a></li>
											<li><a href="{{ route('home.privacy_policy') }}" title="Privacy Policy">Privacy Policy</a></li>
											<li><a href="{{ route('home.terms_conditions') }}" title="Terms and Conditions">Terms and Conditions</a></li>	<li><a href="{{ route('home.refund_policy') }}" title="Refund and Cancellation">Refund and Cancellation</a></li>	
							

										</ul>
										<div class="cp-sec ccp-sec">
											<p><img src="{{ asset('frontend/images/cp.png')}}" alt="">Copyright {{ date('Y') }}</p>
										</div>
									</div><!--tags-sec end-->
								</div><!--main-left-sidebar end-->
							</div>
							<div class="col-lg-6 col-md-8 no-pd">
								<div class="main-ws-sec">


									<!-- <div class="post-topbar">
										<div class="user-picy">
											<img src="{{ asset('frontend/images/resources/user-pic.png')}}" alt="">
										</div>
										<div class="post-st">
											<ul>
												<li><a class="post_project" href="#" title="">Pending Schools</a></li>
												<li><a class="post-jb active" href="#" title="">Pending Applications</a></li>
											</ul>
										</div>
									</div> -->


									<div class="post-topbar start-post">
										<div class="user-picy">
											@if(Auth::user()->profile != null)
												<a href="{{ route('front.my_profile.index',Auth::user()->id) }}"><img src="{{ asset('upload/profile/'.Auth::user()->profile)}}" alt="" class="borderradius" width="50" height="50"></a>
				                            @else
				                                <a href="{{ route('front.my_profile.index',Auth::user()->id) }}"><img src="{{ asset('frontend/images/user.jpg')}}" alt="" width="50" class="borderradius" height="50"></a>
				                            @endif
										</div>
										<div class="post-st">
											<!--<a href="#" class="btn" data-toggle="modal" data-target="#start-post">Start a post</a>-->
											<a href="javascript:;" class="btn start_post_model">Start a Feed</a>
										</div>
										<ul class="post-icon">
											<!-- <li><a href="#" data-toggle="modal" data-target="#upload-photo-text"><i class="far fa-image c-blue upload-photo-btn"></i> Photo</a></li>
												<li><a href="#" data-toggle="modal" data-target="#upload-photo-text"><i class="fab fa-youtube c-green"></i> Video</a></li> -->
											<!-- <li><a href="#" data-toggle="modal" data-target="#document-post"><i class="far fa-file-alt c-orange"></i> Add a document</a></li> -->
											<li><a href="javascript:;" class="btn start_post_model" style="border:0px;text-align: center;background-color: #ebebeb;"><i class="fa fa-wpforms c-blue"></i>Feed</a></li>
											<!-- <li><a href="javascript:;" class="upload-photo-btn"><i class="far fa-image c-blue"></i> Photo</a></li>
											<li><a href="javascript:;" class="upload-video-btn"><i class="fab fa-youtube c-green"></i> Video</a></li> -->
										</ul>
									</div>

									<div class="dynamic_post">
										@include('frontend.home.dynamic_post')
									</div>

									<!-- <div class="posts-section">


									</div> --><!--posts-section end-->


								</div><!--main-ws-sec end-->
							</div>

							<div class="col-lg-3 pd-right-none no-pd">
								{{--
								<div class="right-sidebar">
										<div class="widget suggestions full-width">

											@if(count($get_testimonials) > 0)
											
											<div id="testimonial4" class="carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x" data-ride="carousel" data-pause="hover" data-interval="5000" data-duration="2000">
		             				<div class="carousel-inner" role="listbox">	
		             					@foreach($get_testimonials as $key => $get_testimonial)
					                    <div class="carousel-item @if($key == 0) active @endif">
					                        <div class="testimonial4_slide">
					                        	@if($get_testimonial->document_type == "video")
					                        	<iframe src="{{asset('upload/testimonial/'.$get_testimonial->document)}}" title="description" class="testimonialdoc" style="height: 400px;width: 100%;"></iframe>
					                        	@else
					                        	<img src="{{ asset('upload/testimonial/'.$get_testimonial->document) }}" class="img-circle img-responsive testimonialdoc" />
					                        	@endif
					                            <!-- <img src="https://i.ibb.co/8x9xK4H/team.jpg" class="img-circle img-responsive" /> -->
					                            <h2 class="h3 text-center p-2">{{ $get_testimonial->title ?? '' }}</h2>
					                        </div>
					                    </div>
					                @endforeach

					                	<a class="carousel-control-prev" href="#testimonial4" data-slide="prev">
					                    <span class="carousel-control-prev-icon"></span>
					                </a>
					                <a class="carousel-control-next" href="#testimonial4" data-slide="next">
					                    <span class="carousel-control-next-icon"></span>
					                </a>
		            			</div>
		            			@endif

            				</div>

									


								</div><!--right-sidebar end-->
							</div>
							--}}
						</div>
					</div><!-- main-section-data end-->
				</div> 
			</div>
		</main>




		<div class="post-popup pst-pj">
			<div class="post-project">
				<h3>Post a project</h3>
				<div class="post-project-fields">
					<form>
						<div class="row">
							<div class="col-lg-12">
								<input type="text" name="title" placeholder="Title">
							</div>
							<div class="col-lg-12">
								<div class="inp-field">
									<select>
										<option>Category</option>
										<option>Category 1</option>
										<option>Category 2</option>
										<option>Category 3</option>
									</select>
								</div>
							</div>
							<div class="col-lg-12">
								<input type="text" name="skills" placeholder="Skills">
							</div>
							<div class="col-lg-12">
								<div class="price-sec">
									<div class="price-br">
										<input type="text" name="price1" placeholder="Price">
										<i class="la la-dollar"></i>
									</div>
									<span>To</span>
									<div class="price-br">
										<input type="text" name="price1" placeholder="Price">
										<i class="la la-dollar"></i>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<textarea name="description" placeholder="Description"></textarea>
							</div>
							<div class="col-lg-12">
								<ul>
									<li><button class="active" type="submit" value="post">Post</button></li>
									<li><a href="#" title="">Cancel</a></li>
								</ul>
							</div>
						</div>
					</form>
				</div><!--post-project-fields end-->
				<a href="#" title=""><i class="la la-times-circle-o"></i></a>
			</div><!--post-project end-->
		</div><!--post-project-popup end-->

		<div class="post-popup job_post">
			<div class="post-project">
				<h3>Post a job</h3>
				<div class="post-project-fields">
					<form>
						<div class="row">
							<div class="col-lg-12">
								<input type="text" name="title" placeholder="Title">
							</div>
							<div class="col-lg-12">
								<div class="inp-field">
									<select>
										<option>Category</option>
										<option>Category 1</option>
										<option>Category 2</option>
										<option>Category 3</option>
									</select>
								</div>
							</div>
							<div class="col-lg-12">
								<input type="text" name="skills" placeholder="Skills">
							</div>
							<div class="col-lg-6">
								<div class="price-br">
									<input type="text" name="price1" placeholder="Price">
									<i class="la la-dollar"></i>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="inp-field">
									<select>
										<option>Full Time</option>
										<option>Half time</option>
									</select>
								</div>
							</div>
							<div class="col-lg-12">
								<textarea name="description" placeholder="Description"></textarea>
							</div>
							<div class="col-lg-12">
								<ul>
									<li><button class="active" type="submit" value="post">Post</button></li>
									<li><a href="#" title="">Cancel</a></li>
								</ul>
							</div>
						</div>
					</form>
				</div><!--post-project-fields end-->
				<a href="#" title=""><i class="la la-times-circle-o"></i></a>
			</div><!--post-project end-->
		</div><!--post-project-popup end-->
		
		
		<div class="modal fade sartpost uploadphoto" id="upload-photo-text" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h4 class="modal-title" id="myModalLabel">Edit Photo</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		      </div>
		      
		      <div class="modal-body" style="height:auto;max-height:640px;overflow-y:auto;">
		      
					<div class="modal-flex">
					    <div class="inputfile">
					        <!-- <input type="file" name="" class="custom-input" id="selectfile"> -->
					        <!-- <span  for="selectfile">Select images to share</span> -->
					        <input type="file" name="file-5[]" id="file-5" class="custom-input-text file-5" multiple>
					        <label for="file-5"><span>Select images to share</span></label>
					    </div>
					</div>		          
		          
		      </div>
		      <div class="modal-footer">
		        <div class="right-part">
		          <a href="#" class="closebtn" data-dismiss="modal" aria-label="Close">Cancel</a>
		          <a href="javascript:;" type="button" id="done_post_media_text">Done</a>
		        </div>
		      </div>
		      
		    </div>
		  </div>
		</div>

			
			
		</div>

		
<!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
		<!-- CHAT -->
		



@endsection




@section('scripts')
<script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js'></script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/d3/5.16.0/d3.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/d3-scale/1.0.7/d3-scale.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/d3-axis/1.0.10/d3-axis.min.js'></script>

<script src="https://js.pusher.com/5.0/pusher.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"></script>

<script>


$(document).on('click','.share-opts-open',function(){
	var get_postid = $(this).attr('id');
	var get_attribute = $('#share-options-'+get_postid).attr('data-active');
	if(get_attribute == "active"){
		$('#share-options-'+get_postid).removeClass('active');
		$('#share-options-'+get_postid).attr('data-active','inactive');
	}
	else{
		$('#share-options-'+get_postid).addClass('active');
		$('#share-options-'+get_postid).attr('data-active','active');	
	}
});


//------------------------------------  Comment section start ----------------------------------------------------------

$(document).on('click','.replyComment',function(){
	var post_id=$(this).data('post_id');
	var valueCheck=$('.reply_comment_'+post_id).val();
	if(valueCheck == "")
	{
		alert("Please enter proper comment..");
		return false;
	}
	var formData=$('#reply_'+post_id).serialize();

	$.ajax({
		url: "{{route('reply.comment')}}",
		data:formData,
		type: 'GET',
		success: function(data) {
			$('.dynamic_post').html(data.html);
		}
	});	
});


$(document).on('keyup','.replaytext',function(e){

	if(e.keyCode == 13){
		var post_id=$(this).data('post_id');
		var valueCheck=$('.reply_comment_'+post_id).val();
		if(valueCheck == "")
		{
			alert("Please enter proper comment..");
			return false;
		}
		var formData=$('#reply_'+post_id).serialize();

		$.ajax({
			url: "{{route('reply.comment')}}",
			data:formData,
			type: 'GET',
			success: function(data) {
				$('.dynamic_post').html(data.html);
			}
		});
	}
});

$(document).on('click','.editComment',function(){
	var post_id=$(this).data('post_id');
	var valueCheck=$('.edit_comment_'+post_id).val();
	if(valueCheck == "")
	{
		alert("Please enter proper comment..");
		return false;
	}
	var formData=$('#edit_comment_'+post_id).serialize();

	$.ajax({
		url: "{{route('edit.comment')}}",
		data:formData,
		type: 'GET',
		success: function(data) {
			$('.dynamic_post').html('');
			$('.dynamic_post').html(data.html);
		}
	});	
});


$(document).on('keyup','.edit_commenttext',function(e){
	if(e.keyCode == 13){
		var post_id=$(this).data('post_id');
		var valueCheck=$('.edit_comment_'+post_id).val();
		if(valueCheck == "")
		{
			alert("Please enter proper comment..");
			return false;
		}
		var formData=$('#edit_comment_'+post_id).serialize();

		$.ajax({
			url: "{{route('edit.comment')}}",
			data:formData,
			type: 'GET',
			success: function(data) {
				$('.dynamic_post').html(data.html);
			}
		});				
	}

});

		

$(document).on('click','.editCommentEdit',function(){
	var comment_id=$(this).data('id');
	var post_id=$(this).data('post_id');
	var comment=$(this).data('comment');

	$('.edit_comment_'+post_id).val(comment);
	$('.hideEditDiv_'+post_id).css('display','block');
	$('.hideReplyDiv_'+post_id).css('display','none');
	$('.edit_comment_id_'+post_id).val(comment_id);
});

$(document).on('click','.getComment',function(){

	var comment_id=$(this).data('comment_id');
	var post_id=$(this).data('post_id');
	$('.hideEditDiv_'+post_id).css('display','none');
	$('.hideReplyDiv_'+post_id).css('display','block');
	$('.comment_id_'+post_id).val(comment_id);
});

$(document).on('click','.deleteComment',function(){
	var comment_id=$(this).data('id');

	bootbox.dialog({
		message: "Are you sure you want to delete this comment!",
		title: "Delete Comment",
		onEscape: function() {},
		show: true,
		backdrop: true,
		closeButton: true,
		animate: true,
		className: "sartpost",
		buttons: {
		"Danger!": {
			label:"No",
			className: "btn-cancel",
			callback: function() {}
		},
		success: {   
			label: "Yes",
			className: "btn-default1 text-right",
			callback: function() {
				$.ajax({
					url: "{{route('delete.comment')}}",
					data: {
						"comment_id":comment_id
					},
					type: 'GET',
					success: function(data) {
						$('.dynamic_post').html(data.html);
					}
				});	
			},
			
		}
		}
	});
});


$(document).on('click','.com_show',function(){
	var post_id=$(this).data('post_id');
	$('.hideCommentDiv_'+post_id).css('display','block');
	$('.showCommentDiv_'+post_id).css('display','none');
	$('.hideMoreComment_'+post_id).css('display','block');
})

$(document).on('click','.showCommentDiv',function(){
	var post_id=$(this).data('post_id');
	$('.hideCommentDiv_'+post_id).css('display','block');
	$('.showCommentDiv_'+post_id).css('display','none');
	$('.hideMoreComment_'+post_id).css('display','block');
});

$(document).on('click','.hideMoreComment',function(){
	var post_id=$(this).data('post_id');
	$('.hideCommentDiv_'+post_id).css('display','none');
	$('.showCommentDiv_'+post_id).css('display','block');
	$('.hideMoreComment_'+post_id).css('display','none');
});

$(document).on('click','.commentdiv',function(){
	var post_id=$(this).data('id');
	var post_data=$('.commentdiv_'+post_id).val();
	if(post_data == "")
	{
		alert("Please Enter comment...");
		return false;
	}
	$.ajax({
		url: "{{route('store.comment')}}",
		data: {
			"post_id":post_id,
			"post_data":post_data
		},
		type: 'GET',
		success: function(data) {
			$('.dynamic_post').html(data.html);
		}
	});
});

$(document).on('keyup','.commenttext',function(e){
	if(e.keyCode == 13){
		var post_id=$(this).data('id');
		var post_data=$('.commentdiv_'+post_id).val();
		if(post_data == "")
		{
			alert("Please Enter comment...");
			return false;
		}
		$.ajax({
			url: "{{route('store.comment')}}",
			data: {
				"post_id":post_id,
				"post_data":post_data
			},
			type: 'GET',
			success: function(data) {
				$('.dynamic_post').html(data.html);
			}
		});
	}
});
//------------------------------------  Comment section  Over ---------------------------------------------------------- 


//-----------------------------------  Photo Upload start --------------------------------------------------------------------

$(document).on('change','.custom-input-text',function() {
    var files = $(this)[0].files;
    upload1(files);
});

function syncPosition(el) {
	var count = el.item.count - 1;
	var current = Math.round(el.item.index - (el.item.count / 2) - .5);

	if (current < 0) {
		current = count;
	}
	if (current > count) {
		current = 0;
	}

	//end block

}

function upload1(img) {
    $('.loader').show();
    var form_data = new FormData();
    var imgcount;
    for (imgcount = 0; imgcount < img.length; imgcount++) {
        form_data.append('file[]', img[imgcount]);
    }
    form_data.append('_token', '{{csrf_token()}}');

    $.ajax({
        url: "{{route('ajax.image.post')}}",
        data: form_data,
        type: 'POST',
        contentType: false,
        processData: false,
        success: function(data) {
			$('.loader').hide();
			$('.dynamic_upload_box').html(data.html);
			var owl = $("#sync1");
			owl.owlCarousel({
				items: 1,
				slideSpeed: 2000,
				nav: true,
				autoplay: false, 
				dots: true,
				loop: true,
				responsiveRefreshRate: 200,
				navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
			}).on('changed.owl.carousel', syncPosition);
			$(".custom-input-text").val(null);

        }
    });
}

$("#done_post_media_text").click(function(){
	
	var gethtml_image = $('#gallery_text').html();
	//var gethtml_video = $('#video_gallery_text').html();
	//var	gethtml_document = $('#document_gallery_text').html();

	var post_image_media = $(".imagevalue").map(function()
   	{
       return $(this).val();
   	}).get();

	var post_image_media_name = $(".imagevalue").map(function()
   	{
       return $(this).attr('data-name-value');
   	}).get();
	$('.loader').show();
	$.ajax({
        url: "{{ route('load.post.view') }}",
        data:"post_image_media="+post_image_media+"&post_image_media_name="+post_image_media_name,
        type: 'GET',
        
        success: function(data) {
        		$('.loader').hide();
				$('#upload-photo-text').modal('hide');
				$('#dynamic_post_data_model').html(data.html);
				
				if(data.getmedia_count == 0){
					$('#upload-photo-btn').addClass('upload-photo-btn').css('cursor','pointer');
					$('#upload-video-btn').addClass('upload-video-btn').css('cursor','pointer');
					$('#upload-document-btn').addClass('upload-document-btn').css('cursor','pointer');
				}
				else{
					$('#model_custom_footer').css({'cursor':'not-allowed'});
					$('#upload-photo-btn').removeClass('upload-photo-btn').css({'pointer-events':'none','cursor':'not-allowed'});
					$('#upload-video-btn').removeClass('upload-video-btn').css({'pointer-events':'none','cursor':'not-allowed'});
					$('#upload-document-btn').removeClass('upload-document-btn').css({'pointer-events':'none','cursor':'not-allowed'});
				}
				$('#start-post').modal({show:true});
        }
    });
});


$(document).on('click','#create_post',function(){
	
	var create_post = $('#post_text').html();
	var post_title = $('#post_title').val();

	var post_image_media = $(".imagevalue").map(function()
	{
		return $(this).val();
	}).get();
	var url="{{ route('store.mainpost') }}";

	if(post_title == ''){
		alert('Please enter feed title.');
		return false;
	}
	else{
			$('.loader').show();
			$.ajax({
				url: url,
				headers: {
					'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
				},
				data: {
					'image_name':post_image_media,
					'post_text':create_post,
					'post_title':post_title
				},
				type: 'POST',
				success: function(data) {
					$('.loader').hide();
					$('.dynamic_post').html(data.html);
					$('.modal').modal('hide');
					clearData();
					$('#gallery_text').html('');
					$('#model_custom_footer').css({'cursor':'pointer'});
					$('#upload-photo-btn').addClass('upload-photo-btn').css({'pointer-events':'auto','cursor':'pointer'});
					$('#upload-video-btn').addClass('upload-video-btn').css({'pointer-events':'auto','cursor':'pointer'});
					$('#upload-document-btn').addClass('upload-document-btn').css({'pointer-events':'auto','cursor':'pointer'});
				}
			});	
	}

});

function removeTags(str) {
	if ((str===null) || (str===''))
    	return false;
	else
    str = str.toString();
	return str.replace( /(<([^>]+)>)/ig, '');
}

	
//-----------------------------------  Photo Upload over --------------------------------------------------------------------


//-----------------------------------  Video Upload start --------------------------------------------------------------------

$(document).ready(function() { 
	$('.right-done').css('display','none');
	
	$('#uploadForm').submit(function(e) {	
		// alert('asdf');
		if($('#file_video').val()) {
			e.preventDefault();
			$('#loader-icon').show();
			$(this).ajaxSubmit({ 
				target:   '#targetLayer', 
				beforeSubmit: function() {
					$("#progress-bar").width('0%');
				},
				uploadProgress: function (event, position, total, percentComplete){	
					$("#progress-bar").width(percentComplete + '%');
					$("#progress-bar").html('<div id="progress-status">' + percentComplete +' %</div>')
				},
				success:function (data){

					$('#video_name').val(data.video);
					$('#video_name').attr('data-name-value',data.name_value);
					
					//$('.dynamic_upload_video_box').html(data.html);
					$('.right-submit').css('display','none');
					$('.right-done').css('display','block');
					
				},
				resetForm: true 
			}); 
			return false; 
		}
	});
});

$(document).on('change','.custom-input-text-video',function() {
    var files = $(this)[0].files;
    upload_video(files);
});

function upload_video(img) {
    $('.loader').show();
    var form_data = new FormData();
    var imgcount;
    for (imgcount = 0; imgcount < img.length; imgcount++) {
        form_data.append('file[]', img[imgcount]);
    }
    form_data.append('_token', '{{csrf_token()}}');

    $.ajax({
        url: "{{route('ajax.video.post')}}",
        data: form_data,
        type: 'POST',
        contentType: false,
        processData: false,
        success: function(data) {

			if(data.status == 0){
				$('.loader').hide();
				$('.videoerror').html('');
				$('.videoerror').html('<span class="text-danger">'+data.msg+'</span>');
			}
			else{
				$('.videoerror').html('');
				$('.loader').hide();
				$('.dynamic_upload_video_box').html(data.html);
				var owl = $("#sync1");
				owl.owlCarousel({
					items: 1,
					slideSpeed: 2000,
					nav: true,
					autoplay: false, 
					dots: true,
					loop: true,
					responsiveRefreshRate: 200,
					navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
				}).on('changed.owl.carousel', syncPosition);
				$(".custom-input-text").val(null);
			}

        }
    });
}

$("#done_post_media_video_text").click(function(){
	
	var gethtml_video = $('#video_gallery_text').html();

	var post_video_media = $(".videovalue").map(function()
   	{
       return $(this).val();
   	}).get();

	var post_video_media_name = $(".videovalue").map(function()
   	{
       return $(this).attr('data-name-value');
   	}).get();
	$('.loader').show();
	$.ajax({
        url: "{{ route('load.post.video.view') }}",
        data:"post_video_media="+post_video_media+"&post_video_media_name="+post_video_media_name,
        type: 'GET',
        
        success: function(data) {
        	$('.loader').hide();
			$('#upload-video-text').modal('hide');
			$('#dynamic_post_video_data_model').html(data.html);
			if(data.getmedia_count == 0){
				$('#upload-photo-btn_video').addClass('upload-photo-btn_video').css('cursor','pointer');
				$('#upload-video-btn_video').addClass('upload-video-btn_video').css('cursor','pointer');
				$('#upload-document-btn_video').addClass('upload-document-btn_video').css('cursor','pointer');
			}
			else{
				$('#model_custom_video_footer').css({'cursor':'not-allowed'});
				$('#upload-photo-btn_video').removeClass('upload-photo-btn_video').css({'pointer-events':'none','cursor':'not-allowed'});
				$('#upload-video-btn_video').removeClass('upload-video-btn_video').css({'pointer-events':'none','cursor':'not-allowed'});
				$('#upload-document-btn_video').removeClass('upload-document-btn_video').css({'pointer-events':'none','cursor':'not-allowed'});
			}
			$('#start-video-post').modal({show:true});
        }
    });
	
});

$(document).on('click','#create_video_post',function(){

	var create_post_video = $('#post_text_video').html();
	var post_title_video = $('#post_title_video').val();

	var post_video_media = $(".videovalue").map(function()
	{
		return $(this).val();
	}).get();
		
	var url="{{ route('store.mainpost') }}";
	$('.loader').show();
	$.ajax({
		url: url,
		headers: {
			'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
		},
		data: {
			'video_name':post_video_media,
			'post_text':create_post_video,
			'post_title':post_title_video,
		},
		type: 'POST',
		success: function(data) {
			$('.loader').hide();
			$('.dynamic_post').html(data.html);
			$('.modal').modal('hide');
			clearData();
			$('#video_gallery_text').html('');
			$('#model_custom_video_footer').css({'cursor':'pointer'});
			$('#upload-photo-btn_video').addClass('upload-photo-btn_video').css({'pointer-events':'auto','cursor':'pointer'});
			$('#upload-video-btn_video').addClass('upload-video-btn_video').css({'pointer-events':'auto','cursor':'pointer'});
			$('#upload-document-btn_video').addClass('upload-document-btn_video').css({'pointer-events':'auto','cursor':'pointer'});
		}
	});
});


//-----------------------------------  Video Upload start --------------------------------------------------------------------

//---------------start post-------------------------------------------------------------------------------------------------------

$(document).on('click','.start_post_model',function(){
	var post_image_media = "null";
	var post_image_media_name = "null";

	$.ajax({
        url: "{{ route('load.post.view') }}",
        data:"post_image_media="+post_image_media+"&post_image_media_name="+post_image_media_name,
        type: 'GET',
		success: function(data) {
			
			$('#dynamic_post_data_model').html(data.html);
			$('#start-post').modal({show:true});
		},
	});
	
});


$(document).on('hidden.bs.modal','#start-post', function () {
	$('#model_custom_footer').css({'cursor':'pointer'});
	$('#upload-photo-btn').addClass('upload-photo-btn').css({'pointer-events':'auto','cursor':'pointer'});
	$('#upload-video-btn').addClass('upload-video-btn').css({'pointer-events':'auto','cursor':'pointer'});
	$('#upload-document-btn').addClass('upload-document-btn').css({'pointer-events':'auto','cursor':'pointer'});
});

/*-------- Delete media --------*/

$(document).on('click','.delete_img',function(){
	$('.imagevalue').val('');
	var value=$(this).data('value');
	var name=$(this).data('name');
	deleteMedia(value,name)
});

$(document).on('click','.delete_video',function(){
	$('.videovalue').val('');
	var value=$(this).data('value');
	var name=$(this).data('name');
	deleteMedia(value,name)
});

$(document).on('click','.delete_document',function(){
	var value=$(this).data('value');
	var name=$(this).data('name');
	deleteMedia(value,name)
});

function deleteMedia(value,name){
	$.ajax({
		type: "GET",
		url: "{{route('delete_image')}}",
		data: "image="+value+"&name="+name,
		success: function(result) {
			$.each(result.name, function(index, value){

				var blank_arr=[];
				$('#imgDiv_'+value).html('');
				$('#imgDiv_'+value).css('display','none');
				$('#file-5').val(blank_arr);
				$('#gallery_text').html('');
				$('.media_part').html('');

			});
			if(result.getmedia_count == 0){
				$('#model_custom_footer').css({'cursor':'pointer'});
				$('#upload-photo-btn').addClass('upload-photo-btn').css({'pointer-events':'auto','cursor':'pointer'});
				$('#upload-video-btn').addClass('upload-video-btn').css({'pointer-events':'auto','cursor':'pointer'});
				$('#upload-document-btn').addClass('upload-document-btn').css({'pointer-events':'auto','cursor':'pointer'});
			}
			else{
				$('#model_custom_footer').css({'cursor':'not-allowed'});
				$('#upload-photo-btn').removeClass('upload-photo-btn').css({'pointer-events':'none','cursor':'not-allowed'});
				$('#upload-video-btn').removeClass('upload-video-btn').css({'pointer-events':'none','cursor':'not-allowed'});
				$('#upload-document-btn').removeClass('upload-document-btn').css({'pointer-events':'none','cursor':'not-allowed'});
			}
			$('.media_part').html('');
			$('#dynamic_post_data_model').html('result.html');
			$('#dynamic_post_data_model').html(result.html);
		}
	});
}
/*---------------------------------------------------------------*/


$(document).on("click",".visibility-body li",function(){
	$(this).find('.inputradio').attr('checked', 'checked');
});
$(document).on("click",'.lockup_title a',function(){
	$('.visibilitysection').css('display','block')
});
$(document).on("click",'.backbtt',function(){
	$('.visibilitysection').css('display','none')
});
$(document).on("click",'.subbox',function(){
	$('.mainpost-box').modal('hide')
});

$(function(){
	$('.chatbox-top').click(function(){
		$(this).closest('.chatbox').toggleClass('chatbox-min');
	});
	$('.fa-close').click(function(){
		$(this).closest('.chatbox').hide();
	});
});
function clearData()
{
	$('#post_text').val("");
	$('#create_post_form_video').trigger('reset');
	$('#create_post_form_document').trigger('reset');
	$('#create_post_form_media').trigger('reset');
	$('#create_post_form').trigger('reset');
	$('.post_media').html("");
}

	
$(document).on('click','#create_post_video',function(){
	var create_post = $('.videovalue').val();
	if(create_post){
		//$('#create_post_form_video').submit();
		var formData=$('#create_post_form_video').serialize();
		
		var url="{{ route('store.post') }}";

		$.ajax({
            url: url,
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			},
            data: formData,
            type: 'POST',
            
            success: function(data) {
                $('.dynamic_post').html(data.html);
				$('.modal').modal('hide');
				clearData();
            }
        });
	}
})

$(document).on('click','#create_post_document',function(){
	var create_post = $('.documentvalue').val();
	if(create_post){
		//$('#create_post_form_document').submit();
		var formData=$('#create_post_form_document').serialize();
		var url="{{ route('store.post') }}";
		

		$.ajax({
            url: url,
            data: formData,
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			},
            type: 'POST',
            
            success: function(data) {
                $('.dynamic_post').html(data.html);
				$('.modal').modal('hide');
				clearData();
            }
        });
		
	}
})

$(document).on('click','#create_post_media',function(){
	var create_post = $('.imagevalue').val();
	if(create_post){
		// $('#create_post_form_media').submit();
		var formData=$('#create_post_form_media').serialize();
		var url="{{ route('store.post') }}";
		

		$.ajax({
            url: url,
            data: formData,
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			},
            type: 'POST',
            
            success: function(data) {
                $('.dynamic_post').html(data.html);
				$('.modal').modal('hide');
				clearData();
            }
        });
	}
})

	


$(document).on('click','.delete_post',function(){
	
	//var post_id=$('#create_post_form').serialize();
	var post_id = $(this).attr('data-post-id');
	var url="{{ route('delete.post') }}";

	bootbox.dialog({
		message: "Are you sure you want to delete this feed ?",
		title: "Delete Feed",
		onEscape: function() {},
		show: true,
		backdrop: true,
		closeButton: true,
		animate: true,
		className: "sartpost",
		buttons: {
		"Danger!": {
			label:"No",
			className: "btn-cancel",
			callback: function() {}
		},
		success: {   
			label: "Yes",
			className: "btn-default1 text-right",
			callback: function() {
				$.ajax({
					url: url,
					headers: {
						'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
					},
					data: {
						"post_id":post_id,		
					},
					type: 'POST',
					success: function(data) {
						$('.dynamic_post').html(data.html);
						$('.modal').modal('hide');
						clearData();
					}
				});
			},
			
		}
		}
	});

});


$(document).on('click','.create_like',function(){
	
	var post_id = $(this).attr('data-post-id');
	var url="{{ route('like.post') }}";
	$.ajax({
		url: url,
		headers: {
			'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
		},
		data: {
			"post_id":post_id,
		},
		type: 'POST',
		success: function(data) {
			$('.dynamic_post').html(data.html);
			$('.modal').modal('hide');
			clearData();
		}
	});
	
});


$(document).on('click','.create_comment_like',function(){
	
	var post_id = $(this).attr('data-post-id');
	var comment_id = $(this).attr('data-comment_id');
	var url="{{ route('like.comment') }}";

	$.ajax({
		url: url,
		headers: {
			'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
		},
		data: {
			"post_id":post_id,
			"comment_id":comment_id,
		},
		type: 'POST',
		success: function(data) {
			$('.dynamic_post').html(data.html);
			$('.modal').modal('hide');
			clearData();
		}
	});
	
});


$(document).on('click','.create_like_gallery',function(e){
	
	//var post_id=$('#create_post_form').serialize();
	var post_id = $(this).attr('data-post-id');
	var url="{{ route('like.post.gallery') }}";

	$('.loader').show();
	$.ajax({
		url: url,
		headers: {
			'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
		},
		data: {
			"post_id":post_id,
		},
		type: 'POST',
		success: function(data) {
			$('.loader').hide();
			$('.dynamic_like_box').html(data.html);
			if(data.is_remove == 1){
				$('#gallery_like_'+data.post_id).removeClass('like');	
			}
			//e.preventDefault();
			//$('.dynamic_post').html(data.html);
			//$('.modal').modal('hide');
			//clearData();
		}
	});
	
});

$(document).on('hidden.bs.modal','.gallery_model', function () {
	var url="{{ route('get.post.likes') }}";
	var post_id = $(this).attr('data-post-id');
	$('.loader').show();
	$.ajax({
		url: url,
		headers: {
			'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
		},
		type: 'POST',
		data:{
			'post_id':post_id,
		},
		success: function(data) {
			$('.loader').hide();	
			$('.dynamic_post').html(data.html);
			clearData();
			//e.preventDefault();
			//$('.dynamic_post').html(data.html);
			//$('.modal').modal('hide');
			//clearData();
		}
	});
});


$(document).on('click','.like_show',function(){

	var post_id = $(this).attr('data-post-id');
	var url="{{ route('like.show') }}";

	$.ajax({
		url: url,
		headers: {
			'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
		},
		data: {
		"post_id":post_id,
		},
		type: "POST",
		success: function(result) {
			$('#like_data').html(result.html);
			$('#like_view').modal({show:true});
		} 
	});
})

$(document).on('click','.comment_like_show',function(){

	var post_id = $(this).attr('data-post-id');
	var comment_id = $(this).attr('data-comment-id');
	var url="{{ route('comment.like.show') }}";

	$.ajax({
		url: url,
		headers: {
			'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
		},
		data: {
		"post_id":post_id,
		"comment_id":comment_id,
		},
		type: "POST",
		success: function(result) {
			$('#comment_like_data').html(result.html);
			$('#comment_like_view').modal({show:true});
		} 
	});
});

$(document).ready(function(){

	$(document).on('click','.upload-photo-btn',function(){
		//$('#upload-photo-text').modal({backdrop: 'static', keyboard: false});
		$.ajax({
			url: "{{ route('show.upload.image') }}",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			},
			type: "POST",
			success: function(result) {
				
				$('#start-post').modal('hide');
				$('.data_upload').html(result.html);
				$("#upload-photo-text").modal('show');
				
				
				$( "#file-5" ).trigger( "click" );
				// $('#like_data').html(result.html);
				// $('#like_view').modal({show:true});
			} 
		});
	});

	$(".upload-video-btn").click(function(){
		//$('#video-post-text').modal({backdrop: 'static', keyboard: false});
		$.ajax({
			url: "{{ route('show.upload.video') }}",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			},
			type: "POST",
			success: function(result) {
				
				$('#start-post').modal('hide');
				$('.right-submit').css('display','block');
				$('.right-done').css('display','none');
				$( "#file_video" ).val('');
				$('.data_upload_video').html(result.html);
				$("#upload-video-text").modal('show');
				
				$( "#file_video" ).trigger( "click" );
			} 
		});
    });

	$(".upload-document-btn").click(function(){
        $("#document-post-text").modal('show');
		$( "#file-doc" ).trigger( "click" );
    });
});



$(document).on('click','.post_collapse',function(){
	var post_id = $(this).attr('data-post-id');
	if($( "#ed-options-"+post_id ).hasClass( "active" )){
		$('#ed-options-'+post_id).removeClass('active');
	}
	else{
		$('#ed-options-'+post_id).addClass('active');
	}
})

$(document).on('click','.view_more',function(){
	var post_id = $(this).attr('data-post-id');
	$( "#view_less_"+post_id ).removeClass("hide");
	$('#view_more_'+post_id).addClass('hide');
});

$(document).on('click','.view_less',function(){
	var post_id = $(this).attr('data-post-id');
	$( "#view_more_"+post_id ).removeClass("hide");
	$('#view_less_'+post_id).addClass('hide');
});

function switchStyle() {
	if (document.getElementById('styleSwitch').checked) {
	document.getElementById('gallery').classList.add("custom");
	document.getElementById('exampleModal').classList.add("custom");
	} else {
	document.getElementById('gallery').classList.remove("custom");
	document.getElementById('exampleModal').classList.remove("custom");
	}
}

$(document).on('click',".img-modal-btn.right", function(e){
	e.preventDefault();
	cur = $(this).parent().find('img:visible()');
	next = cur.next('img');
	par = cur.parent();
	if (!next.length) { next = $(cur.parent().find("img").get(0)) }
	cur.addClass('hidden');
	next.removeClass('hidden');
	return false;
});
	
$(document).on('click',".img-modal-btn.left", function(e){
	e.preventDefault();
	cur = $(this).parent().find('img:visible()');
	next = cur.prev('img');
	par = cur.parent();
	children = cur.parent().find("img");
	if (!next.length) { next = $(children.get(children.length-1)) }
	cur.addClass('hidden');
	next.removeClass('hidden');
	return false;
});

$(document).on('click','.gallerybox .imgbox img', function() {
	var post_id = $(this).attr('data-post-id');
	var post_media_id = $(this).attr('data-post-media-id');
	$('.slider_gallery').addClass('hidden');
	$('#slide_'+post_media_id).removeClass('hidden');
	$('#exampleModalCenter_'+post_id).modal('show');
});


/*---- Edit Post */

$(document).on('click','.edit_post',function(){

	var post_id = $(this).attr('data-post-id');
	var url="{{ route('edit.post.view') }}";

	$.ajax({
		url: url,
		headers: {
			'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
		},
		data: {
		"post_id":post_id,
		},
		type: "GET",
		success: function(result) {
			$('#update_post_id_'+result.postdata.id).val(result.postdata.id);
			$('.update_post').attr('id', 'update_post_'+result.postdata.id);
			$('#update_post_'+result.postdata.id).val(result.postdata.id);
			$('.edit_post_data').attr('id', 'edit_post_data_'+result.postdata.id);
			$('#edit_post_data_'+result.postdata.id).html(result.html);
			$('#editpost_view').modal({show:true});
		} 
	});
});

$(document).on('click','.update_post',function(){

	var id = $(this).attr('id');
	var post_id = $('#'+id).val();
	var url="{{ route('edit.post') }}";
	//var post_text = $('#post_text_'+post_id).val();
	
	var post_text = $('#post_text_'+post_id).html();
	var post_title = $('#post_title_'+post_id).val();

	if(post_id != 0){

		if(post_title == ''){
			alert('Please enter feed title.');
			return false;
		}
		else{

			$.ajax({
				url: url,
				headers: {
					'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
				},
				data: {
				"post_id":post_id,
				"post_text":post_text,
				"post_title":post_title
				},
				type: "POST",
				success: function(result) {
					//$('#edit_post_data').html(result.html);
					$('#editpost_view').modal('hide');
					$('.dynamic_post').html(result.html);
					clearData();
				} 
			});
		}
	}

	
});

/*------------------------------------------------------------------------------------------*/

$(document).on('click','.load_more_btn',function(){

	var get_post_load_count = $(this).attr('id');
	var post_load_count = parseInt(get_post_load_count);
	post_load_count = post_load_count += 10;
	$('.loader').show();
	$.ajax({
		url: "{{ route('load.more.post') }}",
		headers: {
			'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
		},
		data: {
		"post_load_count":post_load_count,
		},
		type: "POST",
		success: function(result) {
			$('.loader').hide();
			$(this).attr('id',post_load_count);
			$('.dynamic_post').html(result.html);
			clearData();
		} 
	});
})

/*-------------------------------------------------------------------------------------*/


/*-----------------------  post tag   --------------------*/

$(document).ready(function(){

	$(document).on('click','.post_likes_box ul.tabs li',function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');

		if(tab_id == "post_tab-All"){
			$('.dynamic_tab_likes').addClass('d-none');
		}
		else{
			$('.dynamic_tab_likes').removeClass('d-none');
		}
		$("#"+tab_id).addClass('current');
	});

	$(document).on('click','.comment_likes_box ul.tabs li',function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');

		if(tab_id == "comment_tab-All"){
			$('.dynamic_tab_likes').addClass('d-none');
		}
		else{
			$('.dynamic_tab_likes').removeClass('d-none');
		}
		$("#"+tab_id).addClass('current');
	});

});

/*--------------------------------------------- */

</script>




@endsection


