@extends('frontend.layouts.main')
@section('title','Notification')
@section('css')

<style>

</style>
@endsection

@section('content')


<br/><br/>
<div class="container mt-5">

	<div class="card">
		<div class="row">
			<div class="col-md-9 p-3">
				<h2 class="h2">Notifications List</h2>
			</div>
			<div class="col-md-3 p-3 text-right">
				<a class="btn btn-light" href="{{ route('front.index') }}">Back</a>
			</div>
		</div>
		<hr class="m-0" />

		<div class="card-body p-0">
			<div class="row bg-white">
				<div class="col-lg-12">
					<div class="dynamic_user_list">
						<table class="tablewidth bg-white">
							<tbody>
								<tr class="borderbottom">
									<td class="pb-2 pt-1">
										<div class="row p-2">
											<div class="col-md-9">
												<div class="usy-dt">
													<a href="#" class='link_label'><h3 class="font-weight-bold"><img src="{{ asset('upload/default_user.jpg')}}" alt="" width="50" height="50"></h3></a>
												</div>
												<div class="row pt-3">
													<div class="col-md-12 d-flex">
														<h3><a href="#" class='link_label font-weight-bold'>Smith Mike</a>  Comment on your project.</h3><br/>
													</div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<tr class="borderbottom">
									<td class="pb-2 pt-1">
										<div class="row p-2">
											<div class="col-md-9">
												<div class="usy-dt">
													<a href="#" class='link_label'><h3 class="font-weight-bold"><img src="{{ asset('upload/default_user.jpg')}}" alt="" width="50" height="50"></h3></a>
												</div>
												<div class="row pt-3">
													<div class="col-md-12 d-flex">
														<h3><a href="#" class='link_label font-weight-bold'>Smith Mike</a>  Comment on your project.</h3><br/>
													</div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<tr class="borderbottom">
									<td class="pb-2 pt-1">
										<div class="row p-2">
											<div class="col-md-9">
												<div class="usy-dt">
													<a href="#" class='link_label'><h3 class="font-weight-bold"><img src="{{ asset('upload/default_user.jpg')}}" alt="" width="50" height="50"></h3></a>
												</div>
												<div class="row pt-3">
													<div class="col-md-12 d-flex">
														<h3><a href="#" class='link_label font-weight-bold'>Smith Mike</a>  Comment on your project.</h3><br/>
													</div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<tr class="borderbottom">
									<td class="pb-2 pt-1">
										<div class="row p-2">
											<div class="col-md-9">
												<div class="usy-dt">
													<a href="#" class='link_label'><h3 class="font-weight-bold"><img src="{{ asset('upload/default_user.jpg')}}" alt="" width="50" height="50"></h3></a>
												</div>
												<div class="row pt-3">
													<div class="col-md-12 d-flex">
														<h3><a href="#" class='link_label font-weight-bold'>Smith Mike</a>  Comment on your project.</h3><br/>
													</div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<tr class="borderbottom">
									<td class="pb-2 pt-1">
										<div class="row p-2">
											<div class="col-md-9">
												<div class="usy-dt">
													<a href="#" class='link_label'><h3 class="font-weight-bold"><img src="{{ asset('upload/default_user.jpg')}}" alt="" width="50" height="50"></h3></a>
												</div>
												<div class="row pt-3">
													<div class="col-md-12 d-flex">
														<h3><a href="#" class='link_label font-weight-bold'>Smith Mike</a>  Comment on your project.</h3><br/>
													</div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<tr class="borderbottom">
									<td class="pb-2 pt-1">
										<div class="row p-2">
											<div class="col-md-9">
												<div class="usy-dt">
													<a href="#" class='link_label'><h3 class="font-weight-bold"><img src="{{ asset('upload/default_user.jpg')}}" alt="" width="50" height="50"></h3></a>
												</div>
												<div class="row pt-3">
													<div class="col-md-12 d-flex">
														<h3><a href="#" class='link_label font-weight-bold'>Smith Mike</a>  Comment on your project.</h3><br/>
													</div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<tr class="borderbottom">
									<td class="pb-2 pt-1">
										<div class="row p-2">
											<div class="col-md-9">
												<div class="usy-dt">
													<a href="#" class='link_label'><h3 class="font-weight-bold"><img src="{{ asset('upload/default_user.jpg')}}" alt="" width="50" height="50"></h3></a>
												</div>
												<div class="row pt-3">
													<div class="col-md-12 d-flex">
														<h3><a href="#" class='link_label font-weight-bold'>Smith Mike</a>  Comment on your project.</h3><br/>
													</div>
												</div>
											</div>
										</div>
									</td>
								</tr>	
							</tbody>
						</table>
					</div>	
				</div>
			</div>
		</div>
	</div>

	

</div>






@endsection




@section('scripts')




@endsection


