@extends('frontend.layouts.main')
@section('title','Opportunity Add')

@section('css')
<style>

</style>
@endsection

@section('content')

<br/><br/>
<div class="container mt-5">

	<div class="card">
		<div class="row">
			<div class="col-md-12 p-3">
				<h2 class="h2">Add Opportunity</h2>
			</div>
		</div>
		<hr class="m-0" />


		<div class="card-body">
			
			<form action="{{ route('front.opportunity.store') }}" name="opportunity_form" method="POST">
			@csrf		
				<div class="row mb-3">
					<div class="col-md-12">
						<label class="mb-2">Opportunity Title</label>
						<input type="text" name="title" class="form-control" placeholder="Enter Opportunity Title">
					</div>
				</div>
				<div class="row mb-3">
					<div class="col-md-12">
						<label class="mb-2">Opportunity Description</label>
						<textarea name="description" class="form-control" placeholder="Enter Opportunity Description"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="{{ route('front.opportunity.index') }}" class="btn btn-danger">Cancel</a>
					</div>
				</div>

			</form>

		</div>
	</div>

	

</div>

@endsection




@section('scripts')

<script type="text/javascript">
	$("form[name='opportunity_form']").validate({
        rules: {
            title: 'required',
            description: 'required'
        },
        messages: {
        	title: 'Please enter title.',
            description: 'Please enter description.'
        }
    });
</script>

@endsection


