
@extends('auth.include.main')
@section('title','Refund Policy')
@section('css')
@endsection

@section('content')
<!-- <section class="breatcome_area" style="background-image: url('{{ asset('assets/frontend/images/slider/slide1.jpg')}}');">
		<div class="container">
			<h4>About Us</h4>
		</div>
	</section> -->
	<section class="commform regstep contactus">
		<div class="container">
			<div class="titlebox">
				<!-- <h2>Coming Soon</h2> -->
			</div>
			
	<p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;line-height:normal;font-size:15px;font-family:"Calibri",sans-serif;text-align:justify;'><strong><u><span style="font-size:21px;">Refund And Cancelation Policy</span></u></strong></p>
<p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;line-height:normal;font-size:15px;font-family:"Calibri",sans-serif;text-align:justify;'>12. PAYMENTS, BILLING AND REFUNDS</p>
<p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;line-height:normal;font-size:15px;font-family:"Calibri",sans-serif;text-align:justify;'>(i)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;Some of Dealcracker&apos;s products and services are provided on a trial basis, while others are provided for a fee. Dealcracker&rsquo;s products and services are available on a monthly fee basis or pay per transaction basis. The fees for each product and service vary. The fee payable by the user to Dealcracker may be mutually agreed between the relevant user and Dealcracker. Dealcracker reserves the right to change its fee policy from time to time. In particular, Dealcracker may at its sole discretion introduce new products and services and modify some or all of the existing products and services offered on the Website. In such an event, Dealcracker reserves the right to introduce fees for the new products and services offered or amend/introduce fees for existing products and services, as the case may be. Dealcracker also reserves reserve the right, with or without prior notice, to:</p>
<p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;line-height:normal;font-size:15px;font-family:"Calibri",sans-serif;text-align:justify;text-indent:.5in;'>(a) limit the availability of or to discontinue any product or service;</p>
<p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;line-height:normal;font-size:15px;font-family:"Calibri",sans-serif;text-align:justify;text-indent:.5in;'>(bi) to impose conditions on any promotion;</p>
<ol style="list-style-type: undefined;margin-left:0.25in;">
    <li>to bar any user from making any purchase; and/or</li>
    <li>to refuse to provide any user with any product or service. All fees are payable in the currency, to the entity and in the manner agreed between the relevant user and Dealcracker. You shall be solely responsible for compliance of all applicable laws for making payments to Dealcracker. All payments shall be made in advance in order to subscribe to the Services. Subscribers are entirely responsible for the payment of all taxes.</li>
</ol>
<p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;line-height:normal;font-size:15px;font-family:"Calibri",sans-serif;text-align:justify;'>(ii)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;All orders placed through the Website are subject to our acceptance. Your receipt of an order confirmation does not signify our acceptance of your order. We reserve the right at any time after receipt of your order to accept or decline your order for any or no reason and without liability to you or anyone else. We may require verification of information prior to the acceptance and/or fulfillment of any order.</p>
<p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;line-height:normal;font-size:15px;font-family:"Calibri",sans-serif;text-align:justify;'>(iii)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;All payments for Dealcracker&rsquo;s Services will be made through:</p>
<ol style="list-style-type: undefined;margin-left:0.25in;">
    <li>an electronic and automated collection and remittance service (the &quot;Payment Gateway&quot;), either hosted on the Website through payment gateway service providers or payment links of service providers provided by Dealcracker (the &quot;Service Providers&quot;); or</li>
    <li>wire/bank transfer to a bank account designated by Dealcracker; or</li>
    <li>through payment links provided by Dealcracker. The Payment Gateway service is provided to you in order to facilitate your purchase of Dealcracker&rsquo;s products and services. The processing of payments may be subject to the terms, conditions and privacy policies of the Service Providers in addition to these Terms of Service. The terms of your payment may also be determined by agreements between you and the financial institution, card issuer or other provider of your chosen payment method. By choosing to use paid Services, you agree to pay us, through the Service Providers, all charges at the prices then in effect for any use of such paid Services in accordance with the applicable payment terms. You represent that you have the legal right to use any payment method that you submit to us. Dealcracker makes no representation of any kind, express or implied, as to the operation of the Payment Gateway. Dealcracker is not responsible for any errors by the Service Providers. Dealcracker reserves the right to correct any errors or mistakes that it makes even if it has already requested or received payment. Dealcracker assumes no liability whatsoever for any monetary or other damage suffered by you on account of:</li>
</ol>
<ol start="1" style="list-style-type: upper-alpha;margin-left:75.05px;">
    <li>access to, use of, or reliance on the Payment Gateway services;</li>
    <li>the delay, failure, interruption, or corruption of any data or other information transmitted in connection with use of the Payment Gateway; or</li>
    <li>any interruption or errors in the operation of the Payment Gateway. You expressly agree that your use of the Payment Gateway is entirely at your own risk. Dealcracker does not collect and store any payment details provided by you for transacting through the Payment Gateway. You agree, understand and confirm that personal data including without limitation details relating to debit card/credit card stored and transmitted over the internet is susceptible to misuse, theft and/or fraud and that Dealcracker &nbsp;and/or Mukherjee Holdings has no control over such matters and shall not be liable in any manner whatsoever. Dealcracker does not represent or guarantee that the use of the Payment Gateway will not result in theft and/or unauthorized use of data over the internet.</li>
</ol>
<p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;line-height:normal;font-size:15px;font-family:"Calibri",sans-serif;text-align:justify;'>(iv)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;The records of Dealcracker and/or the Service Provider, generated by the transactions arising out of the use of Dealcracker&rsquo;s Service, including the time the transaction is recorded, shall be conclusive proof of the genuineness and accuracy of the transaction. The details provided by you for use of the Service Provider are correct and accurate and you shall not use a debit/credit card/net banking which are not lawfully owned by you. You further agree and undertake to provide correct and valid debit/credit card/net banking details. In default of the above conditions, Dealcracker shall be entitled to recover the amount of the transaction from you. Further, Dealcracker also reserves the right to initiate any legal action for recovery of cost, penalty or any other measures, as it may deem fit.</p>
<p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;line-height:normal;font-size:15px;font-family:"Calibri",sans-serif;text-align:justify;'>(v)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Dealcracker will not be responsible or assume any liability whatsoever in respect of any loss or damage arising directly or indirectly to you while making payments to Dealcracker. All payments made to Dealcracker &nbsp;and/or &ldquo;Mukherjee Holdings&rdquo;, including payments made towards Services, are non-refundable, unless otherwise agreed in writing between you and Dealcracker.</p>
<p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;line-height:normal;font-size:15px;font-family:"Calibri",sans-serif;text-align:justify;'>(vi)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;All commercial terms with respect to a transaction or deal are on a principal-to-principal basis between the users and use of the payment facility shall not render Dealcracker liable or responsible for the non-completion of any deal or transaction or any loss or damage suffered by the users as regards the deals/transactions listed on the Website.</p>
<p style='margin-top:0in;margin-right:0in;margin-bottom:.0001pt;margin-left:0in;line-height:normal;font-size:15px;font-family:"Calibri",sans-serif;text-align:justify;'>&nbsp;</p>
</div>
	</section>
@endsection
@section('scripts')


@endsection	
    
