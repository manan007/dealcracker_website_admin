@extends('frontend.layouts.main')
@section('title','Blog')

@section('css')
<style>

</style>
@endsection

@section('content')

<br/><br/>
<div class="container mt-5">

	<div class="card">
		<div class="row">
			<div class="col-md-12 p-3">
				<h2 class="h2">Blog List</h2>
			</div>
		</div>
		<hr class="m-0" />


		<div class="card-body">

			<table id="datatable" class="display" style="width:100%">
				<thead>
		            <tr>
		                <th>Blog Title</th>
		            </tr>
		        </thead>
		        <tbody>
		        	@if(count($blogs) > 0)
						@foreach($blogs as $key => $data)
							<tr>
								<td>
									<div class="row">
										<div class="col-xl-11 col-lg-11 col-md-11">
											<h3 class="h6 font-weight-bold">{{ $data->title ?? '' }}</h3>
										</div>
										<div class="col-xl-1 col-lg-1 col-md-1 text-right actionbtns">
											<a href="{{ route('front.blog.view',$data->id) }}" class="btn-sm btn-info"><i class="fas fa-eye"></i></a>
										</div>
									</div>
								</td>
							</tr>
						@endforeach
					@endif
		        </tbody>
			</table>

		</div>
	</div>
	
</div>

@endsection




@section('scripts')
<script type="text/javascript">
//  $(document).on('click','.delete',function(){
//     var id=$(this).data('id');
//     var getval = $(this).data('value');
//     bootbox.confirm("Are you sure you want to delete this opportunity ?", function(result){ 
//         if(result == true)
//         {   
//           $('.submitData_changestatus_'+id).trigger('click');
//         }
//     });
// });
</script>
@endsection



































<!-- <div class="row">
	<div class="col-xl-10 col-lg-10 col-md-10">
		<h3 class="h6 font-weight-bold">Opportunity Title</h3>
		<span>Opportunity Description</span>
	</div>
	<div class="col-xl-2 col-lg-2 col-md-2 text-right">
		<a href="#" class="btn-sm btn-success"><i class="fas fa-pen"></i></a>
		<a href="#" class="btn-sm btn-danger"><i class="fas fa-trash"></i></a>
	</div>
</div>
<hr/> -->















