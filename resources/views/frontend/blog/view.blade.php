@extends('frontend.layouts.main')
@section('title','Blog Details')

@section('css')
<style>

</style>
@endsection

@section('content')

<br/><br/>
<div class="container mt-5">

	<div class="card mb-4">
		<div class="row">
			<div class="col-md-11 p-3">
				<h2 class="h2">Blog Details</h2>
			</div>
			<div class="col-md-1 p-3">
				<a class="btn btn-light" href="{{ route('front.blog.index') }}">Back</a>
			</div>
		</div>
		<hr class="m-0" />


		<div class="card-body">
					
			<div class="row mb-3">
				<div class="col-md-12">
					<label class="mb-2 font-weight-bold">Blog Title</label>
					<h3>{{ $blog_details->title ?? '' }}</h3>
				</div>
			</div>
			<hr/>
			<div class="row mb-3">
				<div class="col-md-12">
					<label class="mb-2 font-weight-bold">Blog Description</label>
					<span>{!! $blog_details->description ?? '' !!}</span>
				</div>
			</div>
			<hr/>
			<div class="row mb-3">
				<div class="col-md-12">
					<label class="mb-2 font-weight-bold">Blog Media</label>
				</div>
			</div>
			<div class="row">
			@if(count($blog_medias) > 0)
				@foreach($blog_medias as $blog_media)
						@if($blog_media->type == "image")
							@if($blog_media->blog_media)
								<div class="col-md-2">
									<img src="{{ asset('upload/blog/'.$blog_media->blog_media)}}" width="100%" height="100%">
								</div>
							@endif
						@else
							@if($blog_media->blog_media)
								<div class="col-md-2">
									<video width="100%" height="100%" controls>
					                    <source src="{{asset('upload/blog/'.$blog_media->blog_media)}}" type='video/mp4'>
					                </video>
								</div>
							@endif		           
						@endif
				@endforeach
			@endif
			</div>

		</div>
	</div>

	

</div>

@endsection




@section('scripts')

@endsection


