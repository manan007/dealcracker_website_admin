@if(Auth::check())


<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>DealCracker - Profile Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="" />
    <meta name="_token" content="{{ csrf_token() }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta name="keywords" content="" />

    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/line-awesome.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0-2/css/fontawesome.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/line-awesome-font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/fontawesome-free/css/all.min.css')}}"> -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/jquery.mCustomScrollbar.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/lib/slick/slick.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/lib/slick/slick-theme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/custome.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/custome1.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/ui.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.0/css/jquery.dataTables.min.css">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/croppie.min.css') }}">
    <style>
        .modal { overflow: auto !important; }
        /* Chat Msg Box Css */
        .loader{
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url("{{ asset('frontend/images/loader.gif') }}") 
                    50% 50% no-repeat rgba(0,0,0,0.5);
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover{
            background: #1E2C75;
            color: #ffffff!important;
            border-color: #1E2C75;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            background: #1E2C75;
            color: #ffffff!important;
            border-color: #1E2C75;
        }
        .bootbox-confirm .close{
            display: none;
        }
    </style>



	<style>
		.radioButton label,
		.radioButton input {
			display: block;
			position: absolute;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
		}

		.radioButton {
			/*float: left;*/
			/*margin: 0 5px 0 0;*/
			width: 100%;
			height: 40px;
			position: relative;
		}

		.btn:hover
		{
		    color: #464646;
		    text-decoration: none;
		}

		.radioButton input[type="radio"] {
		opacity: 0.011;
		z-index: 100;
		}

		.radioButton input[type="checkbox"] {
		opacity: 0.011;
		z-index: 100;
		}

		.radioButton input[type="radio"]:checked + label {
		background: #1E2C75;
		color: #ffffff;
		border-radius: 4px;
		}

		.radioButton input[type="checkbox"]:checked + label {
		background: #1E2C75;
		color: #ffffff;
		border-radius: 4px;
		}

		.radioButton label {
		color:#000;
		background: #d3d3d3;
		border-radius: 4px;
		}
		.cover_image{
			background: #fff;
			border: 2px solid #1E2C75;
			border-radius: 3px;
			color: #1E2C75;
			cursor: pointer;
			display: inline-block;
			font-size: 15px;
			font-weight: 600;
			outline: none;
			padding: 12px 20px;
			position: relative;
			transition: all 0.3s;
			vertical-align: middle;
			margin: 0;
			float: right;
			text-transform: uppercase;
		}
	</style>

  
    @yield('css')
</head>
    <div class="loader" style="display:none;"></div>
    <body>
        <input type="hidden" name="hidden_id" class="hidden_id" value="" />
        <input type="hidden" name="friend_id" id="friend_id" value="{{ $user_detail->id }}" />
        <input type="hidden" name="userid" id="userid" value="{{ Auth::user()->id }}">

        <div class="wrapper">
			<section class="cover-sec">
				@if($user_detail->cover_image != null)
			    	<img src="{{ asset('upload/cover_image/'.$user_detail->cover_image)}}" alt="" height="400px">
				@else	
					<img src="{{ asset('frontend/images/resources/cover-img.jpg') }}" alt="">
				@endif
				<div class="add-pic-box">
					<div class="container">
						<div class="row no-gutters">
							<div class="col-lg-12 col-sm-12">					
							</div>
						</div>
					</div>
				</div>
			</section>

			<main>
				<div class="main-section">
					<div class="container">
						<div class="main-section-data">
							<div class="row">
								<!-- ======================================================= main-left-sidebar start  ===============================================================-->
								<div class="col-lg-3">
									<div class="main-left-sidebar">
										<div class="user_profile">
											<div class="user-pro-img">
									            @if($user_detail->profile != null)
									            <img src="{{ asset('upload/profile/'.$user_detail->profile)}}" alt="">
									            @else
									            <img src="{{ asset('frontend/images/user.jpg')}}" alt="">
									            @endif
									            <!--<img src="{{ asset('assets/frontend/images/resources/user-pro-img.png') }}" alt="">-->
									            <div class="add-dp" id="OpenImgUpload">
									            </div>
									        </div><!--user-pro-img end-->
									        @php 
									        	$profile_likes = App\Models\ProfileLike::where(['friend_id' => $user_detail->id,'status' => 1])->count();
			            						$profile_dislikes = App\Models\ProfileLike::where(['friend_id' => $user_detail->id,'status' => 2])->count();

			            						$my_profile = App\Models\ProfileLike::where(['user_id' => Auth::user()->id,'friend_id' => $user_detail->id])->first();	
									       	@endphp
											<div class="user_pro_status">
												<ul class="flw-status">
													<li>
												        <div class="radioButton w-50">
												            <input type="radio" id="like" name="profile_review" class="specializations profile_review" value="Like" @if($my_profile) 
												            @if($my_profile->status == 1) checked="" @endif @endif />
												            <label class="btn btn-default" for="like"><i class="fa fa-thumbs-up"></i></label>
												        </div>
												    </li>

													<li>
														<div class="radioButton w-50">
												            <input type="radio" id="dislike" name="profile_review" class="specializations profile_review" value="Like" @if($my_profile) 
												            @if($my_profile->status == 2) checked="" @endif @endif />
												            <label class="btn btn-default" for="dislike"><i class="fa fa-thumbs-down"></i></label>
												        </div>
												    </li>
												</ul>
											</div>
											<div class="user_pro_status" style="padding-top: 27px;">
												<ul class="flw-status">
													<li>
														<span>Likes</span>
														<span class="font-weight-bold" id="profile_likes">{{ $profile_likes ?? 0 }}</span>
													</li>
													<li>
														<span>Dislikes</span>
														<span class="font-weight-bold" id="profile_dislikes">{{ $profile_dislikes ?? 0 }}</span>
													</li>
												</ul>
											</div><!--user_pro_status end-->
											
										</div><!--user_profile end-->
										
									</div>
								</div>
								<!-- ======================================================= main-left-sidebar end  ===============================================================-->


								<!-- ======================================================= main-center-section start  ===============================================================-->
								<div class="col-lg-6 user_profile_main_center_section">
									<div class="main-ws-sec">
										<div class="user-tab-sec rewivew">
											<a href="{{ route('front.my_profile.index',$user_detail->id) }}"><h3>{{ $user_detail->name }}</h3></a>
											<div class="star-descp">
												<span>{{ $user_detail->profile_description }}</span>
												
											</div>
			                                <div class="tab-feed st2 settingjb main_heading_icons">
												<ul>
													<li data-tab="feed-dd" class="active">
														<a href="#" title="">
															<i class="fa fa-flag" aria-hidden="true"></i>
															<span>Feed</span>
														</a>
													</li>
													<li data-tab="info-dd">
														<a href="#" title="">
															<i class="fa fa-info-circle" aria-hidden="true"></i>
															<span>Info</span>
														</a>
													</li>
												</ul>
											</div>
										</div>

										<div class="product-feed-tab current" id="feed-dd">
											<div class="posts-section">
												<div class="dynamic_post">
													@include('frontend.home.dynamic_post')
												</div>
											</div>
										</div>

										<div class="product-feed-tab" id="info-dd">
											<div class="user-profile-ov">
												<h3><a href="#" title="" class="overview-open">Information</a>
														@if(\Request::segment(2) == Auth::user()->id)<a href="javascript:;" title="Edit Information" class="edit-info"><i class="fa fa-pencil"></i></a>@endif
												</h3>
												<div class="dynamic_info">
												@include('frontend.my_profile.information.index')
												</div>
											</div>
										</div>

									</div>
								</div>
								<!-- ======================================================= main-center-section end  ===============================================================-->


								<!-- ======================================================= main-right-sidebar start  ===============================================================-->
								<div class="col-lg-3">
									<div class="right-sidebar">
										<!-- <div class="message-btn">
											<a href="profile-account-setting.html" title=""><i class="fas fa-cog"></i> Setting</a>
										</div> -->
										
									</div>
								</div>
								<!-- ======================================================= main-right-sidebar end  ===============================================================-->
							</div>
						</div>
					</div> 
				</div>
			</main>

		</div>
        <!--theme-layout end-->
        <script type="text/javascript" src="{{asset('frontend/js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/popper.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/jquery.mCustomScrollbar.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/lib/slick/slick.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/scrollbar.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/script.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/custome.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.js"></script>
        <script type="text/javascript" src="{{ asset('frontend/js/jquery.validate.min.js') }}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/ui.js')}}"></script>
        <script type="text/javascript" src="{{ asset('frontend/js/select2.min.js') }}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>
    </body>
	
	<script type="text/javascript" src="{{ asset('frontend/js/croppie.js') }}"></script>

	<script type="text/javascript">

	function clearData()
	{
		$('#post_text').val("");
		$('#create_post_form_video').trigger('reset');
		$('#create_post_form_document').trigger('reset');
		$('#create_post_form_media').trigger('reset');
		$('#create_post_form').trigger('reset');
		$('.post_media').html("");
	}

	$(document).on("change",".profile_review",function(){
		var getid = $(this).attr('id');
		var friend_id = $('#friend_id').val();
		$.ajax({
			url: "{{route('other.profile.like')}}",
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			type: "GET",
			data: {"id":getid,"friend_id":friend_id},
			success: function (data) {
				$('.profile_review').prop('checked',false);
				$('#'+getid).prop('checked',true);
				$('#profile_likes').text(data.profile_likes);
				$('#profile_dislikes').text(data.profile_dislikes);
			}
		});
	});



	/*------------- Like ------------*/

	$(document).on('click','.create_like',function(){
	
		var post_id = $(this).attr('data-post-id');
		var userid = $('#userid').val();
		var friend_id = $('#friend_id').val();

		var url="{{ route('other_profile.like.post') }}";
		$.ajax({
			url: url,
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			},
			data: {
				"post_id":post_id,
				"userid":userid,
				"friend_id":friend_id,
			},
			type: 'POST',
			success: function(data) {
				$('.dynamic_post').html(data.html);
				$('.modal').modal('hide');
				clearData();
			}
		});
		
	});


	$(document).on('click','.create_comment_like',function(){
	
		var post_id = $(this).attr('data-post-id');
		var comment_id = $(this).attr('data-comment_id');
		var userid = $('#userid').val();
		var friend_id = $('#friend_id').val();
		var url="{{ route('other_profile.like.comment') }}";

		$.ajax({
			url: url,
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			},
			data: {
				"post_id":post_id,
				"comment_id":comment_id,
				"userid":userid,
				"friend_id":friend_id,
			},
			type: 'POST',
			success: function(data) {
				$('.dynamic_post').html(data.html);
				$('.modal').modal('hide');
				clearData();
			}
		});
		
	});


	//------------------------------------  Comment section start ----------------------------------------------------------

	$(document).on('click','.commentdiv',function(){
		var post_id=$(this).data('id');
		var post_data=$('.commentdiv_'+post_id).val();
		var userid = $('#userid').val();
		var friend_id = $('#friend_id').val();
		if(post_data == "")
		{
			alert("Please Enter comment...");
			return false;
		}
		$.ajax({
			url: "{{route('other_profile.store.comment')}}",
			data: {
				"post_id":post_id,
				"post_data":post_data,
				"userid":userid,
				"friend_id":friend_id,
			},
			type: 'GET',
			success: function(data) {
				$('.dynamic_post').html(data.html);
			}
		});
	});

	$(document).on('keyup','.commenttext',function(e){
		if(e.keyCode == 13){
			var post_id=$(this).data('id');
			var post_data=$('.commentdiv_'+post_id).val();
			var userid = $('#userid').val();
			var friend_id = $('#friend_id').val();
			if(post_data == "")
			{
				alert("Please Enter comment...");
				return false;
			}
			$.ajax({
				url: "{{route('other_profile.store.comment')}}",
				data: {
					"post_id":post_id,
					"post_data":post_data,
					"userid":userid,
					"friend_id":friend_id,
				},
				type: 'GET',
				success: function(data) {
					$('.dynamic_post').html(data.html);
				}
			});
		}
	});

	$(document).on('click','.replyComment',function(){
		var post_id=$(this).data('post_id');
		var valueCheck=$('.reply_comment_'+post_id).val();
		var userid = $('#userid').val();
		var friend_id = $('#friend_id').val();
		if(valueCheck == "")
		{
			alert("Please enter proper comment..");
			return false;
		}
		var formData=$('#reply_'+post_id).serialize();

		$.ajax({
			url: "{{route('other_profile.reply.comment')}}",
			data:formData+"&userid="+userid+"&friend_id="+friend_id,
			type: 'GET',
			success: function(data) {
				$('.dynamic_post').html(data.html);
			}
		});	
	});


	$(document).on('keyup','.replaytext',function(e){

		if(e.keyCode == 13){
			var post_id=$(this).data('post_id');
			var valueCheck=$('.reply_comment_'+post_id).val();
			var userid = $('#userid').val();
			var friend_id = $('#friend_id').val();
			if(valueCheck == "")
			{
				alert("Please enter proper comment..");
				return false;
			}
			var formData=$('#reply_'+post_id).serialize();

			$.ajax({
				url: "{{route('other_profile.reply.comment')}}",
				data:formData+"&userid="+userid+"&="+friend_id,
				type: 'GET',
				success: function(data) {
					$('.dynamic_post').html(data.html);
				}
			});
		}
	});




	$(document).on('click','.editComment',function(){
		var post_id=$(this).data('post_id');
		var valueCheck=$('.edit_comment_'+post_id).val();
		var userid = $('#userid').val();
		var friend_id = $('#friend_id').val();
		if(valueCheck == "")
		{
			alert("Please enter proper comment..");
			return false;
		}
		var formData=$('#edit_comment_'+post_id).serialize();

		$.ajax({
			url: "{{route('other_profile.edit.comment')}}",
			data:formData+"&userid="+userid+"&friend_id="+friend_id,
			type: 'GET',
			success: function(data) {
				$('.dynamic_post').html('');
				$('.dynamic_post').html(data.html);
			}
		});	
	});


	$(document).on('keyup','.edit_commenttext',function(e){
		if(e.keyCode == 13){
			var post_id=$(this).data('post_id');
			var valueCheck=$('.edit_comment_'+post_id).val();
			var userid = $('#userid').val();
			var friend_id = $('#friend_id').val();
			if(valueCheck == "")
			{
				alert("Please enter proper comment..");
				return false;
			}
			var formData=$('#edit_comment_'+post_id).serialize();

			$.ajax({
				url: "{{route('other_profile.edit.comment')}}",
				data:formData+"&userid="+userid+"&friend_id="+friend_id,
				type: 'GET',
				success: function(data) {
					$('.dynamic_post').html(data.html);
				}
			});				
		}

	});

	$(document).on('click','.editCommentEdit',function(){
		var comment_id=$(this).data('id');
		var post_id=$(this).data('post_id');
		var comment=$(this).data('comment');

		$('.edit_comment_'+post_id).val(comment);
		$('.hideEditDiv_'+post_id).css('display','block');
		$('.hideReplyDiv_'+post_id).css('display','none');
		$('.edit_comment_id_'+post_id).val(comment_id);
	});

	$(document).on('click','.getComment',function(){

		var comment_id=$(this).data('comment_id');
		var post_id=$(this).data('post_id');
		$('.hideEditDiv_'+post_id).css('display','none');
		$('.hideReplyDiv_'+post_id).css('display','block');
		$('.comment_id_'+post_id).val(comment_id);
	});

	$(document).on('click','.deleteComment',function(){
		var comment_id=$(this).data('id');
		var userid = $('#userid').val();
		var friend_id = $('#friend_id').val();

		bootbox.dialog({
			message: "Are you sure you want to delete this comment!",
			title: "Delete Comment",
			onEscape: function() {},
			show: true,
			backdrop: true,
			closeButton: true,
			animate: true,
			className: "sartpost",
			buttons: {
			"Danger!": {
				label:"No",
				className: "btn-cancel",
				callback: function() {}
			},
			success: {   
				label: "Yes",
				className: "btn-default1 text-right",
				callback: function() {
					$.ajax({
						url: "{{route('other_profile.delete.comment')}}",
						data: {
							"comment_id":comment_id,
							"userid":userid,
							"friend_id":friend_id,
						},
						type: 'GET',
						success: function(data) {
							$('.dynamic_post').html(data.html);
						}
					});	
				},
				
			}
			}
		});
	});


	$(document).on('click','.com_show',function(){
		var post_id=$(this).data('post_id');
		$('.hideCommentDiv_'+post_id).css('display','block');
		$('.showCommentDiv_'+post_id).css('display','none');
		$('.hideMoreComment_'+post_id).css('display','block');
	});

	$(document).on('click','.showCommentDiv',function(){
		var post_id=$(this).data('post_id');
		$('.hideCommentDiv_'+post_id).css('display','block');
		$('.showCommentDiv_'+post_id).css('display','none');
		$('.hideMoreComment_'+post_id).css('display','block');
	});

	$(document).on('click','.hideMoreComment',function(){
		var post_id=$(this).data('post_id');
		$('.hideCommentDiv_'+post_id).css('display','none');
		$('.showCommentDiv_'+post_id).css('display','block');
		$('.hideMoreComment_'+post_id).css('display','none');
	});

	
	//------------------------------------  Comment section  Over ---------------------------------------------------------- 


	/*----------------------------------    Load More Feed  --------------------------------------------*/

	$(document).on('click','.load_more_btn',function(){

		var get_post_load_count = $(this).attr('id');
		var post_load_count = parseInt(get_post_load_count);
		var userid = $('#userid').val();
		var friend_id = $('#friend_id').val();
		post_load_count = post_load_count += 10;
		$('.loader').show();
		$.ajax({
			url: "{{ route('other_profile.load.more.post') }}",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			},
			data: {
			"post_load_count":post_load_count,
			"userid":userid,
			"friend_id":friend_id,
			},
			type: "POST",
			success: function(result) {
				$('.loader').hide();
				$(this).attr('id',post_load_count);
				$('.dynamic_post').html(result.html);
				clearData();
			} 
		});
	});

	/*-------------------------------------------------------------------------------------------------*/



	</script>
</html>




@else

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<title>DealCracker - Profile Page</title>
	<meta name="_token" content="{{ csrf_token() }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<script type="text/javascript" src="{{ asset('frontend/js/html5.js') }}"></script>
	<!-- ALL CSS -->
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/landing-style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/landing-media.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/custome1.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/fontawesome.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/custome.css')}}">
	<!-- ALL SCRIPT -->
	<script type="text/javascript" src="{{ asset('frontend/js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/select2.min.js') }}"></script>
	
    <style>#exampleInputEmail1-error{color:red;}#exampleInputPassword1-error{color:red;}</style>
    
	<style>
		.radioButton label,
		.radioButton input {
		display: block;
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		}

		.radioButton {
			/*float: left;*/
			/*margin: 0 5px 0 0;*/
			width: 100%;
			height: 40px;
			position: relative;
		}

		.btn:hover
		{
		    color: #464646;
		    text-decoration: none;
		}

		.radioButton input[type="radio"] {
		opacity: 0.011;
		z-index: 100;
		}

		.radioButton input[type="checkbox"] {
		opacity: 0.011;
		z-index: 100;
		}

		.radioButton input[type="radio"]:checked + label {
		background: #1E2C75;
		color: #ffffff;
		border-radius: 4px;
		}

		.radioButton input[type="checkbox"]:checked + label {
		background: #1E2C75;
		color: #ffffff;
		border-radius: 4px;
		}

		.radioButton label {
		color:#000;
		background: #d3d3d3;
		border-radius: 4px;
		}
		.cover_image{
			background: #fff;
			border: 2px solid #1E2C75;
			border-radius: 3px;
			color: #1E2C75;
			cursor: pointer;
			display: inline-block;
			font-size: 15px;
			font-weight: 600;
			outline: none;
			padding: 12px 20px;
			position: relative;
			transition: all 0.3s;
			vertical-align: middle;
			margin: 0;
			float: right;
			text-transform: uppercase;
		}
	</style>

    <style type="text/css">
		.sub_box{
			border-top: 4px solid #ef3e35!important;
			border-radius: 0px!important;
			background-color: #ffffff;
		}
		.sub_box a{
			padding: 9px 15px 8px!important;
		}
		.sub_box a:hover{
			background-color: #ef3e35;
			color: #ffffff;
		}
		.dropdown-menu{
			padding: 0px!important;
		}

		nav{
			width: 29%;
		}

		.custheader .signin {
		    border: 1px solid #FFFFFF;
		    padding: 10px 29px 9px !important;
		    border-radius: 25px;
		    font-family: 'FivoSansMedium';
		    font-size: 15px;
		    color: #ffffff;
		    display: inline-block;
		    position: relative;
		    top: -10px;
		}

		.custheader .custnav .menus > li:hover > a {
		    color: #ffffff;
		}

		.custheader .signin:hover {
		    background-color: #ffffff;
		    color: #1E2C75 !important;
		}


		header {
	        float: left;
	        width: 100%;
	        padding: 0px 0;
	        /*background-color: transparent!important;*/
	        /* border-bottom:1px solid black; */
	    }

	    .header1 {
	        float: left;
	        width: 100%;
	        padding: 15px 0;
	        background-color: #fff!important;
	        /* border-bottom:1px solid black; */
	    }
	    .custheader .custnav .menus > li.dropmenu > a {
	        background: url(../assets/frontend/images/down-arrow.png) no-repeat 100% 5px;
	        padding-right: 20px;
	    }
	    /*.logo{
	        width: 100px!important;
	    }*/
	    .logoimage img{
	        width: 130px!important;
	    }
	    .borderbottom{
	        border-bottom:1px solid #666666;
	        font-size:22px;
	        font-weight:500;
	        padding-bottom:5px;
	        color:#666666;
	    }
	    .cartbox{
	        border-top: 1px solid #e9ebec;
	    }
	    .mainclass{
	        padding-top:0px;
	    }
	    #notfound {
	        position: relative;
	        height: 46vh!important;
	    }
	    .widthtd5{
	        width:20%;
	    }
	    .widthtd10{
	        width:10%;
	    }
	    .dflex{
	        display:flex;
	    }
	    .ed-opts{
			display: none;
		}
		.coverpart{
			margin-top: 8%;
		}
		.sub_box{
			border-top: 4px solid #ef3e35!important;
			border-radius: 0px!important;
			background-color: #ffffff;
		}
		.sub_box a{
			padding: 9px 15px 8px!important;
		}
		.sub_box a:hover{
			background-color: #ef3e35;
			color: #ffffff;
		}
		.dropdown-menu{
			padding: 0px!important;
		}

		.custheader .custnav {
			margin-top: 0px;
		}

		footer {
		    bottom: 0px;
		    position: fixed;
		}
	</style>

    @yield('css')
</head>

<body>

    	@include('auth.include.header')	

        <input type="hidden" name="hidden_id" class="hidden_id" value="" />

		<section class="cover-sec">
			@if($user_detail->cover_image != null)
		    	<img src="{{ asset('upload/cover_image/'.$user_detail->cover_image)}}" alt="" height="400px">
			@else	
				<img src="{{ asset('frontend/images/resources/cover-img.jpg') }}" alt="">
			@endif
			<div class="add-pic-box">
				<div class="container">
					<div class="row no-gutters">
						<div class="col-lg-12 col-sm-12">					
						</div>
					</div>
				</div>
			</div>
		</section>

			<main>
				<div class="main-section">
					<div class="container">
						<div class="main-section-data">
							<div class="row">
								<!-- ======================================================= main-left-sidebar start  ===============================================================-->
								<div class="col-lg-3">
									<div class="main-left-sidebar">
										<div class="user_profile">
											<div class="user-pro-img">
									            @if($user_detail->profile != null)
									            <img src="{{ asset('upload/profile/'.$user_detail->profile)}}" alt="">
									            @else
									            <img src="{{ asset('frontend/images/user.jpg')}}" alt="">
									            @endif
									        </div><!--user-pro-img end-->

									        @php 
									        	$profile_likes = App\Models\ProfileLike::where(['friend_id' => $user_detail->id,'status' => 1])->count();
			            						$profile_dislikes = App\Models\ProfileLike::where(['friend_id' => $user_detail->id,'status' => 2])->count();

			            						$my_profile = App\Models\ProfileLike::where(['user_id' => $user_detail->id,'friend_id' => $user_detail->id])->first();	
									       	@endphp
											<div class="user_pro_status">
												<ul class="flw-status">
													<li>
												        <div class="radioButton w-50" data-toggle="modal" data-target="#login_Modal">
												            <input type="radio" id="like" name="profile_review" data-toggle="modal" data-target="#login_Modal" disabled="true" class="specializations profile_review" value="Like" @if($my_profile) 
												            @if($my_profile->status == 1) checked="" @endif @endif />
												            <label class="btn btn-default" for="like"><i class="fa fa-thumbs-up"></i></label>
												        </div>
												    </li>

													<li>
														<div class="radioButton w-50" data-toggle="modal" data-target="#login_Modal">
												            <input type="radio" id="dislike" name="profile_review"  disabled="true" class="specializations profile_review" value="Like" @if($my_profile) 
												            @if($my_profile->status == 2) checked="" @endif @endif />
												            <label class="btn btn-default" for="dislike"><i class="fa fa-thumbs-down"></i></label>
												        </div>
												    </li>
												</ul>
											</div>
											<div class="user_pro_status" style="padding-top: 27px;">
												<ul class="flw-status">
													<li>
														<span>Likes</span>
														<span class="font-weight-bold" id="profile_likes">{{ $profile_likes ?? 0 }}</span>
													</li>
													<li>
														<span>Dislikes</span>
														<span class="font-weight-bold" id="profile_dislikes">{{ $profile_dislikes ?? 0 }}</span>
													</li>
												</ul>
											</div><!--user_pro_status end-->
											
										</div><!--user_profile end-->	
									</div>
								</div>
								<!-- ======================================================= main-left-sidebar end  ===============================================================-->


								<!-- ======================================================= main-center-section start  ===============================================================-->
								<div class="col-lg-6 user_profile_main_center_section">
									<div class="main-ws-sec">
										<div class="user-tab-sec rewivew">
											<a href="{{ route('front.my_profile.index',$user_detail->id) }}"><h3>{{ $user_detail->name }}</h3></a>
											<div class="star-descp">
												<span>{{ $user_detail->profile_description }}</span>
												
											</div>
			                                <div class="tab-feed1 st2 settingjb main_heading_icons">
												<ul>
													<li data-toggle="modal" data-target="#login_Modal" class="active">
														<a href="#" title="">
															<i class="fa fa-flag" aria-hidden="true"></i>
															<span>Feed</span>
														</a>
													</li>
													<li data-toggle="modal" data-target="#login_Modal"><i class="fa fa-info-circle"></i><span>Info</span></li>
												</ul>
											</div>
										</div>

										<div class="product-feed-tab current" id="feed-dd">
											<div class="posts-section">
												<div class="dynamic_post">
													
													<div class="posts-section">
													    <div class="mainRow">
													    @if(count($posts) > 0)
													        @php $postcnt = 0; @endphp
													        @foreach($posts as $post)

													            @php $postcnt = $postcnt + 1; @endphp
													            @php $getpost_media = App\Models\FeedMedia::where('feed_id',$post->id)->get();
													                 $getpost_like = App\Models\FeedLike::where(['user_id' => $user_detail->id,'feed_id' => $post->id])->first();
													                 $getpostlike_count = App\Models\FeedLike::where(['feed_id' => $post->id])->count('id');

													                 $comments = App\Models\FeedComment::with('user')->where(['feed_id' => $post->id])->orderBy('id','desc')->get();
													                 $getemojies = [];
													            @endphp

													           
													                <input type="hidden" class="post_ids" value="{{$post->id}}">

													                <div class="post-bar">
													                    @php $getshared_post = 0;  @endphp

													                    <div class="post_topbar">
													                        <div class="usy-dt">
													                            @if(isset($post->user->profile))
													                                @if($user_detail->id == $post->user->id)
													                                <a href="{{ route('front.my_profile.index',$user_detail->id) }}" class='link_label'><img src="{{ asset('upload/profile/'.$post->user->profile)}}" alt="" width="40" height="40"></a>
													                                @else
													                                <a href="{{ route('front.my_profile.index',$user_detail->id) }}" class='link_label'><img src="{{ asset('upload/profile/'.$post->user->profile)}}" alt="" width="40" height="40"></a>    
													                                @endif
													                            @else
													                                @if($user_detail->id == $post->user->id)
													                                <a href="{{ route('front.my_profile.index',$user_detail->id) }}" class='link_label'><img src="{{ asset('frontend/images/user.jpg')}}" alt="" width="40" height="40"></a>
													                                @else
													                                <a href="{{ route('front.my_profile.index',$user_detail->id) }}" class='link_label'><img src="{{ asset('frontend/images/user.jpg')}}" alt="" width="40" height="40"></a>
													                                @endif
													                            @endif
													                            <div class="usy-name">
													                                @if($user_detail->id == $post->user->id)
													                                <a href="{{ route('front.my_profile.index',$user_detail->id) }}" class='link_label'><h3>{{ isset($post->user->name) ? $post->user->name:'' }}</h3></a>
													                                @else
													                                <a href="{{ route('front.my_profile.index',$user_detail->id) }}" class='link_label'><h3>{{ isset($post->user->name) ? $post->user->name:'' }}</h3></a>
													                                @endif
													                                <span><img src="{{ asset('frontend/images/clock.png') }}" alt="">{{ $post->created_at->diffForHumans() }}</span>
													                            </div>
													                        </div>
													                        @if($post->user_id == $user_detail->id)
													                        <div class="ed-opts">
													                            <a href="javascript:;" title="" class="ed-opts-open post_collapse" data-post-id="{{ $post->id }}"><i class="la la-ellipsis-v"></i></a>
													                            <ul class="ed-options" id="ed-options-{{ $post->id }}">
													                                <li><a href="javascript:;" title="Edit Post" class="edit_post" data-post-id="{{ $post->id }}">Edit Post</a></li>
													                                <li><a href="javascript:;" title="Delete Post" class="delete_post" data-post-id="{{ $post->id }}">Delete Post</a></li>
													                            </ul>
													                        </div>
													                        @endif
													                    </div>

													                    <div class="epi-sec">
													                        <ul class="descp" style="margin-bottom:0px"></ul>
													                    </div>
													                    <div class="job_descp">
													                        @if($post->title)
													                        <h5 class="h5 m-0">{{ $post->title }}</h5>
													                        @endif
													                        @if(strlen($post->description) > 142)
													                      
													                        <!-- view more -->
													                        <p class="viewtextpost"  id="view_more_{{ $post->id }}">
													                            {{ $post->description }}
													                            <br/><a href="javascript:;" class="view_more" data-post-id="{{ $post->id }}" title="View Less">view more</a>
													                        </p>

													                        <!-- view less -->
													                        <p class="viewtextpost hide" id="view_less_{{ $post->id }}">{{ $post->description }}<br/><a href="javascript:;" class="view_less" data-post-id="{{ $post->id }}" 
													                            title="View More">view less</a></p>
													                        @else
													                        <p>
													                           {{ $post->description }}
													                        </p>
													                        @endif

													                        @if(count($getpost_media) > 0)
													                        
													                            @if(count($getpost_media) == 1)
													                                @foreach($getpost_media as $media)
													                                    <div class="gallerybox">						
													                                        <div class="col12">
													                                            <div class="imgbox">
													                                                @if($media->type ==  "image")
													                                                    <img class="w-100" src="{{asset('upload/post/'.$media->feed_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}" data data-slide-to="0">
													                                                @elseif($media->type ==  "video")
													                                                    <video width="100%" height="100%" controls>
													                                                        <source src="{{asset('upload/post/'.$media->feed_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
													                                                    </video>
													                                                @endif    
													                                            </div>
													                                        </div>
													                                    </div>
													                                @endforeach
													                            @elseif(count($getpost_media) == 2)
													                                @foreach($getpost_media as $media)
													                                    <div class="gallerybox">
													                                        <div class="col2">
													                                            <div class="imgbox">
													                                                @if($media->type ==  "image")
													                                                    <img class="w-100" src="{{asset('upload/post/'.$media->feed_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}" data-post-id="{{ $post->id }}" data-slide-to="0">
													                                                @elseif($media->type ==  "video")
													                                                    <video width="100%" height="100%" controls>
													                                                        <source src="{{asset('upload/post/'.$media->feed_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
													                                                    </video>
													                                                @endif
													                                            </div>
													                                        </div>
													                                    </div>
													                                @endforeach
													                            @elseif(count($getpost_media) == 3)
													                                @foreach($getpost_media as $key => $media)
													                                    <div class="gallerybox">
													                                        @if($key == 0)						
													                                        <div class="col12">
													                                            <div class="imgbox">
													                                                @if($media->type ==  "image")
													                                                    <img class="w-100" src="{{asset('upload/post/'.$media->feed_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}" data-post-media-id="{{ $media->id }}" data-post-id="{{ $post->id }}" data-slide-to="0">
													                                                @elseif($media->type ==  "video")
													                                                    <video width="100%" height="100%" controls>
													                                                        <source src="{{asset('upload/post/'.$media->feed_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
													                                                    </video>
													                                                @endif
													                                            </div>
													                                        </div>
													                                        @else
													                                        <div class="col2">
													                                            <div class="imgbox">
													                                                @if($media->type ==  "image")
													                                                    <img class="w-100" src="{{asset('upload/post/'.$media->feed_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}" data-post-id="{{ $post->id }}" data-slide-to="0">
													                                                @elseif($media->type ==  "video")
													                                                    <video width="100%" height="100%" controls>
													                                                        <source src="{{asset('upload/post/'.$media->feed_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
													                                                    </video>
													                                                @endif
													                                            </div>
													                                        </div>
													                                        @endif
													                                    </div>
													                                @endforeach
													                            @elseif(count($getpost_media) == 4)
													                                @foreach($getpost_media as $media)
													                                    <div class="gallerybox">
													                                        <div class="col2">
													                                            <div class="imgbox">
													                                                @if($media->type ==  "image")
													                                                    <img class="w-100" src="{{asset('upload/post/'.$media->feed_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}" data-post-id="{{ $post->id }}" data-slide-to="0">
													                                                @elseif($media->type ==  "video")
													                                                    <video width="100%" height="100%" controls>
													                                                        <source src="{{asset('upload/post/'.$media->feed_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
													                                                    </video>
													                                                @endif
													                                            </div>
													                                        </div>
													                                    </div>
													                                @endforeach
													                            @elseif(count($getpost_media) == 5)
													                                @foreach($getpost_media as $key => $media)
													                                    <div class="gallerybox">
													                                        @if($key == 0 || $key == 1)						
													                                        <div class="col2">
													                                            <div class="imgbox">
													                                                @if($media->type ==  "image")
													                                                    <img class="w-100" src="{{asset('upload/post/'.$media->feed_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}" data-post-id="{{ $post->id }}" data-slide-to="0">
													                                                @elseif($media->type ==  "video")
													                                                    <video width="100%" height="100%" controls>
													                                                        <source src="{{asset('upload/post/'.$media->feed_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
													                                                    </video>
													                                                @endif
													                                            </div>
													                                        </div>
													                                        @else
													                                        <div class="col1">
													                                            <div class="imgbox">
													                                                @if($media->type ==  "image")
													                                                    <img class="w-100" src="{{asset('upload/post/'.$media->feed_media)}}" alt="First slide" data-post-media-id="{{ $media->id }}" data-post-id="{{ $post->id }}" data-slide-to="0">
													                                                @elseif($media->type ==  "video")
													                                                    <video width="100%" height="100%" controls>
													                                                        <source src="{{asset('upload/post/'.$media->feed_media)}}" type='video/mp4' alt="First slide" data-slide-to="0" data-post-media-id="{{ $media->id }}"  data-post-id="{{ $post->id }}">
													                                                    </video>
													                                                @endif
													                                            </div>
													                                        </div>
													                                        @endif
													                                    </div>
													                                @endforeach
													                            @endif

													                            
													                        @endif
													                    </div>



													                    <div class="checkimg"></div>
													                    <div class="job-status-bar border-none">
													                        {{--  && $post->parent_post_id == 0 --}}
													                        @if($post->shared_post_id != 0)
													                        @php  $getshared_post = App\Models\Post::where(['id' => $post->shared_post_id])->first(); @endphp
													                        <div class="share-details-row mt-3">
													                            <div class="usy-name-share"> 
													                                @if($user_detail->id == $getshared_post->user->id)
													                                <a href="#" class='link_label'><h3>{{ isset($getshared_post->user->first_name) ? $getshared_post->user->first_name:'' }} {{ isset($getshared_post->user->last_name) ? $getshared_post->user->last_name:'' }}</h3></a>
													                                @else
													                                <a href="#" class='link_label'><h3>{{ isset($getshared_post->user->first_name) ? $getshared_post->user->first_name:'' }} {{ isset($getshared_post->user->last_name) ? $getshared_post->user->last_name:'' }}</h3></a>
													                                @endif
													                                <span>{{ date('d M Y',strtotime($getshared_post->created_at)) }} at {{ date('h:i:s A',strtotime($getshared_post->created_at)) }}</span>
													                            </div>
													                        </div>
													                        @endif

													                        <ul class="like-com mt-0">
													                            <li class="mr-0">
													                                <button class="emjbtn_like">
													                                    
													                                    {{--
													                                    @if($getpost_like)
													                                        @php $getemojies_details = App\Models\LikeEmoji::where('id',$getpost_like->like_emoji_id)->first();   @endphp

													                                        @if($getemojies_details)
													                                        <a href="javascript:;" class="create_like" data-toggle="modal" data-target="#login_Modal" data-dislike-status="1" data-emoji-id="{{ $getemojies_details->id }}" 
													                                            data-post-id="{{ $post->id }}">
													                                            <img src="{{asset('public/uploads/emoji/'.$getemojies_details->image)}}"  height="30" width="30" class="emojibtn_class">
													                                        </a>
													                                        @else
													                                        <a href="javascript:;">
													                                            <img src="{{asset('assets/frontend/images/default_like.png')}}" data-toggle="modal" data-target="#login_Modal" height="30" width="30" class="emojibtn_class">
													                                        </a>
													                                        @endif
													                                    @else
													                                        <a href="javascript:;">
													                                            <img src="{{asset('assets/frontend/images/default_like.png')}}" data-toggle="modal" data-target="#login_Modal" height="30" width="30" class="emojibtn_class">
													                                        </a>
													                                    @endif
													                                    --}}

													                                    <div class="emoji-container">
													                                        @if(count($getemojies) > 0)
													                                        @foreach($getemojies as $keyemoji =>  $getemojie)
													                                            <div class="emoji">
													                                                <div class="icon create_like" data-emoji-id="{{ $getemojie->id }}" data-dislike-status="0" data-post-id="{{ $post->id }}">
													                                                    <img src="{{asset('public/uploads/emoji/'.$getemojie->image)}}" height="50">
													                                                </div>
													                                            </div>
													                                        @endforeach
													                                        @endif
													                                    </div>
													                                </button>
													                            </li>
													                            
													                               {{-- @php $getpost_likes = App\Models\FeedLike::where(['feed_id' => $post->id])->where('user_id','!=',$user_detail->id)->get();  @endphp

													                                @if(count($getpost_likes) > 0)
													                                    <li>
													                                        @foreach($getpost_likes as $likes_key => $getpostlike)
													                                            @php $getemojies_likes_details = App\Models\LikeEmoji::where('id',$getpostlike->like_emoji_id)->first();   @endphp
													                                            <img src="{{asset('public/uploads/emoji/'.$getemojies_likes_details->image)}}" data-toggle="modal" data-target="#login_Modal"  height="30" width="30" class="@if($likes_key != 0) emojibtn_class1 @endif">
													                                        @endforeach
													                                    </li>
													                                @endif
													                                
													                                --}}
													                            
													                            <!--== For Like ==-->
													                            
													                            <li class="mt-2">
													                                <!-- <div class="dynamic_like_box">                        
													                                    <a href="javascript:;" class="create_like"><i class="fas fa-thumbs-up"></i> Like</a>
													                                </div> -->
													                                <span class="ml-0 likebt">
													                                    <a href="javascript:;" data-toggle="modal" data-target="#login_Modal" class="create_like @if($getpost_like) like @endif" data-post-id="{{ $post->id }}"><i class="fas fa-thumbs-up"></i></a>
													                                </span>
													                                @if($getpostlike_count != 0)
													                                <span class="ml-0 like_show mrcountbox_postlike" data-post-id="{{ $post->id }}">{{ $getpostlike_count }}</span>
													                                @else
													                                <span class="ml-0 mrcountbox_postlike" data-post-id="{{ $post->id }}">{{ $getpostlike_count }}</span>
													                                @endif
													                            </li>
													                            

													                            <!-- <li>
													                                <a href="#"><i class="fas fa-heart"></i> Like</a>
													                                <img src="{{ asset('assets/frontend/images/liked-img.png') }}" alt="">
													                                <span>25</span>
													                            </li>   -->
													                            {{--
													                            <li><a href="javascript:;" class="com @if(count($comments) > 2) com_show com_{{$post->id}} @endif" data-post_id="{{$post->id}}"><i class="fas fa-comment-alt"></i>  @if(count($comments) > 0) Comments {{count($comments)}} @else Comment @endif</a></li>
													                            
													                            <li><a href="javascript:;" class="share-opts-open com ml-1" id="{{$post->id}}"><i class="fa fa-share"></i>Share</a></li>

													                            
													                            <ul class="share-options" data-active="inactive" id="share-options-{{$post->id}}">
													                                <form action="{{ route('share.post') }}" method="post" id="share-options-form-{{$post->id}}">
													                                @csrf
													                                    <input type="hidden" name="post_id" value="{{ $post->id }}"> 
													                                    <input type="hidden" name="post_user_id" value="{{ $post->user_id }}">
													                                    <li class="sharenowli"><button type="submit" title="Share now" class="share_now">Share now (Only me)</button></li>
													                                </form>
													                                <!-- <li><a href="javascript:;" title="Share to News Feed" class="share_to_news_feed" data-post-user-id="{{ $post->user_id }}" data-post-id="{{$post->id}}">Share to News Feed</button></li> -->
													                            </ul>
													                            --}}
													                        </ul>
													                    </div>
													                    
													                    <!-- ===============Comment Section Start==============  -->

													                    {{--<div class="post-comment mt-2">
													                        <div class="cm_img">
													                            @if(isset($user_detail->profile))
													                                @if($user_detail->profile != null)
													                                    <a href="#"><img src="{{ asset('upload/profile/'.$user_detail->profile)}}" class="borderradius" alt="" width="40" height="40"></a>
													                                @else
													                                    <a href="#"><img src="{{ asset('frontend/images/user.jpg')}}" alt="" width="40" class="borderradius" height="40"></a>
													                                @endif
													                            @else
													                                <a href="#"><img src="{{ asset('frontend/images/user.jpg')}}" alt="" width="40" class="borderradius" height="40"></a>
													                            @endif
													                            <!--<img src="{{ asset('assets/frontend/images/resources/bg-img4.png') }}" alt="">-->
													                        </div>
													                        <div class="comment_box">
													                            <form>
													                                <input type="text" class="commentdiv_{{$post->id}} commenttext" data-id="{{$post->id}}" placeholder="Post a comment">
													                                <button type="button" class="commentdiv" data-id="{{$post->id}}">Send</button>
													                            </form>
													                        </div>
													                    </div>--}}

													                    {{--<div class="comment-section">                      
													                        <div class="comment-sec">
													                            <ul>
													                                @if(count($comments->where('is_reply',0)) > 0)
													                                    @foreach($comments->where('is_reply',0) as $key => $comment)

													                                        @if(count($comments->where('is_reply',0)) > 2 && $key > 1)
													                                            <li class="hideCommentDiv hideCommentDiv_{{$comment->feed_id}}">
													                                        @else
													                                            <li>
													                                        @endif
													                                                <div class="comment-list pt-3">
													                                                    <div class="bg-img">
													                                                        @if($comment->user && $comment->user->profile != null) 
													                                                            <img src="{{ asset('upload/profile/'.$comment->user->profile)}}" alt="" class="borderradius" alt="" width="40" height="40">
													                                                        @else
													                                                        <img src="{{asset('frontend/images/user.jpg')}}" alt="" class="borderradius" alt="" width="40" height="40">
													                                                        @endif
													                                                    </div>
													                                                    <div class="comment">

													                                                        <div class="row">
													                                                            <div class="col-md-12">
													                                                                <div class="comment-profile">
													                                                                    @if($comment->user && $comment->user->profile != null)
													                                                                    <img src="{{ asset('upload/profile/'.$comment->user->profile)}}" alt="" class="borderradius" alt="" width="40" height="40">
													                                                                    @else
													                                                                    <img src="{{asset('frontend/images/user.jpg')}}" alt="" class="borderradius" alt="" width="40" height="40">
													                                                                    @endif
													                                                                    <!--<img src="images/resources/us-pc2.png" alt="">-->
													                                                                </div>
													                                                                <div class="comment-part commentboxs">
													                                                                    <div class="box">
													                                                                        @if($comment->user && $comment->user->first_name && $comment->user->last_name != null)
													                                                                            <h3>{{$comment->user->first_name}}  {{$comment->user->last_name}}</h3>
													                                                                        @else
													                                                                        <h3>User</h3>
													                                                                        @endif
													                                                                        <span style="float: right!important;"><i class="fas fa-clock"></i> {{ $comment->created_at->diffForHumans() }}</span>
													                                                                        <p>{{$comment->comment}}</p>
													                                                                    </div>
													                                                                </div>
													                                                            </div>
													                                                        </div>

													                                                        <div class="row mt-1 ml40cmt">
													                                                            <!-- <div class="col-md-1 comment_first_sec"></div> -->
													                                                            <div class="col-md-1"></div>
													                                                            <div class="col-md-2 pl-0 pr-0 widthcolmd2">
													                                                                <button class="emjbtn_comment">
													                                                                    @php $getpost_comment_like = App\Models\FeedCommentLike::where(['user_id' => $user_detail->id,'feed_id' => $post->id,'comment_id' => $comment->id])->first();  @endphp
													                                                                    <span class="ml-0 likebt likebtcmt">
													                                                                        <a href="javascript:;" class="create_comment_like @if($getpost_comment_like) like @endif" 
													                                                                        data-comment_id="{{$comment->id}}" data-post-id="{{ $post->id }}"><i class="fas fa-thumbs-up"></i></a>
													                                                                    </span>
													                                                                </button>

													                                                            </div>

													                                                            <div class="col-md-7 pl-0 replaybtnwidth replaybtn-margin1">

													                                                                @php $getpost_comments_likes = App\Models\FeedCommentLike::where(['feed_id' => $post->id,'comment_id' => $comment->id])->where('user_id','!=',$user_detail->id)->get();  @endphp


													                                                                <a class="">
													                                                                @php $getpostlike_comment_count = App\Models\FeedCommentLike::where(['feed_id' => $post->id,'comment_id' => $comment->id])->count('id'); @endphp
													                                                                @if($getpostlike_comment_count != 0)
													                                                                <span class="text-light count_span comment_like_show cursor_pointer"  data-post-id="{{ $post->id }}" data-comment-id="{{ $comment->id }}">{{ $getpostlike_comment_count }}</span>
													                                                                @else
													                                                                <span class="text-light count_span" data-post-id="{{ $post->id }}">{{ $getpostlike_comment_count }}</span>
													                                                                @endif
													                                                                </a>
													                                                            <!-- </div>

													                                                            <div class="col-md-7 mt-1 pt-1 replybtn_section"> -->
													                                                                <a href="javascript:;" title="" data-comment_id="{{$comment->id}}" data-post_id="{{$post->id}}" class="active getComment default-color replaybtntxt_coment ml-2"><i class="fa fa-reply-all"></i>Reply</a> 
													                                                            </div>

													                                                            <div class="col-md-2 comment_actionbtn ptcmt3 text-right">

													                                                                @if($post->user_id == $user_detail->id)
													                                                                    @if($comment->user && $comment->user->id == $user_detail->id)
													                                                                        <span><i class="fa fa-edit text-info editCommentEdit" data-comment="{{$comment->comment}}" data-id="{{$comment->id}}"  data-post_id="{{$post->id}}"></i>&nbsp;&nbsp;<i class="fa fa-trash text-danger deleteComment" data-id="{{$comment->id}}"></i>
													                                                                        &nbsp;&nbsp;</span>
													                                                                    @else
													                                                                        <span><i class="fa fa-trash text-danger deleteComment" data-id="{{$comment->id}}"></i></span>   
													                                                                    @endif
													                                                                @else

													                                                                    @if($comment->user && $comment->user->id == $user_detail->id)
													                                                                    <span><i class="fa fa-edit text-info editCommentEdit" data-comment="{{$comment->comment}}" data-id="{{$comment->id}}"  data-post_id="{{$post->id}}"></i>&nbsp;&nbsp;<i class="fa fa-trash text-danger deleteComment" data-id="{{$comment->id}}"></i>&nbsp;&nbsp;</span>
													                                                                    @endif
													                                                                @endif

													                                                            </div>

													                                                        </div>

													                                                    </div>
													                                                </div><!--comment-list end-->
													                                                <ul class="replayDiv mt-0">
													                                                    @if(count($comments->where('to_comment',$comment->id)) > 0)
													                                                        @foreach($comments->where('to_comment',$comment->id) as $reply_comment)
													                                                            <li>
													                                                                <div class="comment-list">
													                                                                    <div class="comment">

													                                                                        <div class="row">
													                                                                            <div class="col-md-12 pr-0">
													                                                                                <div class="comment-profile">
													                                                                                    @if($reply_comment->user && $reply_comment->user->profile)
													                                                                                    <img src="{{ asset('upload/profile/'.$reply_comment->user->profile)}}" alt="" class="borderradius" alt="" width="40" height="40">
													                                                                                    @else
													                                                                                    <img src="{{asset('frontend/images/user.jpg')}}" class="borderradius" alt="">
													                                                                                    @endif
													                                                                                </div>
													                                                                                <div class="comment-part replayboxs" style="width:100%;margin-left:14%;">
													                                                                                    <div class="box">
													                                                                                        @if($reply_comment->user && $reply_comment->user->first_name && $reply_comment->user->last_name != null)
													                                                                                            <h3>{{ $reply_comment->user->name}}</h3>
													                                                                                        @else
													                                                                                        <h3>User</h3>
													                                                                                        @endif
													                                                                                        <span style="float: right!important;"><i class="fas fa-clock"></i>{{ $reply_comment->created_at->diffForHumans() }}</span>
													                                                                                        <p>{{$reply_comment->comment}} </p>
													                                                                                    </div>
													                                                                                </div>
													                                                                            </div>
													                                                                        </div>

													                                                                        <div class="row mt-2">
													                                                                            <div class="col-md-2 replayfirst_section pr-0"></div><!-- style="max-width:13%" -->
													                                                                            <div class="col-md-2 pl-1 pr-0 widthcolmd2_replay">

													                                                                                <button class="emjbtn_replay">
													                                                                                    @php $getpost_comment_like = App\Models\FeedCommentLike::where(['user_id' => $user_detail->id,'feed_id' => $post->id,'comment_id' => $reply_comment->id])->first();  @endphp
													                                                                                    <span class="ml-0 likebt likebtcmt">
													                                                                                        <a href="javascript:;" class="create_comment_like @if($getpost_comment_like) like @endif" 
													                                                                                        data-comment_id="{{$reply_comment->id}}" data-post-id="{{ $post->id }}"><i class="fas fa-thumbs-up"></i></a>
													                                                                                    </span>
													                                                                                </button> 
													                                                                            </div>
													                                                                            <div class="col-md-5 reply_cntbtn pl-0">


													                                                                                @php $getpost_comments_replay_likes = App\Models\FeedCommentLike::where(['feed_id' => $post->id,'comment_id' => $reply_comment->id])->where('user_id','!=',$user_detail->id)->get();  @endphp

													                                                                                

													                                                                                <a class="text-light">
													                                                                                @php $getpostlike_comment_replay_count = App\Models\FeedCommentLike::where(['feed_id' => $post->id,'comment_id' => $reply_comment->id])->count('id'); @endphp
													                                                                                @if($getpostlike_comment_replay_count != 0)
													                                                                                <span class="count_span text-light comment_like_show ml_counterspan cursor_pointer"  data-post-id="{{ $post->id }}" data-comment-id="{{ $reply_comment->id }}">{{ $getpostlike_comment_replay_count }}</span>
													                                                                                @else
													                                                                                <span class="count_span text-light ml_counterspan" data-post-id="{{ $post->id }}">{{ $getpostlike_comment_replay_count }}</span>
													                                                                                @endif
													                                                                                </a>
													                                                                            </div>
													                                                                            <div class="col-md-3 ptcmt3 commentaction_replybtn text-right">
													                                                                        
													                                                                                @if($user_detail->id == $post->user_id)
													                                                                                    @if($reply_comment->user && $reply_comment->user->id == $user_detail->id)
													                                                                                    <span><i class="fa fa-edit text-info editCommentEdit" data-comment="{{$reply_comment->comment}}" data-id="{{$reply_comment->id}}"  data-post_id="{{$post->id}}"></i>&nbsp;&nbsp;<i class="fa fa-trash text-danger deleteComment" data-id="{{$reply_comment->id}}"></i></span>
													                                                                                    @else
													                                                                                    <span><i class="fa fa-trash text-danger deleteComment" data-id="{{$reply_comment->id}}"></i></span>   
													                                                                                    @endif
													                                                                                @else
													                                                                                    @if($reply_comment->user && $reply_comment->user->id == $user_detail->id)
													                                                                                    <span><i class="fa fa-edit text-info editCommentEdit" data-comment="{{$reply_comment->comment}}" data-id="{{$reply_comment->id}}"  data-post_id="{{$post->id}}"></i>&nbsp;&nbsp;<i class="fa fa-trash text-danger deleteComment" data-id="{{$reply_comment->id}}"></i></span>
													                                                                                    @else
													                                                                                    
													                                                                                    @endif
													                                                                                @endif
													                                                                            </div>
													                                                                        </div>
													                                                                    </div>

													                                                                </div>

													                                                            </li>
													                                                        @endforeach
													                                                    @endif
													                                                    
													                                                </ul>
													                                            </li>
													                                    @endforeach
													                                @endif
													                            </ul>
													                        </div><!--comment-sec end-->
													                    </div><!--comment-section end-->
													                    --}}

													                    <!-- ===============Comment Section End==============  -->

													                    {{--@if(count($comments->where('is_reply',0)) > 2)
													                    <a href="javascript:;" class="plus-ic default-color showCommentDiv showCommentDiv_{{$post->id}}" data-post_id="{{$post->id}}">
													                        Load more comments <!--<i class="la la-plus"></i>-->
													                    </a>
													                    @endif--}}
													                    <a href="javascript:;" class="plus-ic hideMoreComment hideMoreComment_{{$post->id}}" data-post_id="{{$post->id}}">
													                        <i class="la la-minus"></i>
													                    </a>

													                    <div class="post-comment hideReplyDiv hideReplyDiv_{{$post->id}}">
													                        <div class="cm_img">
													                        @if($user_detail->profile != null)
													                            <a href="#"><img src="{{ asset('upload/profile/'.$user_detail->profile)}}" class="borderradius" alt="" width="40" height="40"></a>
													                        @else
													                            <a href="#"><img src="{{ asset('frontend/images/user.jpg')}}" alt="" width="40" class="borderradius" height="40"></a>
													                        @endif
													                            {{-- <img src="{{ asset('frontend/images/resources/bg-img4.png') }}" alt=""> --}}
													                        </div>
													                        <div class="comment_box">
													                            <form id="reply_{{$post->id}}">
													                                <input type="hidden" name="post_id" value="{{$post->id}}">
													                                <input type="hidden" name="comment_id" class="comment_id_{{$post->id}}">
													                                <input type="text" name="comment" class="reply_comment_{{$post->id}} replaytext" data-post_id="{{$post->id}}" placeholder="Post a comment">
													                                <button type="button" class="replyComment" data-post_id="{{$post->id}}">Send</button>
													                            </form>
													                        </div>
													                    </div>

													                    <div class="post-comment hideEditDiv hideEditDiv_{{$post->id}}">
													                        <div class="cm_img">
													                            {{--<img src="{{ asset('assets/frontend/images/resources/bg-img4.png') }}" alt="">--}}
													                            @if($user_detail->profile != null)
													                                <a href="#"><img src="{{ asset('upload/profile/'.$user_detail->profile)}}" class="borderradius" alt="" width="40" height="40"></a>
													                            @else
													                                <a href="#"><img src="{{ asset('frontend/images/user.jpg')}}" alt="" width="40" class="borderradius" height="40"></a>
													                            @endif
													                        </div>
													                        <div class="comment_box">
													                            <form id="edit_comment_{{$post->id}}">
													                                <input type="hidden" name="post_id" value="{{$post->id}}">
													                                <input type="hidden" name="comment_id" class="edit_comment_id_{{$post->id}}">
													                                <input type="text" name="comment" class="edit_comment_{{$post->id}} edit_commenttext" data-post_id="{{$post->id}}" placeholder="Edit a comment">
													                                <button type="button" class="editComment" data-post_id="{{$post->id}}">Send</button>
													                            </form>
													                        </div>
													                    </div>

													                </div><!--post-bar end-->

													                <div class="modal fade img-modal gallery_model" id="exampleModalCenter_{{ $post->id }}" data-post-id="{{ $post->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
													                    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
													                        <div class="modal-content">
													                            <div class="modal-body">
													                                <div class="row">
													                                    <div class="col-lg-8 modal-image">
													                                        @foreach($getpost_media as $mediakey => $media)
													                                            
													                                            <img class="img-responsive slider_gallery" id="slide_{{ $media->id }}" src="{{asset('upload/post/'.$media->feed_media)}}">

													                                        @endforeach                            
													                                            <!--<img class="img-responsive hidden" src="http://upload.wikimedia.org/wikipedia/commons/1/1a/Bachalpseeflowers.jpg" />
													                                        <img class="img-responsive hidden" src="http://www.netflights.com/media/216535/hong%20kong_03_681x298.jpg" />-->

													                                        <a href="" class="img-modal-btn left"><i class="fas fa-chevron-left"></i></a>
													                                        <a href="" class="img-modal-btn right"><i class="fas fa-chevron-right"></i> </a>
													                                    </div>
													                                    <div class="col-lg-4 col-md-12 modal-meta">
													                                        <div class="modal-meta-top">
													                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="far fa-times-circle"></i></span></button>
													                                            <div class="img-poster clearfix">
													                                                <div class="imgbox">
													                                                    @if(isset($post->user->profile))
													                                                        <img src="{{ asset('upload/profile/'.$post->user->profile)}}" class="img-circle borderradius" alt="">
													                                                    @else
													                                                        <img src="{{ asset('frontend/images/user.jpg')}}" alt="" class="img-circle borderradius">
													                                                    @endif
													                                                    <!--<img class="img-circle" src="https://s-media-cache-ak0.pinimg.com/736x/3b/7d/6f/3b7d6f60e2d450b899c322266fc6edfd.jpg"/>-->
													                                                </div>
													                                                <div class="usy-name">
													                                                    <h3>{{ $post->user->name ?? '' }}</h3>
													                                                    <span>{{ $post->created_at->diffForHumans() }}</span>
													                                                </div>
													                                            </div>

													                                            <div class="modelmeta">
													                                                <h4 class="mt-3 h4">{{ $post->title ?? '' }}</h4>
													                                                <p class="mt-0">{{ $post->description ?? '' }}</p>
													                                            </div>
													                                            <div class="job-status-bar">
													                                                <ul class="like-com">
													                                                    <li>
													                                                        <div class="dynamic_like_box">                        
													                                                            @include('frontend.home.dynamic_gallery_like')
													                                                        </div>
													                                                    </li> 
													                                                </ul>
													                                            </div>
													                                        </div>
													                                    </div>
													                                </div>
													                            </div>
													                        </div>
													                    </div>
													                </div>
													          
													        @endforeach
													        @if($postcnt == 0)
													        <div class="post-bar">
													            <div class="post_topbar">
													                No post found..
													            </div>
													        </div>
													        @endif
													    </div>

													    @if($total_post_counter >= $post_load_count)                    
													        <!-- <div class="process-comm">
													            <button class="btn loadmore load_more_btn" id="{{ $post_load_count }}">Load More Feeds</button>
													        </div> -->
													    @endif

													        {{--<div class="hideLoader">
													        <input type="hidden" class="last_data_number" value="0">
													        <div class="process-comm">
													            <div class="spinner">
													                <div class="bounce1"></div>
													                <div class="bounce2"></div>
													                <div class="bounce3"></div>
													            </div>
													        </div><!--process-comm end-->
													        </div>--}}
													    @else
													    </div>
													    <div class="post-bar">
													        <div class="post_topbar">
													            No post found..
													        </div>
													    </div>
													    @endif

													    
													</div><!--posts-section end-->
			    



												</div>
											</div>
										</div>

										<div>
											
										</div>

										<div class="product-feed-tab" id="my-bids">
											<div class="posts-section">
												<div class="post-bar">
													<div class="post_topbar">
														<div class="usy-dt">
															<img src="{{ asset('frontend/images/resources/us-pic.png')}}" alt="">
															<div class="usy-name">
																<h3>John Doe</h3>
																<span><img src="{{ asset('frontend/images/clock.png')}}" alt="">3 min ago</span>
															</div>
														</div>
														<div class="ed-opts">
															<a href="#" title="" class="ed-opts-open"><i class="la la-ellipsis-v"></i></a>
															<ul class="ed-options">
																<li><a href="#" title="">Edit Post</a></li>
																<li><a href="#" title="">Unsaved</a></li>
																<li><a href="#" title="">Unbid</a></li>
																<li><a href="#" title="">Close</a></li>
																<li><a href="#" title="">Hide</a></li>
															</ul>
														</div>
													</div>
													<div class="epi-sec">
														<ul class="descp">
															<li><img src="{{ asset('frontend/images/icon8.png')}}" alt=""><span>Frontend Developer</span></li>
															<li><img src="{{ asset('frontend/images/icon9.png')}}" alt=""><span>India</span></li>
														</ul>
														<ul class="bk-links">
															<li><a href="#" title=""><i class="la la-bookmark"></i></a></li>
															<li><a href="#" title=""><i class="la la-envelope"></i></a></li>
															<li><a href="#" title="" class="bid_now">Bid Now</a></li>
														</ul>
													</div>
													<div class="job_descp">
														<h3>Simple Classified Site</h3>
														<ul class="job-dt">
															<li><span>$300 - $350</span></li>
														</ul>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam luctus hendrerit metus, ut ullamcorper quam finibus at. Etiam id magna sit amet... <a href="#" title="">view more</a></p>
														<ul class="skill-tags">
															<li><a href="#" title="">HTML</a></li>
															<li><a href="#" title="">PHP</a></li>
															<li><a href="#" title="">CSS</a></li>
															<li><a href="#" title="">Javascript</a></li>
															<li><a href="#" title="">Wordpress</a></li> 	
															<li><a href="#" title="">Photoshop</a></li> 	
															<li><a href="#" title="">Illustrator</a></li> 	
															<li><a href="#" title="">Corel Draw</a></li> 	
														</ul>
													</div>
													<div class="job-status-bar">
														<ul class="like-com">
															<li>
																<a href="#"><i class="la la-heart"></i> Like</a>
																<img src="{{ asset('frontend/images/liked-img.png')}}" alt="">
																<span>25</span>
															</li> 
															<li><a href="#" title="" class="com"><img src="{{ asset('frontend/images/com.png')}}" alt=""> Comment 15</a></li>
														</ul>
														<a><i class="la la-eye"></i>Views 50</a>
													</div>
												</div><!--post-bar end-->
												<div class="post-bar">
													<div class="post_topbar">
														<div class="usy-dt">
															<img src="{{ asset('frontend/images/resources/us-pic.png')}}" alt="">
															<div class="usy-name">
																<h3>John Doe</h3>
																<span><img src="{{ asset('frontend/images/clock.png')}}" alt="">3 min ago</span>
															</div>
														</div>
														<div class="ed-opts">
															<a href="#" title="" class="ed-opts-open"><i class="la la-ellipsis-v"></i></a>
															<ul class="ed-options">
																<li><a href="#" title="">Edit Post</a></li>
																<li><a href="#" title="">Unsaved</a></li>
																<li><a href="#" title="">Unbid</a></li>
																<li><a href="#" title="">Close</a></li>
																<li><a href="#" title="">Hide</a></li>
															</ul>
														</div>
													</div>
													<div class="epi-sec">
														<ul class="descp">
															<li><img src="{{ asset('frontend/images/icon8.png')}}" alt=""><span>Frontend Developer</span></li>
															<li><img src="{{ asset('frontend/images/icon9.png')}}" alt=""><span>India</span></li>
														</ul>
														<ul class="bk-links">
															<li><a href="#" title=""><i class="la la-bookmark"></i></a></li>
															<li><a href="#" title=""><i class="la la-envelope"></i></a></li>
															<li><a href="#" title="" class="bid_now">Bid Now</a></li>
														</ul>
													</div>
													<div class="job_descp">
														<h3>Ios Shopping mobile app</h3>
														<ul class="job-dt">
															<li><span>$300 - $350</span></li>
														</ul>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam luctus hendrerit metus, ut ullamcorper quam finibus at. Etiam id magna sit amet... <a href="#" title="">view more</a></p>
														<ul class="skill-tags">
															<li><a href="#" title="">HTML</a></li>
															<li><a href="#" title="">PHP</a></li>
															<li><a href="#" title="">CSS</a></li>
															<li><a href="#" title="">Javascript</a></li>
															<li><a href="#" title="">Wordpress</a></li> 	
															<li><a href="#" title="">Photoshop</a></li> 	
															<li><a href="#" title="">Illustrator</a></li> 	
															<li><a href="#" title="">Corel Draw</a></li> 	
														</ul>
													</div>
													<div class="job-status-bar">
														<ul class="like-com">
															<li>
																<a href="#"><i class="la la-heart"></i> Like</a>
																<img src="{{ asset('frontend/images/liked-img.png')}}" alt="">
																<span>25</span>
															</li> 
															<li><a href="#" title="" class="com"><img src="{{ asset('frontend/images/com.png') }}" alt=""> Comment 15</a></li>
														</ul>
														<a><i class="la la-eye"></i>Views 50</a>
													</div>
												</div><!--post-bar end-->
												<div class="post-bar">
													<div class="post_topbar">
														<div class="usy-dt">
															<img src="{{ asset('frontend/images/resources/us-pic.png')}}" alt="">
															<div class="usy-name">
																<h3>John Doe</h3>
																<span><img src="{{ asset('frontend/images/clock.png')}}" alt="">3 min ago</span>
															</div>
														</div>
														<div class="ed-opts">
															<a href="#" title="" class="ed-opts-open"><i class="la la-ellipsis-v"></i></a>
															<ul class="ed-options">
																<li><a href="#" title="">Edit Post</a></li>
																<li><a href="#" title="">Unsaved</a></li>
																<li><a href="#" title="">Unbid</a></li>
																<li><a href="#" title="">Close</a></li>
																<li><a href="#" title="">Hide</a></li>
															</ul>
														</div>
													</div>
													<div class="epi-sec">
														<ul class="descp">
															<li><img src="{{ asset('frontend/images/icon8.png')}}" alt=""><span>Frontend Developer</span></li>
															<li><img src="{{ asset('frontend/images/icon9.png')}}" alt=""><span>India</span></li>
														</ul>
														<ul class="bk-links">
															<li><a href="#" title=""><i class="la la-bookmark"></i></a></li>
															<li><a href="#" title=""><i class="la la-envelope"></i></a></li>
															<li><a href="#" title="" class="bid_now">Bid Now</a></li>
														</ul>
													</div>
													<div class="job_descp">
														<h3>Simple Classified Site</h3>
														<ul class="job-dt">
															<li><span>$300 - $350</span></li>
														</ul>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam luctus hendrerit metus, ut ullamcorper quam finibus at. Etiam id magna sit amet... <a href="#" title="">view more</a></p>
														<ul class="skill-tags">
															<li><a href="#" title="">HTML</a></li>
															<li><a href="#" title="">PHP</a></li>
															<li><a href="#" title="">CSS</a></li>
															<li><a href="#" title="">Javascript</a></li>
															<li><a href="#" title="">Wordpress</a></li> 	
															<li><a href="#" title="">Photoshop</a></li> 	
															<li><a href="#" title="">Illustrator</a></li> 	
															<li><a href="#" title="">Corel Draw</a></li> 	
														</ul>
													</div>
													<div class="job-status-bar">
														<ul class="like-com">
															<li>
																<a href="#"><i class="la la-heart"></i> Like</a>
																<img src="{{ asset('frontend/images/liked-img.png')}}" alt="">
																<span>25</span>
															</li> 
															<li><a href="#" title="" class="com"><img src="{{ asset('frontend/images/com.png')}}" alt=""> Comment 15</a></li>
														</ul>
														<a><i class="la la-eye"></i>Views 50</a>
													</div>
												</div><!--post-bar end-->
												<div class="post-bar">
													<div class="post_topbar">
														<div class="usy-dt">
															<img src="{{ asset('frontend/images/resources/us-pic.png')}}" alt="">
															<div class="usy-name">
																<h3>John Doe</h3>
																<span><img src="{{ asset('frontend/images/clock.png')}}" alt="">3 min ago</span>
															</div>
														</div>
														<div class="ed-opts">
															<a href="#" title="" class="ed-opts-open"><i class="la la-ellipsis-v"></i></a>
															<ul class="ed-options">
																<li><a href="#" title="">Edit Post</a></li>
																<li><a href="#" title="">Unsaved</a></li>
																<li><a href="#" title="">Unbid</a></li>
																<li><a href="#" title="">Close</a></li>
																<li><a href="#" title="">Hide</a></li>
															</ul>
														</div>
													</div>
													<div class="epi-sec">
														<ul class="descp">
															<li><img src="{{ asset('frontend/images/icon8.png')}}" alt=""><span>Frontend Developer</span></li>
															<li><img src="{{ asset('frontend/images/icon9.png')}}" alt=""><span>India</span></li>
														</ul>
														<ul class="bk-links">
															<li><a href="#" title=""><i class="la la-bookmark"></i></a></li>
															<li><a href="#" title=""><i class="la la-envelope"></i></a></li>
															<li><a href="#" title="" class="bid_now">Bid Now</a></li>
														</ul>
													</div>
													<div class="job_descp">
														<h3>Ios Shopping mobile app</h3>
														<ul class="job-dt">
															<li><span>$300 - $350</span></li>
														</ul>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam luctus hendrerit metus, ut ullamcorper quam finibus at. Etiam id magna sit amet... <a href="#" title="">view more</a></p>
														<ul class="skill-tags">
															<li><a href="#" title="">HTML</a></li>
															<li><a href="#" title="">PHP</a></li>
															<li><a href="#" title="">CSS</a></li>
															<li><a href="#" title="">Javascript</a></li>
															<li><a href="#" title="">Wordpress</a></li> 	
															<li><a href="#" title="">Photoshop</a></li> 	
															<li><a href="#" title="">Illustrator</a></li> 	
															<li><a href="#" title="">Corel Draw</a></li> 	
														</ul>
													</div>
													<div class="job-status-bar">
														<ul class="like-com">
															<li>
																<a href="#"><i class="la la-heart"></i> Like</a>
																<img src="{{ asset('frontend/images/liked-img.png')}}" alt="">
																<span>25</span>
															</li> 
															<li><a href="#" title="" class="com"><img src="{{ asset('frontend/images/com.png')}}" alt=""> Comment 15</a></li>
														</ul>
														<a><i class="la la-eye"></i>Views 50</a>
													</div>
												</div><!--post-bar end-->
												<div class="process-comm">
													<a href="#" title=""><img src="{{ asset('frontend/images/process-icon.png')}}" alt=""></a>
												</div><!--process-comm end-->
											</div><!--posts-section end-->
										</div><!--product-feed-tab end-->

									</div>
								</div>
								<!-- ======================================================= main-center-section end  ===============================================================-->


								<!-- ======================================================= main-right-sidebar start  ===============================================================-->
								<div class="col-lg-3">
									<div class="right-sidebar">
										<!-- <div class="message-btn">
											<a href="profile-account-setting.html" title=""><i class="fas fa-cog"></i> Setting</a>
										</div> -->
										
									</div>
								</div>
								<!-- ======================================================= main-right-sidebar end  ===============================================================-->
							</div>
						</div>
					</div> 
				</div>

				<div class="modal fade sartpost mainpost-box" id="login_Modal" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered">
					<div class="modal-content">

						<div class="modal-body" style="height:auto;max-height:640px;overflow-y:auto;">
							<div class="row">
								<div class="col-md-12 text-center pt-3 pb-3 p-2">
									<h4>Please Login First !</h4>
									<br/>
									<a href="{{ route('front.sign_in') }}" class="btn btn-default1">LOGIN TO YOUR ACCOUNT</a>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			</main>

			
		
	@include('auth.include.footer')	

    <!--theme-layout end-->
    <script type="text/javascript" src="{{asset('frontend/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/popper.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/jquery.mCustomScrollbar.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/lib/slick/slick.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/scrollbar.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/script.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/custome.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.js"></script>
    <script type="text/javascript" src="{{ asset('frontend/js/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/ui.js')}}"></script>
    <script type="text/javascript" src="{{ asset('frontend/js/select2.min.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>

    <script>
    $(document).ready(function () {
        $('#datatable').DataTable();
    });


 //    $(document).on('click','.load_more_btn',function(){

	// 	var get_post_load_count = $(this).attr('id');
	// 	var post_load_count = parseInt(get_post_load_count);
	// 	post_load_count = post_load_count += 10;
	// 	$('.loader').show();
	// 	$.ajax({
	// 		url: "{{ route('load.more.post') }}",
	// 		headers: {
	// 			'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	// 		},
	// 		data: {
	// 		"post_load_count":post_load_count,
	// 		},
	// 		type: "POST",
	// 		success: function(result) {
	// 			$('.loader').hide();
	// 			$(this).attr('id',post_load_count);
	// 			$('.dynamic_post').html(result.html);
	// 			clearData();
	// 		} 
	// 	});
	// })
    </script>

</body>

</html>


@endif