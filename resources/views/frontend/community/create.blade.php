@extends('frontend.layouts.main')
@section('title','Community Add')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css')}}">
<style type="text/css">
    .select2-container--default .select2-selection--multiple{
        height:38px!important;
    }
    .select2-container--default.select2-container--focus .select2-selection--multiple {
        border: 1px solid #ced4da!important;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        outline: 0;
    }
    .select2-selection{
    	border-color: #ced4da!important;
    }
    .select2-search__field::placeholder{
    	color: gray;!important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__rendered{
    	width: auto!important;
    }
    .select2-container .select2-search--inline .select2-search__field {
	    color: #495057;
	    padding: 4px 12px;
	}
</style>
@endsection

@section('content')

<br/><br/>
<div class="container mt-5">

	<div class="card">
		<div class="row">
			<div class="col-md-12 p-3">
				<h2 class="h2">Add Community</h2>
			</div>
		</div>
		<hr class="m-0" />


		<div class="card-body">
			
			<form action="{{ route('front.community.store') }}" name="community_form" method="POST">
			@csrf		
				<div class="row mb-3">
					<div class="col-md-12">
						<label class="mb-2">Community Title</label>
						<input type="text" name="title" class="form-control" placeholder="Enter Community Title">
					</div>
				</div>
				<div class="row mb-3">
					<div class="col-md-12">
						<label class="mb-2">Community Description</label>
						<textarea name="description" class="form-control" placeholder="Enter Community Description"></textarea>
					</div>
				</div>
				<div class="row mb-3">
					<div class="col-md-12">
                        <label for="platforms" class="mb-2">Select Members</label>
                        <select class="form-control custfield select2" name="members[]" id="platforms" multiple="true">
                            @if($users)
                            @foreach($users as $users)
                              <option value="{{ $users->id }}">{{ $users->name ?? '' }}</option>
                            @endforeach
                          @endif
                          </select>
                          <div id="members_msg"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="{{ route('front.community.index') }}" class="btn btn-danger">Cancel</a>
					</div>
				</div>

			</form>

		</div>
	</div>

	

</div>

@endsection




@section('scripts')

<script src="{{ asset('assets/plugins/select2/js/select2.full.min.js')}}"></script>


<script type="text/javascript">
	$(".select2").attr("data-placeholder","Select Members");
	$('.select2').select2();

	$("form[name='community_form']").validate({
        rules: {
            title: 'required',
            description: 'required',
            'members[]':'required'
        },
        messages: {
        	title: 'Please enter title.',
            description: 'Please enter description.',
            'members[]':'Please select members.'
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "members[]")
                error.insertAfter("#members_msg");                       
            else
                error.insertAfter(element);
        }
    });
</script>

@endsection


