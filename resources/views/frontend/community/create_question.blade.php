@extends('frontend.layouts.main')
@section('title','Community Add')

@section('css')

@endsection

@section('content')

<br/><br/>
<div class="container mt-5">

	<div class="card">
		<div class="row">
			<div class="col-md-12 p-3">
				<h2 class="h2">Add Community Question</h2>
			</div>
		</div>
		<hr class="m-0" />


		<div class="card-body">
			
			<form action="{{ route('front.community_question.store',$community_id) }}" name="community_question_form" method="POST">
			@csrf		
				<div class="row mb-3">
					<div class="col-md-12">
						<label class="mb-2">Question</label>
						<textarea name="question_text" class="form-control" placeholder="Enter Question Description"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="{{ route('front.community.view',$community_id) }}" class="btn btn-danger">Cancel</a>
					</div>
				</div>

			</form>

		</div>
	</div>

	

</div>

@endsection




@section('scripts')
<script type="text/javascript">
	$("form[name='community_question_form']").validate({
        rules: {
            question_text: 'required'
        },
        messages: {
        	question_text: 'Please enter question description.'
        }
    });
</script>

@endsection


