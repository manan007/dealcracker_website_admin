@extends('frontend.layouts.main')
@section('title','Community Details')

@section('css')
<style>
.memebers_table{
	max-height: 400px;
	overflow: auto;
}
</style>
@endsection

@section('content')

@include('frontend.community.dynamic_answer_form')

<br/><br/>
<div class="container mt-5">

	<div class="card">
		<div class="row">
			<div class="col-md-11 p-3">
				<h2 class="h2">Community Details</h2>
			</div>
			<div class="col-md-1 p-3">
				<a class="btn btn-light" href="{{ route('front.community.index') }}">Back</a>
			</div>
		</div>
		<hr class="m-0" />


		<div class="card-body">			
			<div class="row mb-3">
				<div class="col-md-12">
					<label class="mb-2 font-weight-bold">Community Title</label>
					<p>{{ $community_details->title ?? '' }}</p>
				</div>
			</div>
			<hr/>
			<div class="row mb-3">
				<div class="col-md-12">
					<label class="mb-2 font-weight-bold">Community Description</label>
					<p>{{ $community_details->description ?? '' }}</p>
				</div>
			</div>
		</div>
	</div>

	@if(Auth::user()->id == $community_details->user_id)
	<div class="card mt-3">
		<div class="card-body">
			<div class="row mb-3">
				<div class="col-md-10">
					<label class="mb-2 font-weight-bold">Community Members List</label>
				</div>
				<div class="col-md-2">
					@php $member_array=[]; if($community_details->members){ $member_array = explode(',',$community_details->members); } @endphp
					<h4 class="mb-2 font-weight-bold">Total Members  :  <span>{{ count($member_array) ?? '' }}</span></h4>
				</div>
			</div>
			<hr/>
			<div class="row mb-3">
				<div class="col-md-12">
					<table id="datatable1" class="display" style="width:100%">
						<thead>
				            <tr>
				                <th>Member Details</th>
				            </tr>
				        </thead>
				        <tbody>
				        	@if(count($member_array) > 0)
								@foreach($member_array as $key => $data)
									@php $user_details = App\Models\User::where('id',$data)->first(); @endphp
									<tr>
										<td>
											<div class="row">
												<div class="col-xl-10 col-lg-10 col-md-10">
													@if($user_details->profile != null)
							                            <img src="{{ asset('upload/profile/'.$user_details->profile)}}" class="borderradius" alt="" width="30" height="30">
							                        @else
							                            <img src="{{ asset('frontend/images/user.jpg')}}" class="borderradius" alt="" width="30" height="30">
							                        @endif
													<h3 class="h6 font-weight-bold ml-5 pt-1">{{ $user_details->name ?? '' }}</h3>
													<!-- <span>{{ $data->description ?? '' }}</span> -->
												</div>
												<div class="col-xl-2 col-lg-2 col-md-2 text-right">
													<form action="{{URL::route('front.community_member.delete',[$community_details->id,$data])}}" method="POST" id="change_status_{{$data}}">
							                        @csrf
							                        <button type="submit" name="submit" data-id="{{$data}}"  class="hideBtn submitData_changestatus_{{$data}} d-none"><i class="ft-trash-2"></i> Submit</button>
							                        <a class="btn-sm btn-danger action-btn delete text-light" title="Delete" data-id="{{ $data }}"><i class="fas fa-trash"></i></a>
							                        </form>
												</div>
											</div>
										</td>
									</tr>
								@endforeach
							@endif
				        </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	@endif


	<div class="card mt-3 mb-4">
		<div class="card-body">
			<div class="row mb-3">
				<div class="col-md-9 pt-2">
					<label class="mb-2 font-weight-bold">Community Question List</label>
				</div>
				<div class="col-md-3 text-right">
					@php $member_array=[]; if($community_details->members){ $member_array = explode(',',$community_details->members); } @endphp
					@if(in_array(Auth::user()->id,$member_array) || Auth::user()->id == $community_details->user_id)
					<a class="btn btn-primary" href="{{ route('front.community_question.create',$community_details->id) }}">Add Community Question</a>
					@endif
				</div>
			</div>
			<hr/>
			<div class="row mb-3">
				<div class="col-md-12">
					<table id="datatable2" class="display" style="width:100%">
						<thead>
				            <tr>
				                <th>Question Details</th>
				            </tr>
				        </thead>
				        <tbody>
				        	@if(count($community_questions) > 0)
								@foreach($community_questions as $key => $data)
									@php $user_details = App\Models\User::where('id',$data->user_id)->first(); @endphp
									<tr>
										<td>
											<div class="row">
												<div class="col-xl-9 col-lg-9 col-md-9">
													@if($user_details->profile != null)
							                            <img src="{{ asset('upload/profile/'.$user_details->profile)}}" class="borderradius" alt="" width="30" height="30">
							                        @else
							                            <img src="{{ asset('frontend/images/user.jpg')}}" class="borderradius" alt="" width="30" height="30">
							                        @endif
													<h3 class="h6 font-weight-bold ml-5 pt-1">{{ $user_details->name ?? '' }}</h3>
												</div>
												<div class="col-xl-3 col-lg-3 col-md-3 text-right">
													<a href="{{ route('front.community_question.view',[$data->community_id,$data->id]) }}" class="btn-sm btn-info"><i class="fas fa-eye"></i></a>

													@if(in_array(Auth::user()->id,$member_array) || Auth::user()->id == $community_details->user_id)
													<a class="btn-sm btn-primary add_answer" href="javascript:;" data-community-id="{{ $data->community_id }}" data-community-id="{{ $data->community_id }}" data-question-id="{{ $data->id }}">Add Answer</a>
													@endif
												</div>
												<div class="col-xl-12 col-lg-12 col-md-12">
													<h3 class="h6">{{ $data->question_text ?? '' }}</h3>
												</div>
											</div>
										</td>
									</tr>
								@endforeach
							@endif
				        </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	

</div>

@endsection




@section('scripts')
<script type="text/javascript">
 $(document).on('click','.delete',function(){
    var id=$(this).data('id');
    var getval = $(this).data('value');
    bootbox.confirm("Are you sure you want to remove this community member ?", function(result){ 
        if(result == true)
        {   
          $('.submitData_changestatus_'+id).trigger('click');
        }
    });
});


$(document).on('click','.add_answer',function(){
	var community_id = $(this).attr('data-community-id');
	var question_id = $(this).attr('data-question-id');
	$.ajax({
		url: "{{ route('edit.answer.view') }}",
		headers: {
			'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
		},
		type: "GET",
		data: {
			community_id:community_id,
			question_id:question_id
		},
		success: function(result) {
			$('#dynamic_add_answer_data').html(result.html);
			$('.store_answer').attr('id',result.question_details.id);
			$('#add-answer').modal({show:true});
		} 
	});
});

$(document).on('click','.store_answer',function(){

	var question_id = $(this).attr('id');

	var answer_text = $('#add_answer_text_'+question_id).val();
	var community_id = $('#add_answer_community_id_'+question_id).val();
	var question_id = $('#add_answer_question_id_'+question_id).val();

	if(answer_text == ''){
		alert('Please enter answer.');
		return false;
	}
	else{
		$('.loader').show();
		$.ajax({
			url: "{{ route('store.answer') }}",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			},
			type: "POST",
			data:{
				'question_text':answer_text,
				'community_id':community_id,
				'question_id':question_id
			},
			success: function(result) {
				$('.loader').hide();
				if(result.status == true){
					toastr.success(result.msg);
				}
				//$('.dynamic_name_section').html(result.html);
				$('#add-answer').modal('hide');
			} 
		});
			
	}

	
});


</script>
@endsection


