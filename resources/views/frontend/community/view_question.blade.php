@extends('frontend.layouts.main')
@section('title','Community Question Details')

@section('css')
<style>
.memebers_table{
	max-height: 400px;
	overflow: auto;
}
</style>
@endsection

@section('content')

<br/><br/>
<div class="container mt-5">

	<div class="card">
		<div class="row">
			<div class="col-md-11 p-3">
				<h2 class="h2">Community Question Details</h2>
			</div>
			<div class="col-md-1 p-3">
				<a class="btn btn-light" href="{{ route('front.community.view',$community_id) }}">Back</a>
			</div>
		</div>
		<hr class="m-0" />


		<div class="card-body">			
			<div class="row mb-3">
				<div class="col-md-12">
					<label class="mb-2 font-weight-bold">Question</label>
					<h6 class="h6">{{ $question_details->question_text ?? '' }}</h6>
				</div>
			</div>
			<hr/>
		</div>
	</div>

	<div class="card mt-3 mb-4">
		<div class="card-body">
			<div class="row mb-3">
				<div class="col-md-9 pt-2">
					<label class="mb-2 font-weight-bold">Answer List</label>
				</div>
				<div class="col-md-3 text-right">
					@php $member_array=[]; if($community_details->members){ $member_array = explode(',',$community_details->members); } @endphp
					@if(in_array(Auth::user()->id,$member_array) || Auth::user()->id == $community_details->user_id)
					<a class="btn btn-primary" href="{{ route('front.community_answer.create',[$community_details->id,$question_details->id]) }}">Add Answer</a>
					@endif
				</div>
			</div>
			<hr/>
			<div class="row mb-3">
				<div class="col-md-12">
					<table id="datatable" class="display" style="width:100%">
						<thead>
				            <tr>
				                <th>Answer Details</th>
				            </tr>
				        </thead>
				        <tbody>
				        	@if(count($answer_details) > 0)
								@foreach($answer_details as $key => $data)
									@php $user_details = App\Models\User::where('id',$data->user_id)->first(); @endphp
									<tr>
										<td>
											<div class="row">
												<div class="col-xl-11 col-lg-11 col-md-11">
													@if($user_details->profile != null)
							                            <img src="{{ asset('upload/profile/'.$user_details->profile)}}" class="borderradius" alt="" width="30" height="30">
							                        @else
							                            <img src="{{ asset('frontend/images/user.jpg')}}" class="borderradius" alt="" width="30" height="30">
							                        @endif
													<h3 class="h6 font-weight-bold ml-5 pt-1">{{ $user_details->name ?? '' }}</h3>
													<h3 class="h6">{{ $data->question_text ?? '' }}</h3>
												</div>

												<div class="col-xl-1 col-lg-1 col-md-1 text-right">
													@if($community_details->user_id == Auth::user()->id)
													<form action="{{URL::route('front.community.answer.delete',[$community_details->id,$question_details->id,$data->id])}}" method="POST" id="submitData_deleteanswer_{{$data->id}}">
							                        @csrf
							                        <button type="submit" name="submit" data-id="{{$data->id}}"  class="hideBtn submitData_deleteanswer_{{$data->id}} d-none"><i class="ft-trash-2"></i> Submit</button>
							                        <a class="btn-sm btn-danger action-btn delete_answer text-light" title="Delete" data-id="{{ $data->id }}"><i class="fas fa-trash"></i></a>
							                        </form>
							                        @endif
												</div>
											</div>
										</td>
									</tr>
								@endforeach
							@endif
				        </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>



	

</div>

@endsection




@section('scripts')
<script type="text/javascript">
$(document).on('click','.delete',function(){
    var id=$(this).data('id');
    var getval = $(this).data('value');
    bootbox.confirm("Are you sure you want to remove this community member ?", function(result){ 
        if(result == true)
        {   
          $('.submitData_changestatus_'+id).trigger('click');
        }
    });
});

$(document).on('click','.delete_answer',function(){
    var id=$(this).data('id');
    var getval = $(this).data('value');
    bootbox.confirm("Are you sure you want to delete this answer ?", function(result){ 
        if(result == true)
        {   
          $('.submitData_deleteanswer_'+id).trigger('click');
        }
    });
});

 
</script>
@endsection


