<div class="row mb-3">
    <div class="col-md-12">
        <input type="hidden" name="add_answer_community_id" id="add_answer_community_id_{{ $question_details->id }}" value="{{ $community_id }}">
        <input type="hidden" name="add_answer_question_id" id="add_answer_question_id_{{ $question_details->id }}" value="{{ $question_details->id }}">
        <h6 class="h6 font-weight-bold">{{ $question_details->question_text ?? '' }}</h6>
        <textarea name="question_text" class="form-control" id="add_answer_text_{{ $question_details->id }}" placeholder="Enter Answer Description"></textarea>
    </div>
</div>

