@extends('frontend.layouts.main')
@section('title','Community Add Answer')

@section('css')

@endsection

@section('content')

<br/><br/>
<div class="container mt-5">

	<div class="card">
		<div class="row">
			<div class="col-md-12 p-3">
				<h2 class="h2">Add Answer</h2>
			</div>
		</div>
		<hr class="m-0" />


		<div class="card-body">
			
			<form action="{{ route('front.community_answer.store',[$community_id,$question_id]) }}" name="community_answer_form" method="POST">
			@csrf		
				<div class="row mb-3">
					<div class="col-md-12">
						<label class="mb-2">Answer</label>
						<textarea name="question_text" class="form-control" placeholder="Enter Answer Description"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="{{ route('front.community_question.view',[$community_id,$question_id]) }}" class="btn btn-danger">Cancel</a>
					</div>
				</div>

			</form>

		</div>
	</div>

	

</div>

@endsection




@section('scripts')
<script type="text/javascript">
	$("form[name='community_answer_form']").validate({
        rules: {
            question_text: 'required'
        },
        messages: {
        	question_text: 'Please enter answer description.'
        }
    });
</script>

@endsection


