@extends('frontend.layouts.main')
@section('title','Business')

@section('css')
<style>

</style>
@endsection

@section('content')

<br/><br/>
<div class="container mt-5">

	<div class="card">
		<div class="row">
			<div class="col-md-9 p-3">
				<h2 class="h2">Business</h2>
			</div>
			<div class="col-md-3 p-3 text-right">
				<a class="btn btn-primary" href="{{ route('front.business.create') }}">Add New Business</a>
			</div>
		</div>
		<hr class="m-0" />


		<div class="card-body">
			
			<table id="datatable" class="display" style="width:100%">
				<thead>
		            <tr>
		                <th>Business</th>
		            </tr>
		        </thead>
		        <tbody>
		        	@if(count($business) > 0)
						@foreach($business as $key => $data)
							<tr>
								<td>
									<div class="row">
										<div class="col-xl-10 col-lg-10 col-md-10">
											<h3 class="h6 font-weight-bold">{{ $data->title ?? '' }}</h3>
											<span>{{ $data->description ?? '' }}</span>
										</div>
										<div class="col-xl-2 col-lg-2 col-md-2 text-right actionbtns">
											@if(Auth::user()->id == $data->user_id)
											<a href="{{ route('front.business.view',$data->id) }}" class="btn-sm btn-info"><i class="fas fa-eye"></i></a>
											<a href="{{ route('front.business.edit',$data->id) }}" class="btn-sm btn-success"><i class="fas fa-pen"></i></a>

											<form action="{{URL::route('front.business.delete',$data->id)}}" method="POST" id="change_status_{{$data->id}}">
					                        @csrf
					                        <button type="submit" name="submit" data-id="{{$data->id}}"  class="hideBtn submitData_changestatus_{{$data->id}} d-none"><i class="ft-trash-2"></i> Submit</button>
					                        <a class="btn-sm btn-danger action-btn delete text-light" title="Delete" data-id="{{ $data->id }}"><i class="fas fa-trash"></i></a>
					                        </form>
					                        @else
					                        <a href="{{ route('user',$data->user_id) }}" class="btn-sm btn-info">Send Message</a>
					                        @endif
										</div>
									</div>
								</td>
							</tr>
						@endforeach
					@endif
		        </tbody>
			</table>

		</div>
	</div>

</div>

@endsection




@section('scripts')
<script type="text/javascript">
 $(document).on('click','.delete',function(){
    var id=$(this).data('id');
    var getval = $(this).data('value');
    bootbox.confirm("Are you sure you want to delete this business ?", function(result){ 
        if(result == true)
        {   
          $('.submitData_changestatus_'+id).trigger('click');
        }
    });
});
</script>
@endsection















				
<!-- <div class="row">
	<div class="col-xl-10 col-lg-10 col-md-10">
		<h3 class="h6 font-weight-bold">Business Title</h3>
		<span>Business Description</span>
	</div>
	<div class="col-xl-2 col-lg-2 col-md-2 text-right">
		<a href="" class="btn-sm btn-info"><i class="fas fa-eye"></i></a>
		<a href="" class="btn-sm btn-success"><i class="fas fa-pen"></i></a>
		<a href="#" class="btn-sm btn-danger"><i class="fas fa-trash"></i></a>
	</div>
</div>
<hr/> -->
