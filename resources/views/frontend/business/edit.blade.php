@extends('frontend.layouts.main')
@section('title','Business Edit')

@section('css')
<style>

</style>
@endsection

@section('content')

<br/><br/>
<div class="container mt-5">

	<div class="card">
		<div class="row">
			<div class="col-md-12 p-3">
				<h2 class="h2">Edit Business</h2>
			</div>
		</div>
		<hr class="m-0" />


		<div class="card-body">
			
			<form action="{{ route('front.business.update',$business_details->id) }}" name="business_form" method="POST">
			@csrf
					
				<div class="row mb-3">
					<div class="col-md-12">
						<label class="mb-2">Business Title</label>
						<input type="text" name="title" class="form-control" placeholder="Enter Business Title" value="{{ $business_details->title ?? '' }}">
					</div>
				</div>
				<div class="row mb-3">
					<div class="col-md-12">
						<label class="mb-2">Business Description</label>
						<textarea name="description" class="form-control" placeholder="Enter Business Description" value="{{ $business_details->description ?? '' }}">{{ $business_details->description ?? '' }}</textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="{{ route('front.business.index') }}" class="btn btn-danger">Cancel</a>
					</div>
				</div>

			</form>

		</div>
		
	</div>

</div>

@endsection




@section('scripts')

<script type="text/javascript">
	$("form[name='business_form']").validate({
        rules: {
            title: 'required',
            description: 'required'
        },
        messages: {
        	title: 'Please enter title.',
            description: 'Please enter description.'
        }
    });
</script>


@endsection


