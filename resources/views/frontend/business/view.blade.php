@extends('frontend.layouts.main')
@section('title','Business Details')

@section('css')
<style>

</style>
@endsection

@section('content')

<br/><br/>
<div class="container mt-5">

	<div class="card">
		<div class="row">
			<div class="col-md-11 p-3">
				<h2 class="h2">Business Details</h2>
			</div>
			<div class="col-md-1 p-3">
				<a class="btn btn-light" href="{{ route('front.business.index') }}">Back</a>
			</div>
		</div>
		<hr class="m-0" />


		<div class="card-body">
					
			<div class="row mb-3">
				<div class="col-md-12">
					<label class="mb-2">Business Title</label>
					<p>{{ $business_details->title ?? '' }}</p>
				</div>
			</div>
			<hr/>
			<div class="row mb-3">
				<div class="col-md-12">
					<label class="mb-2">Business Description</label>
					<p>{{ $business_details->description ?? '' }}</p>
				</div>
			</div>

		</div>
	</div>

	

</div>

@endsection




@section('scripts')

@endsection


