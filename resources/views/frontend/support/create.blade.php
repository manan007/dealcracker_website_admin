@extends('frontend.layouts.main')
@section('title','Support - Add Question')

@section('css')
<style>

</style>
@endsection

@section('content')

<br/><br/>
<div class="container mt-5">

	<div class="card">
		<div class="row">
			<div class="col-md-11 p-3">
				<h2 class="h2">Support - Add Question</h2>
			</div>
			<div class="col-md-1 p-3">
				<a class="btn btn-light" href="{{ route('front.support.index') }}">Back</a>
			</div>
		</div>
		<hr class="m-0" />


		<div class="card-body">
			
			<form action="{{ route('front.support.store') }}" name="business_form" method="POST">
			@csrf
					
				<div class="row mb-3">
					<div class="col-md-12">
						<label class="mb-2">Question</label>
						<input type="text" name="question" class="form-control" placeholder="Enter Question">
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>

			</form>

		</div>
	</div>

	

</div>

@endsection




@section('scripts')
<script type="text/javascript">
	$("form[name='business_form']").validate({
        rules: {
            question: 'required'
        },
        messages: {
        	question: 'Please enter question.'
        }
    });
</script>
@endsection


