@extends('frontend.layouts.main')
@section('title','Support')

@section('css')
<style>

</style>
@endsection

@section('content')

<br/><br/>
<div class="container mt-5">

	<div class="card">
		<div class="row">
			<div class="col-md-10 p-3">
				<h2 class="h2">Support Details</h2>
			</div>
			<div class="col-md-2 p-3 text-right">
				<a class="btn btn-primary" href="{{ route('front.support.create') }}">Add New Question</a>
			</div>
		</div>
		<hr class="m-0" />


		<div class="card-body">
			

			<table id="datatable" class="display" style="width:100%">
				<thead>
		            <tr>
		                <th>Support Questions</th>
		            </tr>
		        </thead>
		        <tbody>
		        	@if(count($support_questions) > 0)
						@foreach($support_questions as $key => $data)
							<tr>
								<td>
									<div class="row">
										<div class="col-xl-12 col-lg-12 col-md-12">
											<h3 class="h6 font-weight-bold">{{ $data->question ?? '' }}</h3>
											<span>{{ $data->answer ?? ' ' }}</span>
										</div>
									</div>
								</td>
							</tr>
						@endforeach
					@endif
		        </tbody>
			</table>

	

		</div>
	</div>

	

</div>

@endsection




@section('scripts')

@endsection





































































			<!-- <div class="row">
				<div class="col-md-12">
					<h3 class="h6 font-weight-bold">How are you?</h3>
					<span>I am fine..!</span>
				</div>
			</div> -->
		