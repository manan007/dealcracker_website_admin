
@extends('auth.include.main')
@section('title','Contact Us')
@section('css')
@endsection

@section('content')
<!-- <section class="breatcome_area" style="background-image: url('{{ asset('assets/frontend/images/slider/slide1.jpg')}}');">
		<div class="container">
			<h4>About Us</h4>
		</div>
	</section> -->
	<section class="commform regstep contactus">
		<div class="container">

			<div class="titlebox">
				<h5>Please Fill Out The Form Below and A Representative Will Contact You Shortly</h5>
			<br>
<p><strong>Email</strong>:- <a data-fr-linked="true" href="mailto:dealcrackertm@gmail.com">dealcrackertm@gmail.com</a></p>
<p><strong>Address</strong>: -&nbsp; A-1102, Revell Orchid, Porwal Road, Lohegaon, Pune-411047</p>
			</div>
			<div class="regbox">
				<form action="{{ route('setting.store.contactus') }}" method="post" name="contact_us">
				@csrf
				  	<div class="row">
				  		<div class="form-group col-md-12">
						    <label>Name</label>
						    <input type="text" id="name" name="name" class="form-control custfield" placeholder="Enter Name">
					    </div>
					</div>
					<div class="row">
				  		<div class="form-group col-md-6">
						    <label>Email</label>
						    <input type="text" id="email" name="email" class="form-control custfield" placeholder="Enter Email">
					    </div>
					    <div class="form-group col-md-6">
						    <label>Mobile Number</label>
						    <input type="text" id="phone" name="phone" class="form-control custfield" placeholder="Enter Mobile Number">
					    </div>
					</div>
					<div class="row">
				  		<div class="form-group col-md-12">
						    <label>Message</label>
						    <textarea class="form-control custfield custarea" name="message" placeholder="Enter Message Description" id="comments"></textarea>
					    </div>
					</div>
				  		
				  	<div class="row">
				  		<div class="form-group col-md-4 text-left">
				  			<button type="submit" class="cbttn">Submit</button>
				  		</div>
				  	</div>

				</form>
			</div>
		</div>
	</section>
@endsection
@section('scripts')

<script type="text/javascript">
	$("form[name='contact_us']").validate({
        rules: 
        {
            name: {
                required: true,
            },
            email: {
                required: true,
            },
            phone: {
                required: true,
            },
            message: {
                required: true,
            }
            
        },
        messages: 
        {

        	name: {
                required: "Please enter name.",
            },
            email: {
                required: "Please enter email.",
            },
            phone: {
                required: "Please enter mobile number.",
            },
            message: {
                required: "Please enter message.",
            }
        }
    });
</script>

@endsection	
    
