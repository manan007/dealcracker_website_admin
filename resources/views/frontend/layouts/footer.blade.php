<footer>
    <div class="footy-sec mn no-margin">
        <div class="container">
            <ul>
                <!-- <li><a href="">Privacy Policy</a></li> -->
				<li><a href="{{ route('home.legal') }}">Legal Disclaimers</a></li>
                <!-- <li><a href="{{ route('home.about_us') }}" title="">About Us</a></li> -->
                <li><a href="{{ route('home.how_esportsrecruiter_work') }}" title="">How It Works</a></li>
                <li><a href="{{ route('home.terms_conditions') }}" title="">Terms & Conditions</a></li>
                <!-- <li><a href="#" title="">Join Now</a></li> -->
                <li><a href="{{route('home.contact_us')}}" title="">Contact Us</a></li>
                <li><a href="{{route('home.sitemap')}}" title="">Site Map</a></li>
                <!--    Home | About Us | How It Works | Terms & Conditions | Join Now | Contact Us | Site Map -->
            </ul>
            <p><img src="{{ asset('assets/frontend/images/copy-icon2.png') }}" alt="">Copyright {{ date('Y') }}</p>
        </div>
    </div>
</footer><!--footer end-->