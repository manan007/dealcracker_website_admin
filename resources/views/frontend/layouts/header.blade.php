
<header>
    <div class="container">
        <div class="header-data customehead">
            <div class="logo">
                <!-- <a href="{{ route('front.index') }}" title=""><h1 style="font-size: 34px;color: #fff;font-weight: bold;">DC</h1></a> -->
                <a href="{{ route('front.index') }}" title=""><img src="{{asset('frontend/images/logo.png')}}" alt=""></a>
            </div>
            <!--logo end-->
            <!-- <div class="search-bar">
                <form action="#" method="GET" id="searching_form">
                    <input type="text" name="search" placeholder="Search..." class="" id="user_search">
                    <button type="button"><i class="la la-search"></i></button>
                </form>
            </div> -->
            <!--search-bar end-->
            <nav>
                <ul>
                    <li><a href="{{route('front.index')}}" title="Home"><span><i class="fas fa-home"></i></span>Home</a></li>
                    
                    <li><a href="{{ url('/messages') }}" title="Messages" class="not-box-openm"><span><i class="fas fa-envelope"></i></span>Messages</a></li>

                    <!-- <li><a href="{{ route('front.opportunity.index') }}" title="Opportunity" class="not-box-openm"><span><i class="fas fa-futbol-o"></i></span>Opportunity</a></li> -->

                    <li><a href="{{ route('front.business.index') }}" title="Business" class="not-box-openm"><span><i class="fas fa-building"></i></span>Business</a></li>

                    <li><a href="{{ route('front.business_template.index') }}" title="Business Template" class="not-box-openm"><span><i class="fas fa-list-alt"></i></span>Opportunity</a></li>

                    <!-- <li><a href="{{ route('front.blog.index') }}" title="Business Template" class="not-box-openm"><span><i class="fas fa-blog"></i></span>Blogs </a></li> -->
                    
                    <li><a href="{{ route('front.community.index') }}" title="Community" class="not-box-openm"><span><i class="fas fa-users"></i></span>Community </a></li>

                    {{--
                    <li>
                       <a href="#" title="" class="not-box-open">
                                    <span><i class="fas fa-bell"></i></span>
                                    Notification
                                </a>
                                <div class="notification-box noti" id="notification">
                                    <div class="nt-title">
                                        <h4>Setting</h4>
                                        <a href="#" title="">Clear all</a>
                                    </div>
                                    <div class="nott-list">
                                        <div class="notfication-details">
                                            <div class="noty-user-img">
                                                <img src="{{ asset('frontend/images/resources/ny-img1.png')}}" alt="">
                                            </div>
                                            <div class="notification-info">
                                                <h3><a href="#" title="">Jassica William</a> Comment on your project.</h3>
                                                <span>2 min ago</span>
                                            </div><!--notification-info -->
                                        </div>
                                        <div class="notfication-details">
                                            <div class="noty-user-img">
                                                <img src="{{ asset('frontend/images/resources/ny-img2.png')}}" alt="">
                                            </div>
                                            <div class="notification-info">
                                                <h3><a href="#" title="">Jassica William</a> Comment on your project.</h3>
                                                <span>2 min ago</span>
                                            </div><!--notification-info -->
                                        </div>
                                        <div class="notfication-details">
                                            <div class="noty-user-img">
                                                <img src="{{ asset('frontend/images/resources/ny-img3.png')}}" alt="">
                                            </div>
                                            <div class="notification-info">
                                                <h3><a href="#" title="">Jassica William</a> Comment on your project.</h3>
                                                <span>2 min ago</span>
                                            </div><!--notification-info -->
                                        </div>
                                        <div class="notfication-details">
                                            <div class="noty-user-img">
                                                <img src="{{ asset('frontend/images/resources/ny-img2.png')}}" alt="">
                                            </div>
                                            <div class="notification-info">
                                                <h3><a href="#" title="">Jassica William</a> Comment on your project.</h3>
                                                <span>2 min ago</span>
                                            </div><!--notification-info -->
                                        </div>
                                        <div class="view-all-nots">
                                            <a href="{{ route('front.notification.index') }}" title="">View All Notification</a>
                                        </div>
                                    </div><!--nott-list end-->
                                </div><!--notification-box end-->
                        
                    </li>
                    --}}
                    <!-- <li>
                        <a href="#" title="">
                            <span><i class="fas fa-user-friends"></i></span>
                            Invite Users
                        </a>
                    </li> -->
                </ul>
            </nav>
            <!--nav end-->
            <div class="menu-btn">
                <a href="#" title=""><i class="fa fa-bars"></i></a>
            </div>
            <!--menu-btn end-->
            <div class="user-account">
                <div class="user-info">
                    @if(Auth::check())    
                        @if(Auth::user()->profile != null)
                            <img src="{{ asset('upload/profile/'.Auth::user()->profile)}}" class="borderradius" alt="" width="30" height="30">
                        @else
                            <img src="{{ asset('frontend/images/user.jpg')}}" class="borderradius" alt="" width="30" height="30">
                            {{-- @php $firstCharacter = substr(Auth::user()->name, 0, 1); @endphp
                            <span class="borderradius header_profile">{{ $firstCharacter }}</span> --}}
                        @endif
                    @endif
                    <a href="#" title="">{{ Auth::user()->name }}</a>
                    <i class="la la-sort-down"></i>
                </div>
                <div class="user-account-settingss" id="users">

                    {{--@if(Auth::check())
                        <ul class="us-links p-2"><li><a href="#" class='link_label font-weight-bold'><h4 class="h4 mb-0">{{ Auth::user()->name ?? ''}}</h4></a></li></ul>
                    @endif--}}

                    <ul class="us-links">
                        <li><a href="{{ route('front.my_profile.index',Auth::user()->id) }}" title="">Profile</a></li>
                        <li><a href="{{ route('front.setting.index') }}" title="">Settings</a></li>
                        <li><a href="{{ route('front.support.index') }}" title="">Support</a></li>

                        <!-- For Footer -->
                        <!--
                        <li><a href="#" title="">Contact Us Form</li>
                        <li><a href="#" title="">About US</li>
                        <li><a href="#" title="">Terms and Conditions</a></li>
                        <li><a href="#" title="">Privacy Policy</a></li>-->
                    </ul>
                    
                    <!-- <div class="plan-btn-wrapper"><a href="" class="upgradeplan">Upgrade Plan</a></div> -->
                    <form action="{{route('front.logout')}}" method="GET" id="logout">
                    @csrf
                    <button type="submit" name="submit" data-id="logout"  class="hideBtn submitData_changestatus_logout d-none"><i class="ft-trash-2"></i> Submit</button>
                    <a href="" class="action-btn delete_logout p-3" title="Delete" data-id="logout"><h3 class="tc">Logout</h3></a>
                    </form>

                    <!-- <a href=""><h3 class="tc"><a href="" title="">Logout</a></h3></a> -->
                    
                </div>
                <!--user-account-settingss end-->
            </div>
        </div>
        <!--header-data end-->
    </div>
</header>
