<div class="chat-hist mCustomScrollbar" data-mcs-theme="dark" style="overflow-y: scroll; height: 20rem;">
    <!-- <div class="chat-msg">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum congue leo eget
            malesuada. Vivamus suscipit tortor eget felis porttitor.</p>
        <span>Sat, Aug 23, 1:10 PM</span>
    </div>
    <div class="date-nd">
        <span>Sunday, August 24</span>
    </div>
    <div class="chat-msg st2">
        <p>Cras ultricies ligula.</p>
        <span>5 minutes ago</span>
    </div>
    <div class="chat-msg">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum congue leo eget
            malesuada. Vivamus suscipit tortor eget felis porttitor.</p>
        <span>Sat, Aug 23, 1:10 PM</span>
    </div> -->

    @if(count($getchats) > 0)
        @foreach($getchats as $getchat)
            @if(Auth::user()->id == $getchat->from_id)
                <div class="chat-msg">
                    <p>{{ $getchat->message }}</p>
                    <span>Sat, Aug 23, 1:10 PM</span>
                </div>      
            @else
                <div class="chat-msg st2">
                    <p>{{ $getchat->message }}</p>
                    <span>5 minutes ago</span>
                </div>
            @endif

        @endforeach
    @else   
    @endif

</div>