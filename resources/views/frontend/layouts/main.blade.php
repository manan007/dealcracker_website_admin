<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>DealCracker - @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="" />
    <meta name="_token" content="{{ csrf_token() }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta name="keywords" content="" />

    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/line-awesome.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0-2/css/fontawesome.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/line-awesome-font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/fontawesome-free/css/all.min.css')}}"> -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/jquery.mCustomScrollbar.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/lib/slick/slick.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/lib/slick/slick-theme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/custome.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/custome1.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/ui.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.0/css/jquery.dataTables.min.css">
    <style>
        .modal { overflow: auto !important; }
        /* Chat Msg Box Css */
        .loader{
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url("{{ asset('frontend/images/loader.gif') }}") 
                    50% 50% no-repeat rgba(0,0,0,0.5);
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover{
            background: #1E2C75;
            color: #ffffff!important;
            border-color: #1E2C75;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            background: #1E2C75;
            color: #ffffff!important;
            border-color: #1E2C75;
        }
        .bootbox-confirm .close{
            display: none;
        }
    </style>
  
    @yield('css')
</head>
    <div class="loader" style="display:none;"></div>
    <body>
        <input type="hidden" name="hidden_id" class="hidden_id" value="" />
        <div class="wrapper">
            <!--header start-->
                @include('frontend.layouts.header')
            <!--header end-->
            @yield('content')
        </div>
        <!--theme-layout end-->
        <script type="text/javascript" src="{{asset('frontend/js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/popper.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/jquery.mCustomScrollbar.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/lib/slick/slick.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/scrollbar.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/script.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/custome.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.js"></script>
        <script type="text/javascript" src="{{ asset('frontend/js/jquery.validate.min.js') }}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/ui.js')}}"></script>
        <script type="text/javascript" src="{{ asset('frontend/js/select2.min.js') }}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>
    </body>
    <!-- Jquary Start -->
    <script>
    $(function() {
        @if(session('success'))        
            toastr.success("{{session('success')}}");
        @endif
        @if(session('error'))
            toastr.error("{{session('error')}}");                    
        @endif
    });
    
        
    $(document).find(".alert").delay(8000).fadeOut("slow");

    $(document).on('click', '.coach_form', function(){
        $("#notification").hide();
        $(".user-account-settingss").hide();
    });

    $(document).on('click', '.card', function(){
        $("#notification").hide();
        $(".user-account-settingss").hide();
    });

    $(document).on('click', '.profile-account-setting', function(){
        $("#notification").hide();
        $(".user-account-settingss").hide();
    });
    
    

    function isOnlyNumber(evt) {

        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    $(document).on('click','.delete_logout',function(e){
        e.preventDefault();
        var id=$(this).data('id');
        var getval = $(this).data('value');

        bootbox.dialog({
            message: "Are you sure want to logout ?",
            title: "Logout",
            onEscape: function() {},
            show: true,
            backdrop: true,
            closeButton: true,
            animate: true,
            className: "sartpost",
            buttons: {
                "Danger!": {
                    label:"No",
                    className: "btn-cancel",
                    callback: function() {}
                },
                success: {   
                    label: "Yes",
                    className: "btn-default1 text-right",
                    callback: function() {
                        $('.submitData_changestatus_'+id).trigger('click'); 
                    },
                    
                }
            }
        });


        // bootbox.confirm("Are you sure want to logout ?", function(result){ 
        //     if(result == true)
        //     {   
        //       $('.submitData_changestatus_'+id).trigger('click');
        //     }
        // });
    });

    $(document).ready(function () {
        $('#datatable').DataTable();
        // $('#datatable1').DataTable();

        $('#datatable1').DataTable({
            pageLength : 5,
            lengthMenu: [[5, 10, 20], [5, 10, 20]]
        });

        $('#datatable2').DataTable();
    });

    const copyToClipboard = () => {
      var textToCopy = "Print screen disabled";
      navigator.clipboard.writeText(textToCopy);
    }

    $(window).keyup((e) => {
        if (e.keyCode == 44) {
            setTimeout(copyToClipboard(),1000);
        }
    });

        
    </script>
    @yield('scripts')
</html>
