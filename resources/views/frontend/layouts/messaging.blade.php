
    {{-- <div class="chatbox">
        <div class="chat-mg">
            <a href="#" title=""><img src="{{ asset('assets/frontend/images/resources/usr-img1.png') }}" alt=""></a>
            <span>2</span>
        </div>
        <div class="conversation-box">
            <div class="con-title mg-3">
                <div class="chat-user-info">
                    <img src="{{ asset('assets/frontend/images/resources/us-img1.png') }}" alt="">
                    <h3>John Doe <span class="status-info"></span></h3>
                </div>
                <div class="st-icons">
                    <a href="#" title=""><i class="la la-cog"></i></a>
                    <a href="#" title="" class="close-chat"><i class="la la-minus-square"></i></a>
                    <a href="#" title="" class="close-chat"><i class="la la-close"></i></a>
                </div>
            </div>
            <div class="chat-hist mCustomScrollbar" data-mcs-theme="dark" style="overflow-y: scroll; height: 20rem;">
                <div class="chat-msg">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum congue leo eget
                        malesuada. Vivamus suscipit tortor eget felis porttitor.</p>
                    <span>Sat, Aug 23, 1:10 PM</span>
                </div>
                <div class="date-nd">
                    <span>Sunday, August 24</span>
                </div>
                <div class="chat-msg st2">
                    <p>Cras ultricies ligula.</p>
                    <span>5 minutes ago</span>
                </div>
                <div class="chat-msg">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum congue leo eget
                        malesuada. Vivamus suscipit tortor eget felis porttitor.</p>
                    <span>Sat, Aug 23, 1:10 PM</span>
                </div>
            </div>
            <!--chat-list end-->
            <div class="typing-msg" style="background: #fff">
                <form>
                    <textarea placeholder="Type a message here"></textarea>
                    <button type="button" class=""><i class="fa fa-send"></i></button>
                </form>
                <ul class="ft-options">
                    <li><a href="#" title=""><i class="la la-smile-o"></i></a></li>
                    <li><a href="#" title=""><i class="la la-camera"></i></a></li>
                    <li><a href="#" title=""><i class="fa fa-paperclip"></i></a></li>
                </ul>
            </div>
            <!--typing-msg end-->
        </div>

        <!--chat-history end-->
    </div> --}}
    
<div class="chatbox-list" style="right: 381px;">
    @if (count($user_chat) > 0)

        @foreach ($user_chat as $key => $item)
            
            <div class="chatbox userper_chat" id="{{ $item->id }}">
                <div class="chat-mg">
                    <a href="#" title=""><img src="{{ asset('assets/frontend/images/resources/usr-img2.png') }}"
                            alt=""></a>
                    <span>{{$item->name ?? ''}}</span>
                </div>
                <div class="conversation-box">
                    <div class="con-title mg-3">
                        <div class="chat-user-info">
                            <img src="{{ asset('assets/frontend/images/resources/us-img1.png') }}" alt="">
                            <h3>{{ $item->name ?? '' }}<span class="status-info"></span></h3>
                        </div>
                        <div class="st-icons">
                            <a href="#" title=""><i class="la la-cog"></i></a>
                            <a href="#" title="" class="close-chat"><i class="la la-minus-square"></i></a>
                            <a href="#" title="" class="close-chat"><i class="la la-close"></i></a>
                        </div>
                    </div>

                    <div class="dynamic_chatbox" id="dynamic_chatbox_{{ $item->id }}">
                        @include('layouts.frontend.dynamic_chatbox')
                    </div>
                    <!--chat-list end-->
                    <div class="typing-msg" style="background: #fff">
                        <form>
                            <textarea placeholder="Type a message here" name="chatmsg" class="chatmsg" id="chatmsg_{{ $item->id }}"></textarea>
                            <button type="button" class="submit_btn" data-to-id="{{ $item->id }}"><i class="fa fa-send"></i></button>
                        </form>
                        <ul class="ft-options">
                            <li><a href="#" title=""><i class="la la-smile-o"></i></a></li>
                            <li><a href="#" title=""><i class="la la-camera"></i></a></li>
                            <li><a href="#" title=""><i class="fa fa-paperclip"></i></a></li>
                        </ul>
                    </div>
                    <!--typing-msg end-->
                </div>
                <!--chat-history end-->
            </div>
        @endforeach
    @endif

    <!-- Messagin Icon -->
    <div class="chatbox">
        <div class="chat-mg bx">
            <a href="#" title=""><img src="{{ asset('assets/frontend/images/chat.png') }}" alt=""></a>
            <span>2</span>
        </div>
        <div class="conversation-box">
            <div class="con-title">
                <h3>Messages</h3>
                <a href="#" title="" class="close-chat"><i class="la la-minus-square"></i></a>
            </div>
            <div class="chat-list" style="overflow-y: scroll; height: 10rem;">
                @if (count($users) > 0)
                    @foreach ($users as $user)
                        <div class="conv-list msg_user" data-user-id="{{ $user->user_id ?? '' }}" data-friend-id="{{ $user->friend_id ?? '' }}">
                            <div class="usrr-pic">
                                <img src="assets/frontend/images/resources/usy2.png" alt="">
                            </div>
                            <div class="usy-info">
                                <h3>{{ $user->user->name ?? '' }}</h3>
                                <span>Lorem ipsum dolor <img src="assets/frontend/images/smley.png" alt=""></span>
                            </div>
                            <div class="ct-time">
                                <span>11:39 PM</span>
                            </div>
                        </div>
                    @endforeach
                @endif
                {{-- <div class="conv-list active">
                    <div class="usrr-pic">
                        <img src="assets/frontend/images/resources/usy1.png" alt="">
                        <span class="active-status activee"></span>
                    </div>
                    <div class="usy-info">
                        <h3>John Doe</h3>
                        <span>Lorem ipsum dolor <img src="assets/frontend/images/smley.png" alt=""></span>
                    </div>
                    <div class="ct-time">
                        <span>1:55 PM</span>
                    </div>
                    <span class="msg-numbers">2</span>
                </div> --}}

            </div>
            <!--chat-list end-->
        </div>
        <!--conversation-box end-->
    </div>

</div>
