@extends('frontend.layouts.main')
@section('title','Settings')
@section('css')

<style>
.cuspd{
	padding: 0 10px!important;
}
</style>
@endsection

@section('content')

<section class="profile-account-setting">
	<div class="container">
		<div class="account-tabs-setting">
			<div class="row">
				<div class="col-lg-3">
					<div class="acc-leftbar">
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<a class="nav-item nav-link active" id="nav-acc-tab" data-toggle="tab" href="#nav-acc" role="tab" aria-controls="nav-acc" aria-selected="true"><i class="la la-cogs"></i>Account Setting</a>
						    <a class="nav-item nav-link" id="nav-password-tab" data-toggle="tab" href="#nav-password" role="tab" aria-controls="nav-password" aria-selected="false"><i class="fa fa-lock"></i>Change Password</a>
						    <a class="nav-item nav-link" id="nav-plan-detail-tab" data-toggle="tab" href="#nav-plan-detail" role="tab" aria-controls="nav-deactivate" aria-selected="false"><i class="fa fa-money"></i>Plan Details</a>
						    <a class="nav-item nav-link" id="nav-contactus-detail-tab" data-toggle="tab" href="#nav-contact-us" role="tab" aria-controls="nav-deactivate" aria-selected="false"><i class="fa fa-phone"></i>Contact Us</a>
						  </div>
					</div><!--acc-leftbar end-->
				</div>
				<div class="col-lg-9">
					<div class="tab-content" id="nav-tabContent">


						<div class="tab-pane fade show active" id="nav-acc" role="tabpanel" aria-labelledby="nav-acc-tab">
							<div class="acc-setting">
								<h3>Account Setting</h3>
								<form action="{{ route('store.account.setting') }}" method="post">
									@csrf
									<!--notbar end-->
									<div class="notbar">
										<h4>Send Chat Message Over Email</h4>
										<div class="toggle-btn">
											<div class="custom-control custom-switch">
												<input type="checkbox" name="send_chat_message_over_email" class="custom-control-input" id="customSwitch2" 
												@if(Auth::user()->send_chat_message_over_email != null) @if(Auth::user()->send_chat_message_over_email == 1){{"checked"}} @endif @endif>
												<label class="custom-control-label" for="customSwitch2"></label>
											</div>
										</div>
									</div><!--notbar end-->
									<div class="save-stngs">
										<ul>
											<li><button type="submit">Save Setting</button></li>
										</ul>
									</div><!--save-stngs end-->
								</form>
							</div><!--acc-setting end-->
						</div>

						<!-- ============================== Change Password Start ========================= -->
					  	<div class="tab-pane fade" id="nav-password" role="tabpanel" aria-labelledby="nav-password-tab">
					  		<div class="acc-setting">
								<h3>Change Password</h3>
								<form action="{{ route('change.password') }}" method="post" name="change_password">
                                @csrf 
									<div class="cp-field">
										<h5>Old Password</h5>
										<div class="cpp-fiel">
											<input type="password" name="old_password" placeholder="Old Password" id="old_password">
											<i class="fa fa-lock"></i>
										</div>
									</div>
									<div class="cp-field">
										<h5>New Password</h5>
										<div class="cpp-fiel">
											<input type="password" name="new_password" placeholder="New Password" id="new_password">
											<i class="fa fa-lock"></i>
										</div>
									</div>
									<div class="cp-field">
										<h5>Confirm Password</h5>
										<div class="cpp-fiel">
											<input type="password" name="confirm_password" placeholder="Confirm Password" id="confirm_password">
											<i class="fa fa-lock"></i>
										</div>
									</div>
									<!--<div class="cp-field">
										<h5><a href="#" title="">Forgot Password?</a></h5>
									</div>-->
									<div class="save-stngs pd2">
										<ul>
											<li><button type="submit">Save Setting</button></li>
											<!--<li><button type="submit">Restore Setting</button></li>-->
										</ul>
									</div><!--save-stngs end-->
								</form>
							</div><!--acc-setting end-->
					  	</div>
					  	<!-- ============================== Change Password End ========================= -->


					  	<!-- ============================== Plan Details Start ========================= -->
					  	<div class="tab-pane fade" id="nav-plan-detail" role="tabpanel" aria-labelledby="nav-deactivate-tab">
					  		<div class="acc-setting">
								<h3>Plan Details</h3>
									
								<div class="table_responsive">
									<div class="row mt-3">
										<div class="col-12">
											<h3 class="innerh3">Current Plan</h3>
										</div>
									</div>
									<br/>			
									<div class="row">
										<div class="col-12">
											<table class="table">
												<thead>
													<tr>
														<th class="font-weight-bold">Plan Name</th>
														<th class="font-weight-bold">Plan Amount</th>
														<th class="font-weight-bold">Start Date</th>
														<th class="font-weight-bold">End Date</th>
														<!-- <th class="font-weight-bold">Action</th> -->
													</tr>
												</thead>
												<tbody>
													@if($get_current_plan)
													<tr>
														<th>{{ $get_current_plan->plan_details->name }}</th>
														<th>&#8377; {{ $get_current_plan->plan_details->price }}</th>
														<th>{{ date('d-m-Y H:i:s',strtotime($get_current_plan->user_plan_details->starts_at)) }}</th>
														<th>{{ date('d-m-Y H:i:s',strtotime($get_current_plan->user_plan_details->ends_at)) }}</th>
													</tr>
													@else
													<tr>
														<td>Record not found.</td>
													</tr>
													@endif
												</tbody>
											</table>
										</div>			
									</div>
								</div>
								
								<br/>
		                     	<div class="table_responsive mt-4">
									<div class="row">
										<div class="col-12">
											<h3 class="innerh3">Transaction History</h3>
										</div>
									</div>
									<br/>
									
									<div class="row transaction_history">
										<div class="col-12">
											<table class="table">
												<tr>
													<th class="font-weight-bold">Plan Name</th>
													<th class="font-weight-bold">Plan Amount</th>
													<th class="font-weight-bold">Start Date</th>
													<th class="font-weight-bold">End Date</th>
												</tr>
												<tbody>
													@if(count($get_transaction) > 0)
															@foreach($get_transaction as $transaction_data)
																<tr>
																	<td>{{ $transaction_data->plan_details->name ?? '' }}</td>
																	<td>&#8377; {{ $transaction_data->plan_details->price ?? 0 }}</td>
																	<td>@if(isset($transaction_data->user_plan_details->starts_at)){{ date('d-m-Y H:i:s',strtotime($transaction_data->user_plan_details->starts_at)) }}@endif</td>
																	<td>@if(isset($transaction_data->user_plan_details->ends_at)){{ date('d-m-Y H:i:s',strtotime($transaction_data->user_plan_details->ends_at)) }}@endif</td>
																	
																</tr>
															@endforeach
														@else
															<tr>
																<td>Record not found.</td>
															</tr>
														@endif
												</tbody>
											</table>
										</div>
		                        	</div>
						  		</div>	

							</div>
					  	</div>
					  	

					  	<div class="tab-pane fade" id="nav-contact-us" role="tabpanel" aria-labelledby="nav-deactivate-tab">
					  		<div class="acc-setting">
								<h3>Contact Us</h3>
								<form action="{{ route('setting.store.contactus') }}" method="post" name="contact_us">
                                @csrf 
									<div class="cp-field">
										<h5>Name</h5>
										<input type="text" name="name" placeholder="Enter Name" class="cuspd">
									</div>
									<div class="cp-field">
										<h5>Email</h5>
										<input type="email" name="email" placeholder="Enter Email ID" class="cuspd">
									</div>
									<div class="cp-field">
										<h5>Mobile Number</h5>
										<input type="text" name="phone" placeholder="Enter Mobile Number" maxlength="10" class="cuspd">
									</div>
									<div class="cp-field">
										<h5>Message</h5>
										<textarea name="message" placeholder="Enter Message Description" style="padding:14px 10px;"></textarea>
									</div>
									<div class="save-stngs pd2">
										<ul>
											<li><button type="submit">Submit</button></li>
											<!--<li><button type="submit">Restore Setting</button></li>-->
										</ul>
									</div><!--save-stngs end-->
								</form>
							</div><!--acc-setting end-->
					  	</div>
					  	



					</div>
				</div>
			</div>
		</div><!--account-tabs-setting end-->
	</div>
</section>





@endsection




@section('scripts')

<script type="text/javascript">
	$("form[name='change_password']").validate({
        rules: 
        {
            old_password: {
                required: true,
            },
            new_password: {
                required: true,
            },
            confirm_password: {
                required: true,
                equalTo: "#new_password",
            }
        },
        messages: 
        {
            old_password:{
                required: 'Old Password is required',
            },
            new_password: {
                required: 'New Password is required',
            },
            confirm_password: {
                required: 'Confirm password is required.',
                equalTo:"New Password and Confirm password must match."
            }
        }
    });


    $("form[name='contact_us']").validate({
        rules: 
        {
            name: {
                required: true,
            },
            email: {
                required: true,
            },
            phone: {
                required: true,
            },
            message: {
                required: true,
            }
            
        },
        messages: 
        {

        	name: {
                required: "Please enter name.",
            },
            email: {
                required: "Please enter email.",
            },
            phone: {
                required: "Please enter mobile number.",
            },
            message: {
                required: "Please enter message.",
            }
        }
    });
</script>


@endsection


