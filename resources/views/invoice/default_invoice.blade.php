<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		.itemtable tr th{
			text-align: center;
			padding: 5px;
		}
		.itemtable tr td{
			text-align: center;
			padding: 5px;
		}
		.borderclass{
			border: 2px solid #000;
		}
		.rupeeimg{
			margin-right: 2px;
		}
		.rupeeimg{ font-family: DejaVu Sans, sans-serif; }
	</style>
</head>
<body>

	<div style="">
			
		<div style="border:2px solid #1e7e34;padding: 20px;">
			<table  style="padding:20px; width: 100%;border:2px dashed #acecbb;">
				<tr style="margin-bottom: 0">
					<td style="width:75%;">
						<img src="{{ $business_logo }}" height="100px" width="100px">
						<!-- <h1 style="margin: 0;color: #1e7e34;">BECO</h1> -->
					</td>
					<td style="width:25%;">
						<h1 style="margin: 0">Invoice</h1>
					</td>
				</tr>
				<tr>
					<td style="width:75%;"></td>
					<td style="width:25%;">
						<div>
							<span style="font-weight: bold;">Date : </span>
							<span>{{ date('d-m-Y',strtotime($invoice_details->created_at)) }}</span>
						</div>
						<div>
							<span style="font-weight: bold;">Invoice : </span>
							<span>INC{{ $invoice_details->id }}</span>
						</div>
					</td>
				</tr>
			</table>

			<table style="padding:20px 0px; width: 100%" >
				<tr>
					<td style="width:50%;">
						<div style="display: flex;">
							<h4 style="margin: 0;">From : </h4>
							<div style="margin-left: 18%;">
								<span>{{ $user_details->fullname ?? '' }}</span><br/>
								<span>{{ $user_details->email ?? '' }}</span><br/>
								<!-- <span>Address 1 : </span><br/>
								<span>Address 1 : </span> -->
							</div>
						</div>
					</td>
					<td style="width:50%;">
						<div style="display: flex;">
							<h4 style="margin: 0;">To : </h4>
							<div style="margin-left: 15%;">
								<span>{{ $invoice_details->to_user_name ?? '' }}</span><br/>
								<span>{{ $invoice_details->to_user_email ?? '' }}</span><br/>
								<!-- <span>Address 1 : </span><br/>
								<span>Address 1 : </span> -->
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="padding-top:1%;">
						<!-- <div>
							<h4 style="margin: 0">Terms : </h4>
							<span></span>
						</div> -->
						<div style="display: flex;">
							<h4 style="margin: 0">Due Date : </h4>
							<span style="margin-left: 12%;">{{ date('d-m-Y',strtotime($invoice_details->due_date)) }}</span>
						</div>
					</td>
				</tr>
			</table>

			<table style="width: 100%;border-collapse: collapse;background:#eafaee;border-color: #000;" border="2" class="itemtable">
				<thead>
					<tr class="borderclass">
						<th>Sr No.</th>
						<th>Name</th>
						<th>Description</th>
						<th>Quantity</th>
						<th>Price</th>
						<th>Amount</th>
					</tr>	
				</thead>
				<tbody>
					@php $total_sub_amount = 0; $total_due_amount = 0; @endphp
					@if(count($invoice_item_details) > 0)
						@foreach($invoice_item_details as $invoice_item_detail)

							@php 
								$total_sub_amount = $total_sub_amount + ($invoice_item_detail->price * $invoice_item_detail->qty);

								$total_tax1 = 0;
								if(!empty($invoice_details->tax1_type)){
									$total_tax1 = round((($total_sub_amount/100) * $invoice_details->tax1),2);										
								}

								$total_tax2 = 0;
								if(!empty($invoice_details->tax2_type)){
									$total_tax2 = round((($total_sub_amount/100) * $invoice_details->tax2),2);										
								}
								$total_due_amount = $total_sub_amount + $total_tax1 + $total_tax2;
							@endphp

							<tr>
								<td style="text-align: left;">{{ $invoice_item_detail->sr_no ?? '' }}</td>
								<td style="text-align: left;">{{ $invoice_item_detail->name ?? '' }}</td>
								<td style="text-align: left;">{{ $invoice_item_detail->description ?? '' }}</td>
								<td style="text-align: right;">{{ $invoice_item_detail->qty ?? 0 }}</td>
								<td style="text-align: right;"><span height="10" width="10" class="rupeeimg">{{ $rupee_details->symbol ?? '' }}</span> {{ $invoice_item_detail->price ?? 0 }}</td>
								<td style="text-align: right;"><span height="10" width="10" class="rupeeimg">{{ $rupee_details->symbol ?? '' }}</span> {{ $invoice_item_detail->price * $invoice_item_detail->qty }}</td>
							</tr>
						@endforeach
					@endif
					<tr>
						<td colspan="5" style="text-align: right;font-weight: bold;">Sub Total</td>
						<td style="text-align: right;"><span height="10" width="10" class="rupeeimg">{{ $rupee_details->symbol ?? '' }}</span> {{ $total_sub_amount ?? 0 }}</td>
					</tr>

					@if(!empty($invoice_details->tax1_type))
					<tr>
						<td colspan="5" style="text-align: right;font-weight: bold;">{{ $invoice_details->tax1_type }} ( {{ round(($invoice_details->tax1),2) }} % )</td>
						<td style="text-align: right;"><span height="10" width="10" class="rupeeimg">{{ $rupee_details->symbol ?? '' }}</span> {{ $total_tax1 ?? 0 }}</td>
					</tr>
					@endif

					@if(!empty($invoice_details->tax2_type))
					<tr>
						<td colspan="5" style="text-align: right;font-weight: bold;">{{ $invoice_details->tax2_type }} ( {{ round(($invoice_details->tax2),2) }} % )</td>
						<td style="text-align: right;"><span height="10" width="10" class="rupeeimg">{{ $rupee_details->symbol ?? '' }}</span> {{ $total_tax2 ?? 0 }}</td>
					</tr>
					@endif

					<tr class="borderclass">
						<td colspan="5" style="text-align: right;font-weight: bold;font-size: 24px;">Due Amount</td>
						<td style="text-align: right;"><span height="10" width="10" class="rupeeimg">{{ $rupee_details->symbol ?? '' }}</span><b> {{ $total_due_amount ?? 0 }} </b></td>
					</tr>
				</tbody>

			</table>

			<table style="width: 100%;">
			<tr>
				<td>
					<h3>Notes</h3>
					<div style="width: 100%;height: 120px;border:2px solid #000;padding: 0px 2px;word-break: break-all;overflow: hidden;">
						<span>{{ $invoice_details->note ?? '' }}</span>
					</div>	
				</td>
			</tr>
			</table>
		</div>
		

		

	</div>

</body>
</html>