
@extends('auth.include.main')
@section('title','OTP Verification')
@section('css')

<style type="text/css">
    .container_card h6{color: #1E2C75;font-size:20px}
    .inputs input{width:40px;height:40px}
    input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button{-webkit-appearance: none;-moz-appearance: none;appearance: none;margin: 0}
    .form-control:focus{box-shadow:none;border:2px solid #1E2C75}
    .cbttn{width: 25%;}
</style>

@endsection

@section('content')
<section class="commform">
    <div class="container">
        <div class="titlebox">
            <h4>OTP Verification</h4>
            <!-- <h5>SIGN IN HERE</h5> -->
        </div>
        
        <div class="regbox container_card text-center">
            <form action="{{ route('front.otp_verification.process') }}" name="otp_verification_form" method="POST">
            @csrf

                            <h6>Please enter the one time password <br> to verify your account</h6> 
                            <div> <span>A code has been sent to</span><br/>
                                @php $arr = [1,2,3,4,5,6];  @endphp
                                @foreach($arr as $arrs)
                                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">
                                        <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>
                                    </svg>
                                @endforeach {{ substr(Auth::user()->phone,6) }} </div> 

                            <div id="otp" class="inputs d-flex flex-row justify-content-center mt-2"> 
                                <input class="m-2 text-center form-control rounded" type="text" name="first" id="first" maxlength="1" /> 
                                <input class="m-2 text-center form-control rounded" type="text" name="second" id="second" maxlength="1" /> 
                                <input class="m-2 text-center form-control rounded" type="text" name="third" id="third" maxlength="1" /> 
                                <input class="m-2 text-center form-control rounded" type="text" name="fourth" id="fourth" maxlength="1" /> 
                                <input class="m-2 text-center form-control rounded" type="text" name="fifth" id="fifth" maxlength="1" /> 
                                <input class="m-2 text-center form-control rounded" type="text" name="sixth" id="sixth" maxlength="1" /> 
                            </div> 
                            <p>( OTP : 111111 )</p>
                            <div class="mt-4"> 
                                <button type="submit" class="cbttn">Validate</button> 
                            </div>  
                            <div class="text-center mt-4"> 
                                <span>Didn't get the code</span> <a href="{{ route('front.otp.send') }}" class="text-decoration-none ms-3">Resend</a> 
                            </div> 

            </form>
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script>
    $("form[name='otp_verification_form']").validate({
        rules: {
            first: 'required',
            second: 'required',
            third: 'required',
            fourth: 'required',
            fifth: 'required',
            sixth: 'required'
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "first")
                error.insertAfter("");
            else if (element.attr("name") == "second")
                error.insertAfter("");
            else if (element.attr("name") == "third")
                error.insertAfter("");
            else if (element.attr("name") == "fourth")
                error.insertAfter("");
            else if (element.attr("name") == "fifth")
                error.insertAfter("");
            else if (element.attr("name") == "sixth")
                error.insertAfter("");                       
            else
                error.insertAfter(element);
        }
    });


    // function OTPInput() {
    //     const inputs = document.querySelectorAll('#otp > *[id]');
    //     for (let i = 0; i < inputs.length; i++) {
    //         inputs[i].addEventListener('keydown', function(event) {
    //             if (event.key==="Backspace" ) {
    //                 inputs[i].value='' ; if (i !==0) inputs[i - 1].focus(); 
    //             } 
    //             else { 
    //                 if (i===inputs.length - 1 && inputs[i].value !=='' ) 
    //                 { return true; } 
    //                 else if (event.keyCode> 47 && event.keyCode < 58) 
    //                 { inputs[i].value=event.key; if (i !==inputs.length - 1) inputs[i + 1].focus(); event.preventDefault(); } 
    //                 else if (event.keyCode> 64 && event.keyCode < 91) 
    //                 { inputs[i].value=String.fromCharCode(event.keyCode); if (i !==inputs.length - 1) inputs[i + 1].focus(); event.preventDefault(); } 
    //             } 
    //         }); 
    //     }
    // }
</script>

@endsection	
    
