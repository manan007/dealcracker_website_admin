@extends('auth.include.main')
@section('title','Reset Password')
@section('css')
@endsection

@section('content')
    <section class="commform">
		<div class="container">
			<div class="titlebox">
				<h4>Reset Password</h4>
				<!-- <h5>Enter the email address you used to register for esportsrecruiter,<br>and instructions will be sent to recover your account.</h5> -->
			</div>
			<div class="regbox">
                @if (Session::get('success'))
                    <h6 id="success" class="text-center alert alert-success py-2">
                        {{ Session::get('success') }}</h6>
                @endif

                @if (Session::get('invalid'))
                    <h6 id="invalid_credentials" class="text-center alert alert-danger py-2">
                        {{ Session::get('invalid') }}</h6>
                @endif

				<form action="{{ route('reset_password.store') }}" method="POST" name="reset_password_form">
                @csrf

                        <input type="hidden" name="token" value="{{ Request::segment(2) }}">
                        <div class="form-group">
                            <label>New Password<span class="required">*</span></label>
                            <input type="password" class="form-control custfield" id="password" name="password" placeholder="New Password">
                            @error('password')
                                <span class="invalid-password" role="alert">
                                    <strong class="msg">{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label>Confirm Password<span class="required">*</span></label>
                            <input type="password" class="form-control custfield" id="confirm_password" name="confirm_password" placeholder="Confirm Password">
                            @error('confirm_password')
                                <span class="invalid-confirm_password" role="alert">
                                    <strong class="msg">{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
				  	<div class="form-group row">
				  		<div class="col-sm-6">
				  			<button type="submit" class="cbttn">Reset my password</button>
				  		</div>
					</div>
				</form>
			</div>
		</div>
	</section>
@endsection
@section('scripts')
<script>
$("form[name='reset_password_form']").validate({
    rules: {
        password: {
                required: true,
            },
        confirm_password: {
            required: true,
            equalTo: "#password",
        },
    },
    messages: {
        password: {
            required: 'New password is required',
        },
        confirm_password: {
            required: 'Confirm password is required.',
            equalTo:"New password and Confirm password must match."
        },
    }
})
</script>
@endsection