@extends('auth.include.main')
@section('title','Register')
@section('css')
@endsection

@section('content')
<section class="commform">
        <div class="container">
            <div class="titlebox">
                <h4>Register Now</h4>
                <h5>SIGN UP</h5>
            </div>
            <div class="regbox">

                <form action="{{ route('front.signup_process') }}" method="POST" name="signup_form">
                @csrf 

                    <div class="form-group">
                        <label>Name<span class="required">*</span></label>
                        <input type="text" class="form-control custfield" id="name" name="name" value="{{ old('name') }}" placeholder="Enter Name">
                        @error('name')
                            <span class="invalid-email" role="alert">
                                <strong class="msg">{{ $message }}</strong>
                            </span>
                        @enderror    
                    </div>

                    <div class="form-group">
                        <label>Email address<span class="required">*</span></label>
                        <input type="email" class="form-control custfield" id="email" name="email" value="{{ old('email') }}" placeholder="Enter email">
                        @error('email')
                            <span class="invalid-email" role="alert">
                                <strong class="msg">{{ $message }}</strong>
                            </span>
                        @enderror    
                    </div>

                    <div class="form-group">
                        <label>Phone Number<span class="required">*</span></label>
                        <input type="text" class="form-control custfield" id="phone" name="phone" value="{{ old('phone') }}" placeholder="Enter Phone Number">
                        @error('phone')
                            <span class="invalid-email" role="alert">
                                <strong class="msg">{{ $message }}</strong>
                            </span>
                        @enderror    
                    </div>
                    
                    <div class="form-group">
                        <label>Password<span class="required">*</span></label>
                        <input type="password" class="form-control custfield" id="password" name="password" placeholder="Password">
                        @error('password')
                            <span class="invalid-password" role="alert">
                                <strong class="msg">{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label>Confirm Password<span class="required">*</span></label>
                        <input type="password" class="form-control custfield" id="confirm_password" name="confirm_password" placeholder="Confirm Password">
                        @error('confirm_password')
                            <span class="invalid-confirm_password" role="alert">
                                <strong class="msg">{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    
                    <button type="submit" class="cbttn">Register</button>


                    <div class="form-group mt-3 text-center">
                        <p><span>You have already account?</span> <a class="link" title="Register with Stylexpo" href="{{ route('front.sign_in') }}">Login</a></p>
                    </div>
                </form>     
            </div>
        </div>
    </section>
@endsection

@section('scripts')

<script>
$("form[name='signup_form']").validate({
    rules: 
    {
        name:{
            required: true,
        },
        email: {
            required:true,
            email:true,
        },
        phone:{
            required:true,
        },
        password: {
            required: true,
        },
        confirm_password: {
            required: true,
            equalTo: "#password"
        }
    },
    messages: 
    {
        name:{
            required: 'Please enter name.',
        },
        email:{
            required: 'Please enter email.',
        },
        phone:{
            required: 'Please enter phone number.',
        },
        password: {
            required: 'Please enter password.',
        },
        confirm_password: {
            required: 'Confirm password is required.',
            equalTo:"Password and Confirm password must match."
        }
    }
});



</script>
@endsection    