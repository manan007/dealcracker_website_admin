@extends('auth.include.main')
@section('title','Plan')
@section('css')

<style type="text/css">
/* medium - display 2  */
@media (min-width: 768px) {

    .carousel-inner .carousel-item-right.active,
    .carousel-inner .carousel-item-next {
        transform: translateX(50%);
    }

    .carousel-inner .carousel-item-left.active,
    .carousel-inner .carousel-item-prev {
        transform: translateX(-50%);
    }
}

/* large - display 3 */
@media (min-width: 992px) {

    .carousel-inner .carousel-item-right.active,
    .carousel-inner .carousel-item-next {
        transform: translateX(50%);
    }

    .carousel-inner .carousel-item-left.active,
    .carousel-inner .carousel-item-prev {
        transform: translateX(-50%);
    }
}

@media (max-width: 768px) {
    .carousel-inner .carousel-item>div {
        display: none;
    }

    .carousel-inner .carousel-item>div:first-child {
        display: block;
    }
}

.carousel-inner .carousel-item.active,
.carousel-inner .carousel-item-next,
.carousel-inner .carousel-item-prev {
    display: flex;
}

.carousel-inner .carousel-item-right,
.carousel-inner .carousel-item-left {
    transform: translateX(0);
}
.carousel-control-prev{
    height: 40px;
    padding: 3px 8px 3px 6px;    
}
.carousel-control-next{
    height: 40px;
    padding: 3px 6px 3px 8px;
}
.duration-text{
    font-size: 14px;
}
#logo img{
    padding-top: 10px!important;
    height: 40px!important;
}
</style>
@endsection

@section('content')
<section class="commform planpart">
    <div class="container">
        <div class="titlebox">
            <h4>Select Plan<!-- SIGNUP FOR FREE AND RECEIVE 30 DAYS OF PREMIUM SERVICES. --></h4>
            <!--<h5>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h5>-->
        </div>


        <div class="container">
 
            <div class="row mx-auto my-auto">
                <div id="myCarousel" class="carousel slide w-100" data-ride="carousel">
                    <div class="carousel-inner w-100" role="listbox">

                        @foreach($plans as $key => $plan)
                            @php $get_plans = App\Models\PlanFeature::where(['plan_id' => $plan->id])->get();  @endphp
                            <div class="carousel-item @if($key == 0) active @endif">
                                <div class="col-lg-6 col-md-6">

                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <h2>{{ $plan->name ?? '' }}</h2>
                                                        <h5 class="font-weight-bold text-dark"><span> &#8377; {{ $plan->price ?? '' }} </span> / <span class="duration-text">{{ $plan->duration ?? '' }} Days</span></h5>
                                                    </th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                @if(count($get_plans) > 0)
                                                    @foreach($get_plans as $get_plan)
                                                        <tr>
                                                            <td>{{ $get_plan->feature ?? '' }}</td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>

                                            <tfoot>
                                                <tr>
                                                    @if($plan->price > 0)
                                                        <td class="text-center"><button type="button" data-amount="{{ $plan->price ?? '' }}" 
                                                        class=" razorpayBtn planbtn" data-plan-id="{{ $plan->id }}">&#8377; {{ $plan->price ?? '' }}</button></td>
                                                    @else
                                                        <form action="{{ route('front.plan.purchase') }}" method="POST" id="free_form">
                                                            <input type="hidden" name="hidden_id" value="{{ Auth::user()->id }}">
                                                            <input type="hidden" name="plan_id" value="{{ $plan->id }}">
                                                            <input type="hidden" name="amount" value="0">
                                                            @csrf
                                                            <td class="text-center"><button type="button" class="planbtn free_btn">FREE</button></td>
                                                        </form>
                                                    @endif
                                                </tr>
                                            </tfoot>
                                        </table>
                                </div> 
                            </div>
                        @endforeach
                        
                    </div>

                    <a class="carousel-control-prev bg-dark w-auto" href="#myCarousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next bg-dark w-auto" href="#myCarousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>

        </div>


        

        
    </div>
</section>
@endsection

@section('scripts')

<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.js"></script>

<script type="text/javascript">
    $('#myCarousel').carousel({
        interval: 5000
    })

    $('.carousel .carousel-item').each(function() {
        var minPerSlide = 2;
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i = 0; i < minPerSlide; i++) {
            next = next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo($(this));
        }
    });

    $(document).on('click','.free_btn',function(){
        $('#free_form').submit();
    });
</script>

<script type="text/javascript">
    
    $(document).on('click','.razorpayBtn',function(){

        $.ajaxSetup({
            headers:
                {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });

        var baseUrl = '{{ url("/") }}';
        var key = "{{ env('RAZORPAY_KEY_TEST') }}";
        var user = {
            phone: '{{ Auth::user()->phone }}',
            email: '{{ Auth::user()->email }}',
            options:'{"checkout": {"method": {"netbanking": "1","card": "1","upi": "0","wallet": "0"}}}',
        };
        
        // var type=$('#accreditation_status').val();
        // var status= $('input[name="amount"]:checked').val();  
        var payment= $(this).attr('data-amount'); //$('.amount').val();
        var plan_id = $(this).attr('data-plan-id');

        $.ajax({
            url: "{{route('payment')}}",
            type: 'post',
            dateType: 'json',
            data: {
                "payment":payment,
                "plan_id":plan_id
            },
            success: function (response) {
                let options = null;
                
                    options = {
                        "key": key,
                        "amount": parseInt(response.payment.amount*100),
                        "name": "{{env('APP_NAME')}}",
                        "image": "{{env('APP_URL')}}" + '/public/frontend/images/logo.png',
                        "order_id": response.payment.order_id,
                        "handler": function (handlerResponse) {

                            $('.myModal').modal({backdrop: 'static', keyboard: false});
                            $('.myModal').modal('show');
                            $('#wait').show();
                            $.ajaxSetup({
                                headers:
                                    {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
                            });
                            $.ajax({
                                url: "{{env('APP_URL')}}" + '/payment/success',
                                type: 'post',
                                dataType: 'json',
                                data: {
                                    data: handlerResponse,
                                    local_payment_id: response.payment.id,
                                    amount: parseInt(response.payment.amount*100),
                                    currency: response.payment.currency
                                },
                                success: function (ajaxResponse) {
                                    if (ajaxResponse.status == true) {

                                        //alert('ewf');
                                        bootbox.alert('Your payment has been processed successfully.');
                                        var url="{{route('front.index')}}";
                                        location.replace(url);

                                        /*Swal.fire({
                                            title: 'Success!',
                                            text: ajaxResponse.message,
                                            icon: 'success',
                                        });
                                        setTimeout(function () {
                                            window.location.href = "{{env('APP_URL')}}" + '/home';
                                        }, 3000);*/
                                    } else {
                                       bootbox.alert('Payment failed...');
                                       var url="{{route('front.plan.view')}}";
                                        location.replace(url);
                                    }
                                },
                                  complete: function(){
                                                                   
                                  }
                            });
                        },
                        "prefill": {
                            "contact": user.phone,
                            "email": user.email,
                        },
                        "theme": {
                            "color": "#528FF0"
                        }
                    };
                
                /*console.log(options);*/
                const rzp1 = new Razorpay(options);
                rzp1.open();
            }
        });

    });

</script>



@endsection
	







<!-- <div class="carousel-item active">
    <div class="col-lg-6 col-md-6">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>
                        <h2>Free</h2>
                        <h5 class="font-weight-bold text-dark"><span> &#8377; 0 </span> / <span class="duration-text">30 Days</span></h5>
                    </th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td>Show Feed Details</td>
                </tr>
                <tr>
                    <td>Show Blog Details</td>
                </tr>
                <tr>
                    <td>Show Opportunity Details</td>
                </tr>
                <tr>
                    <td>Show Community Details</td>
                </tr>
            </tbody>

            <tfoot>
                <tr>
                    <td class="text-center"><button type="button" class="planbtn">FREE</button></td>
                </tr>
            </tfoot>
        </table>
    </div>
</div> -->