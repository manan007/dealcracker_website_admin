
@extends('auth.include.main')
@section('title','Login')
@section('css')
@endsection




@section('content')
<section class="commform">
    <div class="container">
        <div class="titlebox">
            <h4>Already a Member?</h4>
            <h5>SIGN IN HERE</h5>
        </div>
        
        <div class="regbox">

            <form action="{{ route('front.signin_process') }}" name="loginform" method="POST">
            @csrf
            @php
                if(isset($_COOKIE['email'])) {
                    $email=\Cookie::get('email');
                }
                else
                {
                    $email=null;   
                }
                if(isset($_COOKIE['password'])) {
                    $password=\Cookie::get('password');
                }
                else
                {
                    $password=null;   
                }

            @endphp

                <div class="form-group">
                    <label for="email">Email<span class="required">*</span></label>
                    <input type="email" class="form-control custfield" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email" value="@if($email != null) {{$email}}  @else {{ old('email') }} @endif">
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password<span class="required">*</span></label>
                    <input type="password" name="password" class="form-control custfield" id="exampleInputPassword1" placeholder="Password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group row">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="input-field col s6">
                                <label class="ml-8"></label>
                                {{--    <label class="ml-8">
                                    <input type="checkbox" name="remember" class="filled-in" {{ old('remember') ? 'checked' : '' }} 
                                    @if(isset($_COOKIE['email']))
                                    checked
                                    @endif
                                        />
                                    <span>Remember Me</span>
                                    </label> --}}
                            </div>
                        </div>
                        <button type="submit" class="cbttn">Login</button>
                    </div>
                    <div class="col-sm-6 text-right">
                        <a href="{{ route('user.forgot_password.view') }}" class="forgotpass">Forgot password?</a>
                    </div>
                </div>
                <!-- <hr/> -->
                <div class="form-group row">
                    <div class="col-sm-12 text-center">
                        <p>New to DealCracker? <a href="{{ route('front.sign_up') }}">Create New Account</a></p>
                    </div>
                </div>
                <hr/>
                <div class="form-group loginsocial">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="ctitle">
                                    <h3>Login With Social Media</h3>
                                </div>
                            </div>
                            {{--<div class="col-sm-4">
                                <a href="javascript:;" title="Facebook Login" class="fb btn bttnfb" style="width: 100%;"><i class="fab fa-facebook-f"></i> Facebook</a>
                            </div>--}}
                            <div class="col-sm-12">
                                <a href="javascript:;" title="Gmail Login" class="gm btn bttngoogle" id="customBtn" style="width: 100%;"><i class="fab fa-google-plus-g"></i> Google</a>
                            </div>
                           {{--<div class="col-sm-6">
                                <a href="{{ url('auth/linkedin') }}" title="Gmail Login" class="linkin btn bttnlinkin" id="customBtn" style="width: 100%;"><i class="fab fa-linkedin"></i> LinkedIn</a>
                            </div> --}}

                        </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
@section('scripts')


<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '510606990855238',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v14.0'
    });
  };
</script>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js"></script>
<script>
    $(document).on('click', '.fb', function(){
        FB.login(function(response) {
            console.log(response);
            if (response.authResponse) {
                FB.api('/me?fields=name,first_name,email,hometown,location{location}',
                function(response) {
                    console.log(response);
                    var id = response.id;
                   // var name = response.name;
                    var email = response.email;
                    var type = "facebook";
                  //  var username = response.first_name;

                    $.ajax({
                        url: "{{route('facebook.callback')}}",
                        type: 'GET',
                        data:'email='+email+'&id='+id+"&type="+type,
                        contentType: false, 
                        processData: false,
                        success: function (data) {
                            var url = '{{env('APP_URL')}}';
                            window.location.href = url+"/"+data;
                            //$('#personal_state').html(data.html);
                        }
                    });

                    // var city = response.location['location'].city;
                    // var country = response.location['location'].country;
                    
                    // console.log(response);

                    // $('#username').val(username);
                    // $('#email').val(email);
                    // $('#name').val(name);
                    // $('#profile_name').val(name);
                });
            } else {
                console.log('User cancelled login or did not fully authorize.');
            }
        });
    });

</script>


<script>
    var check="{{$password}}";
    if(check != '' || check != ' ')
    {
        $('#exampleInputPassword1').val(check);
    }

    setInterval(() => {
        $('#invalid_credentials').css({
            'display': 'none'
        })
    }, 4000);


    // $('#country_select').select2({
    //     tags: true
    // });
    $("form[name='loginform']").validate({
        rules: {
            username: 'required',
            password: {
                required: true,
            }
        },
        messages: {
            username: 'Email is Required',
            password: {
                required: "Password is required"
            }
        }
    })
</script>


<!-- -----------------------------------------  Gmail Login   ---------------------------------------------  -->

<meta name="google-signin-client_id" content="1019355021904-57an296r9t2bj8v8oq7u7tl85e203baj.apps.googleusercontent.com">
<script src="https://apis.google.com/js/api:client.js"></script>
<script>
    var googleUser = {};
    var startApp = function() {
        gapi.load('auth2', function(){
            auth2 = gapi.auth2.init({
                client_id: '1019355021904-57an296r9t2bj8v8oq7u7tl85e203baj.apps.googleusercontent.com',
                cookiepolicy: 'single_host_origin',
                plugin_name: "chat",
            });
            attachSignin(document.getElementById('customBtn'));
        });
    };

    function attachSignin(element) {            
        auth2.attachClickHandler(element, {},
            function(googleUser) {
                var profile = googleUser.getBasicProfile();
                var id = profile.getId();
               // var name = profile.getName();
                var email = profile.getEmail();
                var type = "google";
               // var username = profile.getGivenName();

                $.ajax({
                    url: "{{route('facebook.callback')}}",
                    type: 'GET',
                    data:'email='+email+'&id='+id+"&type="+type,
                    contentType: false, 
                    processData: false,
                    success: function (data) {
                        
                        var url = '{{env('APP_URL')}}';
                        window.location.href = url+"/"+data;
                        signOut();
                        //$('#personal_state').html(data.html);
                    }
                });
            }, function(error) {
                console.log(JSON.stringify(error, undefined, 2));
            });
    }
    function signOut() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function () {
            console.log('User signed out.');
        });
    }
</script>
<script>startApp();</script>







@endsection 
    
