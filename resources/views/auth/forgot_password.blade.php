@extends('auth.include.main')
@section('title','Forgot Password')
@section('css')
@endsection

@section('content')
    <section class="commform">
		<div class="container">
			<div class="titlebox">
				<h4>Forgot your password?</h4>
				<h5>Enter the email address you used to register for DealCracker,<br>and instructions will be sent to recover your account.</h5>
			</div>
			<div class="regbox">
                @if (Session::get('success'))
                    <h6 id="success" class="text-center alert alert-success py-2">
                        {{ Session::get('success') }}</h6>
                @endif

                @if (Session::get('invalid'))
                    <h6 id="invalid_credentials" class="text-center alert alert-danger py-2">
                        {{ Session::get('invalid') }}</h6>
                @endif

				<form action="{{ route('user.forgot_password') }}" method="POST" name="forgot_password_form" id="forgot_password_form">
                @csrf
				  	<div class="form-group">
					    <label for="exampleInputEmail1">Email address</label>
					    <input type="email" class="form-control custfield" id="email" name="email" value="{{ old('email') }}" aria-describedby="emailHelp" placeholder="Enter email">
                        @error('email')
                            <span class="invalid-email" role="alert">
                                <strong class="msg">{{ $message }}</strong>
                            </span>
                        @enderror  
					</div>
				  	<div class="form-group row">
				  		<div class="col-sm-6">
				  			<button type="submit" class="cbttn">Reset my password</button>
				  		</div>
                        <div class="col-sm-6 text-right">
                            <a href="javascript:;" class="forgotpass reset_password">Resend Email</a>
                        </div>
					</div>
				</form>
                
			</div>
		</div>
	</section>
@endsection
@section('scripts')
<script>
$("form[name='forgot_password_form']").validate({
    rules: {
        email: {
            required:true,
            email:true,
        },
    },
    messages: {
        email: {
            required: "Email is required.",
            email:'Please enter valid email.'
        }
    }
})

$(document).on('click','.reset_password',function(){
    $('#forgot_password_form').submit();
})

</script>
@endsection