@extends('auth.include.main')
@section('title','Complete Profile')
@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{ asset('assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

<style type="text/css">
    
    .select2-container--default .select2-selection--multiple{
        height:50px!important;
    }
    /*.select2-container--default .select2-selection--multiple .select2-selection__rendered{
        line-height:40px!important;
    }*/
    /*.select2-container--default .select2-selection--multiple .select2-selection__arrow{
        height:38px;
    }*/

    .select2-container--default.select2-container--focus .select2-selection--multiple {
        border: solid gray 1px!important;
        outline: 0;
    }
    .select2-container {
        display: inline-grid;
    }
    .font14{
        font-size: 14px;
    }

</style>

@endsection

@section('content')
<section class="commform">
    <div class="container">
        <div class="titlebox">
            <h4>Complete Profile</h4>
            <!-- <h5>Complete Profile</h5> -->
        </div>
        
        <div class="regbox">

            <form action="{{ route('front.complete.profile.process') }}" name="complete_profile_form" method="POST">
            @csrf

                <div class="form-group row">
                    <div class="col-sm-12">
                        <label for="address">Address<span class="required">*</span></label>
                        <textarea class="form-control custfield" id="address" name="address" placeholder="Enter Address"></textarea>
                        @error('address')
    		                <span class="invalid-feedback" role="alert">
    		                    <strong>{{ $message }}</strong>
    		                </span>
    		            @enderror
                    </div>
                </div>
                <div class="form-group row">

                    <div class="col-sm-6">
                        <label for="dob">Date Of Birth<span class="required">*</span></label>
                        <input type="date" class="form-control custfield" id="dob" name="dob" value="">
                        @error('dob')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-sm-6">
                        <label for="website">Website URL<span class="required">*</span> <span class="text-danger mt-0 font-weight-bold font14">( Eg :- https://xyz.com )</span></label>
                        <input type="url" class="form-control custfield" id="website" placeholder="Website URL" name="website" value="">
                        @error('website')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-sm-12">
                        <label for="profile_description">Profile Description<span class="required">*</span></label>
                        <textarea class="form-control custfield" id="profile_description" name="profile_description" placeholder="Profile Description"></textarea>
                        @error('profile_description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">

                    <div class="col-sm-6">
                        <label for="key_skills">Key Skills<span class="required">*</span></label>
                        <input type="text" class="form-control custfield" id="key_skills" placeholder="Key Skills" name="key_skills" value="">
                        @error('key_skills')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-sm-6">
                        <label for="business_domain">Business Domain</label>
                        <input type="text" class="form-control custfield" id="business_domain" placeholder="Business Domain" name="business_domain" value="">
                        @error('business_domain')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-sm-12">
                        <label for="platforms" class="mb-0">Select Platform<span class="required">*</span><span class="text-danger mt-0 font-weight-bold font14">( Note : To add more platform please contact admin. )</span></label>
                        <select class="form-control custfield select2" name="platforms[]" id="platforms" multiple="true">
                            @if($platforms)
                            @foreach($platforms as $platform)
                              <option value="{{ $platform->id }}">{{ $platform->name ?? '' }}</option>
                            @endforeach
                          @endif
                          </select>
                          <div id="platforms_msg"></div>
                        @error('platforms')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-12">
                        <label for="other_info">Other Information</label>
                        <input class="form-control custfield" id="other_info" name="other_info" placeholder="Other Information" />
                        @error('other_info')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="cbttn">Submit</button>
                        <!-- <a href="{{ route('front.register.skip') }}" class="btn btn-info mt-3 w-50">SKIP</a> -->
                    </div>
                </div>
                
            </form>
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script src="{{ asset('assets/plugins/select2/js/select2.full.min.js')}}"></script>

<script type="text/javascript">
  $('.select2').select2();
</script>
<script>



    // $('#country_select').select2({
    //     tags: true
    // });
    $("form[name='complete_profile_form']").validate({
        rules: {
            address: 'required',
            dob: 'required',
            website: 'required',
            profile_description: 'required',
            key_skills: 'required',
            'platforms[]':'required'
        },
        messages: {
            address: 'Please enter address.',
            dob: 'Please enter date of birth.',
            website: 'Please enter website url.',
            profile_description: 'Please enter profile description.',
            key_skills: 'Please enter key skill.',
            'platforms[]':'Please select platforms.'
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "platforms[]")
                error.insertAfter("#platforms_msg");                       
            else
                error.insertAfter(element);
        }
    })
</script>
@endsection	
    
