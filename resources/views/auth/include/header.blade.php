
<style type="text/css">
	.sub_box{
		border-top: 4px solid #ef3e35!important;
		border-radius: 0px!important;
		background-color: #ffffff;
	}
	.sub_box a{
		padding: 9px 15px 8px!important;
	}
	.sub_box a:hover{
		background-color: #ef3e35;
		color: #ffffff;
	}
	.dropdown-menu{
		padding: 0px!important;
	}
</style>
<header class="custheader">
	<div class="container-fluid">
		<div class="logo">
			<a href="{{ route('home.index') }}"><img src="{{ asset('frontend/images/logo.png') }}" alt=""></a>
			<!-- <h1 class="text-primary">DC</h1> -->
		</div>
		<div class="joinnowbox">
			<div class="container">
				<a href="{{ route('front.sign_up') }}">Join Now</a>
			</div>
		</div>
		<nav class="custnav">
			<div id="mobilemenu"><i class="fa fa-bars pr-3"></i>Select Page</div>
			<ul class="menus">
				@if(Auth::check())
				<li><a href="{{ route('front.index') }}">Home</a></li>
				@else
				<li><a href="{{ route('home.index') }}">Home</a></li>
				@endif
				<li><a href="{{ route('home.about_us') }}">About Us</a></li>
				<li><a href="{{ route('home.contact_us') }}">Contact Us</a></li>
				@if(Auth::check())
				<li><a href="{{route('front.logout')}}" class="signin">LOGOUT</a></li>
				@else
		        <li><a href="{{ route('front.sign_in') }}" class="signin">SIGN IN</a></li>
				@endif
		      </ul>
		</nav>
	</div>
</header>

{{--  --}}
@if(\Request::route()->getName() != "front.my_profile.index")
<section class="breatcome_area" style="background-image: url('{{ asset('frontend/images/titleimg.jpg')}}');">
	<div class="container">
		@if(\Request::route()->getName() == "front.sign_in")
		<h4>Sign In</h4>
		@elseif(\Request::route()->getName() == "front.sign_up")
		<h4>Sign Up</h4>
		@elseif(\Request::route()->getName() == "front.complete.profile")
		<h4>Complete Profile</h4>
		@elseif(\Request::route()->getName() == "front.plan.view")
		<h4>Select Plan</h4>
		@elseif(\Request::route()->getName() == "home.about_us")
		<h4>About Us</h4>
		@elseif(\Request::route()->getName() == "home.contact_us")
		<h4>Contact Us</h4>
		@elseif(\Request::route()->getName() == "home.terms_conditions")
		<h4>Terms and Conditions</h4>
		@elseif(\Request::route()->getName() == "home.privacy_policy")
		<h4>Privacy Policy</h4>
		@elseif(\Request::route()->getName() == "front.otp.verification.view")
		<h4>OTP Verification</h4>
		@elseif(\Request::route()->getName() == "front.otp.send")
		<h4>OTP Verification</h4>
		@elseif(\Request::route()->getName() == "user.forgot_password.view")
		<h4>Forgot Password</h4>
		@elseif(\Request::route()->getName() == "user.reset_password")
		<h4>Reset Password</h4>

		@endif
	</div>
</section>
@endif
