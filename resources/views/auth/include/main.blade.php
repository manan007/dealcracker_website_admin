<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
	<meta name="_token" content="{{ csrf_token() }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('frontend/images/ESR-Logo.png') }}">
	<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('frontend/images/ESR-Logo.png') }}">
	<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('frontend/images/ESR-Logo.png') }}">
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/frontend/images/apple-icon-76x76.png') }}">
	<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/frontend/images/ESR-Logo.png') }}">
	<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/frontend/images/ESR-Logo.png') }}">
	<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/frontend/images/ESR-Logo.png') }}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/frontend/images/ESR-Logo.png') }}">
	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/frontend/images/ESR-Logo.png') }}">
	<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('assets/frontend/images/ESR-Logo.png') }}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/frontend/images/ESR-Logo.png') }}">
	<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/frontend/images/ESR-Logo.png') }}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/frontend/images/ESR-Logo.png') }}">	
	<meta name="msapplication-TileImage" content="{{ asset('assets/frontend/images/ESR-Logo.png') }}"> -->


	<title>DealCracker - @yield('title')</title>
	<script type="text/javascript" src="{{ asset('frontend/js/html5.js') }}"></script>
	<!-- ALL CSS -->
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

	@if(\Request::route()->getName() == "checkout.view")
		<style>
			header {
				float: left;
				width: 100%;
				padding: 15px 0;
				background-color: transparent!important;
				/* border-bottom:1px solid black; */
			}

			.header1 {
				float: left;
				width: 100%;
				padding: 15px 0;
				background-color: #fff!important;
				/* border-bottom:1px solid black; */
			}
			.custheader .custnav .menus > li.dropmenu > a {
				background: url(../assets/frontend/images/down-arrow.png) no-repeat 100% 5px;
				padding-right: 20px;
			}
		</style>
		<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/landing-style1.css') }}">
	@else
		<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/landing-style.css') }}">
	@endif
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/landing-media.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/custome1.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/fontawesome.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/select2.min.css') }}">
	<!-- ALL SCRIPT -->
	<script type="text/javascript" src="{{ asset('frontend/js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/select2.min.js') }}"></script>
	
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <style>#exampleInputEmail1-error{color:red;}#exampleInputPassword1-error{color:red;}</style>
    @yield('css')
</head>
<body>
    @include('auth.include.header')
	
			@yield('content')
	</section>
	@include('auth.include.footer')
    <script type="text/javascript" src="{{ asset('frontend/js/jquery.validate.min.js') }}"></script>
	<script>

		$(function() {
		    @if(session('success'))        
		        toastr.success("{{session('success')}}");
		    @endif
		    @if(session('error'))
		        toastr.error("{{session('error')}}");                    
		    @endif
		});


		$(document).find(".alert").delay(8000).fadeOut("slow");
		function isOnlyNumber(evt) {
    
			evt = (evt) ? evt : window.event;
			var charCode = (evt.which) ? evt.which : evt.keyCode;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) {
				return false;
			}
			return true;
		}

		if ($(window).width() < 960) {
		   $("header .rpart ul li a").click(function(){
				$(this).parent().find('.submenu').slideToggle();
			});
		}

		$("#mobilemenu").click(function(){
			$("header .custnav ul.menus").slideToggle();
		});
		// For SubMenus
		$('.custnav .msubmenu').click( function() {
			var trig = $(this);
			if ( trig.hasClass('opensub') ) {
				trig.next('.custnav .submenus').toggle();
				trig.removeClass('opensub');
			} else {
				$('.opensub').next('.custnav .submenus').toggle();
				$('.opensub').removeClass('opensub');
				trig.next('.custnav .submenus').toggle();
				trig.addClass('opensub');
			};
		return false;
		});

	</script>

	

	@if(\Request::route()->getName() == "checkout.view")
		<script>
			$(window).scroll(function() {    
		    var scroll = $(window).scrollTop();
		    if($(window).width() > 960){
			    if (scroll > 50) {
                    $("header").addClass("header");
			        $("header").addClass("header1");
			    } 
			    else {
			    	$("header").removeClass("header");
                    $("header").removeClass("header1");
			    }
		    }
		});
		</script>
	@else
		<script>
			$(window).scroll(function() {    
		    var scroll = $(window).scrollTop();
		    if($(window).width() > 960){
			    if (scroll > 50) {
                    $("header").addClass("header");
			        $("header").addClass("header1");
			    } 
			    else {
			    	$("header").removeClass("header");
                    $("header").removeClass("header1");
			    }
		    }
		});
		</script>
	@endif

	<script type="text/javascript">
		$('.dropdown').hover(	
           function () {$(this).children(".dropdown-menu").addClass("show");}, 
           function () {$(".dropdown-menu").removeClass("show")}
        );
	</script>

    @yield('scripts')        
</body>
</html>