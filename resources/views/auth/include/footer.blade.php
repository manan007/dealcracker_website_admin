<footer class="footer">
    <!-- <div class="topfooter">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 col-md-3 col-12 comm">
                    <div class="flogo">
                        <a href="{{ route('home.index') }}"><img src="{{ asset('assets/frontend/images/ESR-Logo.png') }}" alt=""></a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 col-12 comm">
						<h3>Quick Links</h3>
					</div>
					<div class="col-sm-6 col-md-3 col-12 comm">
						<h3>How it Works</h3>
					</div>
					<div class="col-sm-6 col-md-3 col-12 comm">
						<h3>Terms & Conditions</h3>
					</div>
            </div>
        </div>	
    </div> -->
    <div class="copyright">
        <div class="container-fluid">
            <ul class="socialpart">
                <li><a href="#"><img src="{{ asset('frontend/images/icons/facebook-icon.svg') }}" alt=""></a></li>
                <li><a href="#"><img src="{{ asset('frontend/images/icons/instagram-icon.svg') }}" alt=""></a></li>
                <li><a href="#"><img src="{{asset('frontend/images/icons/linkedin-icon.svg')}}" alt=""></a></li>
            </ul>
            <div class="copypart">
                <p>&copy; {{ date('Y') }} Powered By: DealCracker</p>
            </div>
        </div>
    </div>

</footer>