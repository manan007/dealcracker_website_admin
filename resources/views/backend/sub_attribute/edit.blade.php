@extends('backend.layouts.main') 
@section('title','Sub Attribute Edit')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="m-0 text-dark">Edit Sub Attribute</h3>
            </div>
        </div>
      </div>
      <div class="card-body">
        <form action="{{ route('admin.sub_attribute.update',[$attribute_data->id,$sub_attribute_details->id]) }}" name="sub_attribute_form" method="post">
        @csrf

          <div class="row mb-3">
            <div class="col-md-4">
              <label>Sub Attribute Name</label>
              <input class="form-control" type="text" name="name" id="name" value="{{ $sub_attribute_details->name ?? '' }}">
              @error('name')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="{{ route('admin.sub_attribute.index',$attribute_data->id) }}" class="btn btn-danger">Cancel</a>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  $("form[name='sub_attribute_form']").validate({
    rules: 
    {
      name:'required'
    },
    messages: 
    {
      name:'Please enter sub attribute name.'
    }
  });
</script>
@endsection