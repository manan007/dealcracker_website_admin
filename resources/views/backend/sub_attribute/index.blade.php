@extends('backend.layouts.main') 
@section('title','Sub Attribute List')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-11">
                <h3 class="m-0 text-dark"><b>{{ $attribute_data->name ?? '' }}</b> Sub Attribute List</h3>
            </div>
            <div class="col-sm-1 text-right">
                <a href="{{ route('admin.attribute.index') }}" class="btn btn-default">Back</a>
                <a href="{{ route('admin.sub_attribute.create',$attribute_data->id) }}" class="btn btn-primary">Add</a>
            </div>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="dynamic_table">
            <table id="datatable" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>Sr No.</th>
                <th width="70%">Sub Attribute Name</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
                @if(count($sub_attributes) > 0)
                  @foreach($sub_attributes as $key => $data)
                    <tr>
                      <td>{{$key+1}}</td>
                      <td>{{$data->name}}</td>
                      <td class="d-flex">
                        <a class="btn btn-success text-light action-btn mr-3" href="{{route('admin.sub_attribute.edit',[$attribute_data->id,$data->id])}}"><i class="fas fa-pen"></i> </a>

                        <form action="{{URL::route('admin.sub_attribute.delete',[$attribute_data->id,$data->id])}}" method="POST" id="change_status_{{$data->id}}">
                        @csrf
                        <button type="submit" name="submit" data-id="{{$data->id}}"  class="hideBtn submitData_changestatus_{{$data->id}} d-none"><i class="ft-trash-2"></i> Submit</button>
                        <a class="btn btn-danger text-light action-btn delete" title="Delete" data-id="{{ $data->id }}"><i class="fas fa-trash-alt"></i></a>
                        </form>
                      </td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
        </div>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  $(document).on('click','.delete',function(){
        var id=$(this).data('id');
        var getval = $(this).data('value');
        bootbox.confirm("Are you sure you want to delete this sub attribute ?", function(result){ 
            if(result == true)
            {   
              $('.submitData_changestatus_'+id).trigger('click');
            }
        });
    });
</script>
@endsection