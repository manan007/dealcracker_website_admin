@extends('backend.layouts.main') 
@section('title','Transaction List')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-11">
                <h3 class="m-0 text-dark"><b> {{ $user_details->fullname ?? '' }} </b>Transaction List</h3>
            </div>
            <div class="col-sm-1 text-right">
              <a href="{{ route('admin.user.index') }}" class="btn btn-default">Back</a>
            </div>
        </div>
        <!-- <h3 class="m-0 text-dark">User List</h3> -->
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="dynamic_table">
            <table id="datatable" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th width="7%">Sr No.</th>
                <th>Type</th>
                <th>Transaction Id</th>
                <th width="15%">Amount ( $ )</th>
                <th width="10%">Payment Method</th>
                <th width="10%">Status</th>
              </tr>
              </thead>
              <tbody>
                @if(count($transaction_details) > 0)
                  @foreach($transaction_details as $key => $data)
                    <tr>
                      <td>{{$key+1}}</td>
                      <td>{{ ucfirst($data->type) }}</td>
                      <td>{{$data->transaction_id ?? ''}}</td>
                      <td>{{$data->amount ?? 0 }}</td>
                      <td>
                        @if($data->payment_method == "stripe")
                          <img src="{{ asset('assets/dist/img/stripe.png') }}" class="stripe_img">
                        @else
                        {{$data->payment_method ?? '' }}
                        @endif
                      </td>
                      <td>
                      @if($data->is_paid == 1)
                        <span class="badge bg-success">Success</span>
                      @else
                        <span class="badge bg-danger">Fail</span>
                      @endif
                      </td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
        </div>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  $(document).on('click','.delete',function(){
        var id=$(this).data('id');
        var getval = $(this).data('value');
        bootbox.confirm("Are you sure you want to delete this plan ?", function(result){ 
            if(result == true)
            {   
              $('.submitData_changestatus_'+id).trigger('click');
            }
        });
    });
</script>
@endsection