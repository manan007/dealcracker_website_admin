@extends('backend.layouts.main') 
@section('title','Opportunity List')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="m-0 text-dark">Opportunity List</h3>
            </div>
        </div>
        <!-- <h3 class="m-0 text-dark">User List</h3> -->
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="dynamic_table">
            <table id="datatable" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th width="7%">Sr No.</th>
                <th>UserName</th>
                <th>Title</th>
                <th>Description</th>
                <th>Status</th>
                <th width="10%">Action</th>
              </tr>
              </thead>
              <tbody>
                @if(count($opportunity) > 0)
                  @foreach($opportunity as $key => $data)
                    <tr>
                      <td>{{$key+1}}</td>
                      <td>{{ $data->user_details->name ?? '' }}</td>
                      <td>{{$data->title ?? ''}}</td>
                      <td>{{$data->description ?? ''}}</td>
                      <td>
                          <form action="{{URL::route('admin.opportunity.change_status')}}" method="POST" id="change_status_{{$data->id}}">
                          @csrf
                          <button type="submit" name="submit" data-id="{{$data->id}}"  class="hideBtn submitData_changestatus_{{$data->id}} d-none"><i class="ft-trash-2"></i> Submit</button>
                          <input type="checkbox" class="status_submit" name="my-checkbox"  data-value="{{ $data->status }}" data-id="{{ $data->id }}" id="chk_{{ $data->id }}"  @if($data->status == 1) checked @endif data-bootstrap-switch>
                          </form>
                      </td>
                      <td class="d-flex">
                        <form action="{{URL::route('admin.opportunity.delete',$data->id)}}" method="POST" id="submitData_delete_{{$data->id}}">
                          @csrf
                          <button type="submit" name="submit" data-id="{{$data->id}}"  class="hideBtn submitData_delete_{{$data->id}} d-none"><i class="ft-trash-2"></i> Submit</button>
                          <a class="btn btn-outline-danger action-btn delete" title="Delete" data-id="{{ $data->id }}"><i class="fas fa-trash-alt"></i></a>
                        </form>
                      </td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
        </div>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">

  $("input[data-bootstrap-switch]").each(function(){
    $(this).bootstrapSwitch('state', $(this).prop('checked'));
  });

  $(document).on('click','.delete',function(){
      var id=$(this).data('id');
      var getval = $(this).data('value');
      bootbox.confirm("Are you sure you want to delete this opportunity ?", function(result){ 
          if(result == true)
          {   
            $('.submitData_delete_'+id).trigger('click');
          }
      });
  });


  $('.status_submit').on('switchChange.bootstrapSwitch', function(event, state) {
      var id=$(this).data('id');
      var getval = $(this).data('value');
      var state = state;
      if(state){
        changeStatus(id);
      }
      else{
        changeStatus(id);
      }
  });

  function changeStatus(id){
    $('.loader').show();
    $.ajax({
      url: "{{ route('admin.opportunity.change_status') }}",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      },
      data: {
      "id":id,
      },
      type: "GET",
      success: function(result) {
        $('.loader').hide();
        toastr.success(result.msg);
      } 
    });
  }



  // bootbox.confirm("Are you sure you want to change this status ?", function(result){ 
  //     if(result == true)
  //     {
  //       $('.submitData_changestatus_'+id).trigger('click');
  //     }
  //     else{

  //     }
  // });
    
</script>
@endsection