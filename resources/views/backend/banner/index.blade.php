@extends('backend.layouts.main') 
@section('title','Banner List')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-11">
                <h3 class="m-0 text-dark">Banner List</h3>
            </div>
            <div class="col-sm-1 text-right">
                @if(count($banners) <= 0)<a href="{{ route('admin.banner.create') }}" class="btn btn-primary">Add</a>@endif
            </div>
        </div>
        <!-- <h3 class="m-0 text-dark">User List</h3> -->
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="dynamic_table">
            <table id="datatable" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th width="10%">Sr No.</th>
                <th width="75%">Banner Image</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
                @if(count($banners) > 0)
                  @foreach($banners as $key => $data)
                    <tr>
                      <td>{{$key+1}}</td>
                      <td>
                        @if($data->app_banner)
                          <img src="{{ asset('upload/app_banner/'.$data->app_banner) }}" height="100px" width="130px" />
                        @endif
                      </td>
                      <!-- <td>
                        @if($data->web_banner)
                          <img src="{{ asset('upload/web_banner/'.$data->web_banner) }}" height="100px" width="130px" />
                        @endif
                      </td> -->
                      <td class="d-flex">
                        <a class="btn btn-outline-success action-btn mr-3" href="{{route('admin.banner.edit',$data->id)}}"><i class="fas fa-pen"></i> </a>

                        <form action="{{URL::route('admin.banner.delete',$data->id)}}" method="POST" id="change_status_{{$data->id}}">
                        @csrf
                        <button type="submit" name="submit" data-id="{{$data->id}}"  class="hideBtn submitData_changestatus_{{$data->id}} d-none"><i class="ft-trash-2"></i> Submit</button>
                        <a class="btn btn-outline-danger action-btn delete" title="Delete" data-id="{{ $data->id }}"><i class="fas fa-trash-alt"></i></a>
                        </form>
                      </td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
        </div>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  $(document).on('click','.delete',function(){
        var id=$(this).data('id');
        var getval = $(this).data('value');
        bootbox.confirm("Are you sure you want to delete this banner ?", function(result){ 
            if(result == true)
            {   
              $('.submitData_changestatus_'+id).trigger('click');
            }
        });
    });
</script>
@endsection