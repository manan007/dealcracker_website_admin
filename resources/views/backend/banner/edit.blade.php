@extends('backend.layouts.main') 
@section('title','Banner Edit')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="m-0 text-dark">Edit Banner</h3>
            </div>
        </div>
      </div>
      <div class="card-body">
        <form action="{{ route('admin.banner.update',$banner_details->id) }}" name="banner_form" method="post" enctype="multipart/form-data">
        @csrf  

          <div class="row mb-3">
            <div class="col-md-6">
              <label>Banner Image</label>
              <input class="form-control" type="file" name="app_banner" id="app_banner" value="">
              <input type="hidden" name="hidden_app_banner" value="{{ $banner_details->app_banner }}">
              @if($banner_details->app_banner)
                <br/><img src="{{ asset('upload/app_banner/'.$banner_details->app_banner) }}" height="100px" width="130px" />
              @endif
              @error('app_banner')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>
            {{--
            <div class="col-md-6">
              <label>Website Banner</label>
              <input class="form-control" type="file" name="web_banner" id="web_banner" value="">
              <input type="hidden" name="hidden_web_banner" value="{{ $banner_details->web_banner }}">
              @if($banner_details->web_banner)
                <br/><img src="{{ asset('upload/web_banner/'.$banner_details->web_banner) }}" height="100px" width="130px" />
              @endif
              @error('web_banner')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>
            --}}
          </div>

          <div class="row">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="{{ route('admin.banner.index') }}" class="btn btn-danger">Cancel</a>
            </div>
          </div>

        </form>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  // $("form[name='banner_form']").validate({
  //   rules: 
  //   {
  //     name:'required'
  //   },
  //   messages: 
  //   {
  //     name:'Please enter platform name.'
  //   }
  // });
</script>
@endsection