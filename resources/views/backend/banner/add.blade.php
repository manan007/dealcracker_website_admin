@extends('backend.layouts.main') 
@section('title','Banner Add')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="m-0 text-dark">Add Banner</h3>
            </div>
        </div>
        <!-- <h3 class="m-0 text-dark">User List</h3> -->
      </div>
      <div class="card-body">
        <form action="{{ route('admin.banner.store') }}" name="banner_form" method="post" enctype="multipart/form-data">
        @csrf  

          <div class="row mb-3">
            <div class="col-md-6">
              <label>Banner Image</label>
              <input class="form-control" type="file" name="app_banner" id="app_banner" value="">
              @error('app_banner')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>
            <!-- <div class="col-md-6">
              <label>Website Banner</label>
              <input class="form-control" type="file" name="web_banner" id="web_banner" value="">
              @error('web_banner')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div> -->
          </div>

          <div class="row">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="{{ route('admin.banner.index') }}" class="btn btn-danger">Cancel</a>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  $("form[name='banner_form']").validate({
    rules: 
    {
      app_banner:'required',
      // web_banner:'required'
    },
    messages: 
    {
      app_banner:'Please select Banner Image.',
      // web_banner:'Please select website banner.'
    }
  });
</script>
@endsection