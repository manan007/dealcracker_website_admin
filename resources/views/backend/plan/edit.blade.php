@extends('backend.layouts.main') 
@section('title','Subscription Plan Edit')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="m-0 text-dark">Edit Subscription Plan</h3>
            </div>
        </div>
      </div>
      <div class="card-body">
        <form action="{{ route('admin.plan.update',$plan_details->id) }}" name="plan_form" method="post">
        @csrf  

          <div class="row mb-3">
            <div class="col-md-4">
              <label>Name</label>
              <input class="form-control" type="text" name="name" id="name" value="{{ $plan_details->name ?? '' }}">
              @error('name')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>

            <div class="col-md-4">
              <label>Price</label>
              <div class="input-group">
                <input class="form-control" type="text" name="price" id="price" value="{{ $plan_details->price ?? '' }}" onkeypress="return isOnlyNumber();">
                <div class="input-group-append">
                  <div class="input-group-text"><i class="fas fa-rupee-sign"></i></div>
                </div>
              </div>
              <span class="text-danger" id="priceerror"></span>
              @error('price')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>

            <div class="col-md-4">
              <label>Duration <span class="text-danger">(In Days)</span></label>
              <input class="form-control" type="text" name="duration" id="duration" value="{{ $plan_details->duration ?? '' }}" placeholder="Duration" onkeypress="return isOnlyNumber();">
              @error('duration')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>

          </div>

          <div class="row mt-5">
            <div class="col-sm-11">
                <h4 class="m-0 text-dark">Plan Features</h4>
            </div>
            <div class="col-md-1 text-right">
                <a href="javascript:;" class="btn btn-success add_field_button" id="add_field"><i class="fa fa-plus"></i></a>
            </div>
          </div>
          <hr/>
          @if(count($plan_features) > 0)
            @foreach($plan_features as $plan_feature)
            <div class="row mb-3 newMinus">
              <div class="col-md-11">
                <textarea class="form-control" name="feature[]" id="feature" value="{{ $plan_feature->feature ?? '' }}" placeholder="Enter Feature">{{ $plan_feature->feature ?? '' }}</textarea>
              </div>
              <div class="col-md-1">
                <a href="javascript:;" class="btn btn-danger remove_field" id="add_field"><i class="fa fa-minus"></i></a>
              </div>
            </div>
            @endforeach
          @endif

          <div class="newPlus"></div>

          <div class="row">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="{{ route('admin.plan.index') }}" class="btn btn-danger">Cancel</a>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  $("form[name='plan_form']").validate({
    rules: 
    {
      name:'required',
      price:'required'
    },
    messages: 
    {
      name:'Please enter plan name.',
      price:'Please enter plan price.'
    },
    errorPlacement: function(error, element) {
        if (element.attr("name") == "price")
            error.insertAfter("#priceerror");               
        else
            error.insertAfter(element);
    }
  });

  var max_fields      = 30;
  var wrapper         = $(".newPlus");
  var add_button      = $(".add_field_button");

  var x = 1;
  $(add_button).click(function(e){
      e.preventDefault();
      if(x < max_fields){
          x++;
          $(wrapper).append('<div class="newMinus"><div class="row mb-3"><div class="col-md-11"><textarea class="form-control" name="feature[]" id="feature" value="" placeholder="Enter Feature"></textarea></div><div class="col-md-1"><a href="javascript:;" class="btn btn-danger remove_field" id="add_field"><i class="fa fa-minus"></i></a></div></div></div>');
      }
  });

  $(document).on("click",".remove_field", function(e){
      e.preventDefault();
      $(this).closest(".newMinus").remove();
  });

</script>
@endsection