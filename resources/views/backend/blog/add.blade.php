@extends('backend.layouts.main') 
@section('title','Blog Add')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="m-0 text-dark">Add Blog</h3>
            </div>
        </div>
        <!-- <h3 class="m-0 text-dark">User List</h3> -->
      </div>
      <div class="card-body">
        <form action="{{ route('admin.blog.store') }}" name="blog_form" method="post" enctype="multipart/form-data">
        @csrf  

          <div class="row mb-3">
            <div class="col-md-12">
              <label>Title</label>
              <input class="form-control" type="text" name="title" id="title" value="{{ old('title') }}" placeholder="Blog Title">
              @error('name')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>
          </div>

          <div class="row mb-3">
              <div class="form-group col-md-12">
                  <label for="">Description</label>
                  <textarea class="form-control" name="description" id="description" placeholder="Enter Description" value=""></textarea>
              </div>
          </div>

          <div class="row mb-3">
              <div class="form-group col-md-12">
                  <label for="">Select Media <span class="text-danger">( Accepted format: .jpg, .jpeg, .png, .mp4 )</span></label>
                  <input type="file" class="form-control" name="media[]" id="media" multiple="true" accept=".jpg,.jpeg,.png,.mp4" />
              </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="{{ route('admin.blog.index') }}" class="btn btn-danger">Cancel</a>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
<script type="text/javascript">
  $("form[name='blog_form']").validate({
    rules: 
    {
      title:'required'
    },
    messages: 
    {
      title:'Please enter blog title.'
    }
  });

    $(document).ready(function () {
         if (CKEDITOR.instances && CKEDITOR.instances['description']) {
             var instance = CKEDITOR.instances['description'];
             if (instance) {
                 CKEDITOR.remove(instance);
             }
         }
         CKEDITOR.replace('description', {
             contentsLangDirection: 'ltr'
         });

    });
</script>

<!-- <script type="text/javascript">

</script> -->
@endsection