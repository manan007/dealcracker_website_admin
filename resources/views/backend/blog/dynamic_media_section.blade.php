<div class="row">
@if(count($blog_medias) > 0)
	@foreach($blog_medias as $blog_media)
			@if($blog_media->type == "image")
				@if($blog_media->blog_media)
					<div class="col-md-2">
						<img src="{{ asset('upload/blog/'.$blog_media->blog_media)}}" width="90%" height="100%">
						<div class="text-center">
							<form action="{{URL::route('admin.blog.delete_media')}}" method="POST" id="change_status_{{$blog_media->id}}">
	                          @csrf
	                          <input type="hidden" name="blog_id" value="{{$blog_details->id}}" />
	                          <input type="hidden" name="blog_media_id" value="{{$blog_media->id}}" />
	                          <button type="submit" name="submit" data-id="{{$blog_media->id}}"  class="hideBtn submitData_mediadelete_{{$blog_media->id}} d-none"><i class="ft-trash-2"></i> Submit</button>
	                          <a class="text-danger action-btn delete_blog_media text-center" title="Delete" data-id="{{ $blog_media->id }}"><i class="fas fa-trash-alt"></i></a>
	                        </form>
                        </div>
					</div>
				@endif
			@else
				@if($blog_media->blog_media)
					<div class="col-md-2">
						<video width="90%" height="100%" controls>
		                    <source src="{{asset('upload/blog/'.$blog_media->blog_media)}}" type='video/mp4'>
		                </video>
		                <div class="text-center">
							<form action="{{URL::route('admin.blog.delete_media')}}" method="POST" id="change_status_{{$blog_media->id}}">
	                          @csrf
	                          <input type="hidden" name="blog_id" value="{{$blog_details->id}}" />
	                          <input type="hidden" name="blog_media_id" value="{{$blog_media->id}}" />
	                          <button type="submit" name="submit" data-id="{{$blog_media->id}}"  class="hideBtn submitData_mediadelete_{{$blog_media->id}} d-none"><i class="ft-trash-2"></i> Submit</button>
	                          <a class="text-danger action-btn delete_blog_media text-center" title="Delete" data-id="{{ $blog_media->id }}"><i class="fas fa-trash-alt"></i></a>
	                        </form>
                        </div>
					</div>
				@endif		           
			@endif
	@endforeach
@endif
</div>