@extends('backend.layouts.main') 
@section('title','Blog List')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-11">
                <h3 class="m-0 text-dark">Blog List</h3>
            </div>
            <div class="col-sm-1 text-right">
                <a href="{{ route('admin.blog.create') }}" class="btn btn-primary">Add</a>
            </div>
        </div>
        <!-- <h3 class="m-0 text-dark">User List</h3> -->
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="dynamic_table">
            <table id="datatable" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>Sr No.</th>
                <th width="80%">Title</th>
                <!-- <th width="50%">Description</th> -->
                <th width="10%">Action</th>
              </tr>
              </thead>
              <tbody>
                @if(count($blogs) > 0)
                  @foreach($blogs as $key => $data)
                    <tr>
                      <td>{{$key+1}}</td>
                      <td>{{$data->title}}</td>
                      <!-- <td>{!! $data->description !!}</td> -->
                      <td class="d-flex">
                        <a class="btn btn-outline-success action-btn mr-3" href="{{route('admin.blog.edit',$data->id)}}"><i class="fas fa-pen"></i> </a>

                        <form action="{{URL::route('admin.blog.delete',$data->id)}}" method="POST" id="change_status_{{$data->id}}">
                        @csrf
                        <button type="submit" name="submit" data-id="{{$data->id}}"  class="hideBtn submitData_changestatus_{{$data->id}} d-none"><i class="ft-trash-2"></i> Submit</button>
                        <a class="btn btn-outline-danger action-btn delete" title="Delete" data-id="{{ $data->id }}"><i class="fas fa-trash-alt"></i></a>
                        </form>
                      </td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
        </div>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  $(document).on('click','.delete',function(){
        var id=$(this).data('id');
        var getval = $(this).data('value');
        bootbox.confirm("Are you sure you want to delete this blog ?", function(result){ 
            if(result == true)
            {   
              $('.submitData_changestatus_'+id).trigger('click');
            }
        });
    });
</script>
@endsection