@extends('backend.layouts.main') 
@section('title','Attribute Add')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="m-0 text-dark">Add Attribute</h3>
            </div>
        </div>
        <!-- <h3 class="m-0 text-dark">User List</h3> -->
      </div>
      <div class="card-body">
        <form action="{{ route('admin.attribute.store') }}" name="attribute_form" method="post">
        @csrf  

          <div class="row mb-3">
            <div class="col-md-4">
              <label>Attribute Name</label>
              <input class="form-control" type="text" name="name" id="name" value="{{ old('name') }}">
              @error('name')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="{{ route('admin.attribute.index') }}" class="btn btn-danger">Cancel</a>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  $("form[name='attribute_form']").validate({
    rules: 
    {
      name:'required'
    },
    messages: 
    {
      name:'Please enter attribute name.'
    }
  });
</script>
@endsection