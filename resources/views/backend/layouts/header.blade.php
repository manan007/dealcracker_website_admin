<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
    </li>
    <!-- <li class="nav-item d-none d-sm-inline-block">
      <a href="index3.html" class="nav-link">Home</a>
    </li>
    <li class="nav-item d-none d-sm-inline-block">
      <a href="#" class="nav-link">Contact</a>
    </li> -->
  </ul>

  <!-- SEARCH FORM -->
  <form class="form-inline ml-3">
    <!-- <div class="input-group input-group-sm">
      <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-navbar" type="submit">
          <i class="fas fa-search"></i>
        </button>
      </div>
    </div> -->
  </form>

  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
    <!-- Messages Dropdown Menu -->
    
    <!-- Notifications Dropdown Menu -->
    <li class="nav-item dropdown">
      <a class="nav-link p-0" data-toggle="dropdown" href="#">
          <img src="{{asset('assets/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" width="40" height="40" alt="">
          <span class="avatar-name ml-2 pt-1"> {{ Auth::user()->name ?? '' }}</span>
      </a>
      <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
        <!-- <div class="dropdown-divider"></div> -->
        <!-- <a href="#" class="dropdown-item">
          <i class="fas fa-envelope mr-2"></i> Profile
        </a>
        <div class="dropdown-divider"></div> -->

        <a class="dropdown-item" href="{{ route('admin.logout') }}">
            <i class="fas fa-sign-out-alt mr-2"></i> Logout
        </a>

      </div>
    </li>
    <li class="nav-item">
      <!-- <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
        <i class="fas fa-th-large"></i>
      </a> -->
    </li>
  </ul>
</nav>
<!-- /.navbar -->