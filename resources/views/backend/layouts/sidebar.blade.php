<style type="text/css">
  .brand-link {
      display: block;
      font-size: 1.25rem;
      line-height: 1.;
      padding: 21px 27px 4px 66px;
      m: ;
      transition: width .3s ease-in-out;
      white-space: nowrap;
  }
</style>

<aside class="main-sidebar sidebar-dark-primary elevation-4">

    <!-- Brand Logo -->
    <a href="{{ route('admin.dashbord.view') }}" class="brand-link" style="padding: 21px 27px 4px 22px;">
        <!-- <a href="{{ route('admin.login_process') }}" style="font-size: 30px;font-weight: bold;color: #2E742B;"><b>BECO</b></a> -->
        <!-- <img src="{{ asset('assets/dist/img/logo.png')}}" alt="Logo" class="brand-image elevation-3"> -->
        <span class="brand-text font-weight-light" style="color: #ffffff;font-size: 38px;font-weight: bold;line-height: 0;padding-top: 20px!important;"><b>Deal-Cracker</b></span> 
    </a>

    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="{{ asset('assets/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            <a href="#" class="d-block">Alexander Pierce</a>
        </div>
        </div> -->

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                <li class="nav-item">
                    <a href="{{ route('admin.dashbord.view') }}" class="nav-link @if(Request::Route()->getName() == 'admin.dashbord.view') active @endif">
                        <i class="nav-icon fas fa-tachometer-alt"></i><p>Dashboard</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.user.index') }}" class="nav-link @if(Request::Route()->getName() == 'admin.user.index' || Request::Route()->getName() == 'admin.user_detail.view' || Request::Route()->getName() == 'admin.user.create' || Request::Route()->getName() == 'admin.user.edit') active @endif">
                        <i class="nav-icon fas fa-users"></i><p>Users</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.opportunity.index') }}" class="nav-link @if(Request::Route()->getName() == 'admin.opportunity.index') active @endif">
                        <i class="nav-icon fas fa-list"></i><p>Opportunities</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.business.index') }}" class="nav-link @if(Request::Route()->getName() == 'admin.business.index') active @endif">
                        <i class="nav-icon fas fa-list-ul"></i><p>Business Details</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.business_template.index') }}" class="nav-link @if(Request::Route()->getName() == 'admin.business_template.index') active @endif">
                        <i class="nav-icon fas fa-list-ul"></i><p>Business Template</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.feed.index') }}" class="nav-link @if(Request::Route()->getName() == 'admin.feed.index') active @endif">
                        <i class="nav-icon fas fa-list-ul"></i><p>Feeds</p>
                    </a>
                </li>
                    
                <li class="nav-item">
                    <a href="{{ route('admin.business_category.index') }}" class="nav-link @if(Request::Route()->getName() == 'admin.business_category.index' || Request::Route()->getName() == 'admin.business_category.create' || Request::Route()->getName() == 'admin.business_category.edit') active @endif">
                        <i class="nav-icon fas fa-list"></i><p>Business Categories</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.template_category.index') }}" class="nav-link @if(Request::Route()->getName() == 'admin.template_category.index' || Request::Route()->getName() == 'admin.template_category.create' || Request::Route()->getName() == 'admin.template_category.edit') active @endif">
                        <i class="nav-icon fas fa-list"></i><p>Template Categories</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.template_sub_category.index') }}" class="nav-link @if(Request::Route()->getName() == 'admin.template_sub_category.index' || Request::Route()->getName() == 'admin.template_sub_category.create' || Request::Route()->getName() == 'admin.template_sub_category.edit') active @endif">
                        <i class="nav-icon fas fa-list"></i><p>Template Sub Categories</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.plan.index') }}" class="nav-link @if(Request::Route()->getName() == 'admin.plan.index' || Request::Route()->getName() == 'admin.plan.create' || Request::Route()->getName() == 'admin.plan.edit') active @endif">
                        <i class="nav-icon fas fa-list"></i><p>Subscription Plans</p>
                    </a>
                </li>
            
                <li class="nav-item">
                    <a href="{{ route('admin.blog.index') }}" class="nav-link @if(Request::Route()->getName() == 'admin.blog.index' || Request::Route()->getName() == 'admin.blog.create' || Request::Route()->getName() == 'admin.blog.edit') active @endif">
                        <i class="nav-icon fas fa-list-ul"></i><p>Blogs</p>
                    </a>
                </li>
                
                <li class="nav-item">
                    <a href="{{ route('admin.support.index') }}" class="nav-link @if(Request::Route()->getName() == 'admin.support.index' || Request::Route()->getName() == 'admin.support.answer') active @endif">
                        <i class="nav-icon fas fa-list-ul"></i><p>Support</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.contact_us.index') }}" class="nav-link @if(Request::Route()->getName() == 'admin.contact_us.index' || Request::Route()->getName() == 'admin.contact_us.edit') active @endif">
                        <i class="nav-icon fas fa-list-ul"></i><p>Contact US</p>
                    </a>
                </li>        
                


                <li class="nav-item has-treeview @if(Request::Route()->getName() == 'admin.platform.index' || Request::Route()->getName() == 'admin.platform.create' || Request::Route()->getName() == 'admin.platform.edit' || Request::Route()->getName() == 'admin.banner.index' || Request::Route()->getName() == 'admin.banner.create' || Request::Route()->getName() == 'admin.banner.edit' || Request::Route()->getName() == 'admin.testimonial.index' || Request::Route()->getName() == 'admin.testimonial.create' || Request::Route()->getName() == 'admin.testimonial.edit') menu-open @endif">
                    <a href="#" class="nav-link @if(Request::Route()->getName() == 'admin.platform.index' || Request::Route()->getName() == 'admin.platform.create' || Request::Route()->getName() == 'admin.platform.edit' || Request::Route()->getName() == 'admin.banner.index' || Request::Route()->getName() == 'admin.banner.create' || Request::Route()->getName() == 'admin.banner.edit' || Request::Route()->getName() == 'admin.testimonial.index' || Request::Route()->getName() == 'admin.testimonial.create' || Request::Route()->getName() == 'admin.testimonial.edit') active @endif">
                    <!-- <i class="nav-icon fas fa-universal-access"></i> --><p>Master<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.platform.index') }}" class="nav-link @if(Request::Route()->getName() == 'admin.platform.index' || Request::Route()->getName() == 'admin.platform.create' || Request::Route()->getName() == 'admin.platform.edit') active @endif">
                                <i class="far fa-circle nav-icon"></i><p>Platform</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('admin.banner.index') }}" class="nav-link @if(Request::Route()->getName() == 'admin.banner.index' || Request::Route()->getName() == 'admin.banner.create' || Request::Route()->getName() == 'admin.banner.edit') active @endif">
                                <i class="far fa-circle nav-icon"></i><p>Banner</p>
                            </a>
                        </li>

                        {{--<li class="nav-item">
                            <a href="{{ route('admin.testimonial.index') }}" class="nav-link @if(Request::Route()->getName() == 'admin.testimonial.index' || Request::Route()->getName() == 'admin.testimonial.create' || Request::Route()->getName() == 'admin.testimonial.edit') active @endif">
                                <i class="far fa-circle nav-icon"></i><p>Testimonial</p>
                            </a>
                        </li>--}}
                        

                        <!-- <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon"></i><p>Plan</p>
                            </a>
                        </li> -->
                    </ul>
                </li>
                
            </ul>
        </nav>
    </div>
</aside>
