<footer class="main-footer text-center">

  <strong>Copyright &copy; {{ date('Y') }} <a href="{{ route('admin.dashbord.view') }}">DealCracker</a>.</strong>

  All rights reserved.

</footer>