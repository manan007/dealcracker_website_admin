@extends('backend.layouts.main') 
@section('title','Feed List')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="m-0 text-dark">Feed List</h3>
            </div>
        </div>
        <!-- <h3 class="m-0 text-dark">User List</h3> -->
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="dynamic_table">
            <table id="datatable" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th width="7%">Sr No.</th>
                <th>UserName</th>
                <th>Title</th>
                <th>Description</th>
                <th width="10%">Feed Media</th>
                <th width="10%">Action</th>
              </tr>
              </thead>
              <tbody>
                @if(count($feeds) > 0)
                  @foreach($feeds as $key => $data)
                    @php 
                        $feed_medias = App\Models\FeedMedia::where(['feed_id' => $data->id])->get();
                    @endphp
                    <tr>
                      <td>{{$key+1}}</td>
                      <td>{{ $data->user_details->name ?? '' }}</td>
                      <td>{{$data->title ?? ''}}</td>
                      <td>{{$data->description ?? ''}}</td>
                      <td>
                        <a href="#" data-toggle="modal" title="Feed Media" data-target="#feed-media_{{ $key }}" class="btn btn-outline-info action-btn">Feed Media</a>
                      </td>
                      <td class="d-flex">
                        <form action="{{URL::route('admin.feed.delete',$data->id)}}" method="POST" id="change_status_{{$data->id}}">
                          @csrf
                          <button type="submit" name="submit" data-id="{{$data->id}}"  class="hideBtn submitData_changestatus_{{$data->id}} d-none"><i class="ft-trash-2"></i> Submit</button>
                          <a class="btn btn-outline-danger text-light action-btn delete" title="Delete" data-id="{{ $data->id }}"><i class="fas fa-trash-alt"></i></a>
                        </form>
                      </td>
                    </tr>

                    <div class="modal fade" id="feed-media_{{ $key }}" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">Feed Media</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>

                                <div class="modal-body" style="height:auto;max-height:640px;overflow-y:auto;">
                                    <div class="row">
                                        @if(count($feed_medias))
                                            @foreach($feed_medias as $feed_media)
                                                <div class="col-sm-3">
                                                    @if($feed_media->type == "image")
                                                        <a href="{{ asset('upload/feed/'.$feed_media->feed_media) }}" target="_blank"><img src="{{ asset('upload/feed/'.$feed_media->feed_media) }}" height="120px" width="100%"></a>
                                                    @else
                                                        <a href="{{asset('upload/feed/'.$feed_media->feed_media)}}" target="_blank">
                                                        <video width="100%" height="120px" controls>
                                                            <source src="{{asset('upload/feed/'.$feed_media->feed_media)}}" type='video/mp4' alt="First slide">
                                                        </video>
                                                        </a>
                                                    @endif
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="col-sm-12">
                                                No media found.
                                            </div>
                                        @endif    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  @endforeach
                @endif
              </tbody>
            </table>
        </div>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  $(document).on('click','.delete',function(){
        var id=$(this).data('id');
        var getval = $(this).data('value');
        bootbox.confirm("Are you sure you want to delete this feed ?", function(result){ 
            if(result == true)
            {   
              $('.submitData_changestatus_'+id).trigger('click');
            }
        });
    });
</script>
@endsection