@extends('backend.layouts.main') 
@section('title','Template Sub Category Edit')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="m-0 text-dark">Edit Template Sub Category</h3>
            </div>
        </div>
      </div>
      <div class="card-body">
        <form action="{{ route('admin.template_sub_category.update',$template_sub_category_details->id) }}" name="template_sub_category_form" method="post">
        @csrf  

          <div class="row mb-3">


            <div class="col-md-6">
              <label>Template Category</label>
              <select class="form-control" name="template_category_id">
                @if(count($template_categories) > 0)
                  @foreach($template_categories as $template_category)
                    <option value="{{ $template_category->id }}" @if($template_category->id == $template_sub_category_details->template_category_id) selected="" @endif>{{ $template_category->name }}</option>
                  @endforeach
                @endif
              </select>
              @error('template_category_id')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>

            <div class="col-md-6">
              <label>Template Category Name</label>
              <input class="form-control" type="text" name="name" id="name" value="{{ $template_sub_category_details->name ?? '' }}" placeholder="Template Category">
              @error('name')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="{{ route('admin.template_sub_category.index') }}" class="btn btn-danger">Cancel</a>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  $("form[name='template_sub_category_form']").validate({
    rules: 
    {
      name:'required'
    },
    messages: 
    {
      name:'Please enter template sub category name.'
    }
  });
</script>
@endsection