@extends('backend.layouts.main') 
@section('title','Plan')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="m-0 text-dark">Edit Plan</h3>
            </div>
        </div>
      </div>
      <div class="card-body">
        <form action="{{ route('admin.plan_feature.update',[$plan_data->id,$plan_feature_details->id]) }}" name="plan_feature_form" method="post">
        @csrf  

          <div class="row mb-3">
            <div class="col-md-12">
              <label>Feature</label>
              <textarea class="form-control" name="feature" id="feature" value="{{ $plan_feature_details->feature }}" placeholder="Enter Feature">{{ $plan_feature_details->feature }}</textarea>
              @error('feature')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="{{ route('admin.plan_feature.index',$plan_data->id) }}" class="btn btn-danger">Cancel</a>
            </div>
          </div>

        </form>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  $("form[name='plan_feature_form']").validate({
    rules: 
    {
      feature:'required'
    },
    messages: 
    {
      feature:'Please enter feature.'
    }
  });
</script>
@endsection