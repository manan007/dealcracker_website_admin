@extends('backend.layouts.main') 
@section('title','Contact Us Edit')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="m-0 text-dark">Edit Contact Us</h3>
            </div>
        </div>
      </div>
      <div class="card-body">
        <form action="{{ route('admin.contact_us.update',$contactus_details->id) }}" name="contact_form" method="post">
        @csrf  

          <div class="row mb-3">
            <div class="col-md-4">
              <label>Name</label>
              <input class="form-control" type="text" name="name" id="name" value="{{ $contactus_details->name ?? '' }}" placeholder="Name">
              @error('name')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>

            <div class="col-md-4">
              <label>Email</label>
              <input class="form-control" type="email" name="email" id="email" value="{{ $contactus_details->email ?? '' }}" placeholder="Email">
              @error('email')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>

            <div class="col-md-4">
              <label>Phone Number</label>
              <input class="form-control" type="text" name="phone" id="phone" value="{{ $contactus_details->phone ?? '' }}" onkeypress="return isOnlyNumber();" placeholder="Phone Number">
              @error('phone')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>
          </div>

          <div class="row mb-3">
            <div class="col-md-12">
              <label>Message</label>
              <textarea class="form-control" type="text" name="message" id="message" value="{{ $contactus_details->message ?? '' }}" placeholder="Enter Message..">{{ $contactus_details->message ?? '' }}</textarea>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="{{ route('admin.contact_us.index') }}" class="btn btn-danger">Cancel</a>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  $("form[name='contact_form']").validate({
    rules: 
    {
      name:'required',
      email:'required'
    },
    messages: 
    {
      name:'Please enter name.',
      email:'Please enter email.'
    }
  });

</script>
@endsection