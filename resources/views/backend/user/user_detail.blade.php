@extends('backend.layouts.main') 
@section('title','User Details')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-11">
                <h3 class="m-0 text-dark">User Details</h3>
            </div>
            <div class="col-sm-1 text-right">
              <a href="{{ route('admin.user.index') }}" class="btn btn-default">Back</a>
            </div>
        </div>
        <!-- <h3 class="m-0 text-dark">User List</h3> -->
      </div>
      <div class="card-body">

          <div class="row mb-3">
            <div class="col-md-12">
              <div class="row mb-2">
                <div class="col-md-3">
                  <label>Name</label>
                  <p>{{ $user_details->name ?? '' }}</p>
                </div>
                <div class="col-md-3">
                  <label>Email</label>
                  <p>{{ $user_details->email ?? '' }}</p>
                </div>
                <div class="col-md-3">
                  <label>Phone Number</label>
                  <p>{{ $user_details->phone ?? '' }}</p>
                </div>
                <div class="col-md-3">
                  <label>Address</label>
                  <p>{{ $user_details->address ?? '' }}</p>
                </div>
              </div>
              <hr class="mt-0" />
              <div class="row">
                <div class="col-md-3">
                  <label>Date Of Birth</label>
                  <p>{{ $user_details->dob ?? '' }}</p>
                </div>
                <div class="col-md-3">
                  <label>Website URL</label>
                  <p>{{ $user_details->website ?? '' }}</p>
                </div>
                <div class="col-md-3">
                  <label>Key Skills</label>
                  <p>{{ $user_details->key_skills ?? '' }}</p>
                </div>
                <div class="col-md-3">
                  <label>Business Domain</label>
                  <p>{{ $user_details->business_domain ?? '' }}</p>
                </div>
              </div>
            </div>
          </div>
          <hr />
          <div class="row mb-3">
            <div class="col-md-3">
              <label>Address</label>
              <p>{{ $user_details->address ?? '' }}</p>
            </div>
            <div class="col-md-3">
              <label>Profile Description</label>
              <p>{{ $user_details->profile_description ?? '' }}</p>
            </div>
            <div class="col-md-3">
              <label>Other Information</label>
              <p>{{ $user_details->other_info ?? '' }}</p>
            </div>
            <div class="col-md-3">
              <label>Platforms</label>
              @php
                $platforms = [];
                if($user_details->platforms){
                  $platfs = explode(',',$user_details->platforms);
                  if(count($platfs) > 0){
                    foreach($platfs as $platf){
                      $pdetails = App\Models\Platform::where(['id' => $platf])->first();
                      if($pdetails){
                        $platforms[] = $pdetails->name;
                      } 
                    }
                  }
                }
              @endphp
              <p>{{ implode(' , ',$platforms) }}</p>
            </div>
          </div>
          <hr class="@if(empty($user_details->business_logo) && empty($user_details->cv)) mt-5  @endif" />
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  $("form[name='event_category_form']").validate({
    rules: 
    {
      name:'required'
    },
    messages: 
    {
      name:'Please enter event category name.'
    }
  });
</script>
@endsection