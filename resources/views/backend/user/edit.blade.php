@extends('backend.layouts.main') 
@section('title','User Edit')

@section('css')
  <link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endsection

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="m-0 text-dark">Edit User</h3>
            </div>
        </div>
        <!-- <h3 class="m-0 text-dark">User List</h3> -->
      </div>
      <div class="card-body">
        <form action="{{ route('admin.user.update',$user_details->id) }}" name="user_form" method="post">
        @csrf  

          <div class="row mb-3">

            <div class="col-md-3">
              <label>Name</label>
              <input class="form-control" type="text" name="name" id="name" placeholder="Name" value="{{ $user_details->name ?? '' }}">
              @error('name')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>

            <div class="col-md-3">
              <label>Email</label>
              <input class="form-control" type="email" name="email" id="email" placeholder="Email" value="{{ $user_details->email ?? '' }}">
              @error('email')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>

            <div class="col-md-3">
              <label>Password</label>
              <input class="form-control" type="password" name="password" id="password" placeholder="Password" value="">
              @error('password')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>

            <div class="col-md-3">
              <label>Phone Number</label>
              <input class="form-control" type="text" name="phone" id="phone" placeholder="Phone" value="{{ $user_details->phone ?? '' }}" maxlength="10" onkeypress="return isOnlyNumber();">
              @error('phone')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>

          </div>

          <div class="row mb-3">
            
            

            <div class="col-md-3">
              <label>Date Of Birth</label>
              <input class="form-control" type="date" name="dob" id="dob" placeholder="Date Of Birth" value="{{ $user_details->dob ?? '' }}">
              @error('dob')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>

            <div class="col-md-3">
              <label>Website URL</label>
              <input class="form-control" type="url" name="website" id="website" placeholder="Website" value="{{ $user_details->website ?? '' }}">
              @error('website')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>

            <div class="col-md-3">
              <label>Key Skills</label>
              <input class="form-control" type="text" name="key_skills" id="key_skills" placeholder="Key Skills" value="{{ $user_details->key_skills ?? '' }}">
              @error('key_skills')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>


            <div class="col-md-3">
              <label>Business Domain</label>
              <input class="form-control" type="text" name="business_domain" id="business_domain" placeholder="Business Domain" value="{{ $user_details->business_domain ?? '' }}">
              @error('business_domain')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>
            

          </div>  

          
          <div class="row mb-3">
            
            <div class="col-md-6">
              <label>Address</label>
              <textarea class="form-control" name="address" id="address" placeholder="Address" value="{{ $user_details->address ?? '' }}">{{ $user_details->address ?? '' }}</textarea>
              @error('address')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>

            <div class="col-md-6">
              <label>Profile Description</label>
              <textarea class="form-control" name="profile_description" id="profile_description" placeholder="Profile Description" value="{{ $user_details->profile_description ?? '' }}">{{ $user_details->profile_description ?? '' }}</textarea>
              @error('profile_description')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>

          </div>

          
          <div class="row mb-3">   

            <div class="col-md-7">
              <label>Other Information</label>
              <input class="form-control" type="text" name="other_info" id="other_info" placeholder="Other Information" value="{{ $user_details->other_info ?? '' }}">
              @error('other_info')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>
              
            <div class="col-md-5">
              <label>Select Platforms</label>
              <select class="form-control select2" name="platforms[]" id="platforms" multiple="true">
                @if($platforms)
                @foreach($platforms as $platform)
                  @php  $platform_arr=[]; if($user_details->platforms){ $platform_arr = explode(',',$user_details->platforms); } @endphp
                  <option value="{{ $platform->id }}" @if(in_array($platform->id,$platform_arr)) selected="" @endif>{{ $platform->name ?? '' }}</option>
                @endforeach
              @endif
              </select>
              <span class="text-danger" id="platformserror"></span>
            </div>

          </div>
            
              

              

          


          <div class="row">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="{{ route('admin.user.index') }}" class="btn btn-danger">Cancel</a>
            </div>
          </div>

        </form>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script src="{{ asset('assets/plugins/select2/js/select2.full.min.js')}}"></script>

<script type="text/javascript">
  $('.select2').select2();

  $("form[name='user_form']").validate({
    rules: 
    {
      name:'required',
      email:'required',
      phone:'required',
    },
    messages: 
    {
      name:'Please enter name.',
      email:'Please enter email.',
      phone:'Please enter phone number.'
    }
  });



</script>
@endsection