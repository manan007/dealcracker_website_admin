@extends('backend.layouts.main') 
@section('title','Product List')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-11">
                <h3 class="m-0 text-dark"><b> {{ $user_details->fullname ?? '' }} </b>Product List</h3>
            </div>
            <div class="col-sm-1 text-right">
              <a href="{{ route('admin.user.index') }}" class="btn btn-default">Back</a>
            </div>
        </div>
        <!-- <h3 class="m-0 text-dark">User List</h3> -->
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="dynamic_table">
            <table id="datatable" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th width="4%">Sr No.</th>
                <th width="8%">Image</th>
                <th width="10%">Type</th>
                <th width="20%">Name</th>
                <th width="8%">Price</th>
                <th width="10%">Discount ( % )</th>
                <th>Description</th>
                <!-- <th width="10%">Action</th> -->
              </tr>
              </thead>
              <tbody>
                @if(count($products) > 0)
                  @foreach($products as $key => $data)
                    <tr>
                      <td>{{$key+1}}</td>
                      <td>
                        @if($data->image)
                          <img src="{{ asset('upload/product/'.$data->image) }}" class="product_img_table" />
                        @else
                          <img src="{{ asset('upload/product/default_product_image.png') }}" class="product_img_table" />
                        @endif
                      </td>
                      <td>{{ ucfirst($data->product_type) }}</td>
                      <td>{{$data->name ?? ''}}</td>
                      <td>{{$data->price ?? ''}}</td>
                      <td>{{$data->discount ?? '0'}}</td>
                      <td>{{$data->description ?? ''}}</td>
                      {{--<td class="d-flex">
                        <a class="btn btn-info text-light action-btn mr-3" href="{{ route('admin.user_detail.view',$data->id) }}" title="User Details"><i class="fas fa-eye"></i> </a>
                        
                        <a class="btn btn-success text-light action-btn mr-3" href=""><i class="fas fa-pen"></i> </a>

                        <form action="{{URL::route('admin.plan_feature.delete',[$plan_data->id,$data->id])}}" method="POST" id="change_status_{{$data->id}}">
                        @csrf
                        <button type="submit" name="submit" data-id="{{$data->id}}"  class="hideBtn submitData_changestatus_{{$data->id}} d-none"><i class="ft-trash-2"></i> Submit</button>
                        <a class="btn btn-danger text-light action-btn delete" title="Delete" data-id="{{ $data->id }}"><i class="fas fa-trash-alt"></i></a>
                        </form>
                        
                      </td>--}}
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
        </div>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  $(document).on('click','.delete',function(){
        var id=$(this).data('id');
        var getval = $(this).data('value');
        bootbox.confirm("Are you sure you want to delete this plan feature ?", function(result){ 
            if(result == true)
            {   
              $('.submitData_changestatus_'+id).trigger('click');
            }
        });
    });
</script>
@endsection