@extends('backend.layouts.main') 
@section('title','Friend List')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-11">
                <h3 class="m-0 text-dark"><b> {{ $user_details->fullname ?? '' }} </b> Friend List</h3>
            </div>
            <div class="col-sm-1 text-right">
              <a href="{{ route('admin.user.index') }}" class="btn btn-default">Back</a>
            </div>
        </div>
        <!-- <h3 class="m-0 text-dark">User List</h3> -->
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="dynamic_table">
            <table id="datatable" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th width="3%">Sr No.</th>
                <th width="5%">Profile</th>
                <th width="4%">Account Type</th>
                <th width="11%">Name</th>
                <th width="11%">Email</th>
                <th width="7%">Phone</th>
              </tr>
              </thead>
              <tbody>
                @if(count($friends) > 0)
                  @foreach($friends as $key => $data)

                    @php 

                      if($data->user_id == $user_details->id){
                        $friend_details = $data->friend_details;
                      }
                      else{
                        $friend_details = $data->user_details;
                      }

                    @endphp

                    <tr>
                      <td>{{$key+1}}</td>
                      <td>
                        @if($friend_details->avtar)
                           <img src="{{ asset('upload/profile/'.$friend_details->avtar) }}" class="profile_img_table" />
                        @else
                          <img src="{{ asset('upload/profile/defaultuser.png') }}" class="profile_img_table" />
                        @endif
                      </td>
                      <td>@if($friend_details->account_type){{ ucfirst($friend_details->account_type) }} @endif</td>
                      <td>{{$friend_details->fullname ?? ''}}</td>
                      <td>{{$friend_details->email ?? ''}}</td>
                      <td>{{$friend_details->phone ?? ''}}</td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
        </div>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  $(document).on('click','.delete',function(){
        var id=$(this).data('id');
        var getval = $(this).data('value');
        bootbox.confirm("Are you sure you want to delete this plan feature ?", function(result){ 
            if(result == true)
            {   
              $('.submitData_changestatus_'+id).trigger('click');
            }
        });
    });
</script>
@endsection