@extends('backend.layouts.main') 
@section('title','Support Details')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="m-0 text-dark">Support Details List</h3>
            </div>
        </div>
        <!-- <h3 class="m-0 text-dark">User List</h3> -->
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="dynamic_table">
            <table id="datatable" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th width="3%">Sr No.</th>
                <th width="12%">User Name</th>
                <th width="35%">Question</th>
                <th width="35%">Answer</th>
                <th width="15%">Action</th>
              </tr>
              </thead>
              <tbody>
                @if(count($supports) > 0)
                  @foreach($supports as $key => $data)
                    <tr>
                      <td>{{$key+1}}</td>
                      <td>{{$data->user_details->name ?? ''}}</td>
                      <td>{{$data->question}}</td>
                      <td>{{$data->answer}}</td>
                      <td class="d-flex">

                        @if($data->answer)
                          <a class="btn btn-outline-success action-btn mr-3" href="{{route('admin.support.answer',$data->id)}}"><i class="fas fa-pen"></i></a>
                        @else
                          <a class="btn btn-outline-info action-btn mr-3" href="{{route('admin.support.answer',$data->id)}}">Submit Answer</a>
                        @endif

                        <form action="{{URL::route('admin.support.delete',$data->id)}}" method="POST" id="change_status_{{$data->id}}">
                        @csrf
                        <button type="submit" name="submit" data-id="{{$data->id}}"  class="hideBtn submitData_changestatus_{{$data->id}} d-none"><i class="ft-trash-2"></i> Submit</button>
                        <a class="btn btn-outline-danger action-btn delete" title="Delete" data-id="{{ $data->id }}"><i class="fas fa-trash-alt"></i></a>
                        </form>
                      </td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
        </div>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  $(document).on('click','.delete',function(){
        var id=$(this).data('id');
        var getval = $(this).data('value');
        bootbox.confirm("Are you sure you want to delete this question ?", function(result){ 
            if(result == true)
            {   
              $('.submitData_changestatus_'+id).trigger('click');
            }
        });
    });
</script>
@endsection