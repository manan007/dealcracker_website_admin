@extends('backend.layouts.main') 
@section('title','Support')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="m-0 text-dark">Support</h3>
            </div>
        </div>
        <!-- <h3 class="m-0 text-dark">User List</h3> -->
      </div>
      <div class="card-body">
        <form action="{{ route('admin.support.answer_store',$question_details->id) }}" name="submit_answer_form" method="post">
        @csrf  

          <div class="row mb-3">
            <div class="col-md-12">
              <label>Question</label>
              <input class="form-control" type="text" name="question" id="question" value="{{ $question_details->question }}" readonly="true">
              @error('question')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>
          </div>

          <div class="row mb-3">
            <div class="col-md-12">
              <label>Answer</label>
              <textarea class="form-control" type="text" name="answer" id="answer" value="{{ $question_details->answer ?? '' }}" placeholder="Enter Answer">{{ $question_details->answer ?? '' }}</textarea>
              @error('answer')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>

          </div>

          <div class="row">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary">Submit Answer</button>
              <a href="{{ route('admin.support.index') }}" class="btn btn-danger">Cancel</a>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  $("form[name='submit_answer_form']").validate({
    rules: 
    {
      answer:'required'
    },
    messages: 
    {
      answer:'Please enter answer.'
    }
  });




</script>
@endsection