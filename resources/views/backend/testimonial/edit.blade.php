@extends('backend.layouts.main') 
@section('title','Testimonial Edit')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="m-0 text-dark">Edit Testimonial</h3>
            </div>
        </div>
      </div>
      <div class="card-body">
        <form action="{{ route('admin.testimonial.update',$testimonial_details->id) }}" name="testimonial_form" method="post" enctype="multipart/form-data">
        @csrf  

          <div class="row mb-3">
            <div class="col-md-6">
              <label>Title</label>
              <input class="form-control" type="text" placeholder="Title" name="title" id="title" value="{{ $testimonial_details->title }}">
              @error('title')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>
            <div class="col-md-6">
              <label>Document</label>
              <input class="form-control" type="file" name="document" id="document" value="">
              <input type="hidden" name="hidden_document" value="{{ $testimonial_details->document }}">
              <input type="hidden" name="hidden_ext" value="{{ $testimonial_details->document_type }}">
              @if($testimonial_details->document)
                @if($testimonial_details->document_type == "video")
                <br/><video height="140px" width="160px" controls>
                    <source src="{{asset('upload/testimonial/'.$testimonial_details->document)}}" type='video/mp4'>
                </video>
                @else
                <br/><img src="{{ asset('upload/testimonial/'.$testimonial_details->document) }}" height="120px" width="160px" />
                @endif
              @endif
              @error('document')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="{{ route('admin.testimonial.index') }}" class="btn btn-danger">Cancel</a>
            </div>
          </div>

        </form>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
 $("form[name='testimonial_form']").validate({
    rules: 
    {
      title:'required'
    },
    messages: 
    {
      title:'Please enter title.'
    }
  });
</script>
@endsection