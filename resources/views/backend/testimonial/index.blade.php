@extends('backend.layouts.main') 
@section('title','Testimonial List')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-11">
                <h3 class="m-0 text-dark">Testimonial List</h3>
            </div>
            <div class="col-sm-1 text-right">
                <a href="{{ route('admin.testimonial.create') }}" class="btn btn-primary">Add</a>
            </div>
        </div>
        <!-- <h3 class="m-0 text-dark">User List</h3> -->
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="dynamic_table">
            <table id="datatable" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>Sr No.</th>
                <th width="25%">Title</th>
                <th width="25%">Document</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
                @if(count($testimonials) > 0)
                  @foreach($testimonials as $key => $data)
                    <tr>
                      <td>{{$key+1}}</td>
                      <td>{{ $data->title ?? '' }}</td>
                      <td>
                        @if($data->document)
                          @if($data->document_type == "video")
                          <video height="140px" width="160px" controls>
                              <source src="{{asset('upload/testimonial/'.$data->document)}}" type='video/mp4'>
                          </video>
                          @else
                          <img src="{{ asset('upload/testimonial/'.$data->document) }}" height="120px" width="160px" />
                          @endif
                        @endif
                      </td>
                      <td class="d-flex">
                        <a class="btn btn-outline-success action-btn mr-3" href="{{route('admin.testimonial.edit',$data->id)}}"><i class="fas fa-pen"></i> </a>

                        <form action="{{URL::route('admin.testimonial.delete',$data->id)}}" method="POST" id="change_status_{{$data->id}}">
                        @csrf
                        <button type="submit" name="submit" data-id="{{$data->id}}"  class="hideBtn submitData_changestatus_{{$data->id}} d-none"><i class="ft-trash-2"></i> Submit</button>
                        <a class="btn btn-outline-danger action-btn delete" title="Delete" data-id="{{ $data->id }}"><i class="fas fa-trash-alt"></i></a>
                        </form>
                      </td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
        </div>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  $(document).on('click','.delete',function(){
        var id=$(this).data('id');
        var getval = $(this).data('value');
        bootbox.confirm("Are you sure you want to delete this testimonial ?", function(result){ 
            if(result == true)
            {   
              $('.submitData_changestatus_'+id).trigger('click');
            }
        });
    });
</script>
@endsection