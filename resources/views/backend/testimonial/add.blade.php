@extends('backend.layouts.main') 
@section('title','Testimonial Add')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="m-0 text-dark">Add Testimonial</h3>
            </div>
        </div>
        <!-- <h3 class="m-0 text-dark">User List</h3> -->
      </div>
      <div class="card-body">
        <form action="{{ route('admin.testimonial.store') }}" name="testimonial_form" method="post" enctype="multipart/form-data">
        @csrf  

          <div class="row mb-3">
            <div class="col-md-6">
              <label>Title</label>
              <input class="form-control" type="text" placeholder="Title" name="title" id="title" value="">
              @error('title')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>
            <div class="col-md-6">
              <label>Document</label>
              <input class="form-control" type="file" name="document" id="document" value="">
              @error('document')<span class="text-danger"><strong>{{ $message }}</strong></span>@enderror
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="{{ route('admin.testimonial.index') }}" class="btn btn-danger">Cancel</a>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  $("form[name='testimonial_form']").validate({
    rules: 
    {
      title:'required',
      document:'required'
    },
    messages: 
    {
      title:'Please enter title.',
      document:'Please select document.'
    }
  });
</script>
@endsection