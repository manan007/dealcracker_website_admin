<table id="datatable" class="table table-bordered table-hover">
  <thead>
  <tr>
    <th width="4%">Sr No.</th>
    <th width="8%">Image</th>
    <th>User Name</th>
    <th width="10%">Type</th>
    <th width="20%">Name</th>
    <th width="8%">Price</th>
    <th width="10%">Discount ( % )</th>
    <th>Description</th>
    <th width="10%">Action</th>
  </tr>
  </thead>
  <tbody>
    @if(count($products) > 0)
      @foreach($products as $key => $data)
        <tr>
          <td>{{$key+1}}</td>
          <td>
            @if($data->image)
              <img src="{{ asset('upload/product/'.$data->image) }}" class="product_img_table" />
            @else
              <img src="{{ asset('upload/product/default_product_image.png') }}" class="product_img_table" />
            @endif
          </td>
          <td>{{$data->user_details->fullname ?? ''  }}</td>
          <td>{{ ucfirst($data->product_type) }}</td>
          <td>{{$data->name ?? ''}}</td>
          <td>{{$data->price ?? ''}}</td>
          <td>{{$data->discount ?? '0'}}</td>
          <td>{{$data->description ?? ''}}</td>
          <td class="d-flex">
            {{-- <a class="btn btn-info text-light action-btn mr-3" href="{{ route('admin.product.view',$data->id) }}" title="User Details"><i class="fas fa-eye"></i> </a>
            --}}
            <a class="btn btn-success text-light action-btn mr-3" href="{{ route('admin.product.edit',$data->id) }}"><i class="fas fa-pen"></i> </a>

            <form action="{{URL::route('admin.product.delete',$data->id)}}" method="POST" id="change_status_{{$data->id}}">
            @csrf
            <button type="submit" name="submit" data-id="{{$data->id}}"  class="hideBtn submitData_changestatus_{{$data->id}} d-none"><i class="ft-trash-2"></i> Submit</button>
            <a class="btn btn-danger text-light action-btn delete" title="Delete" data-id="{{ $data->id }}"><i class="fas fa-trash-alt"></i></a>
            </form>
            
          </td>
        </tr>
      @endforeach
    @endif
  </tbody>
</table>