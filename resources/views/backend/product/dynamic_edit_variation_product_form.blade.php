<div class="modal-dialog" role="document" style="max-width:1200px">
    <div class="modal-content">
        <div class="modal-header border-0">
            <h5 class="modal-title" id="exampleModalLabel"> Edit Variation Product</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form method="post" id="updateProductVariationForm" enctype="multipart/form-data">
        @csrf    
            <hr class="mt-0" />
            <div class="modal-body">                

                <input type="hidden" name="product_id" value="{{ $variation_product->id }}">
                <input type="hidden" name="attribute" value="{{ implode(',',$attributes) }}">

                <div class="row mb-3">
                    @if(count($attributes) > 0)
                        @foreach($attributes as $attribute)
                        @php
                        $attribute_details = App\Models\Attribute::where(['id' => $attribute])->first();
                        $get_sub_attributes = App\Models\Attribute::where(['is_parent' => $attribute])->get();

                        $get_selected_attributes = App\Models\ProductAttribute::where(['product_id' => $variation_product->id,'attribute_id' => $attribute,'is_delete' => 0])->first();
                        @endphp
                            <div class="col-md-2">
                                <label>Select {{ $attribute_details->name }}</label>
                                <div class="input-group">
                                <select class="form-control select2 sub_attribute" name="{{ $attribute }}_sub_attribute">
                                    @if($get_sub_attributes)
                                    @foreach($get_sub_attributes as $get_sub_attribute)
                                        <option value="{{ $get_sub_attribute->id }}" @if($get_selected_attributes->sub_attribute_id == $get_sub_attribute->id) selected="" @endif>{{ $get_sub_attribute->name ?? '' }}</option>
                                    @endforeach
                                    @endif
                                </select>
                                </div>
                                <span class="text-danger" id="priceerror"></span>
                            </div>
                        @endforeach
                    @endif
                </div>

                <div class="row mb-3">
                    <div class="col-md-3">
                        <label>Name</label>
                      <input class="form-control variation_name" type="text" name="variation_name" value="{{ $variation_product->name ?? '' }}">
                    </div>

                    <div class="col-md-3">
                        <label>Price</label>
                      <div class="input-group">
                        <input class="form-control variation_price" type="text" name="variation_price" value="{{ $variation_product->price ?? '' }}" onkeypress="return isNumberKey(event);">
                        <div class="input-group-append">
                          <div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                        <label>Discount</label>
                        <div class="input-group">
                        <input class="form-control variation_discount" type="text" name="variation_discount" value="{{ $variation_product->discount ?? '' }}" onkeypress="return isNumberKey(event);">
                        <div class="input-group-append">
                          <div class="input-group-text"><i class="fas fa-percentage"></i></div>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Quantity</label>
                      <input type="text" class="form-control variation_quanty" name="variation_quanty" placeholder="Quantity" value="{{ $variation_product->quantity ?? '' }}" onkeypress="return isOnlyNumber();">
                    </div>
                </div>

                <div class="row">
                    <div class="@if($variation_product->image) col-md-4 @else col-md-6 @endif">
                      <label>Variation Image <span class="text-danger">( Accepted format: .jpg, .jpeg, .png )</span></label>
                      <input class="form-control" type="file" name="variation_image" id="variation_image" accept=".jpg,.jpeg,.png">
                    </div>

                    @if($variation_product->image)
                    <div class="col-md-2">
                        <input type="hidden" name="hidden_variation_image_edit" value="{{ $variation_product->image }}">
                        <img src="{{ asset('upload/product/'.$variation_product->image) }}" class="edit_product_img" />
                    </div>
                    @endif

                    <div class="col-md-6">
                      <label>Description</label>
                      <textarea name="variation_description" class="form-control variation_description" placeholder="Enter Description" value="{{ $variation_product->description ?? '' }}">{{ $variation_product->description ?? '' }}</textarea>
                    </div>
                </div>
            </div>
        
            <div class="modal-footer border-0 d-block">
                <div class="row">
                    <div class="col-12 text-right">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <a href="javascript:;" class="btn btn-primary update_product_variation">Save changes</a>
                    </div>
                </div>    
            </div>

        </form>
    </div>
</div>