@if($product_type == "physical")
		
	@if(!empty($product_details))

		<div class="row mb-3">
			<div class="@if($product_details->image) col-md-4 @else col-md-6 @endif">
			  <label>Select Image <span class="text-danger">( Accepted format: .jpg, .jpeg, .png )</span></label>
			  <input class="form-control" type="file" name="image" id="image" accept=".jpg,.jpeg,.png">
			</div>
			@if($product_details->image)
			<div class="col-md-2">
				<input type="hidden" name="hidden_image" value="{{ $product_details->image }}">
                <img src="{{ asset('upload/product/'.$product_details->image) }}" class="edit_product_img" />
			</div>
			@endif

			<div class="col-md-6">
			  <label>Name</label>
			  <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="{{ $product_details->name ?? '' }}">
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-8">
			  <label>Description</label>
			  <textarea name="description" class="form-control" id="description" placeholder="Enter Description" value="{{ $product_details->description ?? '' }}">{{ $product_details->description ?? '' }}</textarea>
			</div>

			<div class="col-md-4">
			  <label>SKU</label>
			  <input type="text" class="form-control" name="sku" id="sku" placeholder="SKU" value="{{ $product_details->sku ?? '' }}">
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-6">
				<label>Select Attribute</label>
				<select class="select2 attribute" name="attribute[]" multiple="multiple" data-placeholder="Select Attribute">
					@if(count($get_attributes))
						@foreach($get_attributes as $get_attribute)
							<option value="{{ $get_attribute->id }}" @if(in_array($get_attribute->id,$attributes)) selected="" @endif>{{ $get_attribute->name ?? '' }}</option>
						@endforeach
					@endif
				</select>
			</div>
		</div>

	@else

		<div class="row mb-3">
			<div class="col-md-6">
			  <label>Select Image <span class="text-danger">( Accepted format: .jpg, .jpeg, .png )</span></label>
			  <input class="form-control" type="file" name="image" id="image" accept=".jpg,.jpeg,.png">
			</div>

			<div class="col-md-6">
			  <label>Name</label>
			  <input type="text" class="form-control" name="name" id="name" placeholder="Name">
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-8">
			  <label>Description</label>
			  <textarea name="description" class="form-control" id="description" placeholder="Enter Description"></textarea>
			</div>

			<div class="col-md-4">
			  <label>SKU</label>
			  <input type="text" class="form-control" name="sku" id="sku" placeholder="SKU">
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-6">
				<label>Select Attribute</label>
				<select class="select2 attribute" name="attribute[]" multiple="multiple" data-placeholder="Select Attribute">
					@if(count($get_attributes))
						@foreach($get_attributes as $get_attribute)
							<option value="{{ $get_attribute->id }}" @if(in_array($get_attribute->id,$attributes)) selected="" @endif>{{ $get_attribute->name ?? '' }}</option>
						@endforeach
					@endif
				</select>
			</div>
		</div>

	@endif

	<div class="dynamic_add_variation_section">
		@include('backend.product.dynamic_add_variation_section')
	</div>

@elseif($product_type == "virtual")

	@if(!empty($product_details))

		<div class="row mb-3">
			<div class="@if($product_details->image) col-md-4 @else col-md-6 @endif">
			  <label>Select Image <span class="text-danger">( Accepted format: .jpg, .jpeg, .png )</span></label>
			  <input class="form-control" type="file" name="image" id="image" accept=".jpg,.jpeg,.png">
			</div>
			@if($product_details->image)
			<div class="col-md-2">
				<input type="hidden" name="hidden_image" value="{{ $product_details->image }}">
                <img src="{{ asset('upload/product/'.$product_details->image) }}" class="edit_product_img" />
			</div>
			@endif

			<div class="col-md-6">
			  <label>Name</label>
			  <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="{{ $product_details->name ?? '' }}">
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-8">
			  <label>Description</label>
			  <textarea name="description" class="form-control" id="description" placeholder="Enter Description" value="{{ $product_details->description ?? '' }}">{{ $product_details->description ?? '' }}</textarea>
			</div>

			<div class="col-md-4">
			  <label>SKU</label>
			  <input type="text" class="form-control" name="sku" id="sku" placeholder="SKU" value="{{ $product_details->sku ?? '' }}">
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-4">
			  	<label>Price</label>
				<div class="input-group">
					<input class="form-control" type="text" name="price" id="price" value="{{ $product_details->price ?? '' }}" onkeypress="return isNumberKey(event);">
					<div class="input-group-append">
						<div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
					</div>
				</div>
			  	<span class="text-danger" id="priceerror"></span>
			</div>

			<div class="col-md-4">
			  	<label>Discount</label>
				<div class="input-group">
					<input class="form-control" type="text" name="discount" id="discount" value="{{ $product_details->discount ?? '' }}" onkeypress="return isNumberKey(event);">
					<div class="input-group-append">
						<div class="input-group-text"><i class="fas fa-percentage"></i></div>
					</div>
				</div>
			  	<span class="text-danger" id="discounterror"></span>
			</div>

			<div class="col-md-4">
			  <label>Quantity</label>
			  <input type="text" class="form-control" name="quantity" id="quantity" placeholder="Quantity" value="{{ $product_details->quantity ?? '' }}" onkeypress="return isOnlyNumber();">
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-6">
			  <label>Select File <span class="text-danger">( Accepted format: .mp3, .mp4 )</span></label>
			  <input class="form-control" type="file" name="virtual_product_file" id="virtual_product_file" accept=".mp3,.mp4">
			</div>
			@if($product_details->virtual_product_file)
			<input type="hidden" name="hidden_virtual_product_file" value="{{ $product_details->virtual_product_file }}">
			<input type="hidden" name="hidden_virtual_product_file_type" value="{{ $product_details->virtual_product_file_type }}">
			<div class="col-md-6 mt-4">
				<p class="mt-2"><a target="_blank" href="{{ asset('/upload/product/virtual_product_file/'.$product_details->virtual_product_file) }}">{{ asset('/upload/product/virtual_product_file/'.$product_details->virtual_product_file) }}</a></p>
			</div> 
            @endif
		</div>

	@else

		<div class="row mb-3">
			<div class="col-md-6">
			  <label>Select Image <span class="text-danger">( Accepted format: .jpg, .jpeg, .png )</span></label>
			  <input class="form-control" type="file" name="image" id="image" accept=".jpg,.jpeg,.png">
			</div>

			<div class="col-md-6">
			  <label>Name</label>
			  <input type="text" class="form-control" name="name" id="name" placeholder="Name">
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-8">
			  <label>Description</label>
			  <textarea name="description" class="form-control" id="description" placeholder="Enter Description"></textarea>
			</div>

			<div class="col-md-4">
			  <label>SKU</label>
			  <input type="text" class="form-control" name="sku" id="sku" placeholder="SKU">
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-4">
			  	<label>Price</label>
				<div class="input-group">
					<input class="form-control" type="text" name="price" id="price" value="{{ old('price') }}" onkeypress="return isNumberKey(event);">
					<div class="input-group-append">
						<div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
					</div>
				</div>
			  	<span class="text-danger" id="priceerror"></span>
			</div>

			<div class="col-md-4">
			  	<label>Discount</label>
				<div class="input-group">
					<input class="form-control" type="text" name="discount" id="discount" value="{{ old('discount') }}" onkeypress="return isNumberKey(event);">
					<div class="input-group-append">
						<div class="input-group-text"><i class="fas fa-percentage"></i></div>
					</div>
				</div>
			  	<span class="text-danger" id="discounterror"></span>
			</div>

			<div class="col-md-4">
			  <label>Quantity</label>
			  <input type="text" class="form-control" name="quantity" id="quantity" placeholder="Quantity" onkeypress="return isOnlyNumber();">
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-6">
			  <label>Select File <span class="text-danger">( Accepted format: .mp3, .mp4 )</span></label>
			  <input class="form-control" type="file" name="virtual_product_file" id="virtual_product_file" accept=".mp3,.mp4">
			</div>
		</div>

	@endif

@elseif($product_type == "service")

	
	@if(!empty($product_details))

		<div class="row mb-3">
			<div class="@if($product_details->image) col-md-4 @else col-md-6 @endif">
			  <label>Select Image <span class="text-danger">( Accepted format: .jpg, .jpeg, .png )</span></label>
			  <input class="form-control" type="file" name="image" id="image" accept=".jpg,.jpeg,.png">
			</div>

			@if($product_details->image)
			<div class="col-md-2">
				<input type="hidden" name="hidden_image" value="{{ $product_details->image }}">
                <img src="{{ asset('upload/product/'.$product_details->image) }}" class="edit_product_img" />
			</div>
			@endif

			<div class="col-md-6">
			  <label>Name</label>
			  <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="{{ $product_details->name ?? '' }}">
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-12">
			  <label>Description</label>
			  <textarea name="description" class="form-control" id="description" placeholder="Enter Description" value="{{ $product_details->description ?? '' }}">{{ $product_details->description ?? '' }}</textarea>
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-6">
			  	<label>Price</label>
				<div class="input-group">
					<input class="form-control" type="text" name="price" id="price" value="{{ $product_details->price ?? '' }}" onkeypress="return isNumberKey(event);">
					<div class="input-group-append">
						<div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
					</div>
				</div>
			  	<span class="text-danger" id="priceerror"></span>
			</div>

			<div class="col-md-6">
			  	<label>Discount</label>
				<div class="input-group">
					<input class="form-control" type="text" name="discount" id="discount" value="{{ $product_details->discount ?? '' }}" onkeypress="return isNumberKey(event);">
					<div class="input-group-append">
						<div class="input-group-text"><i class="fas fa-percentage"></i></div>
					</div>
				</div>
			  	<span class="text-danger" id="discounterror"></span>
			</div>
		</div>

	@else

		<div class="row mb-3">
			<div class="col-md-6">
			  <label>Select Image <span class="text-danger">( Accepted format: .jpg, .jpeg, .png )</span></label>
			  <input class="form-control" type="file" name="image" id="image" accept=".jpg,.jpeg,.png">
			</div>

			<div class="col-md-6">
			  <label>Name</label>
			  <input type="text" class="form-control" name="name" id="name" placeholder="Name">
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-12">
			  <label>Description</label>
			  <textarea name="description" class="form-control" id="description" placeholder="Enter Description"></textarea>
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-md-6">
			  	<label>Price</label>
				<div class="input-group">
					<input class="form-control" type="text" name="price" id="price" value="{{ old('price') }}" onkeypress="return isNumberKey(event);">
					<div class="input-group-append">
						<div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
					</div>
				</div>
			  	<span class="text-danger" id="priceerror"></span>
			</div>

			<div class="col-md-6">
			  	<label>Discount</label>
				<div class="input-group">
					<input class="form-control" type="text" name="discount" id="discount" value="{{ old('discount') }}" onkeypress="return isNumberKey(event);">
					<div class="input-group-append">
						<div class="input-group-text"><i class="fas fa-percentage"></i></div>
					</div>
				</div>
			  	<span class="text-danger" id="discounterror"></span>
			</div>
		</div>


	@endif

@endif