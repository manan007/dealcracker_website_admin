@if($is_optional == 1)
	
	@if(!empty($product_details))

		<div class="row mb-3 optional_row">
			<div class="col-md-4">
			  	<label>Price</label>
				<div class="input-group">
					<input class="form-control" type="text" name="price" id="price" value="{{ $product_details->price }}" onkeypress="return isNumberKey(event);">
					<div class="input-group-append">
						<div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
					</div>
				</div>
			  	<span class="text-danger" id="priceerror"></span>
			</div>

			<div class="col-md-4">
			  	<label>Discount</label>
				<div class="input-group">
					<input class="form-control discount" type="text" name="discount" id="discount" value="{{ $product_details->discount }}" onkeypress="return isNumberKey(event);">
					<div class="input-group-append">
						<div class="input-group-text"><i class="fas fa-percentage"></i></div>
					</div>
				</div>
			  	<span class="text-danger" id="discounterror"></span>
			</div>

			<div class="col-md-4">
			  <label>Quantity</label>
			  <input type="text" class="form-control" name="quantity" id="quantity" placeholder="Quantity" value="{{ $product_details->quantity }}" onkeypress="return isOnlyNumber();">
			</div>
		</div>

	@else

		<div class="row mb-3 optional_row">
			<div class="col-md-4">
			  	<label>Price</label>
				<div class="input-group">
					<input class="form-control" type="text" name="price" id="price" value="{{ old('price') }}" onkeypress="return isNumberKey(event);">
					<div class="input-group-append">
						<div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
					</div>
				</div>
			  	<span class="text-danger" id="priceerror"></span>
			</div>

			<div class="col-md-4">
			  	<label>Discount</label>
				<div class="input-group">
					<input class="form-control discount" type="text" name="discount" id="discount" value="{{ old('discount') }}" onkeypress="return isNumberKey(event);">
					<div class="input-group-append">
						<div class="input-group-text"><i class="fas fa-percentage"></i></div>
					</div>
				</div>
			  	<span class="text-danger" id="discounterror"></span>
			</div>

			<div class="col-md-4">
			  <label>Quantity</label>
			  <input type="text" class="form-control" name="quantity" id="quantity" placeholder="Quantity" onkeypress="return isOnlyNumber();">
			</div>
		</div>

	@endif

@else

	<div class="row mb-3 mt-4 optional_row">
		<div class="col-md-11">
			<h4>Add Product Variation</h4>
		</div>
		<div class="col-md-1 text-right">
			<a href="javascript:;" class="btn btn-success add_field_button" id="add_field"><i class="fa fa-plus"></i></a>
		</div>			
	</div>
	<hr/>

	<div class="newPlus"></div>

	@if(count($variation_products) > 0)
		@foreach($variation_products as $variation_product)

			<div class="variation_row mb-3">

			      <div class="row">
			        <div class="col-md-12 text-right">
			        	<a href="javascript:;" class="btn btn-success edit_product_variation" data-variation-id="{{ $variation_product->id }}"><i class="fa fa-pen"></i></a>
			        	<a href="javascript:;" class="btn btn-danger delete_product_variation" data-variation-id="{{ $variation_product->id }}"><i class="fa fa-trash"></i></a>
			        </div>  
			      </div>

			      <div class="row mb-3">
			      @if(count($attributes) > 0)
			        @foreach($attributes as $attribute)
			          @php
			            $attribute_details = App\Models\Attribute::where(['id' => $attribute])->first();
			            $get_sub_attributes = App\Models\Attribute::where(['is_parent' => $attribute])->get();

			            $get_selected_attributes = App\Models\ProductAttribute::where(['product_id' => $variation_product->id,'attribute_id' => $attribute,'is_delete' => 0])->first();
			          @endphp
			          	<div class="col-md-2">
			            	<label>Select {{ $attribute_details->name }}</label>
			            	<div class="input-group">
			              		<select class="form-control select2 sub_attribute" name="sub_attribute[]" readonly="true" disabled="true">
			                		@if($get_sub_attributes)
			                  			@foreach($get_sub_attributes as $get_sub_attribute)
			                    		<option value="{{ $get_sub_attribute->id }}" @if($get_selected_attributes->sub_attribute_id	== $get_sub_attribute->id) selected="" @endif>{{ $get_sub_attribute->name ?? '' }}</option>
			                  			@endforeach
			                		@endif
			                    </select>
			            	</div>
			              	<span class="text-danger" id="priceerror"></span>
			          	</div>
			        @endforeach
			      @endif
			      </div>

			      <div class="row mb-3">

			        <div class="col-md-3">
			            <label>Name</label>
			          <input class="form-control variation_name" type="text" name="variation_name_edit[]" value="{{ $variation_product->name ?? '' }}" readonly="true">
			        </div>

			        <div class="col-md-3">
			            <label>Price</label>
			          <div class="input-group">
			            <input class="form-control variation_price" type="text" name="variation_price_edit[]" value="{{ $variation_product->price ?? '' }}" readonly="true" onkeypress="return isNumberKey(event);">
			            <div class="input-group-append">
			              <div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
			            </div>
			          </div>
			        </div>

			        <div class="col-md-3">
			           	<label>Discount</label>
			         	<div class="input-group">
			            <input class="form-control variation_discount" type="text" name="variation_discount_edit[]" value="{{ $variation_product->discount ?? '' }}" readonly="true" onkeypress="return isNumberKey(event);">
			            <div class="input-group-append">
			              <div class="input-group-text"><i class="fas fa-percentage"></i></div>
			            </div>
			          </div>
			        </div>

			        <div class="col-md-3">
			          <label>Quantity</label>
			          <input type="text" class="form-control variation_quanty" name="variation_quanty_edit[]" placeholder="Quantity" readonly="true" value="{{ $variation_product->quantity ?? '' }}" onkeypress="return isOnlyNumber();">
			        </div>
			      </div>

			      <div class="row">
			        <div class="@if($variation_product->image) col-md-4 @else col-md-6 @endif">
			          <label>Variation Image <span class="text-danger">( Accepted format: .jpg, .jpeg, .png )</span></label>
			          <input class="form-control" type="file" name="image" id="image" accept=".jpg,.jpeg,.png" readonly="true" disabled="true">
			        </div>
			        @if($variation_product->image)
					<div class="col-md-2">
						<input type="hidden" name="hidden_variation_image_edit" value="{{ $variation_product->image }}">
		                <img src="{{ asset('upload/product/'.$variation_product->image) }}" class="edit_product_img" />
					</div>
					@endif

			        <div class="col-md-6">
			          <label>Description</label>
			          <textarea name="variation_description_edit[]" class="form-control variation_description" placeholder="Enter Description" readonly="true" value="{{ $variation_product->description ?? '' }}">{{ $variation_product->description ?? '' }}</textarea>
			        </div>
			      </div>

			</div>

		@endforeach
	@endif

	

@endif