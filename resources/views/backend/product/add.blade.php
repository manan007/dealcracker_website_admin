@extends('backend.layouts.main') 
@section('title','Product Add')

@section('css')
  <link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endsection

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="m-0 text-dark">Add Product</h3>
            </div>
        </div>
        <!-- <h3 class="m-0 text-dark">User List</h3> -->
      </div>
      <div class="card-body">
        <form action="{{ route('admin.product.store') }}" name="product_form" method="post" enctype="multipart/form-data">
        @csrf

          <div class="row mb-3">
            <div class="col-md-6">
              <label>Select User</label>
              <select class="form-control select2" name="user_id" id="user_id">
                <option value="" selected="" disabled="">Select User</option>
              @if($users)
                @foreach($users as $user)
                  <option value="{{ $user->id }}">{{ $user->fullname ?? '' }}</option>
                @endforeach
              @endif
              </select>
              <span class="text-danger" id="usererror"></span>
            </div>

            <div class="col-md-6">
              <label>Select Product Type</label>
              <select class="form-control" name="product_type" id="product_type">
                  <option value="physical">Physical</option>
                  <option value="virtual">Virtual</option>
                  <option value="service">Service</option>
              </select>
            </div>
          </div>
              
          <div class="dynamic_product_field_section">
              @include('backend.product.dynamic_product_field_section')
          </div>

          <div class="row">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="{{ route('admin.product.index') }}" class="btn btn-danger">Cancel</a>
            </div>
          </div>

        </form>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script src="{{ asset('assets/plugins/select2/js/select2.full.min.js')}}"></script>
<script type="text/javascript">
  $('.select2').select2();

  $("form[name='product_form']").validate({
    rules: 
    {
      user_id:'required',
      image:'required',
      name:'required',
      description:'required',
      sku:'required',
      price:'required',
      discount:'required',
      quantity:'required',
    },
    messages: 
    {
      user_id:'Please select user.',
      image:'Please select image.',
      name:'Please enter name.',
      description:'Please enter description.',
      sku:'Please enter sku.',
      price:'Please enter price.',
      discount:'Please enter discount.',
      quantity:'Please enter quantity.',
      virtual_product_file:'Please select virtual file.'
    },
    errorPlacement: function(error, element) {
        if (element.attr("name") == "user_id")
            error.insertAfter("#usererror");
        else if (element.attr("name") == "price")
            error.insertAfter("#priceerror");
        else if (element.attr("name") == "discount")
            error.insertAfter("#discounterror");               
        else
            error.insertAfter(element);
    }
  });

  $(document).on('change','#product_type',function(){
    var getval = $(this).val();
    var is_optional = 1;
    var attribute = null;
    var is_change = 1;
    var product_id = null;
    $('.loader').show();
    $.ajax({
        url: "{{route('admin.product.get_product_field')}}",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data:{
          product_type:getval,
          attribute:attribute,
          is_optional:is_optional,
          is_change:is_change,
          product_id:product_id
        },
        type: 'POST',
        success: function(data) {
            $('.loader').hide();
            $('.dynamic_product_field_section').html(data.html);
            if(getval == "physical"){
              $("#price").rules("add", "required");
              $("#discount").rules("add", "required");
              $("#quantity").rules("add", "required");

              $('#virtual_product_file').rules("remove", "required");
            }                 
            else if(getval == "virtual"){
              $("#price").rules("add", "required");
              $("#discount").rules("add", "required");
              $("#quantity").rules("add", "required");

              $('#virtual_product_file').rules("add", "required");
            }
            else if(getval == "service"){
              $("#price").rules("add", "required");
              $("#discount").rules("add", "required");

              $("#quantity").rules("remove", "required");
              $('#virtual_product_file').rules("remove", "required");
            }
        },
    });
  });


  $(document).on('change','.attribute',function(){
    var product_type = $('#product_type').val();
    var getval = $(this).val();
    var product_id = null;
    var is_change = 0;
    if(getval == ''){
      $('.newPlus').html('');
      var is_optional = 1;
      $("#price").rules("remove", "required");
      $("#discount").rules("remove", "required");
      $("#quantity").rules("remove", "required");
    }
    else{
      var is_optional = 0;
      $("#price").rules("add", "required");
      $("#discount").rules("add", "required");
      $("#quantity").rules("add", "required");
    }
    $('.loader').show();
    $.ajax({
        url: "{{route('admin.product.get_product_field')}}",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data:{
          product_type:product_type,
          attribute:getval,
          is_optional:is_optional,
          is_change:is_change,
          product_id:product_id
        },
        type: 'POST',
        success: function(data) {
            $('.loader').hide();
            $('.newPlus').html('');
            $('.dynamic_add_variation_section').html(data.html);
            $('.attribute').blur();                 
        },
    });
  });

  $(document).on('click','.add_field_button',function(e){

    var attribute = $('.attribute').val();
    $('.loader').show();
    $.ajax({
        url: "{{route('admin.product.add_variation_section')}}",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data:{
          attribute:attribute,
        },
        type: 'POST',
        success: function(data) {
            $('.loader').hide();   
            $('.newPlus').append(data);
            $('.select2').select2();
        },
    });

  });

  $(document).on("click",".remove_field", function(e){
      e.preventDefault();
      $(this).closest(".variation_row").remove();
  });

</script>
@endsection