@extends('backend.layouts.main') 
@section('title','Product List')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
            <div class="col-sm-11">
                <h3 class="m-0 text-dark"><b></b>Product List</h3>
            </div>
            <div class="col-sm-1 text-right">
              <a href="{{ route('admin.product.create') }}" class="btn btn-primary">Add</a>
            </div>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="dynamic_table">
            @include('backend.product.dynamic_table')
        </div>
      </div>

    </div>
  </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
  $(document).on('click','.delete',function(){
        var id=$(this).data('id');
        var getval = $(this).data('value');
        bootbox.confirm("Are you sure you want to delete this product ?", function(result){ 
            if(result == true)
            {   
              $('.submitData_changestatus_'+id).trigger('click');
            }
        });
    });
</script>
@endsection